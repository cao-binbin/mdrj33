
-- select 
-- 1.查询所有用户信息以及卡数量
Select *,(select count(*) from bankcard where bankcard.AccountId=accountinfo.AccountId ) as 卡数量 from accountinfo;



-- from 
-- 2.查询所有用户信息以及卡数量和总余额
Select *,

(select count(*) from bankcard where bankcard.AccountId=accountinfo.AccountId ) as 卡数量,

(select sum(CardMoney) from bankcard where bankcard.AccountId=accountinfo.AccountId) as 总余额  

from accountinfo 


-- where 
-- 3.查询余额最高的账户

Select * from accountinfo where AccountId=

(Select AccountId from (Select AccountId,sum(CardMoney) from bankcard GROUP BY AccountId ORDER BY sum(CardMoney) DESC LIMIT 1 )
 as temp
) 



-- 4.查询出其他储户余额比关羽((6225098234235)银行卡多的银行卡号，显示卡号，身份证，姓名，余额

Select accountinfo.AccountId 账户id,accountinfo.AccountCode 银行卡号 , accountinfo.RealName 户主名, saq.CardMoney 账户余额

from accountinfo inner join 
(
Select AccountId ,CardMoney from bankcard where CardMoney>(Select CardMoney from bankcard where CardNo='6225098234235')
) as saq 

on  accountinfo.AccountId= saq.AccountId where accountinfo.AccountId!=3;



-- 5.从所有账户信息中查询出余额最高的交易明细（存钱取钱信息）


Select *,sum(MoneyInBank) from cardexchange GROUP BY CardId ORDER BY sum(MoneyInBank) DESC LIMIT 1;

Select *,sum(MoneyOutBank) from cardexchange GROUP BY CardId ORDER BY sum(MoneyOutBank) DESC LIMIT 1;



-- 6.查询有取款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额

Select bankcard.CardNo,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney from bankcard inner join cardexchange on bankcard.CardId=cardexchange.CardId inner JOIN accountinfo on accountinfo.AccountId=bankcard.AccountId
where cardexchange.MoneyOutBank>'0'


-- 7.查询没有存款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额
Select  bankcard.CardNo,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney  from cardexchange inner join bankcard on bankcard.CardId=cardexchange.CardId INNER JOIN accountinfo on accountinfo.AccountId=bankcard.AccountId where cardexchange.MoneyInBank='0'