use bank;
-- select 
-- 1.查询所有用户信息以及卡数量

select a.*,
(
select COUNT(*) from bankcard as b where b.accountid=a.accountid
)卡数量
from accountinfo as a;

-- from 
-- 2.查询所有用户信息以及卡数量和总余额
select * from bankcard;

select a.*,
(select COUNT(*) from bankcard as b where b.accountid=a.accountid)卡数量,
(select sum(CardMoney) from bankcard as b where b.accountid=a.accountid)总余额
from accountinfo as a;

select  (
select accountid,count(*) as 卡数量,sum(CardMoney)总余额 from bankcard group by accountid)temp
inner join accountinfo on accountinfo.accountid=temp.accountid;

select accountinfo.*,temp.卡数量,temp.总余额 from accountinfo
inner join (select accountid,count(*) as 卡数量,sum(CardMoney)总余额 from
bankcard group by accountid)temp on temp.accountid=accountinfo.accountid;

-- where 
-- 3.查询余额最高的账户

select * from accountinfo where accountid=(
select accountid from (
		select accountid,sum(CardMoney) as money from bankcard
		group by accountid
		order by money desc -- 降序由高到低
		limit 1
	)temp
);

-- 4.查询出其他储户余额比关羽某张银行卡多的银行卡号(6225098234235)，显示卡号，身份证，姓名，余额
select * from bankcard;

select * from accountinfo
inner join(
select accountid,CardMoney from bankcard
where CardMoney>(
select CardMoney from bankcard where CardNo='6225098234235')
)temp on temp.accountid=accountinfo.accountid
where accountinfo.accountid !=3;


-- 5.从所有账户信息中查询出余额最高的交易明细（存钱取钱信息）

select * from cardexchange where cardid in -- 同过银行卡调取记录
	(
	select cardid from bankcard where accountid in -- 根据账号拥有者ID获取改用户银行卡号
			(
			select accountid from
				 (
				  select accountid,sum(CardMoney) as money from bankcard
					group by accountid 
					order by money desc -- 查询出余额最高
					limit 1)temp  -- 获取账号拥有者ID
				)
		);


-- 6.查询有取款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额

select a.*,temp.CardMoney from accountinfo as a
inner join (
				select accountid,CardMoney from bankcard where Cardid in
								(select Cardid from cardexchange where MoneyOutBank!=0)
	)temp on temp.accountid=a.accountid;


-- 7.查询没有存款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额

select a.*,temp.CardMoney from accountinfo as a
inner join(
select accountid,CardMoney from bankcard where Cardid in
			(
			select Cardid from cardexchange where MoneyInBank=0
				)
)temp on temp.accountid=a.accountid;

-- 8.查询出没有转账交易记录的银行卡账户信息，显示卡号，身份证，姓名，余额

select a.*,temp.CardMoney from accountinfo as a
inner JOIN(
		select accountid,CardMoney from bankcard where Cardid in
		(
		select CardIdOut from cardtransfer
		)or Cardid in(
		select CardIdIn from cardtransfer
		)
	)temp on temp.accountid=a.accountid
-- on
-- 9.查询所有余额超过100RMB的银行卡信息，包括账户名称

select * from accountinfo as a inner join
(
select * from bankcard where CardMoney >100
)temp on temp.accountid=a.accountid;

-- HAVING

-- 10.查询交易次数（存取款）最多的银行卡账户信息，卡号，身份证，姓名，余额，交易次数
select * from cardexchange;
select * from bankcard;
select * from accountinfo;
select * from cardtransfer;