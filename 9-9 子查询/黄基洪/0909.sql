
-- select 
-- 1.查询所有用户信息以及卡数量
select * from accountinfo -- 查询用户信息表
SELECT * from bankcard	-- 查询账户表

--  所有用户的信息 accountinfo.* 账户表  count() 
-- 通过select 方法来查询 先通过count() 并且通过where来设置条件 ()里面是作用域 里面可以使用外面表的列 而外面无法使用里面表的列
SELECT accountinfo.*,(
SELECT count(*) from bankcard where bankcard.AccountId=accountinfo.AccountId
) 卡的数量
from accountinfo


-- from 
-- 2.查询所有用户信息以及卡数量和总余额
-- 用户信息 accountinfo.* 卡的数量 count(*) 总余额 bankcard sum(余额综合) 
SELECT accountinfo.*,
(
	SELECT count(*) from bankcard where bankcard.AccountId=accountinfo.AccountId
)卡数量,
(
	SELECT sum(CardMoney) from bankcard where bankcard.AccountId=accountinfo.AccountId
)总余额
from accountinfo;

select accountinfo.*,
(SELECT count(*) from bankcard where bankcard.AccountId=accountinfo.AccountId)卡的数量,
(SELECT sum(CardMoney) from bankcard where bankcard.AccountId=accountinfo.AccountId)总金额
from accountinfo


-- where 
-- 3.查询余额最高的账户
-- 需求 余额最高 max 账户信息
select max(CardMoney) from bankcard -- 400000

SELECT accountinfo.*,temp.余额 from accountinfo
INNER JOIN(
select AccountId,max(CardMoney) as 余额 from bankcard
) temp on temp.AccountId=accountinfo.AccountId -- 通过max函数判断出最高的余额 因为需要知道两列 所以需要通过inner join 连接函数 在通过连接两表来输出




select * from accountinfo
where AccountId=
(
select AccountId from 
(
select AccountId,sum(CardMoney) as money from bankcard
GROUP BY AccountId
ORDER BY money DESC
LIMIT 1
) temp
)



--  解题思路 就是通过先计算总金额 和总金额的账户id
		-- 再通过账户id来查询到该用户的信息
-- 4.查询出其他储户余额比关羽某张银行卡多的银行卡号，显示卡号，身份证，姓名，余额
SELECT accountinfo.*,temp.CardMoney 余额 from accountinfo
INNER JOIN
(
	SELECT AccountId,CardMoney from bankcard
	where CardMoney>
(	select CardMoney from bankcard where cardNo='6225098234235'
) 
)temp
on temp.AccountId=accountinfo.AccountId
s

SELECT accountinfo.*,temp.CardMoney from accountinfo
inner join 
(	SELECT AccountId,CardMoney from bankcard 
	where CardMoney>
	(SELECT CardMoney from bankcard where CardNo='6225098234235')
)temp 
on temp.AccountId=accountinfo.AccountId


-- 5.从所有账户信息中查询出余额最高的交易明细（存钱取钱信息）
select CardId,max(CardMoney) from bankcard

select * from cardexchange
where CardId in
(
select CardId from bankcard where CardMoney>=
(
	select max(CardMoney) from bankcard 
)
) -- 首先先做出最大值 max函数来输出表内余额最高 在查询出余额最高的id 在通过id查出取钱存钱的信息

-- 6.查询有取款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额
select CardNo 卡号,AccountCode 身份证,RealName 姓名,CardMoney 余额 from accountinfo
INNER JOIN bankcard on bankcard.AccountId=accountinfo.AccountId
where CardId in
(
	select CardId from cardexchange where MoneyOutBank>0
)

-- 首先通过cardId获取id 获取有取款记录的id 在通过连接表来查询出卡号 

	
-- 7.查询没有存款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额
	select CardId from cardexchange where MoneyInBank>0
	
	select CardNo 卡号,AccountCode 身份证,RealName 姓名,CardMoney 余额 from accountinfo
	inner join bankcard on bankcard.AccountId=accountinfo.AccountId
	where CardId not in 
	(
		select CardId from cardexchange where MoneyInBank>0	
	)


-- 8.查询出没有转账交易记录的银行卡账户信息，显示卡号，身份证，姓名，余额
select CardNo 卡号,AccountCode 身份证,RealName 姓名,CardMoney 余额 from accountinfo 
inner join bankcard on bankcard.AccountId=accountinfo.AccountId
where CardId not in
(
select CardId from cardexchange where MoneyInBank >0 and MoneyOutBank>0
)

-- on
-- 9.查询所有余额超过100RMB的银行卡信息，包括账户名称
	select RealName 账户名称,bankcard.* from accountinfo
	inner join bankcard on accountinfo.AccountId=accountinfo.AccountId
	where CardMoney>100

	select * from bankcard where CardMoney>100 2 3 4 3
	
	select * from accountinfo
	inner join bankcard on bankcard.AccountId=accountinfo.AccountId
	where bankcard.AccountId in (
		select AccountId from bankcard where CardMoney >100
	)

-- HAVING

-- 10.查询交易次数（存取款）最多的银行卡账户信息，卡号，身份证，姓名，余额，交易次数
select CardId,count(*) as money from cardexchange
GROUP BY CardId
ORDER BY money desc
LIMIT 1 -- 算出交易次数和卡号 通过group by 函数和倒序求出

select CardNo 卡号,AccountCode 身份证,RealName 姓名,CardMoney 余额,temp.money 交易次数 from accountinfo
inner join bankcard on bankcard.AccountId=accountinfo.AccountId
inner join (
select CardId,count(*) as money from cardexchange
GROUP BY CardId
order by money DESC
LIMIT 1
) temp on temp.CardId=bankcard.CardId

select * from bankcard where CardNo='6225098134285'

