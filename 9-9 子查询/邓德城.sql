
-- select 
-- 1.查询所有用户信息以及卡数量
SELECT *,
(SELECT COUNT(*) FROM bankcard WHERE accountinfo.AccountId=bankcard.AccountId)卡数量 
from accountinfo 


-- from 
-- 2.查询所有用户信息以及卡数量和总余额
SELECT *,
(SELECT COUNT(*) FROM bankcard WHERE bankcard.AccountId=accountinfo.AccountId)卡数量,
(SELECT SUM(CardMoney) FROM bankcard WHERE bankcard.AccountId=accountinfo.AccountId)总余额
FROM accountinfo;


-- where 
-- 3.1 查询单张卡余额最高的账户
SELECT * FROM accountinfo WHERE AccountId in(
SELECT AccountId FROM bankcard where CardMoney>=(SELECT MAX(CardMoney) FROM bankcard))




-- 3.2.查询总余额最高的账户
SELECT abc.AccountId,MAX(abc.`总余额`)
FROM(SELECT bankcard.AccountId AccountId,SUM(bankcard.CardMoney)总余额 FROM bankcard GROUP BY bankcard.AccountId)abc

-- 4.查询出其他储户余额比关羽某张银行卡多的银行卡号，显示卡号，身份证，姓名，余额
SELECT accountinfo.*, abb.CardMoney FROM accountinfo
INNER JOIN (SELECT AccountId,CardMoney from bankcard WHERE CardMoney>
(SELECT CardMoney FROM bankcard WHERE CardNo='6225098234235')
)abb
ON accountinfo.AccountId=abb.AccountId
WHERE accountinfo.AccountId!=3;


-- 5.从所有账户信息中查询出余额最高的交易明细（存钱取钱信息）

select * from cardexchange where CardId in
(
SELECT CardId FROM bankcard WHERE CardMoney>=(
SELECT MAX(CardMoney) FROM bankcard
)
)


-- 6.查询有取款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额
SELECT bankcard.CardNo,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney from accountinfo
INNER JOIN bankcard on bankcard.AccountId=accountinfo.AccountId
WHERE bankcard.CardId in(SELECT CardId from cardexchange where MoneyOutBank>0)



-- 7.查询没有存款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额
SELECT bankcard.CardNo,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney from accountinfo
LEFT JOIN bankcard on bankcard.AccountId=accountinfo.AccountId
WHERE bankcard.CardId NOT in(SELECT CardId from cardexchange where MoneyOutBank>0)

-- 8.查询出没有转账交易记录的银行卡账户信息，显示卡号，身份证，姓名，余额
SELECT bankcard.CardNo,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney from accountinfo
INNER JOIN bankcard ON bankcard.AccountId=accountinfo.AccountId
WHERE bankcard.CardId NOT in( SELECT CardIdIn FROM cardtransfer WHERE TransferMoney>0)

-- on
-- 9.查询所有余额超过100RMB的银行卡信息，包括账户名称

select bankcard.*,accountinfo.RealName FROM bankcard
INNER JOIN accountinfo ON bankcard.AccountId=accountinfo.AccountId
WHERE bankcard.CardMoney>100;






-- HAVING

-- 10.查询交易次数（存取款）最多的银行卡账户信息，卡号，身份证，姓名，余额，交易次数

SELECT bankcard.CardNo 卡号,accountinfo.AccountCode 身份证,accountinfo.RealName 姓名,bankcard.CardMoney 余额 FROM bankcard
LEFT JOIN (SELECT cardexchange.CardId, count(*) sum FROM cardexchange
GROUP BY cardexchange.CardId) abc
ON bankcard.CardId=abc.CardId
INNER JOIN accountinfo ON accountinfo.AccountId=bankcard.AccountId
where sum=abc.sum;




