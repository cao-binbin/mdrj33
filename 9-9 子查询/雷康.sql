
-- select 
-- 1.查询所有用户信息以及卡数量

select * from accountinfo 
INNER JOIN
(
select AccountId, count(*) 卡数量 from bankcard 
group by AccountId
) temp on temp.AccountId=accountinfo.AccountId

select *,
(
select  count(*)  from bankcard 
where accountinfo.AccountId=bankcard.AccountId
) 卡数量
from accountinfo
-- from 
-- 2.查询所有用户信息以及卡数量和总余额
select *,
(
select count(*) from bankcard where bankcard.AccountId=accountinfo.AccountId
) 卡数量,
(
select sum(CardMoney) from bankcard where bankcard.AccountId=accountinfo.AccountId
) 总金额 from accountinfo
-- where 
-- 3.查询余额最高的账户
select * from bankcard where CardMoney=(select max(CardMoney) from bankcard )
-- 4.查询出其他储户余额比关羽某张银行卡多的银行卡号(6225098234235)，显示卡号，身份证，姓名，余额

select accountinfo.*, bankcard.CardNo,bankcard.CardMoney from bankcard 
inner join accountinfo on accountinfo.AccountId=bankcard.AccountId
where CardMoney>
(
select CardMoney from bankcard where CardNo='6225098234235'
) and RealName!="关羽"
-- 5.从所有账户信息中查询出余额最高的交易明细（存钱取钱信息）
select * from cardexchange where CardId in(
select CardId from bankcard where AccountId=(
select Accountid from(
select AccountId ,sum(CardMoney) money from bankcard
group by AccountId
order by money desc
limit 1) a))
-- 6.查询有取款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额
select AccountCode,RealName from accountinfo where AccountId in
(
select AccountId from bankcard where CardId in
(
select CardId from cardexchange where MoneyOutBank>0
)
)

select * from accountinfo 
inner JOIN bankcard on accountinfo.AccountId=bankcard.Accountid
where CardId  in 
(
select CardId from cardexchange where MoneyInBank >0
)
-- 7.查询没有存款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额

select * from accountinfo 
inner JOIN bankcard on accountinfo.AccountId=bankcard.Accountid
where CardId  not in 
(
select CardId from cardexchange where MoneyInBank >0
)

 8.查询出没有转账交易记录的银行卡账户信息，显示卡号，身份证，姓名，余额

select * from accountinfo 
inner join bankcard on accountinfo.AccountId=bankcard.AccountId
 where CardId not in
(
select  CardIdOut from cardtransfer
) and CardId not in
(
select CardIdIn from cardtransfer
)
-- on
-- 9.查询所有余额超过100RMB的银行卡信息，包括账户名称


select * ,(select CardNo from bankcard where CardMoney >100 limit 1) cardno from accountinfo where AccountId in 
(
select AccountId from bankcard where CardMoney > 100
)
-- HAVING

-- 10.查询交易次数（存取款）最多的银行卡账户信息，卡号，身份证，姓名，余额，交易次数


select bankcard.CardNo,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney ,
(select count(CardId) a from cardexchange 
group by CardId
order by a DESC
limit 1) 次数
from accountinfo inner join bankcard 
on bankcard.AccountId=accountinfo.AccountId
 where bankcard.CardId in 
(
select CardId from cardexchange 
group by CardId 
having count(CardId)>=
(
select count(CardId) a from cardexchange 
group by CardId
order by a DESC
limit 1
)
)


