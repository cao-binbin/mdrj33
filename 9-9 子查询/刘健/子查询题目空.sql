
-- select 
-- 1.查询所有用户信息以及卡数量
select *,
(
	select count(*) from bankcard where  accountinfo.AccountId = bankcard.AccountId 
)卡的总数
from accountinfo;


-- from 
-- 2.查询所有用户信息以及卡数量和总余额
  select *,
		(
			select count(*)from bankcard where accountinfo.AccountId=bankcard.AccountId
		)卡的数量,		(
			select sum(CardMoney) from bankcard where accountinfo.AccountId=bankcard.CardId
		)总金额

	from accountinfo;
	
	
	select accountinfo.*,temp.卡的数量,temp.总额 from
	accountinfo inner join (
		select AccountId,count(*) 卡的数量,sum(CardMoney) 总额 from bankcard
		group by AccountId
	) temp
	on accountinfo.AccountId=temp.AccountId
	
	
	
	
	
-- where 
-- 3.查询余额最高的账户

select * from accountinfo;
select * from bankcard;



-- 4.查询出其他储户余额比关羽某张银行卡多的银行卡号，显示卡号，身份证，姓名，余额



-- 5.从所有账户信息中查询出余额最高的交易明细（存钱取钱信息）


-- 6.查询有取款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额


-- 7.查询没有存款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额



-- 8.查询出没有转账交易记录的银行卡账户信息，显示卡号，身份证，姓名，余额



-- on
-- 9.查询所有余额超过100RMB的银行卡信息，包括账户名称


-- HAVING

-- 10.查询交易次数（存取款）最多的银行卡账户信息，卡号，身份证，姓名，余额，交易次数


