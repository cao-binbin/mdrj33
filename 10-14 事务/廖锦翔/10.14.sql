#以下题目全部用事务实现：
#1.存钱（给定参数：银行账户，存钱金额）
  #要求：修改余额，添加存款记录
DROP PROCEDURE if EXISTS `type`;
delimiter $$
CREATE PROCEDURE type(t_cardid int,t_Money DECIMAL(10,2))
aa:BEGIN
	DECLARE hasErr int DEFAULT 0;
	DECLARE CONTINUE HANDLER for SQLEXCEPTION set hasErr=1;
	#开启事务
	start TRANSACTION;
	
	insert into cardexchange(CardId,MoneyInBank,MoneyOutBank,ExchangeTime)
		VALUES(t_cardid,t_Money,0,now());
	IF ROW_COUNT()<=0 THEN
	select '回滚';
			ROLLBACK;
	LEAVE aa;
END IF;

	update bankcard set CardMoney=CardMoney+t_money
		where Cardid=t_cardid;

		IF hasErr=1 THEN
			select '回滚';
			ROLLBACK;
		ELSE
			select '提交';
			COMMIT;
	END IF;
END
$$
delimiter ;

call type(11,50);


#2.取钱（给定参数：银行账户，密码，取钱金额）
  #要求：1.添加触发器约束：余额不能小于0
  #          2.校验密码是否正确，正确执行3，否则取钱失败
  #          3.修改余额，添加取款记录
drop TRIGGER if EXISTS  triAA ;
create TRIGGER triAA  BEFORE UPDATE on bankcard for each ROW
BEGIN
	if new.CardMoney <0 then
		SIGNAL SQLSTATE 'HY000' set MESSAGE_TEXT = '余额不能为负数';	
	end if ;
END;


DROP PROCEDURE if EXISTS `type1`;
delimiter $$
CREATE PROCEDURE type1(t_CardNo VARCHAR(30),t_CardPwd int,t_Money DECIMAL(10,2))
aa:BEGIN
	DECLARE a varchar(30) DEFAULT '';
	DECLARE b int DEFAULT 0;
	DECLARE haserr int DEFAULT 0;
	DECLARE CONTINUE HANDLER for SQLEXCEPTION set hasErr=1;	
	start transaction;
	
	select CardPwd into a from bankcard where CardNo=t_CardNo;
	select ROW_COUNT();
	if t_CardPwd = a then
	update bankcard set CardMoney = CardMoney - t_Money where CardNO=t_CardNo;
	select ROW_COUNT();
	
	select CardId into b from bankcard where CardNo=t_CardNo;
	
	insert into cardexchange(CardId,MoneyInBank,MoneyOutBank,ExchangeTime)
	VALUES(b,0,t_Money,NOW());
	select ROW_COUNT();
ELSE
	select '密码不正确';
	leave aa;
END IF;
	if haserr = 1  then
 			select '回滚';
 			ROLLBACK;
 		else 
			select '提交';
 			COMMIT;
 		end if;
END;
$$
delimiter ;

call type1('6225098234285',123456,100);

#3.卡片清理：    
#每个用户只能有一张卡
#每个用户以余额最高的卡为主卡，其他的卡余额全部转移到主卡上，并注销非主卡。卡转账要有转账记录
#并同时升级主卡密码为8位以上（添0处理）（已经是8位及以上的不用处理），
#比如：12345升级为12345000  
        #  123456789不处理

