#1.存钱（给定参数：银行账户，存钱金额）
 # 要求：修改余额，添加存款记录
drop PROCEDURE if EXISTS `demo1`;
delimiter $$
create PROCEDURE demo1(par_cardNo varchar(30),par_access DECIMAL(10,2))
BEGIN	
			DECLARE par_cardid int DEFAULT 0;
			DECLARE hasErr int DEFAULT 0;
			# 实现类似高级语言中 TRY CATCH(e)的功能，捕获异常信息
			DECLARE CONTINUE HANDLER FOR  SQLEXCEPTION set hasErr=1;
			#开启事务
			START TRANSACTION ;
			#获取par_cardid的值
			set par_cardid =(select cardid from bankcard where cardno=par_cardNo);
			#存钱
			UPDATE bankcard set CardMoney =CardMoney+par_access where CardNo=par_cardNo;
			
			INSERT into cardtransfer (cardidIn,transfermoney,transfertime)
			VALUES(par_cardid,par_access,NOW());

			if hasErr = 1  then
					select '回滚';
					ROLLBACK;
			else 
					select '提交';
					COMMIT;
			end if;
		
END
$$
delimiter ;

call demo1('6225098234146',200);




/*2.取钱（给定参数：银行账户，密码，取钱金额）
  要求：		1.添加触发器约束：余额不能小于0
            2.校验密码是否正确，正确执行3，否则取钱失败
            3.修改余额，添加取款记录
*/
drop TRIGGER  if EXISTS outs;
create TRIGGER outs BEFORE UPDATE on bankcard for each ROW
BEGIN
		if new.cardmoney <0 THEN
		SIGNAL SQLSTATE 'HY000' set message_text ='余额不能小于0';
		END IF;
END;



drop PROCEDURE if EXISTS `demo2`;
delimiter $$
CREATE PROCEDURE demo2(par_cardNo varchar(30),par_cardpwd VARCHAR(30),par_cardmoneyout DECIMAL(10,2))
BEGIN 
			DECLARE par_cardNo varchar(20) DEFAULT 0;
			DECLARE hasErr int DEFAULT 0;
			DECLARE flag int DEFAULT TRUE;
			#捕获异常信息
			DECLARE CONTINUE HANDLER for SQLEXCEPTION set hasErr=1;
			DECLARE CONTINUE HANDLER for not found set flag =false;
			#开启事务
			START TRANSACTION;
			
			
			


			set par_cardNo =(select cardid from bankcard where cardid=par_cardNo);
			#取钱后剩余
			UPDATE bankcard set cardmoney =cardmoney-par_cardmoneyout where CardNo=par_cardNo;
					if ROW_COUNT()>0 THEN
						select '3';
					else 
						select '取钱失败';
					end if ;
			#添加转账记录
			INSERT INTO cardtransfer(cardidout,transfermoney,transfertime) VALUES(par_cardNo,par_cardmoneyout,NOW());
				
			if hasErr = 1 THEN
				 select '回滚';
				 ROLLBACK;
		  else 
				 select '提交';
				 COMMIT;
		  END if;
END
$$
delimiter ;


call demo2('6225098134285','123456',50);



/*
3.卡片清理：    
每个用户只能有一张卡
每个用户以余额最高的卡为主卡，其他的卡余额全部转移到主卡上，并注销非主卡。卡转账要有转账记录
并同时升级主卡密码为8位以上（添0处理）（已经是8位及以上的不用处理），
比如：12345升级为12345000  
          123456789不处理
*/
drop PROCEDURE if EXISTS `demo3`;
delimiter $$
create PROCEDURE demo3(par_id int,par_cardidIn int,par_cardidout int,par_transfermoney DECIMAL(10,2))
BEGIN
		DECLARE sumMoney DECIMAL(10,2) DEFAULT '';
		DECLARE changepwd varchar(30) DEFAULT '';
		DECLARE hasErr int DEFAULT 0;
		DECLARE flag int DEFAULT 0;
		
		DECLARE CONTINUE HANDLER for SQLEXCEPTION set hasErr = 1;
		DECLARE CONTINUE HANDLER for not found set flag = false;
		
		DECLARE mycur CURSOR for
		(
			select sum(CardMoney) from bankcard where AccountId=par_id
		);
		
		START TRANSACTION;
		
		OPEN mycur;
		aa:WHILE TRUE DO
				FETCH mycur into par_id;
					if flag = false THEN
					LEAVE aa;
					END if;
					if 
		END WHILE;
		
		
		
		
		CLOSE mycur;
END
$$
delimiter ;






























