-- 以下题目全部用事务实现：
-- 1.存钱（给定参数：银行账户，存钱金额）
--   要求：修改余额，添加存款记录
DROP PROCEDURE IF EXISTS `aa`;
delimiter $$
CREATE PROCEDURE aa(idno varchar(20),money DECIMAL(10,2))
BEGIN
DECLARE card int DEFAULT 0;
declare hasErr int DEFAULT 0;
	# 实现类似高级语言中 TRY CATCH(e)的功能，捕获异常信息
	DECLARE CONTINUE HANDLER for SQLEXCEPTION set hasErr =1;
	#开启事务
	START TRANSACTION;
	#存钱	
	UPDATE bankcard set CardMoney=CardMoney + money where Cardid =idno;
	#set card=(SELECT cardid from bankcard where CardNo=idno);
	INSERT INTO  cardtransfer (CardIdIn,TransferMoney,TransferTime) 
			VALUES (idno ,money , NOW());

	if hasErr = 1  then
		select '回滚';
		ROLLBACK;
	else 
		select '提交';
		COMMIT;
	end if;
END
$$
delimiter;
call aa('1',100);


-- 2.取钱（给定参数：银行账户，密码，取钱金额）
--   		 要求：1.添加触发器约束：余额不能小于0
--             2.校验密码是否正确，正确执行3，否则取钱失败
--             3.修改余额，添加取款记录
DROP PROCEDURE IF EXISTS `bb`;
delimiter $$
CREATE PROCEDURE bb(bb_no varchar(20),inpassword varchar(10) ,money DECIMAL(10,2))
BEGIN
DECLARE inmoney DECIMAL DEFAULT 0;
DECLARE pwd1 DECIMAL DEFAULT 0;
DECLARE bb_card int DEFAULT 0;
declare hasErr int DEFAULT 0;
	# 实现类似高级语言中 TRY CATCH(e)的功能，捕获异常信息
	DECLARE CONTINUE HANDLER for SQLEXCEPTION set hasErr =1;
	#开启事务
	START TRANSACTION;
	
	set inmoney= ((SELECT cardmoney from bankcard where cardid=bb_no)-money);
	set pwd1=( SELECT cardpwd from bankcard where cardid=bb_no);
		

		
	if pwd1=inpassword then
			UPDATE bankcard set CardMoney=CardMoney - money where Cardid =bb_no;
			#set bb_card=(SELECT cardid from bankcard where Cardid=bb_no);
			INSERT INTO  cardtransfer (CardIdout,TransferMoney,TransferTime) 
			VALUES (bb_no ,money , NOW());

	else
				set	haserr=1;
	end if;
			
	
	if inmoney<0 then
		set	haserr=1;
	end if;
	
	if hasErr = 1  then
		select '回滚';
		ROLLBACK;
	else 
		select '提交';
		COMMIT;
	end if;

end;
$$
delimiter;

call  bb('1','123456',10)




-- 3.卡片清理：    
-- 每个用户只能有一张卡
-- 每个用户以余额最高的卡为主卡，其他的卡余额全部转移到主卡上，并注销非主卡。卡转账要有转账记录
-- 并同时升级主卡密码为8位以上（添0处理）（已经是8位及以上的不用处理），
-- 比如：12345升级为12345000  
--           123456789不处理
-- 
