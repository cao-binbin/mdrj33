-- 以下题目全部用事务实现：
-- 1.存钱（给定参数：银行账户，存钱金额）
--   要求：修改余额，添加存款记录

drop procedure if exists test_1;
create procedure test_1(par_bankid varchar(30) , par_addmoney decimal(10,2))
BEGIN
 	declare haserr int default 1;
	declare continue handler for sqlexception set haserr=0;
	start transaction;
	update bankcard set CardMoney = CardMoney + par_addmoney where CardNO=par_bankid;
	select CardId into @num from bankcard where CardNo=par_bankid;
	insert into cardexchange(CardId,MoneyInBank,MoneyOutBank,ExchangeTime)
	values(@num,par_addmoney,0,now());
		if haserr = 0  then
		select '回滚';
		ROLLBACK;
	else 
		select '提交';
		COMMIT;
	end if;
END;

call test_1('6225098134285',100);









-- 2.取钱（给定参数：银行账户，密码，取钱金额）
--   要求：1.添加触发器约束：余额不能小于0
--             2.校验密码是否正确，正确执行3，否则取钱失败
--             3.修改余额，添加取款记录
drop trigger if exists trigger1;
create trigger trigger1 BEFORE update on bankcard for each row
BEGIN
	if new.cardmoney<0 then
	signal sqlstate 'HY000' set Message_text='余额不能小于0';
	end if;
END;

drop procedure if exists test_2;
delimiter $$
create procedure test_2(par_bankid varchar(30),par_password int,par_addmoney decimal(10,2))
BEGIN
	declare p varchar(30) default '';
	declare number varchar(30) default '';
	declare haserr int default 0;
	declare continue handler for sqlexception set haserr=1;

	start transaction;
	select cardpwd into p from bankcard where CardNo=par_bankid;
	if par_password = p then
	update bankcard set CardMoney = CardMoney - par_addmoney where CardNO=par_bankid;
	select CardId into @num from bankcard where CardNo=par_bankid;
	insert into cardexchange(CardId,MoneyInBank,MoneyOutBank,ExchangeTime)
	VALUES(@num,0,par_addmoney,NOW());
	else 
		select '错误';
	end if;
 		if haserr = 1  then
 			select '回滚';
 			ROLLBACK;
 		else 
			select '提交';
 			COMMIT;
 		end if;
END;
$$
delimiter ;




call test_2('6225098234146','444444',1000);


-- 3.卡片清理：    
-- 每个用户只能有一张卡
-- 每个用户以余额最高的卡为主卡，其他的卡余额全部转移到主卡上，并注销非主卡。卡转账要有转账记录
-- 并同时升级主卡密码为8位以上（添0处理）（已经是8位及以上的不用处理），
-- 比如：12345升级为12345000  
--           123456789不处理


drop procedure if exists `test`;
delimiter $$
create procedure test()
begin
		declare i int default 0;
		
		declare kkk decimal(10,2) default 0;
		
		declare num1 int default 0;
		declare max decimal(10,2) default 0;
		declare a int default 0;
		declare b decimal(10,2) default 0;
		declare num int default 0;
		declare total decimal(10,2) default 0;
		
		declare flag int default true;
		declare haserr int default 0;
		declare cursor1 cursor for
		(
		select accountid from bankcard
		group by accountid
		);
		
		declare continue handler for not found set flag = false;
	  declare continue handler for sqlexception set haserr = 1;
		
		start transaction;
		open cursor1;
		aa:while true do 
		fetch cursor1 into num;
		 		if flag = false then
					leave aa;
				end if;	
		select * from bankcard where accountid = num;
		select FOUND_ROWS() into num1;	
		if num1>=2 then
 		#求最大金额max
 			select  max(cardmoney) into max from bankcard where accountid = num ;	
			#select max;	
 		#求最大金额的id a
 			select cardid into a from bankcard where cardmoney = 
			(
				select  max(cardmoney)  from bankcard where accountid = num 
			);	
			
			select cardpwd into @t from bankcard where cardid = a;
				set @w=1;
  			while @w<=8 do 
  			if LENGTH(@t)<@w then
  			set @t=concat(@t,'0');
  			end if;
  			set @w=@w+1;		
   			end while;
  		update bankcard set cardpwd = @t where cardid = a;
 		#求去掉最大金额的id的金额总和b
 			select sum(cardmoney) into b from bankcard where accountid=num and cardid!=a;
 		#求所有卡的金额total
 			set total = max + b;
			SET FOREIGN_KEY_CHECKS = 0;
			update bankcard set cardmoney =  total where cardid = a;
			
			select count(*) into @h from bankcard where accountid = num and cardid != a ;
					set @u=1;
					while @u<= @h do
					select cardid into @i from bankcard where accountid = num and cardid != a limit 1;	
					select cardmoney into @kkk from bankcard where cardid = @i limit 1;	
					insert into cardtransfer(CardIdOut,CardIdIn,TransferMoney,TransferTime)
					values (@i,a,@kkk,now());
					delete from bankcard where cardid = @i;
					set @u=@u+1;
					end while;
		else
 		#求最大金额的id a
 			select cardid into a from bankcard where cardmoney = 
			(
				select  max(cardmoney)  from bankcard where accountid = num 
			);	
			
				select cardpwd into @t from bankcard where cardid = a;
				set @w=1;
  			while @w<=8 do 
  			if LENGTH(@t)<@w then
  			set @t=concat(@t,'0');
  			end if;
  			set @w=@w+1;		
   			end while;
  		update bankcard set cardpwd = @t where cardid = a;
		end if;
 		#求最大金额的密码
 			
 		end while;
		
		if haserr = 1  then
 			select '回滚';
 			ROLLBACK;
		else 
 			select '提交';
 			COMMIT;
 		end if;
		close cursor1;
end;
$$
delimiter ;

call test();







drop procedure if exists tt;
create procedure tt()
begin
	declare flag int default true;
	declare q varchar(30) default '';
	declare cursor1 cursor for
		(
		select cardpwd from bankcard
		);
declare continue handler for not found set flag = false;

open cursor1;
aa:while true do
fetch cursor1 into q;
set @w=1;
while @w<=8 do 
				if LENGTH(q)<@w then
				#set @ppp=concat(@ppp,'0');
				set q=concat(q,'0');
				end if;
				
				set @w=@w+1;
				
			end while;
			select q;
			end while;
			close cursor1;
end;



call tt();
