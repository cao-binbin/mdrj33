-- 1.存钱（给定参数：银行账户，存钱金额）
--   要求：修改余额，添加存款记录

drop PROCEDURE if EXISTS `Text1`;
delimiter $$
create PROCEDURE Text1(BankAccount varchar(20),BankMoney DECIMAL(5,2))
BEGIN
	START TRANSACTION;
	
	UPDATE bankcard set CardMoney = CardMoney+BankMoney where CardId=BankAccount;
	
	INSERT into cardtransfer(CardIdIn,TransferMoney,TransferTime)    
	values(BankAccount ,BankMoney,NOW());
	
	COMMIT;
END;
$$
delimiter ;

call Text1('2',101);