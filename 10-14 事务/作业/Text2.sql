-- 2.取钱（给定参数：银行账户，密码，取钱金额）
--   要求：1.添加触发器约束：余额不能小于0
--             2.校验密码是否正确，正确执行3，否则取钱失败
--             3.修改余额，添加取款记录

drop PROCEDURE if EXISTS `Text2`;
delimiter $$
create PROCEDURE Text2(BankAccount varchar(20),BankPassword varchar(6),BankMoney DECIMAL(5,2))
aa:BEGIN
	DECLARE pwd varchar(6) DEFAULT '';
	START TRANSACTION;
	select CardPwd into pwd from bankcard where CardId=BankAccount;
	IF pwd = BankPassword THEN
		update bankcard set CardMoney=CardMoney-BankMoney where CardId=BankAccount;
		INSERT into cardexchange(CardId,MoneyInBank,MoneyOutBank,ExchangeTime)    
		values(BankAccount,0,BankMoney,NOW());
		COMMIT;
	ELSE
		SIGNAL SQLSTATE 'HY000' set MESSAGE_TEXT = '密码错误';	
		ROLLBACK;
		LEAVE aa;
END IF;
END;
$$
delimiter ;
CREATE TRIGGER triA BEFORE UPDATE ON bankcard for EACH ROW
	BEGIN		update bankcard set CardMoney=CardMoney-BankMoney where CardId=BankAccount;
		INSERT into cardexchange(CardId,MoneyOutBank,ExchangeTime)    
		values(BankAccount,BankMoney,NOW());
		COMMIT;
	ELSE
		SIGNAL SQLSTATE 'HY000' set MESSAGE_TEXT = '密码错误';	
		ROLLBACK;
		LEAVE aa;
END IF;
END;
$$
delimiter ;
CREATE TRIGGER triA BEFORE UPDATE ON bankcard for EACH ROW
	BEGIN
	if new.CardMoney <0 then
		SIGNAL SQLSTATE 'HY000' set MESSAGE_TEXT = '余额不能为负数';
	end if ;
	END;


call Text2('03','444445',100);