以下题目全部用事务实现：
1.存钱（给定参数：银行账户，存钱金额）
  要求：修改余额，添加存款记录

2.取钱（给定参数：银行账户，密码，取钱金额）
  要求：1.添加触发器约束：余额不能小于0
            2.校验密码是否正确，正确执行3，否则取钱失败
            3.修改余额，添加取款记录
3.卡片清理：    
每个用户只能有一张卡
每个用户以余额最高的卡为主卡，其他的卡余额全部转移到主卡上，并注销非主卡。卡转账要有转账记录
并同时升级主卡密码为8位以上（添0处理）（已经是8位及以上的不用处理），
比如：12345升级为12345000  
          123456789不处理

-- 以下题目全部用事务实现：
-- 1.存钱（给定参数：银行账户，存钱金额）
--   要求：修改余额，添加存款记录
drop PROCEDURE IF EXISTS `cun`;
create PROCEDURE cun(Inmoney VARCHAR(20) ,sumMoney DECIMAL(10,2))
aa:BEGIN
DECLARE cardIDd int DEFAULT 0;
DECLARE yuyu int DEFAULT 0;
DECLARE CONTINUE HANDLER for SQLEXCEPTION set yuyu=1;
START TRANSACTION;
UPDATE bankcard set CardMoney=CardMoney+sumMoney where CardNo=Inmoney ;
set cardIDd=(select CardId from bankcard where CardNo=Inmoney );
insert into cardtransfer(CardIdIn,TransferMoney,TransferTime)
VALUES(cardIDd, sumMoney,NOW());
if yuyu = 1  then
		select '回滚';
		ROLLBACK;
	else 
		select '提交';
		COMMIT;
	end if;
END;
call cun(6225098134285,-1200);
-- 2.取钱（给定参数：银行账户，密码，取钱金额）
--   要求：1.添加触发器约束：余额不能小于0
--             2.校验密码是否正确，正确执行3，否则取钱失败
--             3.修改余额，添加取款记录
cre

drop PROCEDURE if EXISTS `Mytransfer`;
delimiter $$
create PROCEDURE Mytransfer(id int,pass int,par_transferMoney DECIMAL(10,2))
aa:BEGIN
	declare hasErr int DEFAULT 0;
	# 实现类似高级语言中 TRY CATCH(e)的功能，捕获异常信息
	DECLARE CONTINUE HANDLER for SQLEXCEPTION set hasErr =1;
	#开启事务
	START TRANSACTION;
	#转入账号增加资金
	update bankcard set CardMoney=CardMoney+par_transferMoney where CardId=par_CardIn;

	#转出账号减少资金
	update bankcard set CardMoney=CardMoney-par_transferMoney where CardId=par_CardOut and CardMoney>=par_transferMoney;	
	if ROW_COUNT() <=0 then
		select '回滚000';
		ROLLBACK;
		LEAVE aa;
	end if;
	#添加转账记录
  INSERT into cardtransfer(CardIdOut,CardIdIn,TransferMoney,TransferTime)    values(par_CardOut ,par_CardIn ,par_transferMoney,NOW());
	
	#全部成功，则提交
	#COMMIT;
	#只要有失败，则回滚
	#ROLLBACK;
	if hasErr = 1  then
		select '回滚';
		ROLLBACK;
	else 
		select '提交';
		COMMIT;
	end if;
END;
$$ 
delimiter ;

call Mytransfer(1,2,1);