-- 1.存钱（给定参数：银行账户，存钱金额）
-- 要求：修改余额，添加存款记录
DROP PROCEDURE if EXISTS `Text1`;
delimiter $$
CREATE PROCEDURE Text1(t_id int,t_CardMoney DECIMAL(10,2))
aa:BEGIN
	DECLARE msg INT DEFAULT 0;
	
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET msg =1;
	
	START TRANSACTION;
	
	update bankcard set CardMoney=CardMoney+t_Cardmoney where CardId=t_id;
	if ROW_COUNT()<=0 THEN	
	select '回滚1';
	ROLLBACK;
	leave aa;
	END if;
	
	insert into cardexchange(ExchangeId,CardId,MoneyInBank,MoneyOutBank,ExchangeTime) 
	values (10,t_id,t_Cardmoney,0,NOW());
	
	
	
	
	if msg=1 then 
		SELECT '回滚';
		ROLLBACK;
	else
		select '提交';
		COMMIT;
		end if;


END;
$$
delimiter ;

CALL Text1(1,100);





-- 2.取钱（给定参数：银行账户，密码，取钱金额）
 -- 要求：1.添加触发器约束：余额不能小于0
--            2.校验密码是否正确，正确执行3，否则取钱失败
--          3.修改余额，添加取款记录

drop PROCEDURE if EXISTS `text2`;
delimiter $$
create PROCEDURE text2(t_id int,t_password varchar(10), t_money DECIMAL(10,2))
aa:BEGIN
	declare msg int DEFAULT 0;
	DECLARE CONTINUE HANDLER for SQLEXCEPTION set msg=1;
	
	START TRANSACTION;
	
	update cardexchange set MoneyInBank=MoneyInBank-t_money where CardId=t_id and MoneyInBank>=0;
	if ROW_COUNT()<=0 then 
	select '回滚1';
	ROLLBACK;
	leave aa;
	END if;
	
	if CardPwd=t_password then 
	select '密码正确';
	else 
	select '密码错误';
	ROLLBACK;
	leave aa;
	end  if;
	
	insert into cardexchange(ExchangeId,CardId,MoneyInBank,MoneyOutBank,ExchangeTime) 
	values (10,t_id,0,t_money,NOW());
	
	if msg=1 then 
		SELECT '回滚';
		ROLLBACK;
	else
		select '提交';
		COMMIT;
		end if;

END;

call text2(1,123456,10);




-- 3.卡片清理：    
-- 每个用户只能有一张卡
-- 每个用户以余额最高的卡为主卡，其他的卡余额全部转移到主卡上，并注销非主卡。卡转账要有转账记录
-- 并同时升级主卡密码为8位以上（添0处理）（已经是8位及以上的不用处理），
-- 比如：12345升级为12345000  
--          123456789不处理
