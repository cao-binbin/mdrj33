-- 以下题目全部用事务实现：
-- 1.存钱（给定参数：银行账户，存钱金额）
--   要求：修改余额，添加存款记录
drop PROCEDURE if EXISTS `cun`;
delimiter $$
create PROCEDURE cun(zhanghu int,yue DECIMAL(10,2))
aa:BEGIN
	declare hasErr int DEFAULT 0;
	# 实现类似高级语言中 TRY CATCH(e)的功能，捕获异常信息
	DECLARE CONTINUE HANDLER for SQLEXCEPTION set hasErr =1;
	#开启事务
	START TRANSACTION;
	#账号增加资金
	update bankcard set CardMoney=CardMoney+par_CardMoney where par_cardid=CardId;

	#添加记录
  INSERT into bankcard(Cardid,CardMoney,CardTime)  values(par_Cardid ,par_CardMoney,NOW());
	
	#全部成功，则提交
	#COMMIT;
	#只要有失败，则回滚
	#ROLLBACK;
	if hasErr = 1 then
		select '回滚';
		ROLLBACK;
	else 
		select '提交';
		COMMIT;
	end if;
END;
$$ 
delimiter ;

call cun(1,50); 
SELECT * from bankcard


-- 2.取钱（给定参数：银行账户，密码，取钱金额）
 -- 要求：1.添加触发器约束：余额不能小于0
--            2.校验密码是否正确，正确执行3，否则取钱失败
--          3.修改余额，添加取款记录

drop PROCEDURE if EXISTS `quqian`;
delimiter $$
create PROCEDURE quqian(zhanghu int,mima varchar(10), jine DECIMAL(10,2))
aa:BEGIN
	declare msg int DEFAULT 0;
	DECLARE CONTINUE HANDLER for SQLEXCEPTION set msg=1;
	
	START TRANSACTION;
	
	update cardexchange set MoneyInBank=jine where CardId=zhanghu and MoneyInBank>=0;
	if ROW_COUNT()<=0 then 
	select '回滚1';
	ROLLBACK;
	leave aa;
	END if;
	
	if CardPwd=mima then 
	select '密码正确';
	else 
	select '密码错误';
	ROLLBACK;
	leave aa;
	end  if;
	
	insert into cardexchange(ExchangeId,CardId,MoneyInBank,MoneyOutBank,ExchangeTime) 
	values (10,t_id,0,t_money,NOW());
	
	if msg=1 then 
		SELECT '回滚';
		ROLLBACK;
	else
		select '提交';
		COMMIT;
		end if;

END;

call quqian(1,123456,10);





