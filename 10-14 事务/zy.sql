
-- 以下题目全部用事务实现：
-- 1.存钱（给定参数：银行账户，存钱金额）
--   要求：修改余额，添加存款记录
-- 
DROP PROCEDURE if exists `myty`;
delimiter $$
create procedure myty(par_cardid int,par_cardmoney DECIMAL(5,2))
BEGIN
		START TRANSACTION;
		UPDATE bankcard set CardMoney=CardMoney+par_cardmoney where CardId=par_cardid;
		insert into cardexchange(CardId,MoneyInBank,MoneyOutBank,ExchangeTime)
		VALUES(par_cardid,par_cardmoney,0,NOW());
		
END;
$$
delimiter ;
call myty(1,200);


-- 2.取钱（给定参数：银行账户，密码，取钱金额）
--   要求：1.添加触发器约束：余额不能小于0
--             2.校验密码是否正确，正确执行3，否则取钱失败
--             3.修改余额，添加取款记录

drop PROCEDURE if EXISTS `Text2`;
delimiter $$
create PROCEDURE Text2(BankAccount varchar(20),BankPassword varchar(6),BankMoney DECIMAL(5,2))
aa:BEGIN
	DECLARE pwd varchar(6) DEFAULT '';
	START TRANSACTION;
	select CardPwd into pwd from bankcard where CardId=BankAccount;
	IF pwd = BankPassword THEN
		update bankcard set CardMoney=CardMoney-BankMoney where CardId=BankAccount;
		INSERT into cardexchange(CardId,MoneyInBank,MoneyOutBank,ExchangeTime)    
		values(BankAccount,0,BankMoney,NOW());
		COMMIT;
	ELSE
		SIGNAL SQLSTATE 'HY000' set MESSAGE_TEXT = '密码错误';	
		ROLLBACK;
		LEAVE aa;
END IF;
END;
$$
delimiter ;


-- 3.卡片清理：    
-- 每个用户只能有一张卡
-- 每个用户以余额最高的卡为主卡，其他的卡余额全部转移到主卡上，并注销非主卡。卡转账要有转账记录
-- 并同时升级主卡密码为8位以上（添0处理）（已经是8位及以上的不用处理），
-- 比如：12345升级为12345000  
--           123456789不处理
