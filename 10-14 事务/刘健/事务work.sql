#以下题目全部用事务实现：
#1.存钱（给定参数：银行账户，存钱金额）
 # 要求：修改余额，添加存款记录
 
 drop procedure if exists `demo_1`;
 
 delimiter $$
 create procedure demo_1(par_CardNo varchar(30),par_money DECIMAL(10,2))
 begin
		update bankcard set CardMoney=CardMoney+par_money where CardNo=par_CardNo;
		
	  insert into cardexchange(CardId,MoneyInBank,MoneyOutBank,ExchangeTime) values(p_cardid,p_cardmoney,0,now());
 
 end;
 $$
 delimiter;
 
 
 
 
 -- 2.取钱（给定参数：银行账户，密码，取钱金额）
--   要求：1.添加触发器约束：余额不能小于0
--             2.校验密码是否正确，正确执行3，否则取钱失败
--             3.修改余额，添加取款记录

drop procedure if exists `demo_2`;
delimiter $$
create procedure demo_2(p_cardid int,p_cardpwd int,p_cardmoney decimal(10,2))
aa:begin
	declare a int default 0;
	-- 开启事务
	start TRANSACTION;
	
	-- 2.校验密码是否正确，正确执行3，否则取钱失败
	select count(*) into a from bankcard where CardId=p_cardid and CardPwd=p_cardpwd;
	if a<=0 || a>1 then
		select '回滚';
		rollback;
		leave aa;
	end if;
	update bankcard set CardMoney=CardMoney-p_cardmoney where CardId=p_cardid;
	if row_count()<=0 then
		select '回滚';
		rollback;
		leave aa;
	end if;
	insert into cardexchange(ExchangeId,CardId,MoneyInBank,MoneyOutBank,ExchangeTime) values(11,p_cardid,0,p_cardmoney,now());
	if row_count()<=0 then
		select '回滚';
		rollback;
		leave aa;
	else
		select '提交';
		commit;
	end if;
end;
$$
delimiter ;