-- 以下题目全部用事务实现：
-- 1.存钱（给定参数：银行账户，存钱金额）
--   要求：修改余额，添加存款记录
drop procedure if exists `pro_add`;
delimiter $$
create procedure pro_add (p_cardid int,p_cardmoney decimal(10,2))
aa:begin
	--  开启事务
	start TRANSACTION;

	update bankcard set CardMoney=CardMoney+p_cardmoney where CardId=p_cardid;
	if row_count()<=0 then
		select '回滚';
		rollback;
		leave aa;
	end if;
	
	insert into cardexchange(ExchangeId,CardId,MoneyInBank,MoneyOutBank,ExchangeTime) values(10,p_cardid,p_cardmoney,0,now());
	if row_count()<=0 then
		select '回滚';
		rollback;
		leave aa;
	ELSE
		select '提交';
		commit;
	end if;
end;
$$
delimiter ;
call pro_add(1,500);

-- 
-- 2.取钱（给定参数：银行账户，密码，取钱金额）
--   要求：1.添加触发器约束：余额不能小于0
--             2.校验密码是否正确，正确执行3，否则取钱失败
--             3.修改余额，添加取款记录
-- 1添加触发器约束

drop trigger if exists `update_after_bankcard`;
create trigger update_after_bankcard after update on bankcard for each row
begin
	if old.CardMoney<0 then
		SIGNAL SQLSTATE 'HY000' set message_text ='余额不能小于0';
	end if;
end;

drop procedure if exists `pro_out`;
delimiter $$
create procedure pro_out(p_cardid int,p_cardpwd int,p_cardmoney decimal(10,2))
aa:begin
	declare a int default 0;
	-- 开启事务
	start TRANSACTION;
	
	-- 2.校验密码是否正确，正确执行3，否则取钱失败
	select count(*) into a from bankcard where CardId=p_cardid and CardPwd=p_cardpwd;
	if a<=0 || a>1 then
		select '回滚';
		rollback;
		leave aa;
	end if;
	
	-- 修改余额
	update bankcard set CardMoney=CardMoney-p_cardmoney where CardId=p_cardid;
	if row_count()<=0 then
		select '回滚';
		rollback;
		leave aa;
	end if;
	
	-- 添加取款记录
	insert into cardexchange(ExchangeId,CardId,MoneyInBank,MoneyOutBank,ExchangeTime) values(11,p_cardid,0,p_cardmoney,now());
	if row_count()<=0 then
		select '回滚';
		rollback;
		leave aa;
	else
		select '提交';
		commit;
	end if;
end;
$$
delimiter ;
call pro_out(1,123456,200);

-- 3.卡片清理：    
-- 账户 AccountId -- 
-- 每个用户只能有一张卡
-- 每个用户以余额最高的卡为主卡，其他的卡余额全部转移到主卡上，并注销非主卡。卡转账要有转账记录
-- 并同时升级主卡密码为8位以上（添0处理）（已经是8位及以上的不用处理），
-- 比如：12345升级为12345000  
--           123456789不处理
-- select CardId,CardMoney,AccountId from bankcard where CardMoney in(
-- select max(CardMoney) from bankcard group by AccountId);
drop procedure if exists `pro_t`;
delimiter $$
create procedure pro_t()
bb:begin
	declare t_acc int default 0;-- 账户ID
	declare t_cardid int default 0; -- 获取卡号ID
	declare t_cardmoney decimal(19,4);-- 获取金额
	declare hasErr int default 0;-- 事务错误变量
	declare lenar int default 0;-- 密码长度
	declare lenvar varchar(30) default '';-- 密码补齐
	declare flag int default true;
	-- 游标
	declare msg cursor for
		select CardId,CardMoney,AccountId,CardPwd from bankcard where CardMoney in
		(
			select max(CardMoney) from bankcard group by AccountId
		);
	
	declare continue handler for not found set flag = false;
	declare continue handler for SQLEXCEPTION set hasErr=1;
	
	start TRANSACTION;
	open msg;
	-- 开启事务
	
	aa:while true do
		fetch msg into t_cardid,t_cardmoney,t_acc,lenvar;
		if flag then
			leave aa;
		end if;
		while true do
			-- 调用储存转账、卡转账要有转账记录
			call pro_acc(t_cardid,t_acc);

			-- 调用储存注销
			call pro_afdd(t_cardid,t_acc);

			-- 调用储存升级主卡密码为8位以上（添0处理）
			select CardPwd into lenvar from bankcard where CardId=t_cardid and AccountId=t_acc;
			set lenar=char_length(lenvar);
			while lenar<=8 do	
			  set lenvar=concat(lenvar,'0');
			end while;
			update bankcard set CardPwd=lenvar where CardId=t_cardid and AccountId=t_acc;
		end while;
	end while;
	
	close msg;

			if hasErr=1 then
				select '回滚';
				rollback;
				leave bb;
			else
			select '提交';
			commit;	
			end if;
end;
$$
delimiter ;
-- 调用
call pro_t();

-- 调用储存转账、卡转账要有转账记录
drop procedure if exists `pro_acc`;
delimiter $$
create procedure pro_acc(p_cardid int,p_ac int)
bb:begin
	declare t_money varchar(30) default '';
	declare t_cardid varchar(30) default '';
	declare i int default 0;
	declare k int default 0;
	declare flag int default true;
	
	declare msg cursor for (select CardId,CardMoney from bankcard where AccountId=p_ac);
	declare continue handler for SQLEXCEPTION set k =1;
	declare continue handler for not found set flag=false;
	
	start TRANSACTION;
	open msg;
	aa:while true DO
		fetch msg into t_cardid,t_money;
			if flag then
				leave aa;
			end if;
		select max(CardId) into i from cardexchange;
		
		if t_cardid=p_cardid then
			iterate aa;
		end if;
		update bankcard set CardMoney=CardMoney-t_money where CardId=t_cardid;
			if row_count()<=0 then
				select '回滚';
				rollback;
				leave aa;
			end if;
		
		update bankcard set CardMoney=CardMoney+t_money where CardId=p_cardid;
			if row_count()<=0 then
				select '回滚';
				rollback;
				leave aa;
			end if;
		-- 转出
		set i=i+1;
		insert into cardexchange(ExchangeId,CardId,MoneyInBank,MoneyOutBank,ExchangeTime) values(i,t_cardid,0,t_money,now());
			if row_count()<=0 then
				select '回滚';
				rollback;
				leave aa;
			end if;
		-- 转入
		set i=i+1;
		insert into cardexchange(ExchangeId,CardId,MoneyInBank,MoneyOutBank,ExchangeTime) values(i,p_cardid,t_money,0,now());
			if k=1 then
				select '回滚';
				rollback;
				leave bb;
			end if;
		end while;
		select '提交';
		commit;
	close msg;
end;
$$
delimiter ;

-- 调用储存注销
drop procedure if exists `pro_afdd`;
create procedure pro_afdd(p_cid int,p_aid int)
begin
	declare t_cid int;
	declare flag int default true;
	declare msg cursor for
	select CardId from bankcard where AccountId=p_aid and CardId<>p_cid;
	declare continue handler for not found set flag=1;
	
	open msg;
	
	aa:while true do
	fetch msg into t_cid;
	if flag then
		leave aa;
	end if;
	delete from bankcard where CardId=t_cid;
	end while;
	
	close msg;
end;
