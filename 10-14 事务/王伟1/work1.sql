-- 1.存钱（给定参数：银行账户，存钱金额）
--   要求：修改余额，添加存款记录
drop procedure if exists `upm`;
delimiter $$
create procedure upm(par_in varchar(20),par_mo decimal(10,2))
BEGIN
   declare hti int default 1;
   declare continue handler for sqlexception set hti= 0;
   start transaction ; 
   update bankcard set CardMoney=par_mo+CardMoney where CardNo=par_in;
   select CardId into @n from bankcard where CardNo=par_in;
	 if ROW_COUNT() <=0 then
	    select '回滚';
	    rollback;
	 end if;
   insert into cardexchange(CardId,MoneyOutBank,ExchangeTime)
   values(@n,par_mo,0,NOW());
   if hti=0 THEN
      select '回滚';
      rollback;
   else 
      select '提交';
      commit;
   end if;
end;
$$
delimiter ;
call upm('6225098134285',100)
-- 2.取钱（给定参数：银行账户，密码，取钱金额）
--   要求：1.添加触发器约束：余额不能小于0
--             2.校验密码是否正确，正确执行3，否则取钱失败
--             3.修改余额，添加取款记录
drop trigger if exists `dra`;
create trigger acg before update on bankcard for each ROW
begin
    if new.CardMoney<0 then
		signal sqlstate 'HY000' set message_text='余额不能为负数';
    end if;
end;

drop procedure if exists `draw`
delimiter $$
create procedure draw(par_No varchar(20),par_word int,par_mon decimal(10,2))
BEGIN
   declare hit int default 1;
	 declare continue handler for sqlexception set hit=0;
	 start transaction;
	 if par_word in(select CardPwd from bankcard) then
	 
	 update bankcard set CardMoney=CardMoney-par_mon where CardNo=par_No;
   select CardId into @n from bankcard where CardNo=par_No;
	 if ROW_COUNT()<=0 then
		  select '回滚';
			rollback;
	 end if;
   insert into cardexchange(CardId,MoneyInBank,MoneyOutBank,ExchangeTime)
   values(@n,0,par_mo,NOW());
	 else 
	 if hti=0 THEN
	     select '取钱失败';
       select '回滚';
       rollback;
   else 
       select '提交';
       commit;
      end if;
	 end if; 
end;
$$ 
delimiter ;
call draw('6225098134285',123456,100)


-- 3.卡片清理：    
-- 每个用户只能有一张卡
-- 每个用户以余额最高的卡为主卡，其他的卡余额全部转移到主卡上，并注销非主卡。卡转账要有转账记录
-- 并同时升级主卡密码为8位以上（添0处理）（已经是8位及以上的不用处理），
-- 比如：12345升级为12345000  
--           123456789不处理