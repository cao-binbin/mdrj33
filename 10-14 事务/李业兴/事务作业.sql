-- 1.存钱（给定参数：银行账户，存钱金额）
--   要求：修改余额，添加存款记录
drop procedure if exists `save`;
delimiter $$
create PROCEDURE save(account varchar(50),money decimal(10,2))
aa:begin
	declare hasErr int default 0;
	declare continue handler for sqlexception set hasErr=1;

-- 开启事务
	start transaction;
-- 修改余额
	update bankcard set CardMoney=CardMoney+money where CardId=account;
-- 	 影响行数
		if ROW_COUNT() <=0 then
		select '回滚000';
		ROLLBACK;	 
	 END if;

-- 	添加存款记录
	insert into cardtransfer(CardIdOut,CardIdIn,TransferMoney,TransferTime)
	values (null,account,money,NOW());
-- 	查询是否成功
	if hasErr = 1 then
		select '失败';
		ROLLBACK;
	else 
		select '成功';
		COMMIT;
	end if;
end;
$$
delimiter ;

call save(1,100)

-- 2.取钱（给定参数：银行账户，密码，取钱金额）
--   要求：1.添加触发器约束：余额不能小于0
--             2.校验密码是否正确，正确执行，否则取钱失败
--             3.修改余额，添加取款记录

-- 添加触发器约束：余额不能小于0
drop trigger if exists m_draw;
create trigger m_draw before update on bankcard for each row
begin
	if new.CardMoney < 0 then
		signal sqlstate 'HY000' set message_text = '余额不能小于0';
	end if;
end;



drop procedure if exists `m_Get`;
delimiter $$
create procedure m_Get(par_account varchar(50),par_password varchar(50),par_fetch decimal(10,2))
aa:begin
	declare hasErr int default 0;
	declare CONTINUE HANDLER for SQLEXCEPTION set hasErr=1;
-- 开始事务
	start transaction;
-- 	判断密码是否正确
	 if  CardPwd > par_password  then
		IF CardMoney > par_fetch THEN
	 -- 修改余额
			update bankcard set CardMoney=CardMoney-par_fetch where CardId=par_account;
				if ROW_COUNT() <=0 then
					select '回滚1';
						ROLLBACK;
					leave aa;
				end if;
	 END IF;
	end if;
-- 	添加取钱记录
		insert into cardexchange(CardId,MoneyInBank,MoneyOutBank,ExchangeTime)
		values (par_account,null,par_fetch,NOW());

	 
	 if hasErr = 1 then
		select '回滚';
		ROLLBACK;
	else 
		select '提交';
		COMMIT;
	end if;
	
end;
$$
delimiter;

call m_Get('1','444444',1);

-- 3.卡片清理：    
-- 每个用户只能有一张卡
-- 每个用户以余额最高的卡为主卡，其他的卡余额全部转移到主卡上，并注销非主卡。卡转账要有转账记录
-- 并同时升级主卡密码为8位以上（添0处理）（已经是8位及以上的不用处理），
-- 比如：12345升级为12345000  
--           123456789不处理