-- 以下题目全部用事务实现：
-- 1.存钱（给定参数：银行账户，存钱金额）
--   要求：修改余额，添加存款记录
drop procedure if exists `work1`;
delimiter $$
create procedure `work1`(par_cardid int,par_moneyinbank DECIMAL(10,2))
aa:begin
	#设定cry语句
		declare cry int default 0;
		declare continue handler for sqlexception set cry = 1;
	#开启事务
		start transaction;
	#增加银行账户金额记录
		insert into cardexchange(CardId,MoneyInBank,MoneyOutBank,ExchangeTime) 
		VALUES(par_cardid,par_moneyinbank,0,now());
	#判断增加记录有没有错误
		if row_count() <=0 or par_moneyinbank <=0 then
			select '存钱的数据错误'
			rollback;
			leave aa;
		end if;
	#修改银行账户金额
		update bankcard set CardMoney=CardMoney+par_moneyinbank where CardId=par_cardid;
	#判定事务是否回滚
		if cry = 1 then
			select '错误,数据回滚';
			rollback;
		else 
			select '更改成功';
			commit;
		end if;
end
$$
delimiter ;

call work1(13,20);


	insert into cardexchange(CardId,MoneyInBank,MoneyOutBank,ExchangeTime) VALUES(1,1,0,now())
	update bankcard set CardMoney=CardMoney+1 where CardId=1;

-- 
-- 2.取钱（给定参数：银行账户，密码，取钱金额）
--   要求：1.添加触发器约束：余额不能小于0
--             2.校验密码是否正确，正确执行3，否则取钱失败
--             3.修改余额，添加取款记录

	#添加触发器约束 约束条件为 余额不能小于0
		drop trigger if exists `tri_a`;
		create trigger `tri_a` before update on bankcard for each row
		begin
			if new.CardMoney<0 then
				signal sqlstate 'YH000' set message_text = '当前余额不足';
			end if;
		end;
		
-- 		update bankcard set CardMoney=CardMoney-100 where CardId = 11;

drop procedure if exists `work2`;
delimiter $$
create procedure `work2`(par_cardid int,par_pwd varchar(30),par_moneyoutbank decimal(10,2))
aa:begin
	#查询当前取款卡号密码
		declare pwd varchar(30) default '';
	#设定cry语句
		declare cry int default 0;
		declare continue handler for sqlexception set cry=1;
		select CardPwd into pwd from bankcard where CardId=par_cardid;
-- 	#添加触发器约束 约束条件为 余额不能小于0
-- 		drop if trigger if exists `tri_a`;
-- 		create trigger `tri_a` before update bankcard for each row
-- 		begin
-- 			if new.CardMoney<0 then
-- 				signal sqlstate 'YH000' set message_text = '当前余额不足';
-- 			end if
-- 		end;
	#开始事务
		start transaction;
	#校验密码是否正确 正确执行,否则取钱失败
		if par_pwd = pwd then 
			insert into cardexchange(CardId,MoneyInBank,MoneyOutBank,ExchangeTime)
			values (par_cardid,0,par_moneyoutbank,now());
		else 
			select '密码错误,取钱失败';
			leave aa;
		end if;
	#修改余额，添加取款记录
		update bankcard set cardmoney=cardmoney-par_moneyoutbank where CardId=par_cardid;
	#判定事务是否回滚
		if cry = 1 then
			select '错误,已回滚';
			rollback;
		else
			select '取款成功';
			commit;
		end if;

end
$$
delimiter ;

call work2(3,'444444',50)

set @type=(select CardPwd from bankcard where CardId=3)
select  @type='444444'


-- 3.卡片清理：    
-- 每个用户只能有一张卡
-- 每个用户以余额最高的卡为主卡，其他的卡余额全部转移到主卡上，并注销非主卡。卡转账要有转账记录
-- 并同时升级主卡密码为8位以上（添0处理）（已经是8位及以上的不用处理），
-- 比如：12345升级为12345000  
--           123456789不处理




drop procedure if exists `work3`;
delimiter $$
create procedure `work3`()
begin
	#设定查找用户所有银行卡中余额最高的那张卡
	declare max_money_id int default 0;
	declare max_money int default 0;
	declare user_id int default 0;
	declare card_money DECIMAL(10,2);
	declare flag int default true;
	declare pwd int default 0;
	#创建cry
	declare cry int default 0;
	#查询出所有用户
	declare cur1 CURSOR for (select DISTINCT(AccountId) from bankcard);
	declare continue handler for not found set flag = false;
	declare continue handler for sqlexception set cry =1;
	start transaction;
	open cur1;
	WHILE flag DO
		FETCH cur1 into user_id;
		if flag then
			#查询出用户所有银行卡中余额最高的那张银行卡id 赋值给max_money_id
			select CardId into max_money_id from bankcard where AccountId=user_id order by CardMoney desc limit 1;
			select max(CardMoney) into max_money from bankcard where CardId=max_money_id;
			#查询出除了最高余额的银行卡 所有银行卡的金额相加 赋值给card_money
			select sum(CardMoney) into card_money from bankcard where AccountId=user_id; #and cardid != max_money_id;
			#将所有余额转账记录添加到转账记录表
			insert into cardexchange(CardId,MoneyInBank,MoneyOutBank,ExchangeTime)
			values(max_money_id,card_money-max_money,0,now());
			#其他卡的所有余额转入主卡
			update bankcard set CardMoney=card_money where cardid=max_money_id;
			#将每个用户的非主卡余额全部清空
			update bankcard set CardMoney=0 where CardId != max_money_id and AccountId = user_id;
			#注销非主卡外的所有银行卡
			update bankcard set CardState=1 where CardMoney=0;
			#升级变更主卡密码
			select char_length(CardPwd) into pwd from bankcard where cardid = max_money_id;
			while pwd < 8 do
				set pwd=pwd + 1 ;
				#更新密码
				update bankcard set CardPwd=concat(CardPwd,0) where cardid=max_money_id;
			end while;
		end if;
	END WHILE;
	close cur1;
	if cry = 1 then
		select '操作过程有错误,已回滚';
		rollback;
	else 
		select '操作成功';
		commit;
	end if;	
	select * from bankcard;
	select * from cardexchange;
end
$$
delimiter ;

call work3();
-- 
-- select * from bankcard where CardId !=6 and AccountId=4;
-- select * from cardexchange;
-- update bankcard set CardMoney=0 where CardId != 6 and AccountId = 4;
-- 
-- select * from bankcard
-- select sum(CardMoney) from bankcard where AccountId=3 and cardid != 8
-- 
-- call work3();
-- 
-- update 
-- select * from bankcard
-- select CardId from bankcard where AccountId=4 order by CardMoney desc limit 1;
-- 
-- 
-- 
-- 
-- 
-- 
-- 
-- 
-- select CardNo from bankcard where AccountId=3 and CardMoney=400000;


-- DROP PROCEDURE IF EXISTS `mysql03`;
-- DELIMITER $$
-- CREATE PROCEDURE mysql03()
-- BEGIN 
--  DECLARE sums INT;
--  DECLARE carons VARCHAR(30);
--  DECLARE money DECIMAL(10,2);
--  DECLARE aId INT;
--  DECLARE flag INT DEFAULT TRUE;
--  DECLARE mucur CURSOR FOR
--  (
--   SELECT DISTINCT `AccountId`  FROM `bankcard`
--  );
--  DECLARE CONTINUE HANDLER FOR NOT FOUND SET flag=FALSE;
--  OPEN mucur;
--  aa:WHILE flag DO
--   FETCH mucur INTO aId;
--   IF flag=FALSE THEN
--    LEAVE aa;
--   END IF;
--   SELECT MAX(CardMoney) into money FROM bankcard  WHERE AccountId=aId ;
--   SELECT CardNo INTO carons FROM bankcard WHERE CardMoney=money  and AccountId=aId;
--   UPDATE bankcard SET CardState=1 WHERE CardNo!=carons;
--   SELECT SUM(CardMoney) INTO sums FROM bankcard;
--   UPDATE bankcard SET CardMoney=CardMoney+(sums-CardMoney) WHERE CardState=0 AND AccountId=aId;
--   UPDATE bankcard SET CardMoney=0 WHERE CardState=0 AND AccountId=aId;
-- 	LEAVE aa;
--  END WHILE;
--  CLOSE mucur;
--  
-- END
-- $$
-- DELIMITER ;
-- 
-- 
-- call mysql03()
-- select * from bankcard
