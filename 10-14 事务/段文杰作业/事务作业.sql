-- 以下题目全部用事务实现：
-- 1.存钱（给定参数：银行账户，存钱金额）
--   要求：修改余额，添加存款记录

-- 删除存储
DROP PROCEDURE IF EXISTS  `test1`;
-- 创建存储
CREATE PROCEDURE test1(par_CardId INT,par_CardMoney INT)
BEGIN 
 -- 定义变量
 DECLARE hasErr INT DEFAULT 0;
 --
 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET hasErr=1;

-- 开启事务
START TRANSACTION;
-- 存入账号的增加资金,修改余额
   UPDATE bankcard SET CardMoney=CardMoney+par_CardMoney 
	 WHERE CardId=par_CardId;
-- 添加存款记录
    INSERT INTO cardtransfer(CardIdIn,TransferMoney,transferTime)
    VALUES(par_CardId,par_CardMoney,NOW());
		
	-- 判断所有操作是否完全成功
IF  hasErr=1 THEN
	    SELECT '回滚';
			ROLLBACK;
ELSE
	   SELECT '提交';
		 COMMIT;
END IF;


END;

CALL test1(1,100);




-- 
-- 2.取钱（给定参数：银行账户，密码，取钱金额）
--   要求：1.添加触发器约束：余额不能小于0
--             2.校验密码是否正确，正确执行3，否则取钱失败
--             3.修改余额，添加取款记录

-- 删除存储
#1.添加触发器约束：余额不能小于0						
DROP TRIGGER IF EXISTS `after_cardexchange`;
CREATE TRIGGER after_cardexchange BEFORE UPDATE ON cardexchange FOR EACH ROW
BEGIN
  IF MoneyOutBank<0 THEN
	SIGNAL SQLSTATE 'HY000' set message_text='你的余额不足！';
  END IF;
END ; 
-- 删除存储    
DROP PROCEDURE IF EXISTS `test2`;

-- 创建存储
CREATE PROCEDURE test2(par_CardId INT,par_pwd INT,par_CardMoney INT)
BEGIN 
 -- 定义一个临时变量
 declare hasErr int default 0;
 DECLARE num INT DEFAULT 0;
 declare continue handler for sqlexception set hasErr=1;
  START TRANSACTION;
 SELECT CardPwd INTO num FROM bankcard WHERE CardId=par_CardId;
 IF par_pwd=num THEN
    -- 修改取账户的取款资金
	    UPDATE bankcard SET CardMoney=CardMoeny-par_CardMoney 
WHERE CardId=par_CardId AND CardMoeny>par_CardMoney;
    IF ROW_COUNT() =1 THEN
	     SELECT '金额修改失败！！！';
			 ROLLBACK;
ELSE
	     SELECT '金额修改成功！';
			 COMMIT;
END IF;
-- 添加账户的的取款记录
INSERT INTO cardtransfer(CardOut,transferMoney,transferTime)
VALUES(par_CardId,par_CardMoney,NOW());

SELECT '密码成功！！';
ELSE
 SELECT '密码错误！！！';
	 
END IF;
IF hasErr=1 THEN
	  SELECT '回滚';
		ROLLBACK;
ELSE
	   SELECT '提交';
	  COMMIT;
END IF;


END;


CALL test2(1,123456,100);









-- 3.卡片清理：    
-- 每个用户只能有一张卡
-- 每个用户以余额最高的卡为主卡，其他的卡余额全部转移到主卡上，并注销非主卡。卡转账要有转账记录
-- 并同时升级主卡密码为8位以上（添0处理）（已经是8位及以上的不用处理），
-- 比如：12345升级为12345000  
--           123456789不处理
