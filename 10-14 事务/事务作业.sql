use bank4;


#以下题目全部用事务实现：
#1.存钱（给定参数：银行账户，存钱金额）
  #要求：修改余额，添加存款记录

-- -------------------------------------------------------------------------------------------------
drop procedure if exists `pro_deposit`;
delimiter //
create procedure pro_deposit(in pro_CardId int,in pro_MoneyInBank decimal(10,2))
start_:begin 
  declare mistake int default 0;
  declare continue handler for sqlexception set mistake = 1;
  #开启事务
  start transaction ;

	#修改余额
  update bankcard set CardMoney=CardMoney+pro_MoneyInBank where pro_CardId=CardId;
  if row_count()<=0 then
    select '金额增加失败，执行回滚' 信息;
    rollback;
    leave start_ ;
  end if;
  #添加存款记录		
  insert into cardexchange(CardId,MoneyInBank,MoneyOutBank,ExchangeTime)
  values(pro_CardId,pro_MoneyInBank,0,now());
  
  if mistake=0 then 
   select '操作全部执行成功，执行提交' 信息;
   commit;
  else 
   select '最终操作失败，执行回滚' 信息;
   rollback;
  end if;
end ;
//
delimiter ;

call pro_deposit(1,100);





-- --------------------------------------------------------------------------------
#2.取钱（给定参数：银行账户，密码，取钱金额）
  #要求：1.添加触发器约束：余额不能小于0
            #2.校验密码是否正确，正确执行3，否则取钱失败
            #3.修改余额，添加取款记录
#1.添加触发器约束：余额不能小于0						
drop trigger if exists `after_update_cardexchange`;
create trigger after_update_cardexchange after update on cardexchange for each row
begin
  if MoneyOutBank<0 then
	SIGNAL SQLSTATE 'HY000' set message_text='你的余额不足！';
  end if;
end ;
							
drop procedure if exists `pro`;
delimiter //
create procedure pro(pro_id int,pro_paw varchar(20),pro_money float)
aa:begin
	declare mistake int default 0;
	declare paw varchar(20) default '';
	declare continue handler for sqlexception set mistake=1;
	start transaction ;
  select CardPwd into paw from bankcard where CardId=pro_id;
	if paw=pro_paw then
		update bankcard set CardMoney=CardMoney-pro_money where CardId=pro_id;
		if row_count()=0 then
		  select '金额修改失败';
		end if;
		insert into cardexchange(CardId,MoneyInBank,MoneyOutBank,ExchangeTime)
		values(pro_id,0,pro_money,now());
		if mistake=0 then
	    select '修改成功。';
	    commit;
	  else 
	    select '修改失败。';
	    rollback;
      leave aa;
    end if ;
	else 
	  select '密码出错。';
		leave aa;
  end if;
	
end ;
//
delimiter ;

call pro(1,123456,2)


select * from bankcard;
select * from cardexchange;


-- --------------------------------------------------------------------------------------
#3.卡片清理：    
#每个用户只能有一张卡
#每个用户以余额最高的卡为主卡，其他的卡余额全部转移到主卡上，并注销非主卡。卡转账要有转账记录
#并同时升级主卡密码为8位以上（添0处理）（已经是8位及以上的不用处理），
#比如：12345升级为12345000  
          #123456789不处理
					
drop procedure if exists `pro_clear`;
delimiter //
create procedure pro_clear()
begin
  declare pro_useId int default 1;                      #用户id
	declare pro_maxId int default 0;                        #最大用户id
	select max(AccountId) into pro_maxId from accountinfo;
	aa:while pro_useId<=pro_maxId do
	  if pro_useId=null then
		  set pro_useId=pro_useId+1;
		  iterate aa;
		else 
		  select * from accountinfo where accountinfo.AccountId=pro_useId;
			set pro_useId=pro_useId+1;
		
		end if;
	end while ;


end;
//
delimiter ;

call pro_clear();






















