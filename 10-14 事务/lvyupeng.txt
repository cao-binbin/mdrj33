-- 以下题目全部用事务实现：
-- 1.存钱（给定参数：银行账户，存钱金额）
--   要求：修改余额，添加存款记录
create procedure `data1`(pro_cardid int,pro_moneyin decimal(10,2))
begin

start transaction;
update bankcard set cardmoney=cardmoney+pro_moneyin where cardid=pro_cardid;
insert into cardexchange(cardid,moneyinbank,,exchangetime)
values(pro_cardid,pro_moneyin,NOW());

END





-- 2.取钱（给定参数：银行账户，密码，取钱金额）
--   要求：1.添加触发器约束：余额不能小于0
--             2.校验密码是否正确，正确执行3，否则取钱失败
--             3.修改余额，添加取款记录

create trigger `tri_1` before update on `bankcard ` for each row
begin
if new.Cardmoney<0 then
signal sqlstate`HY000` SET MESSAGE_TEXT='';
END IF

end;
create procedure `data2`(pro_cardno varchar(30),pro_cardpwd varchar(30),pro_moneyout decimal(10,2))
begin

aa:BEGIN
		DECLARE numId int;
		declare hast int default 0;
		declare continue handler for SQLEXCEPTION set hast=1;
		start TRANSACTION;
		
		UPDATE bankcard set CardMoney=CardMoney-pro_moneyout where CardNo=pro_cardno and CardPwd=pro_cardpwd;
		SELECT CardId into numId from bankcard where CardNo=bankUser;
		INSERT into cardexchange(CardId,MoneyInBank,MoneyOutBank,ExchangeTime) VALUES(numId,0,pro_moneyout,NOW());

		if hast=1 THEN
			select '你给老子滚蛋';
			ROLLBACK;
		ELSE
			select '欢迎欢迎';
			COMMIT;
		end if;
END
$$
delimiter ;




end