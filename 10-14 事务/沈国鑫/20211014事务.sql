-- 以下题目全部用事务实现：
-- 1.存钱（给定参数：银行账户，存钱金额）
--   要求：修改余额，添加存款记录

drop procedure if exists `transaction1`;
delimiter $$
create procedure transaction1(par_CardNo varchar(30),par_MoneyIn DECIMAL(10,2))
a:BEGIN
	declare hasErr int default 0 ;
	declare par_CardId int ;
	declare continue handler for sqlexception set hasErr = 1 ;
	set par_CardId = (select CardId from bankcard where CardNo = par_CardNo) ;
	
	start transaction ;
		update bankcard set CardMoney = CardMoney + par_MoneyIn where CardNo = par_CardNo ;
			IF ROW_COUNT() <= 0 THEN
				select '回滚' ;
				ROLLBACK ;
				LEAVE a ;
			END IF;

		insert into cardexchange(CardId,MoneyInBank,MoneyOutBank,ExchangeTime)
		values(par_CardId,par_MoneyIn,0,NOW());
			IF hasErr = 1 THEN
				select '回滚';
				ROLLBACK;
			ELSE 
				select '提交';
				COMMIT;
			END IF;
END;
$$
delimiter ;
call transaction1('6225098234146',200);


-- 2.取钱（给定参数：银行账户，密码，取钱金额）
--   要求：1.添加触发器约束：余额不能小于0
--             2.校验密码是否正确，正确执行3，否则取钱失败
--             3.修改余额，添加取款记录
drop trigger if exists `money_enough`;
create trigger money_enough before update on bankcard for each row
BEGIN
	if new.CardMoney < 0 then
		signal sqlstate 'HY000' set message_text = '余额不能小于0';
	end if;
END;

drop procedure if exists `transaction2`;
delimiter $$
create procedure transaction2(par_CardNo varchar(30),par_pwd varchar(30),par_MoneyOut DECIMAL(10,2))
BEGIN
	declare temp_pwd varchar(30) default '';
	declare num varchar(30) default '';
	
	declare haserr int default 0;
	declare continue handler for sqlexception set haserr = 1;

	start transaction;
		select CardPwd into temp_pwd from bankcard where CardNo=par_CardNo;
		if par_pwd = temp_pwd then
			update bankcard set CardMoney = CardMoney - par_MoneyOut where CardNO=par_CardNo;
			select CardId into @ID from bankcard where CardNo=par_CardNo;
			insert into cardexchange(CardId,MoneyInBank,MoneyOutBank,ExchangeTime)
			VALUES(@ID,0,par_MoneyOut,NOW());
		else 
			select '错误';
		end if;
 		if haserr = 1  then
 			select '回滚';
 			ROLLBACK;
 		else 
			select '提交';
 			COMMIT;
 		end if;
END;
$$
delimiter ;
call transaction2('6225098234146','444444',1000);

-- 3.卡片清理：    
-- 每个用户只能有一张卡
-- 每个用户以余额最高的卡为主卡，其他的卡余额全部转移到主卡上，
-- 并注销非主卡。卡转账要有转账记录
-- 并同时升级主卡密码为8位以上（添0处理）（已经是8位及以上的不用处理），
-- 比如：12345升级为12345000  
--           123456789不处理