-- 以下题目全部用事务实现：
-- 1.存钱（给定参数：银行账户，存钱金额）
--   要求：修改余额，添加存款记录

drop PROCEDURE if EXISTS `Mytransfer`;
delimiter $$
create PROCEDURE mytransfer(par_CardOut int,par_CardIn int,par_transferMoney
DECIMAL(10,2))
BEGIN
   
		
		START TRANSACTION;
		UPDATE bankcard set CardMoney=CardMoney+par_transferMoney
		where CardId=par_CardIn;
    INSERT into cardtransfer(CardIdOut,CardIdIn,TransferMoney,TransferTime)   
	  values(par_CardOut ,par_CardIn ,par_transferMoney,NOW());
	
END;
$$
delimiter ;

call Mytransfer(1,7,100);



-- 
-- 2.取钱（给定参数：银行账户，密码，取钱金额）
--   要求：1.添加触发器约束：余额不能小于0
--             2.校验密码是否正确，正确执行3，否则取钱失败
--             3.修改余额，添加取款记录
drop procedure if exists `pro_out`;
delimiter $$
create procedure pro_out(p_cardid int,p_cardpwd int,p_cardmoney decimal(10,2))
aa:begin
	declare a int default 0;
	-- 开启事务
	start TRANSACTION;
	
	-- 2.校验密码是否正确，正确执行3，否则取钱失败
	select count(*) into a from bankcard where CardId=p_cardid and CardPwd=p_cardpwd;
	if a<=0 || a>1 then
		select '回滚';
		rollback;
		leave aa;
	end if;
	update bankcard set CardMoney=CardMoney-p_cardmoney where CardId=p_cardid;
	if row_count()<=0 then
		select '回滚';
		rollback;
		leave aa;
	end if;
	insert into cardexchange(ExchangeId,CardId,MoneyInBank,MoneyOutBank,ExchangeTime) values(11,p_cardid,0,p_cardmoney,now());
	if row_count()<=0 then
		select '回滚';
		rollback;
		leave aa;
	else
		select '提交';
		commit;
	end if;
end;
$$
delimiter ;
call pro_out(2,100,200);

-- 3.卡片清理：    
-- 每个用户只能有一张卡
-- 每个用户以余额最高的卡为主卡，其他的卡余额全部转移到主卡上，并注销非主卡。卡转账要有转账记录
-- 并同时升级主卡密码为8位以上（添0处理）（已经是8位及以上的不用处理），
-- 比如：12345升级为12345000  
--           123456789不处理


