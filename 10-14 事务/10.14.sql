#1.存钱（给定参数：银行账户，存钱金额）
# 要求：修改余额，添加存款记录
drop PROCEDURE if EXISTS `mybank`;
delimiter $$
CREATE PROCEDURE mybank(par_CardId INT,par_MoneyInBnak DECIMAL(10,2))
BEGIN
		DECLARE num int DEFAULT 0;
		DECLARE CONTINUE HANDLER FOR SQLEXCEPTION set num =1;
# 开启事务
		START TRANSACTION;
#存钱
		UPDATE bankcard set CardMoney=CardMoney+par_MoneyInBnak where CardId=par_CardId;
# 添加转账记录
		INSERT INTO cardexchange(CardId,MoneyInBank,MoneyOutBank,ExchangeTime)
			VALUES(par_CardId,par_MoneyInBnak,0,NOW());
		
	if num = 1 then
		select '提交';
		COMMIT;
	end if;
END;
$$ 
delimiter ;

call mybank(3,200)
#2.取钱（给定参数：银行账户，密码，取钱金额）
#  要求：1.添加触发器约束：余额不能小于0
#           2.校验密码是否正确，正确执行3，否则取钱失败
#          3.修改余额，添加取款记录
drop TRIGGER IF EXISTS triAA;
CREATE TRIGGER triAA BEFORE UPDATE on bankcard for each ROW
BEGIN
	IF CardMoney<0 THEN
		SIGNAL SQLSTATE 'HY000' SET MESSAGE_TEXT ='余额不能小于0';
end if;
END

DROP PROCEDURE IF EXISTS `mybank2`;
delimiter $$
CREATE PROCEDURE mybank2(par_CardId int,par_CardPwd INT,par_MoneyOutBank DECIMAL(10,2))
BEGIN
	DECLARE mgs int DEFAULT 0;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION set mgs =1;
	#开启事务
	START TRANSACTION;

 #取钱
	UPDATE bankcard set CardMoney=CardMoney-par_MoneyOutBnak where CardId=par_CardId; 
# 添加转账信息
	insert INTO cardexchange(CardId,MoneyInBank,MoneyOutBank,ExchangeTime)
		VALUES(par_CardId ,0 ,par_MoneyOutBank,NOW());
IF par_CardPwd=CardPwd THEN
		IF mgs=1 THEN
				SELECT '钱不够';
				ROLLBACK;
			ELSE
			select '提交';
			COMMIT;
	END IF;
	ELSE 
		select '取钱失败';
end IF;
END
$$
delimiter ;

call mybank2(2,444444,200)











#3.卡片清理：    
#每个用户只能有一张卡
#每个用户以余额最高的卡为主卡，其他的卡余额全部转移到主卡上，并注销非主卡。卡转账要有转账记录
#并同时升级主卡密码为8位以上（添0处理）（已经是8位及以上的不用处理），
#比如：12345升级为12345000  
#         123456789不处理