-- 以下题目全部用事务实现：
-- 1.存钱（给定参数：银行账户，存钱金额）
 --  要求：修改余额，添加存款记录
DROP PROCEDURE
IF
	EXISTS `test1`;
CREATE PROCEDURE test1 ( par_CardId INT, par_CardMoney DECIMAL ( 10, 2 ) ) BEGIN#
START TRANSACTION;
UPDATE bankcard 
SET CardMoney = CardMoney + par_CardMoney 
WHERE
	CardId = par_CardId;
INSERT INTO cardexchange ( CardId, MoneyInBank, MoneyOutBank, ExchangeTime )
VALUES
	( par_CardId, par_CardMoney, 0, now( ) );
COMMIT;

END;
CALL test1 ( 1, 200 );
		 
-- 2.取钱（给定参数：银行账户，密码，取钱金额）
  -- 要求：1.添加触发器约束：余额不能小于0
           -- 2.校验密码是否正确，正确执行3，否则取钱失败
           --  3.修改余额，添加取款记录
DROP PROCEDURE
IF
	EXISTS `test2`;
CREATE PROCEDURE test2 ( par_CardId INT, par_CardPwd VARCHAR ( 30 ), par_MoneyOutBank DECIMAL ( 10, 2 ) ) BEGIN
DECLARE
	pwd VARCHAR ( 30 ) DEFAULT '';
SELECT
	CardPwd INTO pwd 
FROM
	bankcard 
WHERE
	CardId = par_CardId;
IF
	par_CardPwd = pwd THEN
	START TRANSACTION;
UPDATE bankcard 
SET CardMoney = CardMoney - par_MoneyOutBank 
WHERE
	CardId = par_CardId;
INSERT INTO cardexchange ( CardId, MoneyInBank, MoneyOutBank, ExchangeTime )
VALUES
	( par_CardId, 0, par_MoneyOutBank, now( ) );
COMMIT;
ELSE SELECT
	'取钱失败,密码错误';

END IF;

END;
CALL test2 ( 1, '123456', 200 );
DROP TRIGGER
IF
	EXISTS `test22`;
CREATE TRIGGER test22 BEFORE UPDATE ON bankcard FOR EACH ROW
BEGIN
IF
	new.CardMoney < 0 THEN
	SIGNAL SQLSTATE 'HY000' 
	SET MESSAGE_TEXT = '余额不能小于0';

END IF;
END;
		DROP TRIGGER
IF
	EXISTS `test31`;
CREATE TRIGGER test31 BEFORE UPDATE ON bankcard FOR EACH ROW
BEGIN
IF
	CHARACTER_LENGTH(old.CardPwd) <8 THEN
	SIGNAL SQLSTATE 'HY000' 
	SET MESSAGE_TEXT = '余额不能小于0';
  set new.CardPwd =CONCAT(CardPwd,'0','0');
END IF;
END;


					 
-- 3.卡片清理：    
-- 每个用户只能有一张卡
--            每个用户以余额最高的卡为主卡，其他的卡余额全部转移到主卡上，并注销非主卡。卡转账要有转账记录
-- 并同时升级主卡密码为8位以上（添0处理）（已经是8位及以上的不用处理），
-- 比如：12345升级为12345000  
          -- 123456789不处理
 drop procedure if exists `test3`;
 
 create  procedure test3()
  
begin 
  
	call test32();
	
	start TRANSACTION;
   
	 
	  delete from  bankcard where CardNo  not  in  
	(
	select CardNo from   (select CardNo from bankcard  GROUP BY AccountId  having max(CardMoney) )t
	);
	
COMMIT;

end ;

DROP PROCEDURE
IF
	EXISTS `test32`;
	delimiter $$
CREATE PROCEDURE test32 ( )
BEGIN

DECLARE
	par_AccountId INT default 0 ;
DECLARE
	par_CardNo VARCHAR ( 30 ) DEFAULT '';
DECLARE
	par_CardPwd VARCHAR ( 30 ) DEFAULT '';
DECLARE
	par_CardMoney DECIMAL ( 10, 2 ) DEFAULT 0;
	DECLARE flag int DEFAULT TRUE;
DECLARE
	myCard CURSOR FOR
	( SELECT
		AccountId,
		CardNo,
		CardPwd,
		sum( CardMoney ) 
		FROM
			bankcard 
		WHERE
			AccountId IN ( SELECT AccountId FROM bankcard GROUP BY AccountId HAVING max( CardMoney ) ) 
		GROUP BY
			AccountId 
		);
	DECLARE CONTINUE HANDLER for not found set flag=FALSE;
	
	open myCard;
	
	#使用游标	
	aa:WHILE TRUE DO
		FETCH myCard into par_AccountId,par_CardNo ,par_CardPwd,par_CardMoney; #flag设置为FLASE
		if flag=FALSE then
			LEAVE aa;
		end if;
	END WHILE;
	 close myCard;
					end 
					$$
					delimiter ;
					
		


select * from bankcard
select AccountId, CardNo,sum(CardMoney) from bankcard  where AccountId in (select AccountId from bankcard GROUP BY AccountId  having max(CardMoney) )    GROUP BY AccountId;

	 