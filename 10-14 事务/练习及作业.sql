-- 以下题目全部用事务实现：
-- 1.存钱（给定参数：银行账户，存钱金额）
--   要求：修改余额，添加存款记录
drop PROCEDURE IF EXISTS `Mycardexchange`;
create procedure Mycardexchange(par_CardIn int,par_transferMoney INT)
begin

-- #开启事务
start transaction;
update bankcard set CardMoney=Cardmoney+par_transferMoney where CardId=par_CardIn;
insert into cardtransfer(CardIdIn,TransferMoney,TransferTime)
values(par_CardIn,par_transferMoney,NOW());
end;

call Mycardexchange(1,100)


-- 2.取钱（给定参数：银行账户，密码，取钱金额）
--   要求：1.添加触发器约束：余额不能小于0
--             2.校验密码是否正确，正确执行3，否则取钱失败
--             3.修改余额，添加取款记录
drop trigger if exists `z`;
create trigger z after update on bankcard for each row
begin
	if old.CardMoney<0 then
		SIGNAL SQLSTATE 'HY000' set message_text ='余额不能小于0';
	end if;
end;


drop PROCEDURE if EXISTS `qu`;
CREATE PROCEDURE qu(par_zhanghu int ,par_password int, par_qmoney int)

aa:begin
	declare a int default 0;
	-- 开启事务
	start TRANSACTION;
	
	select count(*) into a from bankcard where CardId=par_zhanghu and CardPwd=par_password;
	if a<=0 || a>1 then
		select '回滚';
		rollback;
		leave aa;
	end if;
	
	-- 修改余额
	update bankcard set CardMoney=CardMoney-par_qmoney where CardId=par_zhanghu;
	if row_count()<=0 then
		select '回滚';
		rollback;
		leave aa;
	end if;
	
	-- 添加记录
	insert into cardexchange(ExchangeId,CardId,MoneyInBank,MoneyOutBank,ExchangeTime) values(11,par_zhanghu,0,par_qmoney,now());
	if row_count()<=0 then
		select '回滚';
		rollback;
		leave aa;
	else
		select '提交';
		commit;
	end if;
end;

call qu(2,4444,200);


-- 3.卡片清理：    
-- 每个用户只能有一张卡
-- 每个用户以余额最高的卡为主卡，其他的卡余额全部转移到主卡上，并注销非主卡。卡转账要有转账记录
-- 并同时升级主卡密码为8位以上（添0处理）（已经是8位及以上的不用处理），
-- 比如：12345升级为12345000  
--           123456789不处理
-- 
-- 
-- 