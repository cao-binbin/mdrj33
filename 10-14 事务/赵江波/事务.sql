-- 以下题目全部用事务实现：
-- 1.存钱（给定参数：银行账户，存钱金额）
--   要求：修改余额，添加存款记录
drop PROCEDURE if exists `text1`;
delimiter $$
create procedure text1(par_cardia int,par_money decimal(10,2))
aa:begin
    start TRANSACTION;

	update bankcard set CardMoney=CardMoney+par_money where CardId=par_cardia;
	if row_count()<=0 then
		select '回滚';
		rollback;
		leave aa;
	end if;
	
	insert into cardexchange(ExchangeId,CardId,MoneyInBank,MoneyOutBank,ExchangeTime) values(10,par_cardia,par_money,0,now());
	if row_count()<=0 then
		select '回滚';
		rollback;
		leave aa;
	ELSE
		select '提交';
		commit;
	end if;
end;
$$
delimiter ;
call text1(1,500);


-- 
-- 2.取钱（给定参数：银行账户，密码，取钱金额）
--   要求：1.添加触发器约束：余额不能小于0
--             2.校验密码是否正确，正确执行3，否则取钱失败s
--             3.修改余额，添加取款记录
						
drop trigger if exists `update_after_bankcard`;
create trigger update_after_bankcard after update on bankcard for each row
begin
	if old.CardMoney<0 then
		SIGNAL SQLSTATE 'HY000' set message_text ='余额不能小于0';
	end if;
end;

drop procedure if exists `text2`;
delimiter $$
create procedure text2(p_cardid int,p_cardpwd int,p_cardmoney decimal(10,2))
aa:begin
	declare a int default 0;

	start TRANSACTION;

	select count(*) into a from bankcard where CardId=p_cardid and CardPwd=p_cardpwd;
	if a<=0 || a>1 then
		select '回滚';
		rollback;
		leave aa;
	end if;

	update bankcard set CardMoney=CardMoney-p_cardmoney where CardId=p_cardid;
	if row_count()<=0 then
		select '回滚';
		rollback;
		leave aa;
	end if;
	

	insert into cardexchange(ExchangeId,CardId,MoneyInBank,MoneyOutBank,ExchangeTime) values(11,p_cardid,0,p_cardmoney,now());
	if row_count()<=0 then
		select '回滚';
		rollback;
		leave aa;
	else
		select '提交';
		commit;
	end if;
end;
$$
delimiter ;
call text2(1,123456,200);						
	
						
						
						
						
-- 3.卡片清理：    
-- 每个用户只能有一张卡
-- 每个用户以余额最高的卡为主卡，其他的卡余额全部转移到主卡上，并注销非主卡。卡转账要有转账记录
-- 并同时升级主卡密码为8位以上（添0处理）（已经是8位及以上的不用处理），
-- 比如：12345升级为12345000  
--           123456789不处理


