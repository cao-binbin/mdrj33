-- 以下题目全部用事务实现：
-- 1.存钱（给定参数：银行账户，存钱金额）
--   要求：修改余额，添加存款记录
drop PROCEDURE if EXISTS `affair_01`;
CREATE PROCEDURE affair_05(par_bankCardID VARCHAR(25),par_money DECIMAL(10,2))
BEGIN
			DECLARE harerr int DEFAULT 1;
			DECLARE num int DEFAULT 0;
			DECLARE CONTINUE HANDLER for SQLEXCEPTION set harerr = 0 ;
			#开始事物
			START TRANSACTION;
			#修改余额 
			UPDATE bankcard set CardMoney = CardMoney + par_money where CardNo=par_bankCardID;
			#添加存款记录
			SELECT CardId into num from bankcard where CardNo=par_bankCardID;
			INSERT into cardexchange(CardId,MoneyInBank,MoneyOutBank,ExchangeTime)
			VALUES(num,par_money,0,NOW());
			#提交事务
			IF harerr=0 THEN
			   SELECT '回滚';
				 ROLLBACK;
			ELSE
					SELECT '提交';
					COMMIT;			
		  End If;			
END;
CALL affair_05('6225098134285',100);


-- 2.取钱（给定参数：银行账户，密码，取钱金额）
--   要求：1.添加触发器约束：余额不能小于0
--             2.校验密码是否正确，正确执行，否则取钱失败
--             3.修改余额，添加取款记录

			#添加一个触发器 判断余额
CREATE TRIGGER UPDATE_bankcard_before before UPDATE on bankcard for each row
BEGIN
			if new.CardMoney <0 then
					SIGNAL SQLSTATE 'HY001' set MESSAGE_tEXT='余额不能为0';
			End if;
End;
show TRIGGERS;




drop PROCEDURE if EXISTS `affair_02`;
CREATE PROCEDURE affair_02(par_CardID int,par_PassWord VARCHAR(20),par_money DECIMAL(10,2))
aa:BEGIN
			#新建一个变量存储正确或者错误
			DECLARE harerr int DEFAULT 0;
			#寻找异常错误信息的操作
			DECLARE CONTINUE HANDLER for SQLEXCEPTION set harerr=1;
			#开始事务
			START TRANSACTION;
			
			#进行取钱的操作 满足两个条件 一个是账号id相同 并且密码正确
			UPDATE bankcard set CardMoney=CardMoney - par_money where CardId=par_CardID and
			CardPwd=par_PassWord;

			IF harerr=1 THEN
						SELECT '密码输入错误';
						ROLLBACK;
						LEAVE aa;
	    End If;

			#进行添加取款记录的信息
		INSERT into cardexchange(CardId,MoneyInBank,MoneyOutBank,ExchangeTime)
		VALUES(par_CardID,0,par_money,NOW());
		IF harerr=1 THEN
				SELECT '回滚';
				ROLLBACK;
		ELSE
				SELECT '取款成功';
				COMMIT;
		End If;
END;
			
CALL affair_02(1,'123',100);

#实现转账效果
CREATE PROCEDURE work33(par_cardIn int,par_cardOut int,money DECIMAL(10,2))
Begin
		#设置变量存储
		DECLARE harerr int DEFAULT 0;
		#设置异常信息判断
		DECLARE CONTINUE HANDLER for SQLEXCEPTION set harerr = 1;

		#开启事物 TRANSACTION
		START TRANSACTION;
		#实现转出的效果
		UPDATE bankcard set CardMoney=CardMoney-money where CardId=par_cardOut;
		#实现转入的效果
		UPDATE bankcard set CardMoney=CardMoney+money where CardId=par_cardIn;
		#实现添加数据的效果
		INSERT into cardtransfer(CardIdOut,CardIdIn,TransferMoney,TransferTime)
		VALUES(par_cardOut,par_cardIn,money,NOW());
		
		#如果出现错误 则回滚 没有则提交 
		IF harerr=1 Then
						SELECT '数据回滚';
						ROLLBACK;
				ELSE
						SELECT '提交';
						COMMIT;
		End IF;
END;

call work33(1,2,50);

