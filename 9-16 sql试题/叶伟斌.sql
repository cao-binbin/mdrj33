-- 14.查询各科成绩最高分、最低分和平均分,以如下形式显示：
-- 课程 ID，课程 name，最高分，最低分，平均分，及格率，中等率，优良率，优秀率
-- 及格为>=60，中等为：70-80，优良为：80-90，优秀为：>=90
-- 要求输出课程号和选修人数，查询结果按人数降序排列，若人数相同，按课程号升序
	####1.
	select *,
	(
		select max(score) from sc where sc.cid=course.cid
	)最高分,
	(
		select min(score) from sc where sc.cid=course.cid
	)最低分,
	(
		select avg(score) from sc where sc.cid=course.cid
	)平均分,
	(
		( select count(score) from sc where sc.cid=course.cid and score>=60 )-- 及格人数
		/
		( select count(score) from sc where sc.cid=course.cid) 
	)及格率,
	(
		( select count(score) from sc where sc.cid=course.cid and score>=70 and score<80 )-- 中等人数
		/
		( select count(score) from sc where sc.cid=course.cid)
	)中等率,-- 总人数	
	(
		( select count(score) from sc where sc.cid=course.cid and score>=80 and score<90 )
		/	
		( select count(score) from sc where sc.cid=course.cid)
	)优良率,
	(
		( select count(score) from sc where sc.cid=course.cid and score>=90  )
		/
		( select count(score) from sc where sc.cid=course.cid)
	)优秀率
	from course;
	
########2.
	#最高分，最低分，平均分，及格率，中等率，优良率，优秀率
	SELECT cid,max(score) 最高分,min(score)最低分,avg(score)平均分,
	(
		count(IF(score>=60,score,null))/count(*)
	)及格率,
	(
		count(IF(score>=70 and score<80 ,score,null))/count(*)
	)中等率,
	(
		count(IF(score>=80 and score<90,score,null))/count(*)
	)优良率,
	(
		count(IF(score>=90,score,null))/count(*)
	)优秀率
	FROM sc
	GROUP BY cid;

-- 15.按各科成绩进行排序，并显示排名， Score 重复时保留名次空缺
-- SQL SERVER  RANK()  
-- MYSQL 5.7 没有RANK  ;8.0才有RANK函数
select *,
(
	SELECT count(score)+1 from sc temp where temp.cid=sc.cid and temp.score>sc.score
)名次
from sc
order by cid asc,名次 asc;

-- 15.1 按各科成绩进行行排序，并显示排名， Score 重复时合并名次
SELECT sc1.sid,sc1.cid,sc1.score,count(DISTINCT sc2.score)+1 名次 from sc sc1
left join sc sc2 on sc1.sid != sc2.sid and sc1.cid=sc2.cid and sc2.score>sc1.score
GROUP BY sc1.sid,sc1.cid,sc1.score
order by sc1.cid asc,名次 asc;

select *,
(
	SELECT count(DISTINCT score)+1 from sc temp where temp.cid=sc.cid and temp.score>sc.score
)名次
from sc
order by cid asc,名次 asc;

-- 16.查询学生的总成绩，并进行排名，总分重复时保留名次空缺
-- select *,
-- (
-- 	select sum(score) from sc temp where temp.sid=student.sid 
-- )总成绩
-- from student

select *,
(
	SELECT count(总成绩)+1 from 
	(
		select *,
		(
			select IFNULL(sum(score),0) from sc temp where temp.sid=student.sid 
		)总成绩
		from student
	)	temp where temp.总成绩>t.总成绩
)名次
from 
	(
		select *,
		(
			select IFNULL(sum(score),0) from sc temp where temp.sid=student.sid 
		)总成绩
		from student
	) t
order by 总成绩 desc

-- 17. 统计各科成绩各分数段人数：课程编号，课程名称，[100-85]，[85-70]，[70-60]，[60-0] 及所占百分比
	SELECT cid,
	(
		SELECT cname from course where course.cid=sc.cid
	)课程名称,
	(
		CONCAT(ROUND(100*count(IF(score>=85,1,null))/count(*),2),'%') 
	)`[100-85]`,
	(
		count(IF(score>=70 && score<85,1,null))/count(*)
	)`[85-70]`,
	(
		count(IF(score>=60 && score<70,1,null))/count(*)
	)`[70-60]`,
	(
		count(IF(score<60,1,null))/count(*)
	)`[60-0]`
	from sc
	GROUP BY cid

-- mysql 8.0 才有rank 
-- 18.查询各科成绩前三名的记录
SELECT * from 
(
	select *,
	(
		SELECT count(score)+1 from sc temp where temp.cid=sc.cid and temp.score>sc.score
	)名次
	from sc
	order by cid asc,名次 asc
)temp 
where temp.名次<=3

-- 19.查询每门课程被选修的学生数
SELECT *,
(
	SELECT count(*) from sc WHERE sc.cid=course.cid
)数量
from course

-- 20.查询出只选修两门课程的学生学号和姓名
SELECT * from student
where sid in
(
	SELECT sid from sc
	GROUP BY sid
	HAVING count(*)=2
)

-- 21. 查询男生、女生人数
SELECT 
(
	select count(*) from student where ssex='男'
)男生人数,
(
	select count(*) from student where ssex='女'
)女生人数

-- 22. 查询名字中含有「风」字的学生信息
select * from student where sname like "%风"

-- 23查询同名同姓学生名单，并统计同名人数
select sname,count(*)人数 from student
GROUP BY sname
HAVING count(*)>1

-- 25.查询每门课程的平均成绩，结果按平均成绩降序排列，平均成绩相同时，按课程编号升序排列
SELECT cid,avg(score)平均成绩 FROM sc
GROUP BY cid
ORDER BY 平均成绩 desc,cid asc