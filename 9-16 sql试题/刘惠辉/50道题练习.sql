
-- 1.查询"01"课程比"02"课程成绩高的学生的信息及课程分数
select * from  student, (select * from sc where sc.cid=1 ) as a
,(select * from sc where sc.cid=2 ) as b 
where   student.sid=a.sid  and
a.sid =b.sid and a.cid='01'  and b.cid='02' 
and  a.score > b.score

   select score from sc where cid=1  GROUP BY sid
   select score from sc where cid=2  GROUP BY sid
-- 1.1 查询存在" 01 "课程但可能不存在" 02 "课程的情况(不存在时显示为 null )
  select * from (select * from sc where sc.cid=1 ) as a,(select * from sc where sc.cid=2 ) as b  where a.sid=b.sid 
	
	SELECT * FROM SC WHERE SC.SId NOT IN (SELECT SId FROM  SC WHERE SC.CId="01") AND SC.CId="02";

-- 1.2 查询同时存在01和02课程的情况
SELECT * FROM SC WHERE SC.SId  IN (SELECT SId FROM  SC WHERE SC.CId="01") AND SC.CId="02";

-- 1.3 查询选择了02课程但没有01课程的情况
SELECT * FROM SC WHERE SC.SId NOT IN (SELECT SId FROM  SC WHERE SC.CId="02") AND SC.CId="01";

-- 2.查询平均成绩大于等于 60 分的同学的学生编号和学生姓名和平均成绩
  select  student.sid, student.sname,  avg(score) from   student left  join sc  on student.sid= sc.sid  having  avg(score)>=60  
 

-- 3.查询在 SC 表存在成绩的学生信息
  select student.sid,sname,sage,ssex from student ,sc  where student.sid= sc.sid   and score is not null

-- 4.查询所有同学的学生编号、学生姓名、选课总数、所有课程的成绩总和
 select  student.sid, student.sname, COUNT(sc.cid)  , SUM(sc.score) from   student ,sc  where student.sid= sc.sid  GROUP BY student.sid
 
 

-- 5.查询「李」姓老师的数量
select COUNT(*) 数量  from teacher where tname like '李%%';

-- 6.查询学过「张三」老师授课的同学的信息
     select student.sid ,student.sname,student.ssex ,student.sage from student,course,teacher ,sc where teacher.tname="张三" and student.sid=sc.sid and sc.cid=course.cid and course.tid=teacher.tid

-- 7.查询没有学全所有课程的同学的信息
select student.sid ,student.sname,student.ssex ,student.sage from student,course,sc where  student.sid=sc.sid and sc.cid=course.cid and cid =;

-- 8.查询至少有一门课与学号为" 01 "的同学所学相同的同学的信息








-- 9.查询和" 01 "号的同学学习的课程完全相同的其他同学的信息
select * from sc ;
select  student.*
from student, sc
where student.sid=sc.sid
and student.sid!=01
group by sc.sid
having count(sc.cid)=3;

-- 10.查询没学过"张三"老师讲授的任一门课程的学生姓名


-- 11.查询两门及其以上不及格课程的同学的学号，姓名及其平均成绩


-- 12.检索" 01 "课程分数小于 60，按分数降序排列的学生信息
    

-- 13.按平均成绩从高到低显示所有学生的所有课程的成绩以及平均成绩
select sid ,avg(score) from sc  GROUP BY sid
select DISTINCT sc.sid,sc.cid,t 平均成绩 from sc inner join (select sid ,avg(score) as t from sc  GROUP BY sid)re on re.sid=sc.sid ORDER BY t desc


-- 14.查询各科成绩最高分、最低分和平均分,以如下形式显示：
-- 课程 ID，课程 name，最高分，最低分，平均分，及格率，中等率，优良率，优秀率
-- 及格为>=60，中等为：70-80，优良为：80-90，优秀为：>=90
-- 要求输出课程号和选修人数，查询结果按人数降序排列，若人数相同，按课程号升序
-- 排列
select sc.cid, course.cname,count(sc.sid) as '选修人数', max(sc.score), min(score), avg(score),
 (sum(case when score>=60 then 1 else 0 end)/count(sc.sid)) as '及格率',
 (sum(case when score>=70 and score<80 then 1 else 0 end) /count(sc.sid)) as '中等率',
 (sum(case when score>=80 and score<90 then 1 else 0 end)/count(sc.sid)) as '优良率',
 (sum(case when score>=90 then 1 else 0 end)/count(sc.sid)) as '优秀率'
 
from sc, course
where sc.cid=course.cid
group by sc.cid
order by count(sc.sid) desc;
#cid ; #按选修人数降序排列，如果人数相同，按课程号cid升序排列


-- 15.按各科成绩进行排序，并显示排名， Score 重复时保留名次空缺
select * from 
select  a.sid ,a.cid, a.score, count(a.score<b.score)+1  as rank
from sc a  left join sc b  on a.cid=b.cid  and a.score<b.score
group by a.cid, a.sid
order by a.cid, a.score desc;
-- 15.1 按各科成绩进行行排序，并显示排名， Score 重复时合并名次
select  a.sid ,a.cid, a.score, count( distinct b.score)+1 as rank
from sc a left join   sc b  on a.cid=b.cid  and a.score<b.score
group by a.cid, a.sid
order by a.cid, a.score desc;

-- 16.查询学生的总成绩，并进行排名，总分重复时保留名次空缺
select sid,sum(score) , from sc GROUP BY sid
select a.* ,count(a.cj<b.cj)+1 as rank
from 
   (select sid , sum(score)  as cj  from sc  group by sid) a
left join 
   ( select sid, sum(score) as cj from sc group by sid) b
on a.cj<b.cj
group by a.sid
order by a.cj desc
;

-- 17. 统计各科成绩各分数段人数：课程编号，课程名称，[100-85]，[85-70]，[70-60]，[60-0] 及所占百分比
select sc.cid,cname,count(sid) as '总人数',
   concat(round(sum(case when score>=0 and score<60 then 1 else 0 end) /count(sid)*100,2),'%') as '0-60',
   concat(round(sum(case when score>=60 and score<70 then 1 else 0 end)/count(sid)*100,2),"%")  as '60-70',
   concat(round(sum(case when score>=70 and score<85 then 1 else 0 end)/count(sid)*100,2),"%") as '70-85',
   concat(round(sum(case when score<=100 and score>=85 then 1 else 0 end )/count(*)*100,2),'%') as '85-100'
from sc join course on sc.cid=course.cid 
group by cid;

-- 18.查询各科成绩前三名的记录
select cid,score from  sc GROUP BY cid,sid
select * from (
select  a.sid ,a.cid, a.score, count(a.score<b.score)+1  as rank
from sc a  left join sc b  on a.cid=b.cid  and a.score<b.score
group by a.cid, a.sid
order by a.cid, a.score desc)c
where c.rank<=3;
-- 19.查询每门课程被选修的学生数
select cid from sc GROUP BY cid;
select count(*) from sc where cid=01;
select count(*) from sc where cid=02;
select count(*) from sc where cid=01;
select cid , COUNT(*) 人数 from sc  GROUP BY cid;
-- 20.查询出只选修两门课程的学生学号和姓名
select * from sc ;
select sc.sid, student.sname
from student, sc
where student.sid=sc.sid
group by sc.sid
having count(sc.cid)=2;
-- 21. 查询男生、女生人数
select * from student;
select ssex as '性别' , count(*) as '人数'
from student
group by ssex;

-- 22. 查询名字中含有「风」字的学生信息
select * from student where sname like  "%风%";

-- 23查询同名同性学生名单，并统计同名人数
select  sname ,COUNT(*) 人数  from student GROUP BY sname

-- 24.查询 1990 年出生的学生名单

select * from student having YEAR(sage)=1990
-- 25.查询每门课程的平均成绩，结果按平均成绩降序排列，平均成绩相同时，按课程编号升序排列
select cid, avg(score) as '平均成绩'
from sc
group by cid
order by avg(score) desc, cid;
 

-- 26.查询平均成绩大于等于 85 的所有学生的学号、姓名和平均成绩
     select student.sid,student.sname,rt from student
		 inner join (
		 select sid ,AVG(score) as rt from sc
		 GROUP BY sid
		 )ag on
		 student.sid=ag.sid
		 where rt >=85;
select student.sid, student.sname, a.pj
from student join 
(select sid, avg(score) as 'pj'
from sc
group by sid
having avg(score)>=85) a
on student.sid=a.sid ;

-- 27.查询课程名称为「数学」，且分数低于 60 的学生姓名和分数
select cid from course where  cname="数学";
select student.sname,score from student
inner join 
sc
on sc.sid=student.sid
where cid=
(select cid from course where  cname="数学")and score<60;
 
select student.sname, score
from student, sc, course
where student.sid=sc.sid and sc.cid=course.cid and score<60 and cname='math';
-- 28. 查询所有学生的课程及分数情况（存在学生没成绩，没选课的情况）
select  student.sid,student.sname, sc.cid,sc.score
from student left join sc on student.sid=sc.sid
order by student.sid;

-- 29.查询任何一门课程成绩在 70 分以上的姓名、课程名称和分数

select sname, cname ,score
from (sc join student on sc.sid=student.sid) join course on sc.cid=course.cid
where  score>70
order by sc.sid;

-- 30.查询不及格的课程
  select *from sc where score<60 GROUP BY cid,sid

-- 31.查询课程编号为 01 且课程成绩在 80 分以上的学生的学号和姓名
    select sid from sc where cid=01 and score>=80;
		select student.sid 学号 ,sname 姓名 from student inner join
		(select sid from sc where cid=01 and score>=80) s
	on s.sid=student.sid	
		;
		


-- 32.求每门课程的学生人数
 select cid,COUNT(*) 人数 from sc GROUP BY cid

-- 33.成绩不重复，查询选修「张三」老师所授课程的学生中，成绩最高的学生信息及其成绩


-- 34.成绩有重复的情况下，查询选修「张三」老师所授课程的学生中，成绩最高的学生

-- 35.查询不同课程成绩相同的学生的学生编号、课程编号、学生成绩
  select a.sid,a.cid,a.score from sc a,sc b where a.sid=b.sid and a.score=b.score
select    sc.sid, sc.cid , sc.score
from sc join  (select sid, score from sc  group by sid, score having count(cid)>=2) a 
        on sc.sid=a.sid and sc.score=a.score
order by sid;
-- 36. 查询每门成绩最好的前两名
select a.cid, a.sid, sname, a.score,  count(distinct b.score)+1 as 'rank'
from ( sc a left join sc b on a.cid=b.cid and a.score<b.score) join student on a.sid=student.sid

group by a.cid, a.sid
having rank<=2
order by cid
;


-- 37. 统计每门课程的学生选修人数（超过 5 人的课程才统计）

select sc.cid,cname,count(sid) as '选修人数'
from sc join course on sc.cid=course.cid
group by sc.cid
having count(sid)>4;



-- 38.检索至少选修两门课程的学生学号
select sid
from sc
group by sid
having count(cid)>=2;
 

-- 39.查询选修了全部课程的学生信息
select student.*
from sc join student on sc.sid=student.sid
group by sid 
having count(*) in (

select count(*)  from course);


-- 40.查询各学生的年龄，只按年份来算

select student.sid, student.sname,student.ssex, year(now())-year(student.sage)  as 'age'
from student;
-- 41. 按照出生日期来算，当前月日 < 出生年月的月日则，年龄减一
select * from student  
select student.sid, student.sname,student.ssex, year(now())-year(student.sage)  as 'age'
from student where date(now())<date(student.sage)  ;
select student.sid, student.sname,student.ssex, sage,
timestampdiff(year,sage,now()) as '按月日计算',  
year(now())-year(sage) as '按年份计算'  
from student;


-- 42.查询本周过生日的学生

select * 
from student 
where week(concat_ws('-',year(now()),date_format(sborn,'%m-%d')))=week(now());


-- 43. 查询下周过生日的学生
select * 
from student 
where week(concat_ws('-',year(now()),date_format(sborn,'%m-%d')))=week(now())+1;
 

-- 44.查询本月过生日的学生
select *
from student
where month(student.sage)=month(now());
 

-- 45.查询下月过生日的学生
select *
from student
where month(student.sage)=month(now())+1;
 