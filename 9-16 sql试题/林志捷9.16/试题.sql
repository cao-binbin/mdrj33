use student;

-- 1.查询"01"课程比"02"课程成绩高的学生的信息及课程分数

select * from(
select s.* 
,(select score from sc where cid='01' and sid=s.sid)as 01成绩
,(select score from sc where cid='02' and sid=s.sid)as 02成绩
from student as s)temp
where 01成绩>IFNULL(02成绩,0) -- IFNULL(被判断字段是否为空，为空换的值)

-- 1.1 查询存在" 01 "课程但可能不存在" 02 "课程的情况(不存在时显示为 null )

select sid,cid'01课程'
,(select cid from sc as s where cid='02' and s.sid=sc.sid)'02课程'
from sc where cid='01'

-- 1.2 查询同时存在01和02课程的情况

select s.*,s1.cid as 01课程,s2.cid as 02课程 from student as s
inner join sc as s1 on s1.sid=s.sid and s1.cid='01'
inner join sc as s2 on s2.sid=s.sid and s2.cid='02'

-- 1.3 查询选择了02课程但没有的情况

select * from(
select s.*
,(select cid from sc where cid='01' and sc.sid=s.sid)as 01课程
,(select cid from sc where cid='02' and sc.sid=s.sid)as 02课程
from student as s)temp
where 01课程 is null and 02课程 is not null

-- 2.查询平均成绩大于等于 60 分的同学的学生编号和学生姓名和平均成绩
 
 select sid as 学生编号
 ,(select sname from student where sid=sc.sid)as 学生姓名
 ,ROUND((score)) as 平均成绩 from sc
 group by sid
 having 平均成绩>60

-- 3.查询在 SC 表存在成绩的学生信息

select * from student where sid in(
select sid from sc)

-- 4.查询所有同学的学生编号、学生姓名、选课总数、所有课程的成绩总和

select s.sid,s.sname
,(select count(cid) from sc where sc.sid=s.sid group by sid)选课总数
,(select sum(score) from sc where sc.sid=s.sid group by sid)成绩总和
from student as s

-- 5.查询「李」姓老师的数量

select count(tid) '李姓老师的数量' from teacher where tname like '李%'

-- 6.查询学过「张三」老师授课的同学的信息

select * from student where sid in(
select sid from sc where cid in(
select cid from course where tid in(
select tid from teacher where tname='张三')))

-- 7.查询没有学全所有课程的同学的信息

select *,(select count(cid) from sc where sc.sid=student.sid) as 选课数量
from student where sid not in
(
select sid from sc
group by sid
having count(cid)=(select count(cid) from course as c)
)

select * from(
select *,
(select count(cid) from sc where sc.sid=student.sid)as 选课数量
from student)temp
where 选课数量<(select count(cid) from course as c)

-- 8.查询至少有一门课与学号为" 01 "的同学所学相同的同学的信息

	select * from student 
	where sid in(
		select distinct sid from sc
		where cid in(
			select cid from sc where sid='01'
		 )and sid!='01');
	
	select distinct student.* from student
	inner join sc on sc.sid=student.sid
	where cid in(
		select cid from sc where sid='01'
	)and student.sid!='01'

-- 9.查询和" 01 "号的同学学习的课程完全相同的其他同学的信息

	select s.* from sc
	inner join student as s on s.sid=sc.sid
	group by sc.sid
	having GROUP_CONCAT(sc.cid order by sc.cid)=(
				select GROUP_CONCAT(b.cid order by b.cid) from sc as b
				where sid='01'
				group by sid
				)
				
-- 10.查询没学过"张三"老师讲授的任一门课程的学生姓名

select sname from student where sid in(
select distinct sid from sc 
where cid in(
		select cid from course where tid=(
				select tid from teacher where tname='张三'
								)
						)
				)

-- 11.查询两门及其以上不及格课程的同学的学号，姓名及其平均成绩

select s.sid as 学号,s.sname as 姓名,temp.平均成绩 from student as s
inner join(
select sid,ROUND(avg(score),1) 平均成绩 from sc
where score<60
group by sid
having count(*)>=2)temp on temp.sid=s.sid

select s.sid as 学号,s.sname as 姓名,(select ROUND(avg(score),1) from sc where sc.sid=s.sid)as 平均成绩
from student as s
where sid in(
	select sid from sc where score<60
	group by sid
	having count(*)>=2)

-- 12.检索" 01 "课程分数小于 60，按分数降序排列的学生信息

select s.* from student as s
inner join(
select * from sc where cid='01' and score<60
)temp on temp.sid=s.sid
order by temp.score desc

-- 13.按平均成绩从高到低显示所有学生的所有课程的成绩以及平均成绩

select *
,(select ROUND(avg(score),1) from sc where sc.sid=a.sid group by sid)as 平均成绩
from sc as a
order by 平均成绩 desc

SELECT *,
	(SELECT score from sc where sc.sid=student.sid and cid='01')语文,
	(SELECT score from sc where sc.sid=student.sid and cid='02')数学,
	(SELECT score from sc where sc.sid=student.sid and cid='03')英语 ,
	(SELECT AVG(score) from sc where sc.sid=student.sid )平均分
  from student
	order by 平均分 desc

-- 14.查询各科成绩最高分、最低分和平均分,以如下形式显示：
-- 课程 ID，课程 name，最高分，最低分，平均分，及格率，中等率，优良率，优秀率
-- 及格为>=60，中等为：70-80，优良为：80-90，优秀为：>=90
-- 要求输出课程号和选修人数，查询结果按人数降序排列，若人数相同，按课程号升序
-- 排列

select sc.cid,course.cname,max(score) as 最高分
,min(score) as 最低分
,ROUND(avg(score),1) as 平均分
,sum(case when score>=60 then 1 else 0 end)
/count(distinct sid) as 及格率
,sum(case when score>=70 and score<80 then 1 else 0 end)
/count(distinct sid) as 中等率
,count(case when score>=80 and score<90 then 1 else null end)
/count(distinct sid) as 优良率
,count(case when score>=90 then 1 else null end)
/count(distinct sid) as 优秀率
from sc
join course on course.cid=sc.cid
group by cid

-- 15.按各科成绩进行排序，并显示排名， Score 重复时保留名次空缺 rank()

select * 
,(select count(sc.score)+1 from sc where sc.cid=a.cid and sc.score>a.score) as 排名
from sc as a
order by cid asc,排名 asc


-- 15.1 按各科成绩进行行排序，并显示排名， Score 重复z时合并名次

select *
,(select count(distinct sc.score)+1 from sc where sc.cid=a.cid and sc.score>a.score)as 排名
from sc as a
order by cid asc,排名 asc

-- 16.查询学生的总成绩，并进行排名，///总分重复时保留名次空缺

select *
,(
		select count(总成绩)+1 from
		(
			select *,(select IFNULL(sum(score),0) from sc temp where temp.sid=student.sid)总成绩
			from student
		)temp 
			where temp.总成绩>t.总成绩
	) as 排名
from(
select *,
(select IFNULL(sum(score),0) from sc where sc.sid=student.sid) as 总成绩
from student)t
order by 总成绩 desc

-- 16.1查询学生的总成绩，并进行排名，总分重复时不保留名次空缺

select *
,(
		select count(distinct 总成绩)+1 from  -- distinct 作用于是否重复扩值
		(
			select *,(select IFNULL(sum(score),0) from sc temp where temp.sid=student.sid)总成绩
			from student
		)temp 
			where temp.总成绩>t.总成绩
	) as 排名
from(
select *,
(select IFNULL(sum(score),0) from sc where sc.sid=student.sid) as 总成绩
from student)t
order by 总成绩 desc

-- 17. 统计各科成绩各分数段人数：课程编号，课程名称，[100-85]，[85-70]，[70-60]，[60-0] 及所占百分比

 select sc.cid as 课程编号
 ,(select cname from course where course.cid=sc.cid) as 课程名称
 ,CONCAT(round(100*sum(if(sc.score>=85,1,0))/count(*),2),'%') as '[100-85]'
 ,CONCAT(round(100*sum(if(sc.score>=70 and sc.score<85,1,0))/count(*),2),'%') as '[85-70]'
 ,CONCAT(round(100*sum(if(sc.score>=60 and sc.score<70,1,0))/count(*),2),'%') as '[70-60]'
 ,CONCAT(round(100*sum(if(sc.score<60,1,0))/count(*),2),'%') as '[60-0]'
 from sc
 group by cid

-- 18.查询各科成绩前三名的记录

select sc.* from sc
where (select count(*) from sc as b where b.cid=sc.cid and b.score>sc.score)<3
order by cid asc,score desc

select * from(
	select *
			,(select count(score)+1 from sc as b where b.cid=sc.cid and b.score>sc.score)as 名次
				from sc
	order by cid,score desc)temp
where 名次<=3

-- 19.查询每门课程被选修的学生数

select sid
,(select cname from course where course.cid=sc.cid) as 课程名称
,count(cid) as 选修的学生数
from sc
group by cid

-- 20.查询出只选修两门课程的学生学号和姓名

select sid,sname from student where sid in(
select sid from sc
group by sid
having count(cid)=2)

-- 21. 查询男生、女生人数

select *,count(ssex) as 男女生人数  from student
group by ssex

-- 22. 查询名字中含有「风」字的学生信息

select * from student as s where sname like '%风%'

-- 23查询同名同性学生名单，并统计同名人数

select *,count(sname) as 同名人数 from student
group by sname
having count(sname)>1

-- 24.查询 1990 年出生的学生名单

select * from student where year(sage)='1990'

-- 25.查询每门课程的平均成绩，结果按平均成绩降序排列，平均成绩相同时，按课程编号升序排列

select cid
,(select cname from course as b where b.cid=sc.cid)as 课程名称
,round(avg(score),1) as 平均成绩
from sc
group by cid
order by 平均成绩 desc,cid asc

-- 26.查询平均成绩大于等于 85 的所有学生的学号、姓名和平均成绩

select * from(
select s.sid,s.sname,(select round(avg(score)) from sc where sc.sid=s.sid)as 平均成绩
from student as s
)temp where temp.平均成绩>85

-- 27.查询课程名称为「数学」，且分数低于 60 的学生姓名和分数

select 
	(select sname from student as s where s.sid=sc.sid) as 学生姓名
	,score as 分数
from sc where score<60 and 
	cid=(
		select cid from course where cname='数学'
	)

