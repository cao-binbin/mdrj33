/*==============================================================*/
/* DBMS name:      Sybase SQL Anywhere 12                       */
/* Created on:     2021/9/8 11:25:10                            */
/*==============================================================*/


if exists(select 1 from sys.sysforeignkey where role='FK_专家信息表_REFERENCE_科室类别表') then
    alter table 专家信息表
       delete foreign key FK_专家信息表_REFERENCE_科室类别表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_出诊表_REFERENCE_科室类别表') then
    alter table 出诊表
       delete foreign key FK_出诊表_REFERENCE_科室类别表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_出诊表_REFERENCE_专家信息表') then
    alter table 出诊表
       delete foreign key FK_出诊表_REFERENCE_专家信息表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_预约表_REFERENCE_患者建档表') then
    alter table 预约表
       delete foreign key FK_预约表_REFERENCE_患者建档表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_预约表_REFERENCE_科室类别表') then
    alter table 预约表
       delete foreign key FK_预约表_REFERENCE_科室类别表
end if;

drop table if exists 专家信息表;

drop table if exists 出诊表;

drop table if exists 患者建档表;

drop table if exists 科室类别表;

drop table if exists 预约表;

/*==============================================================*/
/* Table: 专家信息表                                                 */
/*==============================================================*/
create table 专家信息表 
(
   id                   int                            not null,
   professor_name       varchar(50)                    null,
   photo                varchar(255)                   null,
   office_id            int                            null,
   office_name          varchar(50)                    null,
   job_name             varchar(50)                    null,
   skill                varchar(50)                    null,
   visit_time           datetime                       null,
   add_time             datetime                       null,
   constraint PK_专家信息表 primary key clustered (id)
);

/*==============================================================*/
/* Table: 出诊表                                                   */
/*==============================================================*/
create table 出诊表 
(
   id                   int                            not null,
   "time"               datetime                       null,
   office_id            int                            null,
   office_name          varchar(50)                    null,
   professor_id         int                            null,
   remaining_place      int                            null,
   constraint PK_出诊表 primary key clustered (id)
);

/*==============================================================*/
/* Table: 患者建档表                                                 */
/*==============================================================*/
create table 患者建档表 
(
   id                   int                            not null,
   name                 varchar(50)                    null,
   sex                  bit                            null,
   people_id            varchar(18)                    null,
   phone                varchar(11)                    null,
   pwd                  varchar(20)                    null,
   address              varchar(50)                    null,
   constraint PK_患者建档表 primary key clustered (id)
);

/*==============================================================*/
/* Table: 科室类别表                                                 */
/*==============================================================*/
create table 科室类别表 
(
   id                   char(10)                       not null,
   kind_name            char(10)                       null,
   constraint PK_科室类别表 primary key clustered (id)
);

/*==============================================================*/
/* Table: 预约表                                                   */
/*==============================================================*/
create table 预约表 
(
   id                   int                            not null,
   office_id            int                            null,
   office_name          varchar(50)                    null,
   professor_name       varchar(50)                    null,
   order_time           datetime                       null,
   user_id              int                            null,
   constraint PK_预约表 primary key clustered (id)
);

alter table 专家信息表
   add constraint FK_专家信息表_REFERENCE_科室类别表 foreign key (office_id)
      references 科室类别表 (id)
      on update restrict
      on delete restrict;

alter table 出诊表
   add constraint FK_出诊表_REFERENCE_科室类别表 foreign key (office_id)
      references 科室类别表 (id)
      on update restrict
      on delete restrict;

alter table 出诊表
   add constraint FK_出诊表_REFERENCE_专家信息表 foreign key (professor_id)
      references 专家信息表 (id)
      on update restrict
      on delete restrict;

alter table 预约表
   add constraint FK_预约表_REFERENCE_患者建档表 foreign key (user_id)
      references 患者建档表 (id)
      on update restrict
      on delete restrict;

alter table 预约表
   add constraint FK_预约表_REFERENCE_科室类别表 foreign key (office_id)
      references 科室类别表 (id)
      on update restrict
      on delete restrict;

