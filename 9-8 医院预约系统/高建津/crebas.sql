/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/8 11:28:25                            */
/*==============================================================*/


drop table if exists 专家表;

drop table if exists 坐诊表;

drop table if exists 用户表;

drop table if exists 科室表;

drop table if exists 预约表;

/*==============================================================*/
/* Table: 专家表                                                   */
/*==============================================================*/
create table 专家表
(
   expertID             int not null auto_increment,
   DepartmentID         int,
   name                 varchar(10),
   photo                binary(0),
   sex                  varchar(1),
   Speciality           text,
   primary key (expertID)
);

/*==============================================================*/
/* Table: 坐诊表                                                   */
/*==============================================================*/
create table 坐诊表
(
   OfADoxtorID          int not null,
   frequency            int,
   expertID             int,
   time                 date,
   bit                  char(10),
   primary key (OfADoxtorID)
);

/*==============================================================*/
/* Table: 用户表                                                   */
/*==============================================================*/
create table 用户表
(
   UserID               int not null auto_increment,
   name                 varchar(5),
   sex                  varchar(1),
   IDcard               varchar(18),
   phone                int,
   password             password,
   ConfirmPassword      password,
   address              varchar(255),
   primary key (UserID)
);

/*==============================================================*/
/* Table: 科室表                                                   */
/*==============================================================*/
create table 科室表
(
   DepartmentID         int not null,
   expertID             int,
   orderID              int,
   category             varchar(10),
   primary key (DepartmentID)
);

/*==============================================================*/
/* Table: 预约表                                                   */
/*==============================================================*/
create table 预约表
(
   orderID              int not null,
   UserID               int,
   Ordertime            date,
   DepartementID        int,
   primary key (orderID)
);

alter table 专家表 add constraint FK_Reference_5 foreign key (DepartmentID)
      references 科室表 (DepartmentID) on delete restrict on update restrict;

alter table 坐诊表 add constraint FK_Reference_7 foreign key (expertID)
      references 专家表 (expertID) on delete restrict on update restrict;

alter table 科室表 add constraint FK_Reference_3 foreign key (expertID)
      references 专家表 (expertID) on delete restrict on update restrict;

alter table 科室表 add constraint FK_Reference_4 foreign key (orderID)
      references 预约表 (orderID) on delete restrict on update restrict;

alter table 预约表 add constraint FK_Reference_6 foreign key (UserID)
      references 用户表 (UserID) on delete restrict on update restrict;

