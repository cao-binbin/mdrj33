/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/8 11:28:09                            */
/*==============================================================*/


drop table if exists 专家表;

drop table if exists 出诊表;

drop table if exists 挂号表;

drop table if exists 用户表;

drop table if exists 科室表;

/*==============================================================*/
/* Table: 专家表                                                   */
/*==============================================================*/
create table 专家表
(
   expertId             int not null auto_increment,
   name                 varchar(10),
   headPortrait         varchar(100),
   sex                  varchar(5),
   sectionID            int,
   post                 varchar(20),
   primary key (expertId)
);

/*==============================================================*/
/* Table: 出诊表                                                   */
/*==============================================================*/
create table 出诊表
(
   visiID               int not null auto_increment,
   visitTime            datetime,
   expertID             int,
   weekDay              tinyint,
   primary key (visiID)
);

/*==============================================================*/
/* Table: 挂号表                                                   */
/*==============================================================*/
create table 挂号表
(
   registrationID       int not null auto_increment,
   sitid                int,
   userID               int,
   orderDate            date,
   primary key (registrationID)
);

/*==============================================================*/
/* Table: 用户表                                                   */
/*==============================================================*/
create table 用户表
(
   userID               int not null auto_increment,
   userName             varchar(10),
   usersex              varchar(3),
   IDcard               varchar(18),
   mobile               varchar(11),
   password             varchar(20),
   confirmPassword      varchar(20),
   HA                   varchar(50),
   primary key (userID)
);

/*==============================================================*/
/* Table: 科室表                                                   */
/*==============================================================*/
create table 科室表
(
   科室id                 int not null auto_increment,
   科室名称                 varchar(30),
   primary key (科室id)
);

alter table 专家表 add constraint FK_Reference_1 foreign key (sectionID)
      references 科室表 (科室id) on delete restrict on update restrict;

alter table 出诊表 add constraint FK_Reference_2 foreign key (expertID)
      references 专家表 (expertId) on delete restrict on update restrict;

alter table 挂号表 add constraint FK_Reference_3 foreign key (sitid)
      references 出诊表 (visiID) on delete restrict on update restrict;

alter table 挂号表 add constraint FK_Reference_4 foreign key (userID)
      references 用户表 (userID) on delete restrict on update restrict;

