/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/8 19:51:04                            */
/*==============================================================*/


drop table if exists 专家表;

drop table if exists 坐诊室;

drop table if exists 用户表;

drop table if exists 科室类别表;

drop table if exists 预约表;

/*==============================================================*/
/* Table: 专家表                                                   */
/*==============================================================*/
create table 专家表
(
   id                   int not null,
   name                 varchar(10),
   sex                  varchar(2),
   phoneID              int(11),
   speciality           text,
   primary key (id)
);

/*==============================================================*/
/* Table: 坐诊室                                                   */
/*==============================================================*/
create table 坐诊室
(
   id                   int,
   userID               int,
   name                 varchar(10),
   time                 time
);

/*==============================================================*/
/* Table: 用户表                                                   */
/*==============================================================*/
create table 用户表
(
   id                   int not null,
   name                 varchar(10),
   sex                  varchar(2),
   age                  int(2),
   phone                int(11),
   site                 text,
   IDcard               varchar(18),
   primary key (id)
);

/*==============================================================*/
/* Table: 科室类别表                                                 */
/*==============================================================*/
create table 科室类别表
(
   id                   int not null,
   name                 varchar(10),
   primary key (id)
);

/*==============================================================*/
/* Table: 预约表                                                   */
/*==============================================================*/
create table 预约表
(
   id                   int not null,
   userID               int,
   doctorID             int,
   divisionID           int,
   divisionName         varchar(10),
   primary key (id)
);

alter table 专家表 add constraint FK_Reference_5 foreign key (phoneID)
      references 科室类别表 (id) on delete restrict on update restrict;

alter table 坐诊室 add constraint FK_Reference_1 foreign key (id)
      references 用户表 (id) on delete restrict on update restrict;

alter table 预约表 add constraint FK_Reference_2 foreign key (userID)
      references 用户表 (id) on delete restrict on update restrict;

alter table 预约表 add constraint FK_Reference_3 foreign key (doctorID)
      references 专家表 (id) on delete restrict on update restrict;

alter table 预约表 add constraint FK_Reference_4 foreign key (divisionID)
      references 科室类别表 (id) on delete restrict on update restrict;

