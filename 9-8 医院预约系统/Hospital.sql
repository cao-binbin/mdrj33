use lesson1

CREATE TABLE 科室表(
	Id int primary key auto_increment comment'科室编号',
	name varchar(30) comment'科室姓名',
	Type varchar(30) unique comment'科室类别'
);
CREATE TABLE 专家表(
		Id int primary key auto_increment comment'专家编号',
		departmentId int not null comment'部门编号',
		name varchar(30) not null comment'专家姓名',
		image varchar(20) not null comment'专家照片',
		sex char(1) not null comment'专家性别',
		job varchar(30) not null comment'专家科室',
		specialty text not null comment'特长描述',
		createTime datetime not null comment'出诊时间'
);
CREATE TABLE 用户表(
	Id int primary key auto_increment comment'用户id',
	name varchar(30) not null comment'用户名字',
	sex char(1) not null comment'用户性别',
	idCode varchar(18) not null comment'身份证',
	phone varchar(11) not null comment'电话',
	psd varchar(255) not null comment'密码',
	confirmPwd varchar(255) not null comment'确认密码',
	address varchar(255) not null comment'地址'
);
CREATE TABLE 坐诊表(
	Id int primary key auto_increment comment'坐诊编号',
	expertId int unique not null comment'专家id',
	weekDay TINYINT not null comment'坐诊时间',
	FOREIGN KEY (expertId) references 专家表(Id)
);
CREATE TABLE 预约表(
	Id int primary key auto_increment comment'预约编号',
	userId int not null comment'用户id',
	sitId int not null comment'坐诊id',
	orderDate date not null comment'预约时间',
	FOREIGN KEY (userId) references 用户表(Id),
	FOREIGN KEY (sitId) references 坐诊表(Id)
);
