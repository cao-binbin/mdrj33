/*==============================================================*/
/* DBMS name:      Sybase SQL Anywhere 12                       */
/* Created on:     2021/9/9 10:47:07                            */
/*==============================================================*/


if exists(select 1 from sys.sysforeignkey where role='FK_专家信息表_REFERENCE_科室表') then
    alter table 专家信息表
       delete foreign key FK_专家信息表_REFERENCE_科室表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_专家信息表_REFERENCE_出诊表') then
    alter table 专家信息表
       delete foreign key FK_专家信息表_REFERENCE_出诊表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_出诊表_REFERENCE_用户信息表') then
    alter table 出诊表
       delete foreign key FK_出诊表_REFERENCE_用户信息表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_预约表_REFERENCE_科室表') then
    alter table 预约表
       delete foreign key FK_预约表_REFERENCE_科室表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_预约表_REFERENCE_专家信息表') then
    alter table 预约表
       delete foreign key FK_预约表_REFERENCE_专家信息表
end if;

drop table if exists 专家信息表;

drop table if exists 出诊表;

drop table if exists 用户信息表;

drop table if exists 科室表;

drop table if exists 预约表;

/*==============================================================*/
/* Table: 专家信息表                                                 */
/*==============================================================*/
create table 专家信息表 
(
   专家id                 int                            not null,
   姓名                   varchar(20)                    null,
   头像照片                 varchar(20)                    null,
   性别                   varchar(1)                     null,
   科室id                 int                            null,
   出诊id                 int                            null,
   职务名称                 varchar(20)                    null,
   特长概述                 varchar(50)                    null,
   门诊id                 int                            null,
   constraint PK_专家信息表 primary key clustered (专家id)
);

/*==============================================================*/
/* Table: 出诊表                                                   */
/*==============================================================*/
create table 出诊表 
(
   出诊id                 int                            not null,
   出诊时间                 datetime                       null,
   用户id                 int                            null,
   constraint PK_出诊表 primary key clustered (出诊id)
);

/*==============================================================*/
/* Table: 用户信息表                                                 */
/*==============================================================*/
create table 用户信息表 
(
   用户id                 int                            not null,
   名称                   varchar(20)                    null,
   性别                   varchar(1)                     null,
   身份证号码                varchar(18)                    null,
   手机号码                 varchar(11)                    null,
   密码                   varchar(16)                    null,
   确认密码                 varchar(16)                    null,
   家庭住址                 varchar(50)                    null,
   constraint PK_用户信息表 primary key clustered (用户id)
);

/*==============================================================*/
/* Table: 科室表                                                   */
/*==============================================================*/
create table 科室表 
(
   科室id                 int                            not null,
   科室名称                 varchar(20)                    null,
   constraint PK_科室表 primary key clustered (科室id)
);

/*==============================================================*/
/* Table: 预约表                                                   */
/*==============================================================*/
create table 预约表 
(
   预约id                 int                            not null,
   时间                   datetime                       null,
   科室id                 int                            null,
   专家id                 int                            null,
   剩余名额                 int                            null,
   constraint PK_预约表 primary key clustered (预约id)
);

alter table 专家信息表
   add constraint FK_专家信息表_REFERENCE_科室表 foreign key (科室id)
      references 科室表 (科室id)
      on update restrict
      on delete restrict;

alter table 专家信息表
   add constraint FK_专家信息表_REFERENCE_出诊表 foreign key (出诊id)
      references 出诊表 (出诊id)
      on update restrict
      on delete restrict;

alter table 出诊表
   add constraint FK_出诊表_REFERENCE_用户信息表 foreign key (用户id)
      references 用户信息表 (用户id)
      on update restrict
      on delete restrict;

alter table 预约表
   add constraint FK_预约表_REFERENCE_科室表 foreign key (科室id)
      references 科室表 (科室id)
      on update restrict
      on delete restrict;

alter table 预约表
   add constraint FK_预约表_REFERENCE_专家信息表 foreign key (专家id)
      references 专家信息表 (专家id)
      on update restrict
      on delete restrict;

