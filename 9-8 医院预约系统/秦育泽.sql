/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/8 11:24:02                            */
/*==============================================================*/


drop table if exists Expertinformation;

drop table if exists ReservationRecord;

drop table if exists UserInfo;

drop table if exists departmentType;

drop table if exists visitApatientAtHome;

/*==============================================================*/
/* Table: Expertinformation                                     */
/*==============================================================*/
create table Expertinformation
(
   ID                   int not null,
   departmentID         int not null,
   name                 varchar(60),
   photo                varchar(255),
   jobTitle             varchar(60),
   speciality           text,
   department           varchar(60) not null,
   update               date,
   primary key (ID, departmentID)
);

/*==============================================================*/
/* Table: ReservationRecord                                     */
/*==============================================================*/
create table ReservationRecord
(
   ID                   int not null,
   UserID               int,
   sitID                int,
   Date_time            date,
   primary key (ID)
);

/*==============================================================*/
/* Table: UserInfo                                              */
/*==============================================================*/
create table UserInfo
(
   ID                   int not null auto_increment,
   name                 varchar(60),
   sex                  char(41),
   IDcode               varbinary(20),
   phone                varchar(11),
   password             varchar(20),
   adress               varchar(60),
   primary key (ID)
);

/*==============================================================*/
/* Table: departmentType                                        */
/*==============================================================*/
create table departmentType
(
   ID                   int not null,
   departmentName       varchar(60),
   departmentType       varchar(60),
   primary key (ID)
);

/*==============================================================*/
/* Table: visitApatientAtHome                                   */
/*==============================================================*/
create table visitApatientAtHome
(
   ID                   int not null,
   departmentID         int,
   weekDay              tinyint,
   primary key (ID)
);

alter table visitApatientAtHome comment '1---------星期一
2---------星期二
3---------星期三
';

alter table Expertinformation add constraint FK_Reference_3 foreign key (department)
      references departmentType (ID) on delete restrict on update restrict;

alter table ReservationRecord add constraint FK_Reference_4 foreign key (UserID)
      references UserInfo (ID) on delete restrict on update restrict;

alter table ReservationRecord add constraint FK_Reference_5 foreign key (sitID)
      references visitApatientAtHome (ID) on delete restrict on update restrict;

alter table visitApatientAtHome add constraint FK_Reference_2 foreign key (ID, departmentID)
      references Expertinformation (ID, departmentID) on delete restrict on update restrict;

