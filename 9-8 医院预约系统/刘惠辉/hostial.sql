﻿/*==============================================================*/
/* DBMS name:      Sybase SQL Anywhere 12                       */
/* Created on:     2021/9/8 18:26:48                            */
/*==============================================================*/


if exists(select 1 from sys.sysforeignkey where role='FK_专家表_REFERENCE_科室表') then
    alter table 专家表
       delete foreign key FK_专家表_REFERENCE_科室表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_用户表_REFERENCE_预约表') then
    alter table 用户表
       delete foreign key FK_用户表_REFERENCE_预约表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_科室表_REFERENCE_预约表') then
    alter table 科室表
       delete foreign key FK_科室表_REFERENCE_预约表
end if;

drop table if exists 专家表;

drop table if exists 用户表;

drop table if exists 科室表;

drop table if exists 预约表;

/*==============================================================*/
/* Table: 专家表                                                   */
/*==============================================================*/
create table 专家表 
(
   expert_id            int                            not null,
   department_id        int                            null,
   name                 varchar(10)                    null,
   sex                  char(1)                        null,
   expert_job           varchar(20)                    null,
   "time"               datetime                       null,
   week                 varchar(5)                     null,
   constraint PK_专家表 primary key clustered (expert_id)
);

/*==============================================================*/
/* Table: 用户表                                                   */
/*==============================================================*/
create table 用户表 
(
   user_id              int                            not null,
   appointment_id       int                            null,
   name                 varchar(10)                    null,
   sex                  char(1)                        null,
   password             varchar(10)                    null,
   IDcard               varchar(20)                    null,
   phone                varchar(13)                    null,
   address              varchar(30)                    null,
   constraint PK_用户表 primary key clustered (user_id)
);

/*==============================================================*/
/* Table: 科室表                                                   */
/*==============================================================*/
create table 科室表 
(
   department_id        int                            not null,
   appointment_id       int                            null,
   name                 varchar(10)                    null,
   expert_id            int                            null,
   constraint PK_科室表 primary key clustered (department_id)
);

/*==============================================================*/
/* Table: 预约表                                                   */
/*==============================================================*/
create table 预约表 
(
   appointment_id       int                            not null,
   department_id        int                            null,
   user_id              id                             null,
   datetime             datetime                       null,
   operation            varchar(0)                     null,
   constraint PK_预约表 primary key clustered (appointment_id)
);

alter table 专家表
   add constraint FK_专家表_REFERENCE_科室表 foreign key (department_id)
      references 科室表 (department_id)
      on update restrict
      on delete restrict;

alter table 用户表
   add constraint FK_用户表_REFERENCE_预约表 foreign key (appointment_id)
      references 预约表 (appointment_id)
      on update restrict
      on delete restrict;

alter table 科室表
   add constraint FK_科室表_REFERENCE_预约表 foreign key (appointment_id)
      references 预约表 (appointment_id)
      on update restrict
      on delete restrict;

