/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/8 11:24:46                            */
/*==============================================================*/


drop table if exists appointment;

drop table if exists department;

drop table if exists expert_system;

drop table if exists pathogeny;

drop index Index_1 on sickperson;

drop table if exists sickperson;

drop table if exists visitTime;

/*==============================================================*/
/* Table: appointment                                           */
/*==============================================================*/
create table appointment
(
   id                   int not null,
   patient_id           int,
   departmentId         int,
   department           varchar(10),
   specialist_id        int,
   dateTime             date,
   primary key (id)
);

/*==============================================================*/
/* Table: department                                            */
/*==============================================================*/
create table department
(
   id                   int not null auto_increment,
   name                 varchar(10),
   primary key (id)
);

/*==============================================================*/
/* Table: expert_system                                         */
/*==============================================================*/
create table expert_system
(
   id                   int not null,
   department_id        int not null,
   name                 varchar(20) not null,
   sex                  varchar(2),
   department           varchar(10),
   postName             varchar(10) not null,
   specialty            varchar(200),
   img                  varchar(500),
   primary key (id)
);

/*==============================================================*/
/* Table: pathogeny                                             */
/*==============================================================*/
create table pathogeny
(
   id                   int not null auto_increment,
   patient_id           int,
   specialist_id        int,
   result               varchar(200),
   time                 datetime,
   primary key (id)
);

/*==============================================================*/
/* Table: sickperson                                            */
/*==============================================================*/
create table sickperson
(
   id                   int not null,
   name                 varchar(20),
   sex                  varchar(2),
   cardId               varchar(18),
   phone                int,
   password             varchar(50),
   address              varchar(100),
   primary key (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on sickperson
(
   cardId
);

/*==============================================================*/
/* Table: visitTime                                             */
/*==============================================================*/
create table visitTime
(
   id                   int not null,
   specialist_id        int,
   time                 date,
   surplus              int,
   speciality           varchar(200),
   primary key (id)
);

alter table appointment add constraint FK_Reference_2 foreign key (specialist_id)
      references expert_system (id) on delete restrict on update restrict;

alter table appointment add constraint FK_Reference_3 foreign key (patient_id)
      references sickperson (id) on delete restrict on update restrict;

alter table appointment add constraint FK_Reference_5 foreign key (departmentId)
      references department (id) on delete restrict on update restrict;

alter table expert_system add constraint FK_Reference_1 foreign key (department_id)
      references department (id) on delete restrict on update restrict;

alter table expert_system add constraint FK_Reference_6 foreign key (id)
      references pathogeny (id) on delete restrict on update restrict;

alter table sickperson add constraint FK_Reference_7 foreign key (id)
      references pathogeny (id) on delete restrict on update restrict;

alter table visitTime add constraint FK_Reference_4 foreign key (specialist_id)
      references expert_system (id) on delete restrict on update restrict;

