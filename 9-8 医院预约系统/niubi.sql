/*
 Navicat Premium Data Transfer

 Source Server         : Zbc
 Source Server Type    : MySQL
 Source Server Version : 80023
 Source Host           : localhost:3306
 Source Schema         : hospital

 Target Server Type    : MySQL
 Target Server Version : 80023
 File Encoding         : 65001

 Date: 09/09/2021 09:45:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for anticipate
-- ----------------------------
DROP TABLE IF EXISTS `anticipate`;
CREATE TABLE `anticipate`  (
  `anId` int(0) NOT NULL AUTO_INCREMENT COMMENT '预约iD',
  `userId` int(0) NOT NULL COMMENT '预约用户',
  `visitId` int(0) NOT NULL COMMENT '预约信息',
  `createTime` datetime(0) NOT NULL COMMENT '预约时间',
  PRIMARY KEY (`anId`) USING BTREE,
  INDEX `userId`(`userId`) USING BTREE,
  INDEX `visitId`(`visitId`) USING BTREE,
  CONSTRAINT `anticipate_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `userinfo` (`userId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `anticipate_ibfk_2` FOREIGN KEY (`visitId`) REFERENCES `visit` (`visitId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department`  (
  `departId` int(0) NOT NULL AUTO_INCREMENT COMMENT '科室id',
  `deparName` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '科室名称',
  `deparBoss` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '科室负责人',
  PRIMARY KEY (`departId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for expert
-- ----------------------------
DROP TABLE IF EXISTS `expert`;
CREATE TABLE `expert`  (
  `expertId` int(0) NOT NULL AUTO_INCREMENT COMMENT '专家id',
  `experName` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '专家名称',
  `experImg` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '专家头像',
  `experSex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '专家性别',
  `deparId` int(0) NOT NULL COMMENT '科室',
  `post` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '职务',
  `Specialty` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '特长',
  `createTime` datetime(0) NOT NULL COMMENT '出诊时间',
  PRIMARY KEY (`expertId`) USING BTREE,
  INDEX `deparId`(`deparId`) USING BTREE,
  CONSTRAINT `expert_ibfk_1` FOREIGN KEY (`deparId`) REFERENCES `department` (`departId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for userinfo
-- ----------------------------
DROP TABLE IF EXISTS `userinfo`;
CREATE TABLE `userinfo`  (
  `userId` int(0) NOT NULL AUTO_INCREMENT COMMENT '用户Id',
  `userName` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户姓名',
  `userSex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户性别',
  `idCode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '身份证',
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '手机号码',
  `passwords` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '家庭地址',
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for visit
-- ----------------------------
DROP TABLE IF EXISTS `visit`;
CREATE TABLE `visit`  (
  `visitId` int(0) NOT NULL AUTO_INCREMENT COMMENT '出诊安排数',
  `expertId` int(0) NOT NULL COMMENT '专家ID',
  `Time` int(0) NOT NULL COMMENT '1 星期一上午 2星期一下午........',
  PRIMARY KEY (`visitId`) USING BTREE,
  INDEX `expertId`(`expertId`) USING BTREE,
  CONSTRAINT `visit_ibfk_1` FOREIGN KEY (`expertId`) REFERENCES `expert` (`expertId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
