/*==============================================================*/
/* DBMS name:      Sybase SQL Anywhere 12                       */
/* Created on:     2021/9/8 11:19:09                            */
/*==============================================================*/


if exists(select 1 from sys.sysforeignkey where role='FK_专家表_REFERENCE_科室表') then
    alter table 专家表
       delete foreign key FK_专家表_REFERENCE_科室表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_就诊值班表_REFERENCE_专家表') then
    alter table 就诊值班表
       delete foreign key FK_就诊值班表_REFERENCE_专家表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_预约表_REFERENCE_登录表') then
    alter table 预约表
       delete foreign key FK_预约表_REFERENCE_登录表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_预约表_REFERENCE_就诊值班表') then
    alter table 预约表
       delete foreign key FK_预约表_REFERENCE_就诊值班表
end if;

drop table if exists 专家表;

drop table if exists 就诊值班表;

drop table if exists 登录表;

drop table if exists 科室表;

drop table if exists 预约表;

/*==============================================================*/
/* Table: 专家表                                                   */
/*==============================================================*/
create table 专家表 
(
   id                   int                            not null,
   name                 varchar(30)                    null,
   sex                  char                           null,
   category             int                            null,
   adm                  varchar(30)                    null,
   duty                 varchar(30)                    null,
   speciality           varchar(60)                    null,
   constraint PK_专家表 primary key clustered (id)
);

/*==============================================================*/
/* Table: 就诊值班表                                                 */
/*==============================================================*/
create table 就诊值班表 
(
   id                   int                            not null,
   doctor               int                            null,
   "time"               datetime                       null,
   constraint PK_就诊值班表 primary key clustered (id)
);

/*==============================================================*/
/* Table: 登录表                                                   */
/*==============================================================*/
create table 登录表 
(
   id                   int                            not null,
   name                 varchar(30)                    null,
   sex                  char                           null,
   "ID number"          varchar(30)                    null,
   "phone number"       int                            null,
   password             varchar(30)                    null,
   "verify password"    varchar(30)                    null,
   site                 varchar(225)                   null,
   constraint PK_登录表 primary key clustered (id)
);

/*==============================================================*/
/* Table: 科室表                                                   */
/*==============================================================*/
create table 科室表 
(
   id                   int                            not null,
   category             varchar(30)                    null,
   constraint PK_科室表 primary key clustered (id)
);

/*==============================================================*/
/* Table: 预约表                                                   */
/*==============================================================*/
create table 预约表 
(
   id                   int                            not null,
   "user"               int                            null,
   doctor               int                            null,
   "time"               datetime                       null,
   constraint PK_预约表 primary key clustered (id)
);

alter table 专家表
   add constraint FK_专家表_REFERENCE_科室表 foreign key (category)
      references 科室表 (id)
      on update restrict
      on delete restrict;

alter table 就诊值班表
   add constraint FK_就诊值班表_REFERENCE_专家表 foreign key (doctor)
      references 专家表 (id)
      on update restrict
      on delete restrict;

alter table 预约表
   add constraint FK_预约表_REFERENCE_登录表 foreign key ("user")
      references 登录表 (id)
      on update restrict
      on delete restrict;

alter table 预约表
   add constraint FK_预约表_REFERENCE_就诊值班表 foreign key (doctor)
      references 就诊值班表 (id)
      on update restrict
      on delete restrict;

