/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/8 20:39:33                            */
/*==============================================================*/


drop table if exists class;

drop table if exists expert;

drop table if exists make;

drop table if exists usre;

/*==============================================================*/
/* Table: class                                                 */
/*==============================================================*/
create table class
(
   classID              int not null auto_increment,
   category             varchar(30),
   desk                 varchar(30),
   primary key (classID)
);

/*==============================================================*/
/* Table: expert                                                */
/*==============================================================*/
create table expert
(
   expertid             int not null auto_increment,
   classID              int,
   name                 varchar(30),
   image                varchar(255),
   sex                  char(1),
   category             varchar(30),
   desk                 varchar(30),
   postname             varchar(30),
   strong               text,
   visitdate            datetime,
   primary key (expertid)
);

/*==============================================================*/
/* Table: make                                                  */
/*==============================================================*/
create table make
(
   id                   int not null auto_increment,
   Uid                  int,
   sitid                int,
   orderDate            datetime,
   primary key (id)
);

/*==============================================================*/
/* Table: usre                                                  */
/*==============================================================*/
create table usre
(
   Uid                  int not null auto_increment,
   name                 varchar(30),
   sex                  char(1),
   identity             varchar(18),
   Column_5             int(11),
   password             varchar(16),
   confirm              varchar(16),
   site                 varchar(50),
   primary key (Uid)
);

alter table expert add constraint FK_Reference_1 foreign key (classID)
      references class (classID) on delete restrict on update restrict;

alter table make add constraint FK_Reference_2 foreign key (sitid)
      references expert (expertid) on delete restrict on update restrict;

alter table make add constraint FK_Reference_3 foreign key (Uid)
      references usre (Uid) on delete restrict on update restrict;

