/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/9 9:29:55                             */
/*==============================================================*/


drop table if exists appointment;

drop table if exists expert;

drop table if exists section;

drop table if exists user;

drop table if exists visit;

/*==============================================================*/
/* Table: appointment                                           */
/*==============================================================*/
create table appointment
(
   id                   int not null auto_increment,
   datetime             time,
   expertID             int not null,
   userID               int,
   primary key (id)
);

/*==============================================================*/
/* Table: expert                                                */
/*==============================================================*/
create table expert
(
   ID                   int not null auto_increment,
   Name                 varchar(25),
   sex                  bit(1),
   sectionclass         varchar(25),
   section              varchar(25),
   post                 varchar(25),
   time                 datetime,
   speciality           text,
   primary key (ID)
);

/*==============================================================*/
/* Table: section                                               */
/*==============================================================*/
create table section
(
   id                   int not null,
   visitid              int,
   primary key (id)
);

/*==============================================================*/
/* Table: user                                                  */
/*==============================================================*/
create table user
(
   ID                   int not null auto_increment,
   Tel                  int,
   password             varchar(25),
   name                 varchar(25),
   sex                  bit(1),
   "ID number"          int,
   address              varchar(25),
   primary key (ID)
);

/*==============================================================*/
/* Table: visit                                                 */
/*==============================================================*/
create table visit
(
   id                   int not null auto_increment,
   expertID             int,
   time                 bit(1),
   primary key (id)
);

alter table appointment add constraint FK_Reference_1 foreign key (expertID)
      references expert (ID) on delete restrict on update restrict;

alter table appointment add constraint FK_Reference_2 foreign key (userID)
      references user (ID) on delete restrict on update restrict;

alter table section add constraint FK_Reference_4 foreign key (visitid)
      references visit (id) on delete restrict on update restrict;

alter table visit add constraint FK_Reference_3 foreign key (expertID)
      references expert (ID) on delete restrict on update restrict;

