/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/8 11:04:46                            */
/*==============================================================*/


drop table if exists administrative;

drop table if exists specialist;

drop table if exists specialist2;

drop table if exists user;

/*==============================================================*/
/* Table: administrative                                        */
/*==============================================================*/
create table administrative
(
   id                   int not null,
   roomID               int,
   uid                  int,
   createTime           datetime,
   primary key (id)
);

/*==============================================================*/
/* Table: specialist                                            */
/*==============================================================*/
create table specialist
(
   id                   int not null auto_increment,
   name                 varchar(10) not null,
   image                varchar(255),
   sex                  char(1) not null,
   jopid                int,
   jodName              varchar(10),
   speciality           varchar(50),
   createTime           datetime,
   primary key (id)
);

/*==============================================================*/
/* Table: specialist2                                           */
/*==============================================================*/
create table specialist2
(
   id                   int not null,
   room                 varchar(20),
   sID                  int,
   oderDate             varchar(10),
   primary key (id)
);

/*==============================================================*/
/* Table: user                                                  */
/*==============================================================*/
create table user
(
   id                   int not null auto_increment,
   name                 varchar(10) not null,
   sex                  char(1),
   identity             varchar(11) not null,
   number               varbinary(20) not null,
   password             varchar(11) not null,
   isTrue               varchar(11) not null,
   adress               varchar(50) not null,
   primary key (id)
);

alter table administrative add constraint FK_Reference_2 foreign key (uid)
      references user (id) on delete restrict on update restrict;

alter table specialist2 add constraint FK_Reference_1 foreign key (sID)
      references specialist (id) on delete restrict on update restrict;

alter table specialist2 add constraint FK_Reference_3 foreign key (id)
      references administrative (id) on delete restrict on update restrict;

