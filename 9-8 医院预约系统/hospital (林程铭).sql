/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/8 11:19:44                            */
/*==============================================================*/


drop table if exists 专家信息表;

drop table if exists 出诊时间表;

drop table if exists 用户表;

drop table if exists 科室表;

drop table if exists 预约表;

/*==============================================================*/
/* Table: 专家信息表                                                 */
/*==============================================================*/
create table 专家信息表
(
   doctor_id            int not null,
   doctor_name          varchar(20),
   sex                  char(1),
   department_category  varchar(20),
   work_name            varchar(20),
   major                varchar(10),
   outcalls             datetime,
   primary key (doctor_id)
);

/*==============================================================*/
/* Table: 出诊时间表                                                 */
/*==============================================================*/
create table 出诊时间表
(
   outcalls_doctor_id   int not null auto_increment,
   outcalls_time        datetime,
   outcallls_department varchar(20),
   primary key (outcalls_doctor_id)
);

/*==============================================================*/
/* Table: 用户表                                                   */
/*==============================================================*/
create table 用户表
(
   user_id              int not null auto_increment,
   username             varchar(30),
   user_sex             char(1),
   IDcard               varchar(18),
   phone                varchar(11),
   password             varchar(11),
   address              text,
   primary key (user_id)
);

/*==============================================================*/
/* Table: 科室表                                                   */
/*==============================================================*/
create table 科室表
(
   department_id        int not null auto_increment,
   department_name      varchar(20),
   primary key (department_id)
);

/*==============================================================*/
/* Table: 预约表                                                   */
/*==============================================================*/
create table 预约表
(
   department_id        int not null auto_increment,
   department_category  varchar(20),
   doctor_name          varchar(20),
   appointment_time     datetime,
   primary key (department_id)
);

alter table 专家信息表 add constraint FK_Reference_5 foreign key (doctor_id)
      references 科室表 (department_id) on delete restrict on update restrict;

alter table 出诊时间表 add constraint FK_Reference_2 foreign key (outcalls_time)
      references 专家信息表 (doctor_id) on delete restrict on update restrict;

alter table 科室表 add constraint FK_Reference_1 foreign key (department_name)
      references 专家信息表 (doctor_id) on delete restrict on update restrict;

alter table 预约表 add constraint FK_Reference_3 foreign key (appointment_time)
      references 用户表 (user_id) on delete restrict on update restrict;

alter table 预约表 add constraint FK_Reference_4 foreign key (appointment_time)
      references 出诊时间表 (outcalls_doctor_id) on delete restrict on update restrict;

