/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/8 21:03:21                            */
/*==============================================================*/


drop index Index_1 on Department;

drop table if exists Department;

drop table if exists Racomt;

drop table if exists orderinfo;

drop index Index_1 on specialist;

drop table if exists specialist;

drop index Index_2 on userinfo;

drop index Index_1 on userinfo;

drop table if exists userinfo;

/*==============================================================*/
/* Table: Department                                            */
/*==============================================================*/
create table Department
(
   De_id                int not null auto_increment comment '科室类别序号',
   De_name              varchar(20) not null comment '科室类比名称',
   De_gotime            datetime not null comment '创建时间',
   De_endtime           datetime not null comment '更新时间',
   primary key (De_id)
);

alter table Department comment '科室类别表';

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on Department
(
   De_name
);

/*==============================================================*/
/* Table: Racomt                                                */
/*==============================================================*/
create table Racomt
(
   Re_id                int not null auto_increment comment '科室ID',
   Re_De_id             int not null comment '外键科室类别',
   Re_name              varchar(0) not null comment '科室名称',
   Re_gotime            datetime not null comment '创建时间',
   Re_endtime           datetime not null comment '更新时间',
   primary key (Re_id)
);

alter table Racomt comment '科室表';

/*==============================================================*/
/* Table: orderinfo                                             */
/*==============================================================*/
create table orderinfo
(
   Or_id                int not null auto_increment,
   Or_time              datetime not null comment '预约时间',
   Or_Us_id             int not null comment '用户信息',
   Or_Sp_id             int not null,
   Or_if_count          int not null comment '名额',
   primary key (Or_id)
);

alter table orderinfo comment '预约表';

/*==============================================================*/
/* Table: specialist                                            */
/*==============================================================*/
create table specialist
(
   Sp_id                int not null auto_increment comment '专家序号',
   Sp_name              varchar(10) not null comment '姓名',
   Sp_jpg               varchar(50) not null comment '链接图片',
   Sp_sex               char(1) not null comment '性别',
   Sp_De_id             int not null comment '科室类别',
   Sp_Re_id             int not null comment '科室',
   Sp_centename         varchar(0) not null comment '职务',
   Sp_stron             varchar(0) not null comment '特长',
   Sp_gotime            datetime not null comment '创建时间',
   SP_Visitime          date not null comment '出诊时间',
   primary key (Sp_id)
);

alter table specialist comment '专家信息表';

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on specialist
(
   Sp_jpg
);

/*==============================================================*/
/* Table: userinfo                                              */
/*==============================================================*/
create table userinfo
(
   Us_id                int not null auto_increment,
   Us_name              varchar(10) not null,
   Us_sex               char(1) not null,
   Us_ip                varchar(20) not null,
   Us_phone             varchar(11) not null,
   Us_password          varchar(30) not null,
   Us_home              varchar(50) not null,
   Us_gotime            char(10) not null,
   Us_endtime           char(10) not null,
   primary key (Us_id)
);

alter table userinfo comment '用户表';

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on userinfo
(
   Us_phone
);

/*==============================================================*/
/* Index: Index_2                                               */
/*==============================================================*/
create unique index Index_2 on userinfo
(
   Us_ip
);

alter table Racomt add constraint FK_Reference_2 foreign key (Re_De_id)
      references Department (De_id) on delete restrict on update restrict;

alter table orderinfo add constraint FK_Reference_4 foreign key (Or_Us_id)
      references userinfo (Us_id) on delete restrict on update restrict;

alter table orderinfo add constraint FK_Reference_6 foreign key (Or_Sp_id)
      references specialist (Sp_id) on delete restrict on update restrict;

alter table specialist add constraint FK_Reference_3 foreign key (Sp_De_id)
      references Department (De_id) on delete restrict on update restrict;

alter table specialist add constraint FK_Reference_5 foreign key (Sp_Re_id)
      references Racomt (Re_id) on delete restrict on update restrict;

