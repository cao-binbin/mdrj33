create database 电力系统表

use 电力系统表

create table 用户信息表
(
		用户ID int PRIMARY key identity,
		用户账号 VARCHAR(20) UNIQUE,用户ID int PRIMARY key identity,
		用户账号 VARCHAR(20) UNIQUE,
		用户密码 VARCHAR(20),
		用户姓名 VARCHAR(50),
		用户身份证 VARCHAR(20) UNIQUE,
		用户住址 VARCHAR(50),
		用户电话 VARCHAR(30),
		用户所用电力梯度 int,
		FOREIGN key (用户所用电力梯度) REFERENCES 电力梯度表(电力梯度),
		余额额度 DECIMAL(10,2),
		所属区域ID int,
		FOREIGN key (所属区域ID) REFERENCES 区域信息表(区域ID),
		所属维护人员ID int,
		FOREIGN key (所属维护人员ID) REFERENCES 维护人员信息表(维护人员ID),
		是否欠费 char(1),
		创建时间 datetime,
		更新时间 datetime
)

create table 用电分类表
(
		用户ID int,
		FOREIGN key (用户ID) REFERENCES 用户信息表(用户ID),
		家庭用店 char(1),
		大工业用电 char(1),
		商业用电 char(1)
)

create table 区域信息表
(
		区域ID int PRIMARY key identity,
		区域名称 VARCHAR(50),
		盈利状况 DECIMAL(20,2),
		区域用电量 int,
		区域负责人 VARCHAR(50)
)

create table 区域人员信息表
(
		区域ID int,
		FOREIGN key (区域信息表) REFERENCES 区域信息表(区域ID),
		负责人ID int,
		负责人电话 varchar(20)
)

create table 用户账单表
(
		账单号 int PRIMARY key identity,
		用户ID int,
		FOREIGN key (用户ID) REFERENCES 用户信息表(用户ID),
		用户使用电力梯度 int,
		FOREIGN (用户使用电力梯度) REFERENCES 电力梯度表(电力梯度),
		本月用电量 DECIMAL(20,2),
		本月充值电费 DECIMAL(20,2),
		本月剩余余额 DECIMAL(20,2)
)

create table 电力梯度表
(
		用户ID int,
		FOREIGN (用户ID) REFERENCES 用户信息表(用户ID),
		电力梯度 VARCHAR(10)
		用户密码 VARCHAR(20),
		用户姓名 VARCHAR(50),
		用户身份证 VARCHAR(20) UNIQUE,
		用户住址 VARCHAR(50),
		用户电话 VARCHAR(30),
		用户所用电力梯度 int,
		FOREIGN key (用户所用电力梯度) REFERENCES 电力梯度表(电力梯度),
		余额额度 DECIMAL(10,2),
		所属区域ID int,
		FOREIGN key (所属区域ID) REFERENCES 区域信息表(区域ID),
		所属维护人员ID int,
		FOREIGN key (所属维护人员ID) REFERENCES 维护人员信息表(维护人员ID),
		是否欠费 char(1),
		创建时间 datetime,
		更新时间 datetime
)

create table 用电分类表
(
		用户ID int,
		FOREIGN key (用户ID) REFERENCES 用户信息表(用户ID),
		家庭用店 char(1),
		大工业用电 char(1),
		商业用电 char(1)
)

create table 区域信息表
(
		区域ID int PRIMARY key identity,
		区域名称 VARCHAR(50),
		盈利状况 DECIMAL(20,2),
		区域用电量 int,
		区域负责人 VARCHAR(50)
)

create table 区域人员信息表
(
		区域ID int,
		FOREIGN key (区域信息表) REFERENCES 区域信息表(区域ID),
		负责人ID int,
		负责人电话 varchar(20)
)

create table 用户账单表
(
		账单号 int PRIMARY key identity,
		用户ID int,
		FOREIGN key (用户ID) REFERENCES 用户信息表(用户ID),
		用户使用电力梯度 int,
		FOREIGN (用户使用电力梯度) REFERENCES 电力梯度表(电力梯度),
		本月用电量 DECIMAL(20,2),
		本月充值电费 DECIMAL(20,2),
		本月剩余余额 DECIMAL(20,2)
)

create table 电力梯度表
(
		用户ID int,
		FOREIGN (用户ID) REFERENCES 用户信息表(用户ID),
		电力梯度 VARCHAR(10)
)

create table 维护人员信息表
(
		维护人员ID int PRIMARY key identity,
		维护人员姓名 VARCHAR(30),
		维护人员电话 VARCHAR(20),
		维护人员状况 char(1)
)

create table 维护信息表
(
		维护人员ID int,
		FOREIGN key (维护人员ID) REFERENCES 维护人员信息表(维护人员ID),
		电力设备维护次数 int,
		设备维护记录 VARCHAR(50)
)


