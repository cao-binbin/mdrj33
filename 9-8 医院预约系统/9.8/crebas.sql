/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/8 11:29:26                            */
/*==============================================================*/


drop table if exists 专家信息表;

drop table if exists 坐诊信息表;

drop index Index_1 on 患者信息表;

drop table if exists 患者信息表;

drop table if exists 科室类别表;

drop table if exists 预约信息表;

/*==============================================================*/
/* Table: 专家信息表                                                 */
/*==============================================================*/
create table 专家信息表
(
   ID                   int not null auto_increment,
   name                 varchar(10),
   picture              varchar(255),
   sex                  char(1),
   type                 varchar(30),
   section              varchar(20),
   speciality           text,
   Visittime            date,
   creationtime         datetime,
   primary key (ID)
);

/*==============================================================*/
/* Table: 坐诊信息表                                                 */
/*==============================================================*/
create table 坐诊信息表
(
   id                   int not null,
   name                 varchar(10),
   sex                  char(1),
   speciality           text,
   visit                date,
   places               int,
   primary key (id)
);

/*==============================================================*/
/* Table: 患者信息表                                                 */
/*==============================================================*/
create table 患者信息表
(
   id                   int not null auto_increment,
   name                 varchar(10),
   IDcard               varchar(18),
   phone                varchar(11),
   password             varchar(11),
   password1            varchar(11),
   site                 text,
   creationtime         datetime,
   primary key (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on 患者信息表
(
   IDcard
);

/*==============================================================*/
/* Table: 科室类别表                                                 */
/*==============================================================*/
create table 科室类别表
(
   ID                   int not null,
   designation          varchar(20),
   primary key (ID)
);

/*==============================================================*/
/* Table: 预约信息表                                                 */
/*==============================================================*/
create table 预约信息表
(
   id                   int not null,
   Expertsid            int,
   name                 varchar(10),
   category             varchar(30),
   Section              varchar(20),
   make                 date,
   creationtime         datetime,
   primary key (id)
);

alter table 坐诊信息表 add constraint FK_Reference_2 foreign key (id)
      references 专家信息表 (ID) on delete restrict on update restrict;

alter table 科室类别表 add constraint FK_Reference_1 foreign key (designation)
      references 专家信息表 (ID) on delete restrict on update restrict;

alter table 预约信息表 add constraint FK_Reference_3 foreign key (id)
      references 患者信息表 (id) on delete restrict on update restrict;

alter table 预约信息表 add constraint FK_Reference_4 foreign key (Expertsid)
      references 专家信息表 (ID) on delete restrict on update restrict;

