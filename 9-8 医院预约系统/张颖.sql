/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/8 20:20:33                            */
/*==============================================================*/


drop table if exists 专家表;

drop table if exists 专家预约表;

drop table if exists 病人信息表;

drop table if exists 病人预约表;

drop table if exists 科室类别表;

/*==============================================================*/
/* Table: 专家表                                                   */
/*==============================================================*/
create table 专家表
(
   id                   int not null,
   name                 varchar(20),
   sex                  varchar(2),
   officeType           varchar(20),
   office               varchar(10),
   dutyname             varchar(15),
   "strong point"       varchar(100),
   time                 datetime,
   primary key (id)
);

/*==============================================================*/
/* Table: 专家预约表                                                 */
/*==============================================================*/
create table 专家预约表
(
   id                   int not null,
   visitTime            date,
   primary key (id)
);

/*==============================================================*/
/* Table: 病人信息表                                                 */
/*==============================================================*/
create table 病人信息表
(
   id                   int not null,
   name                 varchar(30),
   sex                  varchar(2),
   IDnumber             varchar(30),
   phoneNumb            varchar(30),
   password             varchar(30),
   upPassword           varchar(30),
   "add"                varchar(30),
   primary key (id)
);

/*==============================================================*/
/* Table: 病人预约表                                                 */
/*==============================================================*/
create table 病人预约表
(
   ID                   int not null,
   zjID                 int,
   zjType               varchar(300),
   time                 datetime,
   primary key (ID)
);

/*==============================================================*/
/* Table: 科室类别表                                                 */
/*==============================================================*/
create table 科室类别表
(
   ID                   int not null,
   type                 varchar(10),
   name                 varchar(10),
   primary key (ID)
);

alter table 专家表 add constraint FK_Reference_2 foreign key (officeType)
      references 科室类别表 (ID) on delete restrict on update restrict;

alter table 专家预约表 add constraint FK_Reference_1 foreign key (id)
      references 专家表 (id) on delete restrict on update restrict;

alter table 病人预约表 add constraint FK_Reference_3 foreign key (ID)
      references 病人信息表 (id) on delete restrict on update restrict;

alter table 病人预约表 add constraint FK_Reference_4 foreign key (zjID)
      references 专家表 (id) on delete restrict on update restrict;

