/*==============================================================*/
/* DBMS name:      Sybase SQL Anywhere 12                       */
/* Created on:     2021/9/8 11:34:07                            */
/*==============================================================*/


if exists(select 1 from sys.sysforeignkey where role='FK_专家信息_REFERENCE_预约挂号') then
    alter table 专家信息
       delete foreign key FK_专家信息_REFERENCE_预约挂号
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_专家信息_REFERENCE_科室查询表') then
    alter table 专家信息
       delete foreign key FK_专家信息_REFERENCE_科室查询表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_专家信息_REFERENCE_用户表') then
    alter table 专家信息
       delete foreign key FK_专家信息_REFERENCE_用户表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_患者信息表_REFERENCE_预约挂号') then
    alter table 患者信息表
       delete foreign key FK_患者信息表_REFERENCE_预约挂号
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_患者信息表_REFERENCE_用户表') then
    alter table 患者信息表
       delete foreign key FK_患者信息表_REFERENCE_用户表
end if;

drop table if exists 专家信息;

drop table if exists 患者信息表;

drop table if exists 用户表;

drop table if exists 科室查询表;

drop table if exists 预约挂号;

/*==============================================================*/
/* Table: 专家信息                                                  */
/*==============================================================*/
create table 专家信息 
(
   zjId                 int                            not null,
   zjName               varchar(20)                    null,
   zjZp                 binary(30)                     null,
   zjXb                 varchar(20)                    null,
   zjKslb               varchar(20)                    null,
   zjKs                 varchar(20)                    null,
   zjZwmc               varchar(20)                    null,
   zjTcgs               varchar(30)                    null,
   zjCzsj               datetime                       null,
   yyID                 int                            null,
   constraint PK_专家信息 primary key clustered (zjId)
);

/*==============================================================*/
/* Table: 患者信息表                                                 */
/*==============================================================*/
create table 患者信息表 
(
   hzId                 int                            not null,
   hzSjh                int                            not null,
   hzMm                 int                            not null,
   zjid                 int                            not null,
   constraint PK_患者信息表 primary key clustered (hzId)
);

/*==============================================================*/
/* Table: 用户表                                                   */
/*==============================================================*/
create table 用户表 
(
   yhId                 int                            not null,
   yhName               varchar(30)                    null,
   yhsex                varchar(20)                    not null,
   yhdh                 int                            null,
   yhmm                 int                            null,
   zjId                 int                            null,
   hzId                 int                            null,
   constraint PK_用户表 primary key clustered (yhId)
);

/*==============================================================*/
/* Table: 科室查询表                                                 */
/*==============================================================*/
create table 科室查询表 
(
   lbId                 int                            not null,
   lbName               varchar(20)                    null,
   lbSex                varchar(20)                    null,
   lbKslb               varchar(20)                    null,
   lbKs                 varchar(20)                    null,
   lbZwmc               varchar(20)                    null,
   lbTjsj               datetime                       null,
   zjId                 int                            null,
   constraint PK_科室查询表 primary key clustered (lbId)
);

/*==============================================================*/
/* Table: 预约挂号                                                  */
/*==============================================================*/
create table 预约挂号 
(
   yyId                 int                            not null,
   yySj                 datetime                       null,
   yyKslb               varchar(30)                    null,
   yyKs                 varchar(30)                    null,
   constraint PK_预约挂号 primary key clustered (yyId)
);

alter table 专家信息
   add constraint FK_专家信息_REFERENCE_预约挂号 foreign key (zjId)
      references 预约挂号 (yyId)
      on update restrict
      on delete restrict;

alter table 专家信息
   add constraint FK_专家信息_REFERENCE_科室查询表 foreign key (yyID)
      references 科室查询表 (lbId)
      on update restrict
      on delete restrict;

alter table 专家信息
   add constraint FK_专家信息_REFERENCE_用户表 foreign key (zjId)
      references 用户表 (yhId)
      on update restrict
      on delete restrict;

alter table 患者信息表
   add constraint FK_患者信息表_REFERENCE_预约挂号 foreign key (hzId)
      references 预约挂号 (yyId)
      on update restrict
      on delete restrict;

alter table 患者信息表
   add constraint FK_患者信息表_REFERENCE_用户表 foreign key (hzId)
      references 用户表 (yhId)
      on update restrict
      on delete restrict;

