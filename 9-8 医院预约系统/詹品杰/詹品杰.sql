/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/8 23:08:35                            */
/*==============================================================*/


drop table if exists Booked_Table;

drop table if exists OfficeInfo;

drop table if exists Office_Type;

drop table if exists Staff_Name;

drop table if exists UserInfo;

drop table if exists doctorInfo;

/*==============================================================*/
/* Table: Booked_Table                                          */
/*==============================================================*/
create table Booked_Table
(
   Booked_ID            int not null auto_increment,
   Booked_User          int,
   Booked_WeekDay       varbinary(10),
   Booked_WeekdayTiem   datetime,
   Booked_Doctor        int,
   primary key (Booked_ID)
);

alter table Booked_Table comment 'Booked_Table';

/*==============================================================*/
/* Table: OfficeInfo                                            */
/*==============================================================*/
create table OfficeInfo
(
   Office_Id            int not null,
   Office_Name          varchar(10),
   primary key (Office_Id)
);

alter table OfficeInfo comment 'OfficeInfo';

/*==============================================================*/
/* Table: Office_Type                                           */
/*==============================================================*/
create table Office_Type
(
   OfficType_ID         int not null,
   officType_Name       varbinary(10),
   primary key (OfficType_ID)
);

alter table Office_Type comment 'Office_Type';

/*==============================================================*/
/* Table: Staff_Name                                            */
/*==============================================================*/
create table Staff_Name
(
   Staff_ID             int not null,
   Staff_Name           varbinary(10),
   primary key (Staff_ID)
);

alter table Staff_Name comment 'Staff_Name';

/*==============================================================*/
/* Table: UserInfo                                              */
/*==============================================================*/
create table UserInfo
(
   UserInfo_ID          int not null,
   UserInfo_Name        varbinary(10),
   UserInfo_Sex         varbinary(2),
   UserInfo_Age         int,
   UserInfo_TLP         varbinary(11) not null,
   UserInfo_identity    varbinary(18) not null,
   UserInfo_PassWord    varbinary(8),
   UserInfo_Address     varbinary(100),
   unique key AK_uq_TLP_identity (UserInfo_TLP, UserInfo_identity)
);

alter table UserInfo comment 'UserInfo';

/*==============================================================*/
/* Table: doctorInfo                                            */
/*==============================================================*/
create table doctorInfo
(
   Doctor_ID            int not null,
   Doctor_Name          varchar(1),
   Doctor_Sex           varchar(1),
   Doctor_Img           varchar(1),
   Doctor_office        int,
   Doctor_officeType    int,
   Doctor_staffName     int,
   Doctor_speciality    varchar(1),
   Doctor_Staff_TimeDay datetime,
   primary key (Doctor_ID)
);

alter table doctorInfo comment 'doctorInfo';

alter table OfficeInfo add constraint FK_Reference_3 foreign key (Office_Id)
      references doctorInfo (Doctor_ID) on delete restrict on update restrict;

alter table Office_Type add constraint FK_Reference_4 foreign key (OfficType_ID)
      references doctorInfo (Doctor_ID) on delete restrict on update restrict;

alter table Staff_Name add constraint FK_Reference_5 foreign key (Staff_ID)
      references doctorInfo (Doctor_ID) on delete restrict on update restrict;

alter table UserInfo add constraint FK_Reference_1 foreign key (UserInfo_ID)
      references Booked_Table (Booked_ID) on delete restrict on update restrict;

alter table doctorInfo add constraint FK_Reference_2 foreign key (Doctor_ID)
      references Booked_Table (Booked_ID) on delete restrict on update restrict;

