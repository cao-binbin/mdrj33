/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/8 21:03:24                            */
/*==============================================================*/


drop table if exists Table_5;

drop table if exists 专家表;

drop table if exists 坐诊表;

drop table if exists 科室表;

drop table if exists 预约表;

/*==============================================================*/
/* Table: Table_5                                               */
/*==============================================================*/
create table Table_5
(
   id                   int not null auto_increment,
   name                 varchar(10) binary not null,
   sex                  char(1) not null,
   i_code               varchar(20) binary not null,
   mobile               varchar(11) binary not null,
   password             varchar(255) binary not null,
   address              varchar(255) binary not null,
   primary key (id)
);

/*==============================================================*/
/* Table: 专家表                                                   */
/*==============================================================*/
create table 专家表
(
   id                   int not null,
   d_id                 int not null,
   name                 varchar(15) binary not null,
   sex                  char(1) binary not null,
   job                  varchar(25) binary not null,
   sty                  text,
   image                varchar(25) binary not null,
   ctime                datetime,
   primary key (id)
);

/*==============================================================*/
/* Table: 坐诊表                                                   */
/*==============================================================*/
create table 坐诊表
(
   id                   int not null,
   e_id                 int,
   w_day                tinyint,
   max_m                int,
   primary key (id)
);

/*==============================================================*/
/* Table: 科室表                                                   */
/*==============================================================*/
create table 科室表
(
   id                   int not null,
   type                 varchar(30) binary not null,
   name                 varchar(30) binary not null,
   primary key (id)
);

/*==============================================================*/
/* Table: 预约表                                                   */
/*==============================================================*/
create table 预约表
(
   id                   int not null,
   Tab_id               int,
   u_id                 int,
   s_id                 int,
   o_day                date,
   primary key (id)
);

alter table 专家表 add constraint FK_Reference_1 foreign key (id)
      references 科室表 (id) on delete restrict on update restrict;

alter table 坐诊表 add constraint FK_Reference_2 foreign key (id)
      references 专家表 (id) on delete restrict on update restrict;

alter table 预约表 add constraint FK_Reference_3 foreign key (id)
      references 坐诊表 (id) on delete restrict on update restrict;

alter table 预约表 add constraint FK_Reference_4 foreign key (Tab_id)
      references Table_5 (id) on delete restrict on update restrict;

