 ---绝对值
 select abs (-100);
 -- 取整
 select  CEIL(1.6);
 
 select CEIL(1.2);
 
 select ceiling(1.2);
 
 -- 取整
 select floor(1.6);
 
 -- 随机数
 select RAND(3)
 
 -- 四舍五入
 select round (1.6)
 
 select round (1.2)
 
 
 -- 长度length
 select LENGTH('iooo')
 
 -- 替换
 select REPLACE('abcf','a',1);
 
 -- 截取left
 select LEFT('123466789',5);
 
 -- 拼接concat
 select CONCAT('abd','ww')
 
-- 大小写
-- 大写转小写 lower
select LOWER('WEEE')

-- 小写转大写upper
select UPPER('www');

-- 逆序
select REVERSE('dfff555')


-- 去空格子trim
select TRIM('  ff  ');

-- ltrim
select LTRIM('  ddd')
-- rtrim
select rtrim ('deef   ')

