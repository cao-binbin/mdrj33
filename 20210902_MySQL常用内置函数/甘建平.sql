/*数值型函数*/
-- 绝对值
SELECT ABS(-5);

-- 向上取整
SELECT CEIL(3.2);

-- 向下取整
SELECT FLOOR(3.5);

-- 随机数
SELECT RAND();

-- 四舍五入
SELECT ROUND(5.6);


/*字符串函数*/
-- 长度
SELECT LENGTH('123abc奇偶');
SELECT CHAR_LENGTH('123abc奇偶');

-- 字符串替换
SELECT REPLACE( '123456','2','000');
SELECT INSERT('123456',2,3,'ABC');


-- 截取
SELECT LEFT('123456',3);
SELECT RIGHT('123456',3);
SELECT SUBSTRing('123456',-2,3);

-- 拼接
SELECT CONCAT('123','abc') ;


-- 逆序
SELECT REVERSE('123456');


-- 大小写转换
SELECT UPPER('abcDEF123');
SELECT LOWER('abcDEF123');

-- 去空格
SELECT TRIM('       123      ');
SELECT LTRIM('      123      ');
SELECT RTRIM('      123      ');


/*时间函数*/
-- 获取当前时间
SELECT NOW();
SELECT UNIX_TIMESTAMP('1970-05-24 10:30:21');
SELECT FROM_UNIXTIME(12364221);


-- 流程控制
SELECT *,IF(i!=0,'正常','异常')资金状态 from user;


 select *,
(
	CASE status
	WHEN 1 THEN'在线'
	WHEN 2 THEN'离线'
	WHEN 3 THEN'退出'
	ELSE
		'未知'
END
)
 from user;


SELECT *, IFNULL(i,0)IDCODE from user;



