/*
 Navicat Premium Data Transfer

 Source Server         : Zbc
 Source Server Type    : MySQL
 Source Server Version : 80023
 Source Host           : localhost:3306
 Source Schema         : shopdata

 Target Server Type    : MySQL
 Target Server Version : 80023
 File Encoding         : 65001

 Date: 07/09/2021 22:35:43
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for address
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address`  (
  `addressId` int(0) NOT NULL AUTO_INCREMENT COMMENT '地址id',
  `province` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '省份',
  `city` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '市区',
  PRIMARY KEY (`addressId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for aftertable
-- ----------------------------
DROP TABLE IF EXISTS `aftertable`;
CREATE TABLE `aftertable`  (
  `afterId` int(0) NOT NULL AUTO_INCREMENT COMMENT '售后数码',
  `noYesOut` bit(1) NOT NULL COMMENT '是否退还',
  `userId` int(0) NOT NULL COMMENT '用户Id',
  `userInfo` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户评价',
  PRIMARY KEY (`afterId`) USING BTREE,
  INDEX `userId`(`userId`) USING BTREE,
  CONSTRAINT `aftertable_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `usertable` (`userId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for commclass
-- ----------------------------
DROP TABLE IF EXISTS `commclass`;
CREATE TABLE `commclass`  (
  `commClassId` int(0) NOT NULL AUTO_INCREMENT COMMENT '商品类id',
  `commClassName` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '商品名称',
  `commClassInfo` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '商品类详情',
  PRIMARY KEY (`commClassId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for commhome
-- ----------------------------
DROP TABLE IF EXISTS `commhome`;
CREATE TABLE `commhome`  (
  `commHomeId` int(0) NOT NULL AUTO_INCREMENT COMMENT '商家id',
  `commHomeName` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '商家名称',
  `addressId` int(0) NOT NULL COMMENT '地址id',
  `commHomeInfo` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '商家详情',
  `recorId` int(0) NOT NULL COMMENT '记录Id',
  `afterId` int(0) NOT NULL COMMENT '售后id',
  PRIMARY KEY (`commHomeId`) USING BTREE,
  INDEX `recorId`(`recorId`) USING BTREE,
  INDEX `addressId`(`addressId`) USING BTREE,
  INDEX `afterId`(`afterId`) USING BTREE,
  CONSTRAINT `commhome_ibfk_1` FOREIGN KEY (`recorId`) REFERENCES `recor` (`recorId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `commhome_ibfk_2` FOREIGN KEY (`addressId`) REFERENCES `address` (`addressId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `commhome_ibfk_3` FOREIGN KEY (`afterId`) REFERENCES `aftertable` (`afterId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for commhomecomm
-- ----------------------------
DROP TABLE IF EXISTS `commhomecomm`;
CREATE TABLE `commhomecomm`  (
  `commNumComm` int(0) NOT NULL AUTO_INCREMENT COMMENT '条数',
  `commId` int(0) NOT NULL COMMENT '商品id',
  `commClass` int(0) NOT NULL COMMENT '商品类id',
  `commHomeId` int(0) NOT NULL COMMENT '商家id',
  `ooiID` int(0) NOT NULL COMMENT '上下线数码Id',
  PRIMARY KEY (`commNumComm`) USING BTREE,
  INDEX `commId`(`commId`) USING BTREE,
  INDEX `commClass`(`commClass`) USING BTREE,
  INDEX `commHomeId`(`commHomeId`) USING BTREE,
  INDEX `ooiID`(`ooiID`) USING BTREE,
  CONSTRAINT `commhomecomm_ibfk_1` FOREIGN KEY (`commId`) REFERENCES `commtable` (`commId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `commhomecomm_ibfk_2` FOREIGN KEY (`commClass`) REFERENCES `commclass` (`commClassId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `commhomecomm_ibfk_3` FOREIGN KEY (`commHomeId`) REFERENCES `commhome` (`commHomeId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `commhomecomm_ibfk_4` FOREIGN KEY (`ooiID`) REFERENCES `overoutinfo` (`ooiID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for commtable
-- ----------------------------
DROP TABLE IF EXISTS `commtable`;
CREATE TABLE `commtable`  (
  `commId` int(0) NOT NULL AUTO_INCREMENT COMMENT '商品id',
  `commClassId` int(0) NOT NULL COMMENT '商品类表',
  `commName` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '商品名称表',
  `commMoney` decimal(10, 2) NOT NULL COMMENT '商品金额',
  `commInfo` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '商品详细信息',
  PRIMARY KEY (`commId`) USING BTREE,
  INDEX `commClassId`(`commClassId`) USING BTREE,
  CONSTRAINT `commtable_ibfk_1` FOREIGN KEY (`commClassId`) REFERENCES `commclass` (`commClassId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dataortable
-- ----------------------------
DROP TABLE IF EXISTS `dataortable`;
CREATE TABLE `dataortable`  (
  `dateNum` int(0) NOT NULL AUTO_INCREMENT COMMENT '仓库进出数',
  `dataId` int(0) NOT NULL COMMENT '仓库id',
  `overDataTime` datetime(0) NOT NULL COMMENT '进库时间',
  `outDataTime` datetime(0) NOT NULL COMMENT '出库时间',
  `overNum` int(0) NOT NULL COMMENT '进库数量',
  `outNum` int(0) NOT NULL COMMENT '出库数量',
  PRIMARY KEY (`dateNum`) USING BTREE,
  INDEX `dataId`(`dataId`) USING BTREE,
  CONSTRAINT `dataortable_ibfk_1` FOREIGN KEY (`dataId`) REFERENCES `datatable` (`dataId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dataortablenum
-- ----------------------------
DROP TABLE IF EXISTS `dataortablenum`;
CREATE TABLE `dataortablenum`  (
  `dataOrId` int(0) NOT NULL AUTO_INCREMENT COMMENT '仓库进出号',
  `commClassId` int(0) NOT NULL COMMENT '商品类id',
  `commHomeId` int(0) NOT NULL COMMENT '商家id',
  `commId` int(0) NOT NULL COMMENT '商品id',
  PRIMARY KEY (`dataOrId`) USING BTREE,
  INDEX `commClassId`(`commClassId`) USING BTREE,
  INDEX `commId`(`commId`) USING BTREE,
  INDEX `commHomeId`(`commHomeId`) USING BTREE,
  CONSTRAINT `dataortablenum_ibfk_1` FOREIGN KEY (`commClassId`) REFERENCES `commclass` (`commClassId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dataortablenum_ibfk_2` FOREIGN KEY (`commId`) REFERENCES `commtable` (`commId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `dataortablenum_ibfk_3` FOREIGN KEY (`commHomeId`) REFERENCES `commhome` (`commHomeId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for datatable
-- ----------------------------
DROP TABLE IF EXISTS `datatable`;
CREATE TABLE `datatable`  (
  `dataId` int(0) NOT NULL COMMENT '仓库id',
  `dataName` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '仓库名称',
  `dateSum` int(0) NOT NULL COMMENT '仓库存储最大数量',
  `addressId` int(0) NOT NULL COMMENT '地址id',
  PRIMARY KEY (`dataId`) USING BTREE,
  INDEX `addressId`(`addressId`) USING BTREE,
  CONSTRAINT `datatable_ibfk_1` FOREIGN KEY (`addressId`) REFERENCES `address` (`addressId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department`  (
  `departemntId` int(0) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `departemntName` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '部门名称',
  `departemntManage` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '部门管理',
  PRIMARY KEY (`departemntId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for history
-- ----------------------------
DROP TABLE IF EXISTS `history`;
CREATE TABLE `history`  (
  `historyId` int(0) NOT NULL AUTO_INCREMENT COMMENT '历史数码',
  `userId` int(0) NOT NULL COMMENT '用户Id',
  `commHomeId` int(0) NOT NULL COMMENT '商家Id',
  `commId` int(0) NOT NULL COMMENT '商品Id',
  PRIMARY KEY (`historyId`) USING BTREE,
  INDEX `commHomeId`(`commHomeId`) USING BTREE,
  INDEX `userId`(`userId`) USING BTREE,
  INDEX `commId`(`commId`) USING BTREE,
  CONSTRAINT `history_ibfk_1` FOREIGN KEY (`commHomeId`) REFERENCES `commhome` (`commHomeId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `history_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `usertable` (`userId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `history_ibfk_3` FOREIGN KEY (`commId`) REFERENCES `commtable` (`commId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `orNums` int(0) NOT NULL AUTO_INCREMENT COMMENT '数码',
  `orderId` int(0) NOT NULL COMMENT '订单号',
  `usercommId` int(0) NOT NULL COMMENT '用户商品界面商品Id',
  `commHomeId` int(0) NOT NULL COMMENT '商家Id',
  `commNum` int(0) NOT NULL COMMENT '商品数量',
  `commSumMoney` decimal(10, 2) NOT NULL COMMENT '商品总金额',
  PRIMARY KEY (`orNums`) USING BTREE,
  INDEX `commHomeId`(`commHomeId`) USING BTREE,
  INDEX `orderId`(`orderId`) USING BTREE,
  CONSTRAINT `order_ibfk_1` FOREIGN KEY (`commHomeId`) REFERENCES `commhome` (`commHomeId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `order_ibfk_2` FOREIGN KEY (`orderId`) REFERENCES `orderid` (`orderId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for orderid
-- ----------------------------
DROP TABLE IF EXISTS `orderid`;
CREATE TABLE `orderid`  (
  `orderId` int(0) NOT NULL AUTO_INCREMENT COMMENT '订单号',
  `userId` int(0) NOT NULL COMMENT '用户id',
  `steamId` int(0) NOT NULL COMMENT '物流Id',
  `startTime` datetime(0) NOT NULL COMMENT '下单时间',
  PRIMARY KEY (`orderId`) USING BTREE,
  INDEX `userId`(`userId`) USING BTREE,
  INDEX `steamId`(`steamId`) USING BTREE,
  CONSTRAINT `orderid_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `usertable` (`userId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `orderid_ibfk_2` FOREIGN KEY (`steamId`) REFERENCES `stream` (`streamId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for overoutinfo
-- ----------------------------
DROP TABLE IF EXISTS `overoutinfo`;
CREATE TABLE `overoutinfo`  (
  `ooiID` int(0) NOT NULL AUTO_INCREMENT COMMENT '上下线数码Id',
  `ooInfo` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '商品状况',
  PRIMARY KEY (`ooiID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for person
-- ----------------------------
DROP TABLE IF EXISTS `person`;
CREATE TABLE `person`  (
  `perSonId` int(0) NOT NULL AUTO_INCREMENT COMMENT '人员id',
  `deparId` int(0) NOT NULL COMMENT '部门Id',
  `perSonName` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '员工姓名',
  `personInfo` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '员工信息',
  PRIMARY KEY (`perSonId`) USING BTREE,
  INDEX `deparId`(`deparId`) USING BTREE,
  CONSTRAINT `person_ibfk_1` FOREIGN KEY (`deparId`) REFERENCES `department` (`departemntId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for recor
-- ----------------------------
DROP TABLE IF EXISTS `recor`;
CREATE TABLE `recor`  (
  `recorId` int(0) NOT NULL AUTO_INCREMENT COMMENT '记录id',
  `recorName` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '记录',
  PRIMARY KEY (`recorId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for revenue
-- ----------------------------
DROP TABLE IF EXISTS `revenue`;
CREATE TABLE `revenue`  (
  `revenueId` int(0) NOT NULL AUTO_INCREMENT COMMENT '收入码数',
  `commHomeId` int(0) NOT NULL COMMENT '商家Id',
  `orderId` int(0) NOT NULL COMMENT '单号',
  `revenMoney` decimal(10, 2) NOT NULL COMMENT '收入金额',
  `revenTime` datetime(0) NOT NULL COMMENT '收入时间',
  PRIMARY KEY (`revenueId`) USING BTREE,
  INDEX `commHomeId`(`commHomeId`) USING BTREE,
  INDEX `orderId`(`orderId`) USING BTREE,
  CONSTRAINT `revenue_ibfk_1` FOREIGN KEY (`commHomeId`) REFERENCES `commhome` (`commHomeId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `revenue_ibfk_2` FOREIGN KEY (`orderId`) REFERENCES `orderid` (`orderId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for shopping
-- ----------------------------
DROP TABLE IF EXISTS `shopping`;
CREATE TABLE `shopping`  (
  `shopId` int(0) NOT NULL AUTO_INCREMENT COMMENT '购物车id',
  `userId` int(0) NOT NULL COMMENT '用户id',
  `commId` int(0) NOT NULL COMMENT '商品id',
  `commHomeId` int(0) NOT NULL COMMENT '商家id',
  `commNum` int(0) NOT NULL COMMENT '商品数量',
  PRIMARY KEY (`shopId`) USING BTREE,
  INDEX `commId`(`commId`) USING BTREE,
  INDEX `commHomeId`(`commHomeId`) USING BTREE,
  INDEX `userId`(`userId`) USING BTREE,
  CONSTRAINT `shopping_ibfk_1` FOREIGN KEY (`commId`) REFERENCES `commtable` (`commClassId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `shopping_ibfk_2` FOREIGN KEY (`commHomeId`) REFERENCES `commhome` (`commHomeId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `shopping_ibfk_3` FOREIGN KEY (`userId`) REFERENCES `usertable` (`userId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for stream
-- ----------------------------
DROP TABLE IF EXISTS `stream`;
CREATE TABLE `stream`  (
  `streamId` int(0) NOT NULL AUTO_INCREMENT COMMENT '物流Id',
  `steamName` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '物流公司名称',
  `toutsTime` datetime(0) NOT NULL COMMENT '揽件时间',
  `outDataTime` datetime(0) NOT NULL COMMENT '出库时间',
  `estiTime` datetime(0) NOT NULL COMMENT '预计到达时间',
  `addressId` int(0) NOT NULL COMMENT '地址Id',
  PRIMARY KEY (`streamId`) USING BTREE,
  INDEX `addressId`(`addressId`) USING BTREE,
  CONSTRAINT `stream_ibfk_1` FOREIGN KEY (`addressId`) REFERENCES `address` (`addressId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for token
-- ----------------------------
DROP TABLE IF EXISTS `token`;
CREATE TABLE `token`  (
  `toKenId` int(0) NOT NULL AUTO_INCREMENT COMMENT '代金卷Id',
  `toKenMoney` decimal(10, 2) NOT NULL COMMENT '代金卷金额',
  `minMoney` decimal(10, 2) NOT NULL COMMENT '最小使用金额',
  `commHomeId` int(0) NOT NULL COMMENT '商家id',
  PRIMARY KEY (`toKenId`) USING BTREE,
  INDEX `commHomeId`(`commHomeId`) USING BTREE,
  CONSTRAINT `token_ibfk_1` FOREIGN KEY (`commHomeId`) REFERENCES `commhome` (`commHomeId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for usercommtable
-- ----------------------------
DROP TABLE IF EXISTS `usercommtable`;
CREATE TABLE `usercommtable`  (
  `usercommId` int(0) NOT NULL AUTO_INCREMENT COMMENT '条目id',
  `commId` int(0) NOT NULL COMMENT '商品id',
  `commClassId` int(0) NOT NULL COMMENT '商品类表',
  `usercommName` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '商品名称表',
  `commHomeId` int(0) NOT NULL COMMENT '商家id',
  `usercommMoney` decimal(10, 2) NOT NULL COMMENT '商品金额',
  `usercommInfo` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '商品详细信息',
  PRIMARY KEY (`usercommId`) USING BTREE,
  INDEX `commClassId`(`commClassId`) USING BTREE,
  INDEX `commHomeId`(`commHomeId`) USING BTREE,
  INDEX `commId`(`commId`) USING BTREE,
  CONSTRAINT `usercommtable_ibfk_1` FOREIGN KEY (`commClassId`) REFERENCES `commclass` (`commClassId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `usercommtable_ibfk_2` FOREIGN KEY (`commHomeId`) REFERENCES `commhome` (`commHomeId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `usercommtable_ibfk_3` FOREIGN KEY (`commId`) REFERENCES `commtable` (`commId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for usertable
-- ----------------------------
DROP TABLE IF EXISTS `usertable`;
CREATE TABLE `usertable`  (
  `userId` int(0) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `userName` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户名称',
  `userPaw` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户密码',
  `userInfo` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户详情',
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
