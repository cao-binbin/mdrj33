-- 绝对值
select abs(-1);
select abs(-3.14);

-- 向上取整
select ceil(1.2);
select ceiling(2.1);

-- 向下取整
select floor(2.7);

-- 随机数
select rand();
select rand();

-- 四舍五入
select round(5.5);
select ROUND(5.555555,1);

-- 长度
select length('abcdefg')
select char_length('abc');

-- 字符串替换
select replace('111222333111222333','2','a');
select insert('111222333111222333','6','1','aaaaaa')

-- 截取
select left('123456789',6);
select right('123456789',3);
select substring('123456789',3,3);

-- 拼接
select concat('123','abc','456');

-- 大小写
select upper('ABCabc123');
select lower('ABCabc123');

-- 逆序
select reverse(reverse('123abc'));

-- 去空格
select trim(' 1 2 3 ');
select ltrim(' 1 2 3 ');
select rtrim(' 1 2 3 ');
select replace(' 1 2 3 ',' ','');

-- 时间函数
select now();
select UNIX_TIMESTAMP(now());
select FROM_UNIXTIME(1630572780);

-- 流程控制
drop database if exists shop;

create database shop;

use shop;

drop table if exists user;
create table user(
	money decimal(10,2)
);
insert into user(money) values (10);
insert into user(money) values (0);
insert into user(money) values (-10); 

select *,if (money>=0,'正常','异常')资金状态 from user;

select *,(
	CASE
			when money>=0
			then '正常'
			else '异常'
	END
)资金状态 from user;

select IFNULL(1,0);
insert into user(money) values (null); 
select *,ifnull(money,5)ifnull from user;