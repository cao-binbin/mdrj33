-- 1.输入教师名称，查询学过该教师课程的学生姓名
  -- 如果人数超过3人，则打印出大班；否则小班；

DROP PROCEDURE IF EXISTS `aa`;
delimiter $$
CREATE PROCEDURE aa(type VARCHAR(20))
BEGIN
DECLARE sum int DEFAULT 0;
set sum=
(
	SELECT * from student 
	INNER JOIN sc on sc.sid=student.sid
	inner JOIN course on course.cid=sc.cid
	where tid=(SELECT tid  from teacher where teacher.tname=type)
);
if sum>3 THEN
SELECT '大班';
ELSE 
SELECT '小班';
end if;
END
$$
delimiter;
call aa('张三')


-- 2.给定课程ID，如果优秀率大于优良率的，则打印学霸班；否则打印普通班
drop PROCEDURE if EXISTS `bb`;
delimiter $$
create PROCEDURE bb(num int )
BEGIN
	DECLARE yx DOUBLE DEFAULT 0;
	DECLARE yl DOUBLE DEFAULT 0;
	
	set yx= (select (SELECT count(*) from sc where sc.cid=course.cid and score>=90)
						/
					(SELECT count(*) from sc where sc.cid=course.cid )
					FROM course where cid=num);
					
	set yl= (select (SELECT count(*) from sc where sc.cid=course.cid and score>=80 and score<90)
						/
					(SELECT count(*) from sc where sc.cid=course.cid )
					FROM course where cid=num);
					
			if yx>yl then 
				select '学霸班';
			else 
				select '普通班';
			end if;
	
END
$$
delimiter;
CALL bb(4)

-- 3.给定一个值，计算阶乘  n! = n*(n-1)....*1

DROP PROCEDURE IF EXISTS `cc`;
delimiter $$
CREATE PROCEDURE cc(num int )
BEGIN 
DECLARE sum int DEFAULT 1;
	WHILE num>0 DO
		set sum = sum * num;
		set num = num-1;
	end WHILE;
SELECT sum;

END
$$
delimiter;

call cc(3);


-- 4.查询每年出生的学生信息，时间范围限制在85到89年、91到95年（要求单个循环处理）
DROP PROCEDURE IF EXISTS `dd`;
delimiter $$
CREATE PROCEDURE dd( )
begin
	DECLARE num int DEFAULT 1980;
	DECLARE sum int DEFAULT 1980;
	aa:WHILE TRUE DO
	if num>2000 then
		LEAVE aa;
		end if;
		
		if num<85 and num>89 then
			bb:BEGIN
				set num = num+1;
				ITERATE aa;		
			END;
		end if;
		
	  set sum = (SELECT year(sage) from student where year(sage) =num);
		set num = num +1;
	END WHILE;
	SELECT sum;
end
$$
delimiter;

call dd()










