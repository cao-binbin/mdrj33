#1.输入教师名称，查询学过该教师课程的学生姓名
 # 如果人数超过3人，则打印出大班；否则小班；

DROP PROCEDURE if EXISTS `teac`;
CREATE PROCEDURE teac(teacher VARCHAR(5))
BEGIN 
	DECLARE num int DEFAULT 0;
	start_:BEGIN 
	SELECT student.sname from student where sid in
	(
	select sid from sc where cid in
	(
	select cid from course where tid in 
	(
	select tid from teacher where teacher.tname='李四'
	)
	)
	);
	end;
	if_:BEGIN
		SELECT COUNT(*) into num from student where sid in
	(
	select sid from sc where cid in
	(
	select cid from course where tid in 
	(
	select tid from teacher where teacher.tname like teacher
	)
	)
	);

	CASE num
	when num<3 THEN
	select '小班';
	ELSE
	SELECT '大班';
 END CASE;
END;
end;


call teac('李四');

#2.给定课程ID，如果优秀率大于优良率的，则打印学霸班；否则打印普通班

drop PROCEDURE if EXISTS `course`;
CREATE PROCEDURE course(cid int)
BEGIN
	DECLARE num decimal(5,4) DEFAULT 0;
	declare a1 decimal(5,4) default 0;
	declare b1 decimal(5,4) default 0;
	if_: BEGIN
		select 
		(
			( select count(score) from sc where sc.cid=course.cid and score>=80 and score<90 )
			/
			( select count(score) from sc where sc.cid=course.cid)
		)aa,
		(

			( select count(score) from sc where sc.cid=course.cid and score>=90  )
			/
			( select count(score) from sc where sc.cid=course.cid)
		)bb
		into a1,b1
		from course where course.cid=cid;
		END;
		for_:BEGIN
			CASE 
		WHEN num=(a1-b1)>0 THEN
			select '学霸班';
		ELSE
			SELECT '普通班';
	END CASE;
	end;
end;


call course(1);

#3.给定一个值，计算阶乘  n! = n*(n-1)....*1
DROP PROCEDURE IF EXISTS `stratum`;
CREATE PROCEDURE stratum(num INT)
BEGIN
	DECLARE sum int DEFAULT 1;
	WHILE num>0 DO
	SET sum = sum*num;
	SET num = num-1;
	END WHILE;
	SELECT sum;
END

CALL stratum(5)
#4.查询每年出生的学生信息，时间范围限制在85到89年、91到95年（要求单个循环处理）


drop PROCEDURE if EXISTS `yearly`;
delimiter $$
create PROCEDURE yearly()
BEGIN
		DECLARE sum1 int DEFAULT 0;
		DECLARE sum2 int DEFAULT 0;
		select COUNT(*) into sum1 from student;
		aa:WHILE sum1>0 DO
		select year(sage) into sum2 from student where sid=sum1;
		IF 1985<=sum2<=1989 || 1991<=sum2<=1995 THEN
			select * from student where sid = sum1;
			set sum1=sum1-1;
ELSE
	set sum1=sum1-1;
	ITERATE aa;
END IF;
END WHILE;
end;
$$
delimiter ;
		
	
call yearly()










