#1.输入教师名称，查询学过该教师课程的学生姓名
#   如果人数超过3人，则打印出大班；否则小班；

drop procedure if exists class ;
delimiter // 
CREATE PROCEDURE class(t_name varchar(10))
BEGIN
	DECLARE num int DEFAULT 0;
	SET num = (select count(*) from student join sc on student.sid=sc.sid
	join course on sc.cid=course.cid
	join teacher on course.tid=teacher.tid
	where tname=t_name) ;	
	IF num>3 THEN
		select '大班' ;
	ELSE
		select '小班' ;
	END IF ;
	
END ;

//
delimiter ;
CALL class('张三')


# 2.给定课程ID，如果优秀率大于优良率的，
#   则打印学霸班；否则打印普通班


DROP PROCEDURE IF EXISTS `test1`;
delimiter // 
CREATE PROCEDURE test1(IN par_id INT)
BEGIN 
  -- 定义临时变量
	   /*临时变量区*/
		 DECLARE num1 DECIMAL(5,2);
		 DECLARE num2 FLOAT(5,2);
		 SELECT 
		((SELECT COUNT(score) FROM sc WHERE  sc.cid=course.cid and score>=90)/(SELECT COUNT(score) FROM sc WHERE sc.cid=course.cid))优秀率,
		((SELECT COUNT(score) FROM sc WHERE sc.cid=course.cid and score>=80 and score<90)/(SELECT COUNT(score) FROM sc WHERE sc.cid=course.cid))优良率
		INTO num1,num2
		FROM course WHERE cid=par_id;
		 
 IF num1>num2 THEN
	   SELECT '学霸班';
ELSE
	   SELECT '普通班';
END IF;

END;
//
delimiter ;
CALL test1(03);




# 3.给定一个值，计算阶乘  n! = n*(n-1)....*1

DROP PROCEDURE IF EXISTS `test2`;
delimiter //
CREATE PROCEDURE test2(type INT)
BEGIN
  DECLARE sum INT DEFAULT 1;
 WHILE type>0 DO
	  SET sum=sum*type;
	  SET type=type-1;
END WHILE;
  SELECT sum;
END;
//
delimiter //
CALL test2(5);





# 4.查询每年出生的学生信息，时间范围限制在85到89年、91到95年
# （要求单个循环处理）

drop procedure if exists `test3` ;
delimiter // 
create procedure test3()
begin

	declare sum int default 1985;
	
	aa:while sum>=1985 do
	select * from student where sage=sum;
	set sum=sum+1;
		if sum>1989 then
			leave aa;
		end if;
	end while;
	set sum=sum+1;
	
	bb:while sum>=1991 do
	select * from student where sage=sum;
	set sum=sum+1;
		if sum>1995 then
			leave bb;
		end if;
	end while;

end;
//
delimiter ;
call test3();