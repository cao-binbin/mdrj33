#1.1.输入教师名称，查询学过该教师课程的学生姓名
  #如果人数超过3人，则打印出大班；否则小班；
	drop procedure if exists `teacher`;
	delimiter $$
	create procedure teacher(tea varchar(10))
BEGIN 
	DECLARE num int DEFAULT 0;
	select count(*) into num from sc where cid=(
	select cid from course where tid=(
	select tid from teacher where tname like tea));
	IF num>3 THEN
		select '大班';
	ELSE
		select '小班';
	END IF;
END
	$$
	delimiter ;

call teacher('张三');





#2..给定课程ID，如果优秀率大于优良率的，则打印学霸班；否则打印普通班

drop procedure if exists `class`;
delimiter $$
create procedure `class`(id int)
BEGIN
	DECLARE num1 int DEFAULT 0;
	DECLARE num2 int DEFAULT 0;
	select (select count(*) from sc where cid=id and score >=60)/
	(select count(*) from sc where cid=id) into num1;
	select (select count(*) from sc where cid=id and score >=85)/
	(select count(*) from sc where cid=id) into num2;
	
	IF num2>num1 THEN
		select '学霸班';
	ELSE
		select '普通班';
	END IF;
END
$$
delimiter ;

call class(01);





#3.给定一个值，计算阶乘  n! = n*(n-1)....*1

drop PROCEDURE if EXISTS `cj`;

create PROCEDURE cj(num int)
BEGIN
	DECLARE sum int DEFAULT 1;
	WHILE num>0 DO
	  set sum = sum*num;
		set num = num -1;
	END WHILE;
	SELECT sum;
END;

call cj(5);


#4.查询每年出生的学生信息，时间范围限制在85到89年、91到95年（要求单个循环处理）

drop procedure if exists `dath`;
create procedure dath(yea int)
BEGIN
   if 1985<=yea and yea<=1989 then
	 select * from student where year(sage)=yea;
	 elseif 1991<=yea and yea<=1995 then
	 select * from student where year(sage)=yea;
	 else
	 select('不在范围内');
	 end if;
end;


   call dath(1986);







