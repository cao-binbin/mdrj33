# 过程：
# 1.输入教师名称，查询学过该教师课程的学生姓名
# 如果人数超过3人，则打印出大班；否则小班
drop procedure if exists `pro_name`
create procedure pro_name(ckname varchar(30))
begin
      declare num int default 0;
			declare num1 int default 0;
			declare num2 int default 0;
			select tid into num from teacher where tname=ckname;
			select cid into num1 from course where tid=num;
			select count(*) into num2 from sc where cid=num1;
			select (select sname from student where student.sid=sc.sid)as      学生姓名 from sc where cid=num1;
			if num2>3 then
			   select '大班';
		  else
				 select '小班';
		  end if;
end
set @name='王五';
call pro_name(@name);

#2.给定课程ID，如果优秀率大于优良率的，则打印学霸班；则打印普通班

drop procedure if exists `pro_ID`;
create procedure pro_ID(type varchar(30))
begin
	  declare num decimal(3,2) default 0;
	  declare num1 decimal(3,2) default 0;
	  select sum(IF (sc.score >= 70 AND sc.score < 85, 1, 0))/count(*) into num from sc where cid=type;
	  select SUM(IF(sc.score >= 85, 1, 0))/count(*) into num1 from sc where cid=type;
	  select num as 优良,num1 as 优秀;
	  if num<num1 then
		  select '学霸班';
	  else
		  select '普通班';
	  end if;
end;

call pro_ID('01');

#3.给定一个值，计算阶乘  n! = n*(n-1)....*1
drop procedure if exists `pro_n`;
create procedure pro_n(type int)
begin
	declare num int default 1;
	while type>0 DO
		set num=num*type;
		set type=type-1;
	end while;
	select num as 阶乘;
end;

call pro_n(5);

#4.查询每年出生的学生信息，时间范围限制在85到89年、91到95年（要求单个循环处理）
drop procedure if exists `pro_age`;
create procedure pro_age()
begin
	declare age int default 0;
	declare s_sid int default 1;
	declare cou int default 0;
	select count(*) into cou from student;
   aa:while s_sid<=cou DO
		select year(sage)%1900 into age from student where sid=s_sid;
		 if age>=85 and age<=95 then
				if age=90 then
					set s_sid=s_sid+1;
					ITERATE aa;
				end if;
				select * from student where sid=s_sid;
				set s_sid=s_sid+1;
		 end if; 
	end while;
end;

call pro_age();

