/*过程：
1.输入教师名称，查询学过该教师课程的学生姓名
  如果人数超过3人，则打印出大班；否则小班；
*/
drop PROCEDURE if EXISTS `testif`;
delimiter $$
CREATE PROCEDURE testif(type VARCHAR(10))
BEGIN
	DECLARE num int DEFAULT 0;
	select count(*) into num from sc where cid=(
	select cid from course where tid=(
	select tid from teacher where tname like type));
		IF num>3 THEN
			SELECT '大班';
		else
		 SELECT '小班';
		END IF;
END;
$$
delimiter ;
call testif('王五');


/*2.给定课程ID，如果优秀率大于优良率的，则打印学霸班；否则打印普通班*/
drop procedure if exists `testif1`;
delimiter $$
create procedure testif1(inid int)
begin
	DECLARE num1 int DEFAULT 0;
	DECLARE num2 int DEFAULT 0;
	select (select count(*) from sc where cid=inid and score >=60)/
	(select count(*) from sc where cid=inid) into num1;
	select (select count(*) from sc where cid=inid and score >=85)/
	(select count(*) from sc where cid=inid) into num2;
	
	IF num2>num1 THEN
		select '学霸班';
	ELSE
		select '普通班';
	END IF;
END
$$
delimiter ;

call testif1(90);


/*3.给定一个值，计算阶乘  n! = n*(n-1)....*1*/
drop PROCEDURE if EXISTS `testif2`;
create PROCEDURE testif2(num int)
BEGIN
	DECLARE sum int DEFAULT 1;
	WHILE num>0 DO
	  set sum = sum*num;
		set num = num -1;
	END WHILE;
	SELECT sum;
END;

call testif2(3);

/*4.查询每年出生的学生信息，时间范围限制在85到89年、91到95年（要求单个循环处理）*/


