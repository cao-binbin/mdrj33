
#1.输入教师名称，查询学过该教师课程的学生姓名
#如果人数超过3人，则打印出大班；否则小班；
DROP PROCEDURE IF exists `test1`;
CREATE PROCEDURE test1(IN par_name varchar(20))
BEGIN 
    -- 临时变量定义区
		/* 临时变量定义*/
		DECLARE num1 INT;
		DECLARE num2 INT;
		DECLARE type INT;
		
	SELECT tid into num1 FROM teacher WHERE tname=par_name;
	SELECT cid into num2 FROM course WHERE tid=num1;
	SELECT COUNT(*) into type FROM sc WHERE cid=num2;

	IF  type>3 THEN
	    SELECT '大班';
ELSE
		  SELECT '小班';
END IF;

END ;

CALL test1('王五');


 -- 2.给定课程ID，如果优秀率大于优良率的，则打印学霸班；否则打印普通班
DROP PROCEDURE IF EXISTS `test2`;
CREATE PROCEDURE test2(IN par_id INT)
BEGIN 
  -- 定义临时变量
	   /*临时变量区*/
		 DECLARE num1 DECIMAL(5,2);
		 DECLARE num2 FLOAT(5,2);
		 SELECT 
		((SELECT COUNT(score) FROM sc WHERE  sc.cid=course.cid and score>=90)/(SELECT COUNT(score) FROM sc WHERE sc.cid=course.cid))优秀率,
		((SELECT COUNT(score) FROM sc WHERE sc.cid=course.cid and score>=80 and score<90)/(SELECT COUNT(score) FROM sc WHERE sc.cid=course.cid))优良率
		INTO num1,num2
		FROM course WHERE cid=par_id;
		 
 IF num1>num2 THEN
	   SELECT '学霸班';
ELSE
	   SELECT '普通班';
END IF;

END;

CALL test2(03);





-- 3.给定一个值，计算阶乘  n! = n*(n-1)....*1
DROP PROCEDURE IF EXISTS `test3while`;
CREATE PROCEDURE test3while(type INT)
BEGIN
  DECLARE sum INT DEFAULT 1;
 WHILE type>0 DO
	  SET sum=sum*type;
	  SET type=type-1;
END WHILE;
  SELECT sum;
END;

CALL test3while(5);


-- 4.查询每年出生的学生信息，时间范围限制在85到89年、91到95年（要求单个循环处理）


     





