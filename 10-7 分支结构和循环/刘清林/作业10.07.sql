#1.输入教师名称，查询学过该教师课程的学生姓名
#如果人数超过3人，则打印出大班；否则小班；
drop PROCEDURE if exists `work1`;
delimiter $$
create PROCEDURE work1(in n VARCHAR(20))
start_begin:begin 
  declare sum int default 0;
		studentName_begin:begin
	select sname from student where sid in
	(
		select sid from sc where cid in
		(
			select cid from course where tid in 
			(
				select tid from teacher where tname like 'n'
			)
		)
	);
	end;
		big_small_class:BEGIN

		select count(*) into sum from student where sid in
			(
				select sid from sc where cid in
				(
					select cid from course where tid in 
					(
						select tid from teacher where tname like 'n'
					)
				)
			);
		if sum>3 THEN
			select '大班';
		ELSE
			select '小班';
		end if;
	end;
end;
$$
delimiter ;
call work1 ('张三');



#2.给定课程ID，如果优秀率大于优良率的，则打印学霸班；否则打印普通班
drop procedure if exists `work2`;
delimiter $$
create procedure work2(in num varchar(10))
begin
	declare sum1 decimal(5,4) default 0;
	declare sum2 decimal(5,4) default 0;
	#查询出优秀率和优良率
	percentage:begin
		select
		((select count(*) from sc where sc.cid=course.cid and sc.score>=90)/(select count(*) from sc where sc.cid=course.cid)) 优秀率,
		((select count(*) from sc where sc.cid=course.cid and sc.score>=80 and sc.score<90)/(select count(*) from sc where sc.cid=course.cid)) 优良率
		into sum1,sum2
		from course where cid = num;
	end;
	#比较两个值的大小	
	compare:begin
		if sum1>sum2 then
		  select '学霸班' 班级类型;
		else
		  select '普通班' 班级类型;
		end if;
	end;
end;
$$
delimiter ;


 #3给定一个值，计算阶乘  n! = n*(n-1)....*1
drop PROCEDURE if EXISTS `work3`;
 delimiter $$
create PROCEDURE work3(num int)
cc:BEGIN
	DECLARE sum int DEFAULT 0;
	if num<0 then 
		set num = -num;
	end if;
	aa:WHILE TRUE DO
		if num<0 then
			LEAVE aa;
		end if;
		if num=50 then
			bb:BEGIN
				set num = num-1;
				ITERATE aa;	
			END;
		end if;
	  set sum = sum+num;
		set num = num -1;
	END WHILE;
	SELECT sum;
END;
 $$
 delimiter ;
 
 
# 4.查询每年出生的学生信息，时间范围限制在85到89年、91到95年（要求单个循环处理）
 drop PROCEDURE if EXISTS `work4`;
delimiter $$
create procedure work4()
begin
	declare aa int default 0;
	declare bb int default 0;
	select count(*) into aa from student;
	zz:while aa>0 do
		select year(sage) into bb from student where sid=aa;
		IF bb>=1985 && bb<=1989 || bb>=1991 && bb<=1995 THEN
			select * from student where sid = aa;
			set aa=aa-1;
		ELSE
		set aa=aa-1;
			iterate zz;
		END IF;
	end while;
end;
$$ 
delimiter;
