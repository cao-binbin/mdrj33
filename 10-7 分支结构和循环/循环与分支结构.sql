1.输入教师名称，查询学过该教师课程的学生姓名
  如果人数超过3人，则打印出大班；否则小班；
drop procedure if exists `teacher`;
create procedure teacher(nam varchar(10))
begin
DECLARE sum int DEFAULT 0;
set sum=(select count(*) from student where sid in
(
select sid from sc where cid=
(
select cid from course where tid=
(
select tid from teacher where tname=nam
)
)
)
);
   if sum>3 THEN
      select '大班';
   else 
      select '小班';
end if;
end;
call teacher('张三');

2.给定课程ID，如果优秀率大于优良率的，则打印学霸班；否则打印普通班--

drop procedure if exists `aaaa`;
create procedure aaaa(type varchar(20))
begin
declare num1 decimal(3,2) default 0;
declare num2 decimal(3,2) default 0;
select sum(if (sc.score>=70 and sc.score<85,1,0))/count(*) into num1 from sc where cid=type;
select sum(if (sc.score>=85,1,0))/count(*) into num2 from sc where cid=type;
select num1 as 优良,num2 as 优秀;
if num1<num2 then 
     select '学霸班';
 else 
     select '普通班';
   end if;
end;
call aaaa('01');

3.给定一个值，计算阶乘  n! = n*(n-1)....*1
drop procedure if exists `bbbb`;
create procedure bbbb(type int)
begin 
   declare num int default 1;
	 while type>0 do 
	    set num=num*type;
			set type=type-1;
		end while;
		select num as 阶乘;
end;
	call bbbb(2);	
	 
4.查询每年出生的学生信息，时间范围限制在85到89年、91到95年（要求单个循环处理）
drop procedure if exists `dddd`;
create procedure dddd()
begin
	declare age int default 0;
	declare s_sid int default 1;
	declare cou int default 0;
	select count(*) into cou from student;
   aa:while s_sid<=cou DO
		select year(sage)%1900 into age from student where sid=s_sid;
		 if age>=85 and age<=95 then
				if age=90 then
					set s_sid=s_sid+1;
					ITERATE aa;
				end if;
				select * from student where sid=s_sid;
				set s_sid=s_sid+1;
		 end if; 
	end while;
end;
call dddd();




