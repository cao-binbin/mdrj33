#黄基洪
-- 1.查询"01"课程比"02"课程成绩高的学生的信息及课程分数
		

-- 1.1 查询存在" 01 "课程但可能不存在" 02 "课程的情况(不存在时显示为 null )
	SELECT * from 
	(	
		SELECT *,
		(
			SELECT score from sc where cid='01' and sid=student.sid
		)01课程成绩,
			(
			SELECT score from sc where cid='02' and sid=student.sid
		)02课程成绩
		from student 
	)temp where 01课程成绩>IFNULL(02课程成绩,0)
	;

-- 1.2 查询同时存在01和02课程的情况
	#1.关联
	SELECT student.*,sc1.score 01课程成绩,sc2.score 02课程成绩 from student
	inner join sc sc1 on  student.sid=sc1.sid and sc1.cid='01' 
	inner join sc sc2 on  student.sid=sc2.sid and sc2.cid='02' 
	where sc1.score>sc2.score
	;
	#2.子查询
	
	SELECT * from 
	(	
		SELECT *,
		(
			SELECT score from sc where cid='01' and sid=student.sid
		)01课程成绩,
			(
			SELECT score from sc where cid='02' and sid=student.sid
		)02课程成绩
		from student 
	)temp where 01课程成绩>02课程成绩
	;
	
-- 1.3 查询选择了02课程但没有01课程的情况
	SELECT * from 
	(	
		SELECT *,
		(
			SELECT score from sc where cid='01' and sid=student.sid
		)01课程成绩,
			(
			SELECT score from sc where cid='02' and sid=student.sid
		)02课程成绩
		from student 
	)temp where 02课程成绩 is not null and 01课程成绩 is null
	;


-- 2.查询平均成绩大于等于 60 分的同学的学生编号和学生姓名和平均成绩
	  SELECT sid,
		(
			select sname from student where student.sid=sc.sid
		)学生姓名,ROUND(AVG(score),2)平均成绩
		from sc
		GROUP BY sid
		HAVING AVG(score)>=60;


-- 3.查询在 SC 表存在成绩的学生信息
	SELECT * from student where sid in 
	(
		SELECT DISTINCT sid from sc
  )

-- 4.查询所有同学的学生编号、学生姓名、选课总数、所有课程的成绩总和

SELECT *,
(
	SELECT count(*) from sc where sid=student.sid
)选课总数,
(
	SELECT sum(score) from sc where sid=student.sid
)成绩总和
from student


-- 5.查询「李」姓老师的数量

-- 6.查询学过「张三」老师授课的同学的信息

SELECT * from student where sid in 
(
	select sid from sc where cid in
	(
		SELECT cid from course where tid=
		(
			SELECT tid from teacher where tname='张三'
		)
	)
)
	
-- 7.查询没有学全所有课程的同学的信息

SELECT * from 
(
	SELECT *,
	(
		SELECT count(*) from sc where sid=student.sid
	)选课总数
	from student
)temp
where 选课总数<3



-- 8.查询至少有一门课与学号为" 01 "的同学所学相同的同学的信息
	
	SELECT DISTINCT student.* from student 
	inner join sc on sc.sid=student.sid
	where cid in 
	(
		select cid from sc where sid='01'
	) and student.sid !='01'
	

	

	


#沈国鑫




-- 9.查询和" 01 "号的同学学习的课程完全相同的其他同学的信息

-- 1.课程数量相同 01 =》3，
-- 2.至少有一门课与学号为" 01 "的同学所学相同
-- 3.查找学了01同学没学过的课程
-- 
-- 语文 数学 英语 体育
-- 
-- 01：语文 数学 
-- 
-- 02：数学 体育


SELECT student.*,sc1.cid ,sc2.cid ,sc3.cid from student 
LEFT join sc sc1 on  student.sid=sc1.sid and sc1.cid='01' 
LEFT join sc sc2 on  student.sid=sc2.sid and sc2.cid='02' 
LEFT join sc sc3 on  student.sid=sc3.sid and sc3.cid='03' 
where student.sid !='02'
and IFNULL(sc1.cid,0) =  IFNULL((SELECT cid from sc where sid='02' and cid='01'),0)
and IFNULL(sc2.cid,0) =  IFNULL((SELECT cid from sc where sid='02' and cid='02'),0)
and IFNULL(sc3.cid,0)  =  IFNULL((SELECT cid from sc where sid='02' and cid='03'),0)


-- 10.查询没学过"张三"老师讲授的任一门课程的学生姓名


SELECT * from student where sid not in
(
	select sid from sc WHERE cid=
	(
		SELECT cid from course where tid=
		(
			SELECT tid from teacher where tname='张三'
		)
	)
)




	

-- 11.查询两门及其以上不及格课程的同学的学号，姓名及其平均成绩


-- 12.检索" 01 "课程分数小于 60，按分数降序排列的学生信息

-- 13.按平均成绩从高到低显示所有学生的所有课程的成绩以及平均成绩


-- 14.查询各科成绩最高分、最低分和平均分,以如下形式显示：
-- 课程 ID，课程 name，最高分，最低分，平均分，及格率，中等率，优良率，优秀率
-- 及格为>=60，中等为：70-80，优良为：80-90，优秀为：>=90
-- 要求输出课程号和选修人数，查询结果按人数降序排列，若人数相同，按课程号升序
-- 排列


-- 15.按各科成绩进行排序，并显示排名， Score 重复时保留名次空缺



-- 15.1 按各科成绩进行行排序，并显示排名， Score 重复时合并名次


-- 16.查询学生的总成绩，并进行排名，总分重复时保留名次空缺


-- 17. 统计各科成绩各分数段人数：课程编号，课程名称，[100-85]，[85-70]，[70-60]，[60-0] 及所占百分比




#张泊昌




-- mysql 8.0 才有rank 
-- 18.查询各科成绩前三名的记录


-- 19.查询每门课程被选修的学生数


-- 20.查询出只选修两门课程的学生学号和姓名

-- 21. 查询男生、女生人数


-- 22. 查询名字中含有「风」字的学生信息


-- 23查询同名同性学生名单，并统计同名人数


-- 24.查询 1990 年出生的学生名单


-- 25.查询每门课程的平均成绩，结果按平均成绩降序排列，平均成绩相同时，按课程编号升序排列


-- 26.查询平均成绩大于等于 85 的所有学生的学号、姓名和平均成绩


-- 27.查询课程名称为「数学」，且分数低于 60 的学生姓名和分数



#林程铭





-- 28. 查询所有学生的课程及分数情况（存在学生没成绩，没选课的情况）


-- 29.查询任何一门课程成绩在 70 分以上的姓名、课程名称和分数


-- 30.查询不及格的课程


-- 31.查询课程编号为 01 且课程成绩在 80 分以上的学生的学号和姓名


-- 32.求每门课程的学生人数


-- 33.成绩不重复，查询选修「张三」老师所授课程的学生中，成绩最高的学生信息及其成绩


-- 34.成绩有重复的情况下，查询选修「张三」老师所授课程的学生中，成绩最高的学生

-- 35.查询不同课程成绩相同的学生的学生编号、课程编号、学生成绩


-- 36. 查询每门成绩最好的前两名



-- 37. 统计每门课程的学生选修人数（超过 5 人的课程才统计）




#苏灿莹







-- 38.检索至少选修两门课程的学生学号

-- 39.查询选修了全部课程的学生信息


-- 40.查询各学生的年龄，只按年份来算


-- 41. 按照出生日期来算，当前月日 < 出生年月的月日则，年龄减一



-- 42.查询本周过生日的学生




-- 43. 查询下周过生日的学生


-- 44.查询本月过生日的学生


-- 45.查询下月过生日的学生
