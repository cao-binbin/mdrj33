-- 1.输入教师名称，查询学过该教师课程的学生姓名,如果人数超过3人，则打印出大班；否则小班；
drop procedure if exists `work1`;
delimiter $$
create procedure work1(nam varchar(10) )
BEGIN
   declare sum int default 0;
   set sum= (select count(*)from student where sid in(
   select sid from sc where cid=
   (select cid from course where tid=
   (select tid from teacher where tname=nam))));
   IF sum>3 then
   select '大班';
   else
   select '小班';
   end if;
end;
$$
delimiter;
call work1('李四');

-- 2.给定课程ID，如果优秀率大于优良率的，则打印学霸班；否则打印普通班
drop PROCEDURE if EXISTS `work2`;
delimiter $$
create PROCEDURE work2(num int )
BEGIN
	DECLARE yx DOUBLE DEFAULT 0;
	DECLARE yl DOUBLE DEFAULT 0;
	
	set yx= (select (SELECT count(*) from sc where sc.cid=course.cid and score>=90)
						/
					(SELECT count(*) from sc where sc.cid=course.cid )
					FROM course where cid=num);
					
	set yl= (select (SELECT count(*) from sc where sc.cid=course.cid and score>=80 and score<90)
						/
					(SELECT count(*) from sc where sc.cid=course.cid )
					FROM course where cid=num);
					
			if yx>yl then 
				select '学霸班';
			else 
				select '普通班';
			end if;
	
END
$$
delimiter;
CALL work2(4);


-- 3.给定一个值，计算阶乘  n! = n*(n-1)....*1

DROP PROCEDURE IF EXISTS `work3`;
delimiter $$
CREATE PROCEDURE work3(num int )
BEGIN 
DECLARE sum int DEFAULT 1;
	WHILE num>0 DO
		set sum = sum * num;
		set num = num-1;
	end WHILE;
SELECT sum;

END
$$
delimiter;
call work3(6);


-- 4.查询每年出生的学生信息，时间范围限制在85到89年、91到95年（要求单个循环处理）
drop procedure if exists `work4`;
delimiter $$
create procedure work4(yea int)
begin
   if 1985<=yea and yea<=1989 then
	 select * from student where year(sage)=yea;
	 elseif 1991<=yea and yea<=1995 then
	 select * from student where year(sage)=yea;
	 else
	 select('不在时间范围内');
	 end if;
end;
$$
delimiter;
call work4(1990);




