use studentsys;


-- 过程：
-- 1.输入教师名称，查询学过该教师课程的学生姓名
--  如果人数超过3人，则打印出大班；否则小班；
delimiter // 
drop procedure if exists procedure1;
create procedure procedure1(in aa varchar(20))
start_begin:begin 
  #存放学过该教师课程的学生人数
  declare bb int default 0;
  #查询学过该教师课程的学生姓名
	studentName_begin:begin
	select sname from student where sid in
	(
		select sid from sc where cid in
		(
			select cid from course where tid in 
			(
				select tid from teacher where tname like aa
			)
		)
	);
	end;
	#判断大小班
	big_small_class:BEGIN

		select count(*) into bb from student where sid in
			(
				select sid from sc where cid in
				(
					select cid from course where tid in 
					(
						select tid from teacher where tname like aa
					)
				)
			);
		if bb>3 THEN
			select '大班';
		ELSE
			select '小班';
		end if;
	end;
end;
//
delimiter ;


call procedure1('王五');
select * from teacher;
select * from course;


-- 2.给定课程ID，如果优秀率大于优良率的，则打印学霸班；否则打印普通班
delimiter // 
drop procedure if exists procedure2;
create procedure procedure2(in aa varchar(10))
begin
	declare bb decimal(5,4) default 0;
	declare cc decimal(5,4) default 0;
	#查询出优秀率和优良率
	percentage:begin
		select
		((select count(*) from sc where sc.cid=course.cid and sc.score>=90)/(select count(*) from sc where sc.cid=course.cid)) 优秀率,
		((select count(*) from sc where sc.cid=course.cid and sc.score>=80 and sc.score<90)/(select count(*) from sc where sc.cid=course.cid)) 优良率
		into bb,cc
		from course where cid = aa;
	end;
	#比较两个值的大小	
	compare:begin
		if bb>cc then
		  select '学霸班' 班级类型;
		else
		  select '普通班' 班级类型;
		end if;
	end;
end;
//
delimiter ;

call procedure2('01');



-- 3.给定一个值，计算阶乘  n! = n*(n-1)....*1
delimiter //
drop procedure if exists procedure3;
create procedure procedure3(in aa int)
start_begin:begin
declare bb int default 1;
	while aa>0 do
		set bb=bb*aa;
		set aa=aa-1;
	end while;
  select bb; 
end;
//
delimiter ;

call procedure3(4);

-- 4.查询每年出生的学生信息，时间范围限制在1985到1989年、1991到1995年（要求单个循环处理）
delimiter //
drop procedure if exists procedure4;
create procedure procedure4()
begin
	declare aa int default 0;
	declare bb int default 0;
	select count(*) into aa from student;
	zz:while aa>0 do
		select year(sage) into bb from student where sid=aa;
		IF bb>=1985 && bb<=1989 || bb>=1991 && bb<=1995 THEN
			select * from student where sid = aa;
			set aa=aa-1;
		ELSE
		set aa=aa-1;
			iterate zz;
		END IF;
	end while;
end;
//
delimiter ;

call procedure4();



















