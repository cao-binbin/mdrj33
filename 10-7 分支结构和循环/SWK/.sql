-- 1.输入教师名称，查询学过该教师课程的学生姓名,如果人数超过3人，则打印出大班；否则小班；
create procedure a1(nam varchar(10) )
BEGIN
   declare sum int default 0;
   set sum= (select count(*)from student where sid in(
   select sid from sc where cid=
   (select cid from course where tid=
   (select tid from teacher where tname=nam))));
   IF sum>3 then
   select '大班';
   else
   select '小班';
   end if;
end;
call a1('张三');

-- 2.给定课程ID，如果优秀率大于优良率的，则打印学霸班；否则打印普通班
create procedure a2(id int)
BEGIN
   declare sum int default 0;
   declare sun int default 0;

   set sum=(select (count(IF(score>=90,score,null))/count(*)) 优秀率 from sc GROUP BY cid having cid = id);
   set sun=(select (count(if(score>=80 and score<90,score, null))/count(*))优良率 from sc GROUP BY cid having cid=01);
   if sum>sun THEN
   select '学霸班';
   else 
   select '普通班';
   end if;
end;
   call a2(03)
-- 3.给定一个值，计算阶乘  n! = n*(n-1)....*1
create procedure a3(num int)
begin 
   declare sun int default 1;
while num>0 DO
   set sun=sun*num;
   set num=num-1;
end while;
   select sun;
end;
call a3(3)

-- 4.查询每年出生的学生信息，时间范围限制在85到89年、91到95年（要求单个循环处理）
create procedure a4(yea int)
begin
   if 1985<=yea and yea<=1989 then
	 select * from student where year(sage)=yea;
	 elseif 1991<=yea and yea<=1995 then
	 select * from student where year(sage)=yea;
	 else
	 select('不在时间范围内');
	 end if;
end;
   call a4(1990)