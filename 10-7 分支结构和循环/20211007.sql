-- 1.输入教师名称，查询学过该教师课程的学生姓名
--   如果人数超过3人，则打印出大班；否则小班；

drop procedure if exists class ;
CREATE PROCEDURE class(t_name varchar(10))
BEGIN
	DECLARE num int DEFAULT 0;
	SET num = (select count(*) from student join sc on student.sid=sc.sid
	join course on sc.cid=course.cid
	join teacher on course.tid=teacher.tid
	where tname=t_name) ;	
	IF num>3 THEN
		select '大班' ;
	ELSE
		select '小班' ;
	END IF ;
END ;
CALL class('张三')


-- 2.给定课程ID，如果优秀率大于优良率的，
--   则打印学霸班；否则打印普通班

DROP PROCEDURE IF EXISTS type ;
CREATE PROCEDURE type(id int)
BEGIN
	DECLARE a double ;
	DECLARE b double ;
	SET a = (select ((select count(*) from sc where score>=80 and cid=id)
					 /(select count(*) from sc where cid=id))as 优秀率) ;
	SET b = (select ((select count(*) from sc where score>70 and score<80 and cid=id)
					 /(select count(*) from sc where cid=id))as 优良率) ;
IF a>b THEN
	select '学霸班' ;
ELSEIF b>a THEN
	select '普通班' ;
END IF;
END;
call type(1);


-- 3.给定一个值，计算阶乘  n! = n*(n-1)....*1

DROP PROCEDURE if exists allsum ;
create PROCEDURE allsum(num int)
BEGIN
	declare sum int default 1 ;
	label_while:while num>0 do
	set sum=sum*num;
	set num=num-1;
	end while;
	select sum;
END;
call allsum(5);


-- 4.查询每年出生的学生信息，时间范围限制在85到89年、91到95年
-- （要求单个循环处理）

drop procedure if exists birthyear ;
create procedure birthyear()
begin

	declare sum int default 1985;
	
	aa:while sum>=1985 do
	select * from student where sage=sum;
	set sum=sum+1;
		if sum>1989 then
			leave aa;
		end if;
	end while;
	set sum=sum+1;
	
	bb:while sum>=1991 do
	select * from student where sage=sum;
	set sum=sum+1;
		if sum>1995 then
			leave bb;
		end if;
	end while;

end;

call birthyear();


