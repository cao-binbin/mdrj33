/*1.输入教师名称，查询学过该教师课程的学生姓名
  如果人数超过3人，则打印出大班；否则小班；*/
	
drop PROCEDURE if EXISTS `class`;
delimiter $$
create PROCEDURE class(t_name varchar(5))
BEGIN
			DECLARE num int DEFAULT 0;
			SET num=(

		select count(*) from sc where cid=(
		select cid from course where cid=(
    SELECT tid from teacher where tname=t_name
)
)
);
   if num>3 THEN
		SELECT '大班';
	 ELSE
		SELECT '小班';
	 END if;
	 select class;
END
$$
delimiter ;

call class('李四');





/*
2.给定课程ID，如果优秀率大于优良率的，则打印学霸班；否则打印普通班(优秀率：0.7-0.8 优良率：0.5-0.6)
*/
drop PROCEDURE if EXISTS `class1`;

create PROCEDURE class1(course_id int)
BEGIN
		DECLARE num int DEFAULT 0;
		SET num=(
		select sum(score)/count(sc.cid)/100 as 班级平均分 from sc 
		GROUP BY cid
		HAVING cid=course_id
		);
		
		if num>0.7 && num<0.8 THEN
		select '学霸班';
		
		ELSEIF num>0.5 && num<0.6 THEN
		select '普通班';
		
		END IF;
		select class1;
END;

call class1(1);



#3.给定一个值，计算阶乘  n! = n*(n-1)....*1

drop PROCEDURE if EXISTS `class3`;

create PROCEDURE class3(num int )
BEGIN
			DECLARE sum int DEFAULT 1; 
			IF num>0 THEN
	    set sum=sum*num;
			set num=num-1;
			ELSEIF  num<0 THEN
			select '错误';	
      END IF;
			select class3;
END;

call class3(3);



#4.查询每年出生的学生信息，时间范围限制在85到89年、91到95年（要求单个循环处理）
drop PROCEDURE if EXISTS `class4`;
delimiter $$
create PROCEDURE class4(years YEAR)
BEGIN
			DECLARE demo int DEFAULT 0;
		
			IF years>=1985 && years<=1995 THEN
							set demo =
							(
							select year(sage) from student where sid=years
							);
							END IF;		
						bb:WHILE TRUE DO
						IF years=1990 THEN
								ITERATE bb ;
						END IF;
							 END WHILE;
						

END
$$
delimiter ;

call class4(1990);
























