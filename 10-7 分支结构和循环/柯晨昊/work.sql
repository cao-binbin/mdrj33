/*
1.输入教师名称，查询学过该教师课程的学生姓名
  如果人数超过3人，则打印出大班；否则小班；
*/

drop procedure if exists `work1`;
delimiter $$
create procedure work1(tea varchar(10))
begin 
	DECLARE num int DEFAULT 0;
	select count(*) into num from sc where cid=(
	select cid from course where tid=(
	select tid from teacher where tname like tea));
	IF num>3 THEN
		select '大班';
	ELSE
		select '小班';
	END IF;
END
$$
delimiter ;

call work1('李四');


#2.给定课程ID，如果优秀率大于优良率的，则打印学霸班；否则打印普通班
drop procedure if exists `work2`;
delimiter $$
create procedure `work2`(inid int)
begin
	DECLARE num1 int DEFAULT 0;
	DECLARE num2 int DEFAULT 0;
	select (select count(*) from sc where cid=inid and score >=60)/
	(select count(*) from sc where cid=inid) into num1;
	select (select count(*) from sc where cid=inid and score >=85)/
	(select count(*) from sc where cid=inid) into num2;
	
	IF num2>num1 THEN
		select '学霸班';
	ELSE
		select '普通班';
	END IF;
END
$$
delimiter ;

call work2(1);


#3.给定一个值，计算阶乘  n! = n*(n-1)....*1

drop procedure if exists `work3`;
delimiter $$
create procedure `work3`(num int)
begin
	DECLARE temp int default 1;
	WHILE num>0 DO
	set temp=temp*num;
	set num=num-1;
	END WHILE;
	select temp;     
END
$$
delimiter ;

call work3(5);

#4.查询每年出生的学生信息，时间范围限制在85到89年、91到95年（要求单个循环处理）

-- drop procedure if exists `work4`;
-- delimiter $$
-- create procedure `work4`()
-- begin
-- 
-- END
-- $$
-- delimiter ;
-- 
-- 
-- select YEAR(sage)  from student



drop procedure if exists `work4`;
drop table if exists students;
create table students(
sid varchar(10),
sname varchar(10),
sage datetime,
ssex varchar(10)
);

delimiter $$
create procedure `work4`()
begin
declare num int DEFAULT 85;
declare yearzs varchar(15);
DECLARE ssid varchar(10) ;
DECLARE ssname varchar(10) ;
DECLARE ssage datetime ;
DECLARE sssex varchar(10) ;
aa:while num<=95 do
set num=num+1;
if num=90 then
iterate aa;
end if;
set yearzs=CONCAT('19',num);
select sid,sname,sage,ssex into ssid,ssname,ssage,sssex from student where YEAR(sage)=yearzs; 
if ssid !='' then
INSERT into students(sid,sname,sage,ssex) values(ssid,ssname,ssage,sssex);
end if;
set ssid='';
end WHILE;
select * from students;
drop TABLE students;
end 
$$
delimiter ;
