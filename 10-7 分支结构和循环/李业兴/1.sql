#1.输入教师名称，查询学过该教师课程的学生姓名
#  如果人数超过3人，则打印出大班；否则小班；
delimiter $$
drop PROCEDURE if EXISTS `Cteacher`;

create procedure Cteacher(Tea varchar(10))
BEGIN
	declare num int default 0 ;
	if Tea='张三' or Tea='李四' or Tea='王五' THEN
		select count(*) into num  from sc where cid=
		(select cid from course where tid=
		(select tid from teacher where tname = Tea));
	else
		select '没有该教师';
	end if;
	IF Tea>3 THEN
	select '大班';
	ELSE 
	select '小班';
	end IF;
END;
$$
delimiter ;
call Cteacher('王五');

#2.给定课程ID，如果优秀率大于优良率的，则打印学霸班；
#否则打印普通班
#优秀率＝优秀的人数÷测试的总人数x100%
#优良率＝良好的人数÷测试的总人数x100%

drop PROCEDURE if exists `CClass`;
create PROCEDURE CClass(ID int)
BEGIN
	#`优秀率`
	declare excellent DECIMAL(5,4) default 0;
	#`优良率`
	declare fine decimal(5,4) default 0;
		set excellent =
			(select ((select count(*) from sc where score>=80 and cid=ID)/(select count(*) from sc where cid=ID)) 优秀率) ;
		set fine =
			(select ((select count(*) from sc where score>70 and score<80 and cid=ID)/(select count(*) from sc where cid=ID)) 优良率) ;
		if excellent>fine then
			select '学霸班';
		ELSEif fine>excellent then
			select '普通班';
	END IF;
end;
call CClass(1);

#3.给定一个值，计算阶乘  n! = n*(n-1)....*1
drop PROCEDURE if exists `zhi`;
create PROCEDURE zhi(num int)
BEGIN
	declare sum int DEFAULT 1;#默认为1。如果是0，所乘的数都为0。
	WHILE num>0 DO
	  set sum = sum*num;
		set num = num -1;
	END WHILE;
	SELECT sum;
END;

call zhi(3);

#4.查询每年出生的学生信息，时间范围限制在85到89年、91到95年（要求单个循环处理）
drop PROCEDURE if exists `Stu`;
create PROCEDURE Stu()
BEGIN
	#定义一个虚拟值，为1985。
	declare sum int default 1985;
	#while判断，符合条件往下执行
	aa:while sum>=1985 do
	select * from student where sage=sum;
	#年份+1
	set sum=sum+1;
		#if判断sum是否符合条件
		if sum>1989 then
			#迭代器，返回aa
			leave aa;
		#结束if判断
		end if;
	#结束while判断
	end while;
	#sum继续加1，直到符合下一个条件(1991)
	set sum=sum+1;
	
	#while判断，符合条件往下执行
	bb:while sum>=1991 do
	select * from student where sage=sum;
	#年份+1
	set sum=sum+1;
		#if判断sum是否符合条件
		if sum>1995 then
			#迭代器，返回bb
			leave bb;
		#结束if判断
		end if;
	#结束while判断
	end while;
end;

call Stu();