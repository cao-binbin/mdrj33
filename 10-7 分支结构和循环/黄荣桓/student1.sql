
-- 1.输入教师名称，查询学过该教师课程的学生姓名
-- 如果人数超过3人，则打印出大班；否则小班；
drop procedure if exists `first1`;
create procedure first1(nam varchar(10) )

BEGIN
   declare sum int default 0;
   set sum= (select count(*)from student where sid in(
   select sid from sc where cid=
   (select cid from course where tid=
   (select tid from teacher where tname=nam))));
   IF sum>3 then
   select '大班';
   else
   select '小班';
   end if;
end;

call first1('李四');



-- 2.给定课程ID，如果优秀率大于优良率的，则打印学霸班；否则打印普通班
drop procedure if exists `second2`;

create procedure second2(id int)
BEGIN
   declare num int default 0;
   declare sum int default 0;

   set num=(select (count(IF(score>=90,score,null))/count(*)) 优秀率 from sc GROUP BY cid having cid = id);
	 
   set sum=(select (count(if(score>=80 and score<90,score, null))/count(*))优良率 from sc GROUP BY cid having cid=01);
   if num>sum THEN
   select '学霸班';
   else 
   select '普通班';
   end if;
END;

   call second2(01)



-- 3.给定一个值，计算阶乘  n! = n*(n-1)....*1

drop PROCEDURE if EXISTS `third`;

create PROCEDURE third(num int)

BEGIN

	DECLARE sum int DEFAULT 1;
	WHILE num>0 DO
	  set sum = sum*num;
		set num = num -1;
	END WHILE;
	SELECT sum;
	
END;

call third(3);

-- 4.查询每年出生的学生信息，时间范围限制在85到89年、91到95年（要求单个循环处理）
drop procedure if exists `four4`;

create procedure four4(yea int)

begin

   if 1985<=yea and yea<=1989 then
	 select * from student where year(sage)=yea;
	 elseif 1991<=yea and yea<=1995 then
	 select * from student where year(sage)=yea;
	 else
	 select('不在时间范围内');
	 end if;
	 
END;
   call four4(1990)


