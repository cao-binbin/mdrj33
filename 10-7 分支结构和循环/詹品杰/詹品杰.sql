-- 1.输入教师名称，查询学过该教师课程的学生姓名
--  如果人数超过3人，则打印出大班；否则小班； 

drop PROCEDURE if EXISTS `T_Name_count`;
delimiter $$
CREATE PROCEDURE T_Name_count(T_Name VARCHAR(10))
BEGIN
-- 创建临时变量
	DECLARE t_count int DEFAULT 0; 
	DECLARE s_Name VARCHAR(30) DEFAULT '';
	-- 获取人数
	Select COUNT(*)  into t_count from sc where cid in (Select cid from course where tid in (select tid from teacher where tname= CONCAT(T_Name)));
	if t_count>3 then
	SELECT '大班';
	ELSEIF t_count<3 THEN
	SELECT '小班';
	END if;
END;
$$
delimiter;

#2.给定课程ID，如果优秀率大于优良率的，则打印学霸班；否则打印普通班
drop PROCEDURE IF EXISTS `C_Name_count`;
delimiter $$
CREATE PROCEDURE C_Name_count(c_id int)
BEGIN
	DECLARE good_1 FLOAT DEFAULT 0;
	DECLARE good_2 FLOAT DEFAULT 0;
	Select (COUNT(IF(score>=80 and score<-90,score,NULL))/COUNT(*)) 优良率,(COUNT(IF(score>=90,score,NULL))/COUNT(*)) 优秀率
	 into good_1,good_2 from sc  where cid=c_id GROUP BY cid;
	 if good_2>good_1 THEN
	  Select '学霸班';
		ELSE
		SELECT '普通班';
		end if;
END;
$$
delimiter;

 #3给定一个值，计算阶乘  n! = n*(n-1)....*1
 drop PROCEDURE if EXISTS `com_x1`;
 delimiter $$
 CREATE PROCEDURE com_x1(n int)
 BEGIN
	DECLARE sum int DEFAULT 1;
	WHILE n>0 do 
	set sum=sum*n;
	set n=n-1;
	end WHILE;
		Select sum as 乘阶;
 END;
 $$
 delimiter;
 
-- 4.查询每年出生的学生信息，时间范围限制在85到89年、91到95年（要求单个循环处理）
drop PROCEDURE if EXISTS `Time_BETWEEN`;
delimiter $$
CREATE PROCEDURE Time_BETWEEN()
BEGIN
-- 临时变量
	DECLARE Stu_id_count int DEFAULT 0;
	DECLARE Count_jj_8589 int DEFAULT 0;
	DECLARE Count_jj_9195 int DEFAULT 0;
	Select COUNT(*) into Stu_id_count from student; -- 获取循变量值
	aa:WHILE Stu_id_count>0 do
			if Stu_id_count=0 then
				LEAVE aa; -- 小于0则结束整个循环;
			end if ; 
				Select sid into Count_jj_8589  from student where sid=Stu_id_count and sage BETWEEN'1985-1-1' and '1989-12-31';
				Select sid into Count_jj_9195  from student where sid=Stu_id_count and sage BETWEEN'1991-1-1' and '1995-12-31';
				if Count_jj_8589!=0 then
					Select * from student WHERE sid=Count_jj_8589;
					Set Count_jj_8589=0; -- 避免冲突 进行归0
				ELSEIF Count_jj_9195!=0 then
					Select * from student WHERE sid=Count_jj_9195;
					Set Count_jj_9195=0; -- 避免冲突 进行归0
				end if ;
			Set Stu_id_count=Stu_id_count-1;
		end WHILE;
END;
$$ 
delimiter;



-- 解题答案
-- 1# 
call T_Name_count('张三') ;

-- 2# 
CALL C_Name_count(03);

-- 3#
CALL com_x1(4);

-- 4#
CALL Time_BETWEEN();












