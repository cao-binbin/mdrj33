   SELECT * from student;
   SELECT * from teacher;
   SELECT * from course;
   SELECT * from sc;


#1.输入教师名称，查询学过该教师课程的学生姓名
# 如果人数超过3人，则打印出大班；否则小班；
DROP PROCEDURE if EXISTS class;
CREATE PROCEDURE class(type varchar(10))
BEGIN
    DECLARE num int DEFAULT 0;
    set num=(SELECT count(*) from student join sc on student.sid=sc.sid
             join course on sc.cid=course.cid
             join teacher on course.tid=teacher.tid
             where tname=type
             );   
    IF num>3 THEN
    SELECT '大班';
    ELSE
    SELECT '小班';
    end if;
END;
call class('张三')



#2.给定课程ID，如果优秀率大于优良率的，则打印学霸班；否则打印普通班
DROP PROCEDURE if EXISTS testwhile1;
CREATE PROCEDURE testwhile1(id int)
BEGIN
DECLARE a DOUBLE;
DECLARE b DOUBLE;
	SET a = (select ((select count(*) from sc where score>=80 and cid=id)
					 /(select count(*) from sc where cid=id))as 优秀率) ;
	SET b = (select ((select count(*) from sc where score>70 and score<80 and cid=id)
					 /(select count(*) from sc where cid=id))as 优良率) ;
IF a>b THEN
	select '学霸班' ;
ELSEIF b>a THEN
	select '普通班' ;
END IF;
END;
call type();





#3.给定一个值，计算阶乘  n! = n*(n-1)....*1
drop PROCEDURE if EXISTS testwhile;
create PROCEDURE testwhile(num int)
BEGIN
     DECLARE sum int DEFAULT 1;
     WHILE num>0 DO
     set sum=sum*num;
     set num=num-1;
     END WHILE;
     SELECT sum;
END;
call testwhile(3);



#4.查询每年出生的学生信息，时间范围限制在85到89年、91到95年（要求单个循环处理）
DROP PROCEDURE if EXISTS testwhile2;
CREATE PROCEDURE testwhile2()
begin 
DECLARE num int DEFAULT 1985;
aa: while sum>=1985 do
SELECT * from student where sage=num;
set num=num+1;
if sum>1989 then 
LEAVE aa;
end if;
end while;

set num=num+1;
bb: while num>1991 do 
SELECT * from student where sage=num;
set num=num+1;
if num>1995 then
LEAVE bb;
end if;
end while;
END;
call testwhile2(1986)





