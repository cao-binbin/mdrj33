drop PROCEDURE if EXISTS `Lesson1`;
delimiter $$
create procedure Lesson1(in par_tname varchar(20))
begin
	CASE 
		WHEN  par_tname='张三' THEN
			select count(*) into @num1 from student where sid in(
			select sid from sc where cid in(
				select cid from course where tid =(
					select tid from teacher where tname="张三"
				)
			)
		);
				IF @num1>3 THEN
				SELECT '大班';
			ELSE
				SELECT '小班';
			END IF;

		WHEN  par_tname='李四' THEN
			select count(*) into @num2 from student where sid in(
			select sid from sc where cid in(
				select cid from course where tid =(
					select tid from teacher where tname="李四"
				)
			)
		);
				IF @num2>3 THEN
				SELECT '大班';
			ELSE
				SELECT '小班';
			END IF;

		WHEN  par_tname='王五' THEN
			select count(*) into @num3 from student where sid in(
			select sid from sc where cid in(
				select cid from course where tid =(
					select tid from teacher where tname="王五"
				)
			)
		);
				IF @num3>3 THEN
				SELECT '大班';
			ELSE
				SELECT '小班';
				END IF;
				
		ELSE
			SELECT '没有';
	END CASE;

END;
$$
delimiter ;
call Lesson1('张三');
#过程：
#1.输入教师名称，查询学过该教师课程的学生姓名
#  如果人数超过3人，则打印出大班；否则小班；


#2.给定课程ID，如果优秀率大于优良率的，则打印学霸班；否则打印普通班
drop procedure if exists `cou`;
create procedure cou(id int)
BEGIN
   declare sum int default 0;
   declare sun int default 0;

   set sum=(select (count(IF(score>=90,score,null))/count(*)) 优秀率 from sc GROUP BY cid having cid = id);
   set sun=(select (count(if(score>=80 and score<90,score, null))/count(*))优良率 from sc GROUP BY cid having cid=01);
   if sum>sun THEN
   select '学霸班';
   else 
   select '普通班';
   end if;
end;
   call cou(01)


#3.给定一个值，计算阶乘  n! = n*(n-1)....*1
DROP PROCEDURE if EXISTS `t`;
create PROCEDURE t(num int)
BEGIN
	DECLARE sum int DEFAULT 1;
	WHILE num>0 DO
	  set sum = sum*num;
		set num = num -1;
	END WHILE;
	SELECT sum;
END;

call testwhile2(3);


#4.查询每年出生的学生信息，时间范围限制在85到89年、91到95年（要求单个循环处理）

drop procedure if exists `d`;
create procedure d(yea int)
begin
   if 1985<=yea and yea<=1989 then
	 select * from student where year(sage)=yea;
	 elseif 1991<=yea and yea<=1995 then
	 select * from student where year(sage)=yea;
	 else
	 select('不在时间范围内');
	 end if;
end;
   call d(1990)
