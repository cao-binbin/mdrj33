//  过程：
//  1.输入教师名称，查询学过该教师课程的学生姓名
 // 如果人数超过3人，则打印出大班；否则小班；
   
   drop procedure if exists teachernum ;
	 create  procedure  teachernum(teacherw  varchar(10) )
	  begin 
		declare  num int  DEFAULT 0;
SELECT
	count( * ) into num
FROM
	student
	INNER JOIN course
	INNER JOIN sc
	ON
	teacher.tid = course.tid 
	AND course.cid = sc.cid 
where
	teacher.tname  =  CONCAT(techerw)
	GROUP BY sc.cid;
		IF num>3 THEN
	select'大班';
ELSE
	select'小班';
END IF;
	 end;
call teachernum('张三');
// 2.给定课程ID，如果优秀率大于优良率的，则打印学霸班；否则打印普通班
drop PROCEDURE IF EXISTS `Class_name`;
delimiter //
CREATE PROCEDURE Class_name(c_id int)
BEGIN
	DECLARE good_1 FLOAT DEFAULT 0;
	DECLARE good_2 FLOAT DEFAULT 0;
	Select (COUNT(IF(score>=80 and score<-90,score,NULL))/COUNT(*)) 优良率,(COUNT(IF(score>=90,score,NULL))/COUNT(*)) 优秀率
	 into good_1,good_2 from sc  where cid=c_id GROUP BY cid;
	 if good_2>good_1 THEN
	  Select '学霸班';
		ELSE
		SELECT '普通班';
		end if;
END
//
delimiter;

// 3.给定一个值，计算阶乘  n! = n*(n-1)....*1
 drop procedure if exists num;
 create procedure n (in num int )
 BEGIN
	DECLARE sum int DEFAULT 1;
	WHILE num>0 DO
	  set sum = sum*num;
		set num = num -1;
	END WHILE;
	SELECT sum;
END;

 call n(7);
  

//  4.查询每年出生的学生信息，时间范围限制在85到89年、91到95年（要求单个循环处理）


