-- 3.给定一个值，计算阶乘  n! = n*(n-1)....*1
drop PROCEDURE if exists `Lesson3`;
create PROCEDURE Lesson3(in par_num int)
begin
	
	declare sum int default 1;

	WHILE par_num>0 DO
		set sum=sum*par_num;
		set par_num=par_num-1;
END WHILE;
	select sum;

end

call Lesson3(5);