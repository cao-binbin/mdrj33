过程：
1.输入教师名称，查询学过该教师课程的学生姓名
  如果人数超过3人，则打印出大班；否则小班；
drop PROCEDURE if EXISTS `Lesson1`;
create procedure Lesson1(in par_tname varchar(20))
begin
IF par_tname ="张三" THEN
	select count(*) into @num1 from student where sid in(
		select sid from sc where cid in(
			select cid from course where tid =(
				select tid from teacher where tname="张三"
			)
		)
	);
	IF @num1 > 3 THEN
	select "大班";
ELSE
	select "小班";
END IF;

ELSEIF par_tname="李四" THEN
	select count(*) into @num2 from student where sid in(
	select sid from sc where cid in(
		select cid from course where tid =(
			select tid from teacher where tname="李四"
		)
	)
);
	IF @num2>3 THEN
	select "大班";
ELSE
	select "小班";
END IF;	

ELSEIF par_tname="王五" THEN
	select count(*) into @num3 from student where sid in(
	select sid from sc where cid in(
		select cid from course where tid =(
			select tid from teacher where tname="王五"
		)
	)
);
	IF @num3>3 THEN
	select "大班";
ELSE
	select "小班";
END IF;	

ELSE
	SELECT "没有这个教师";
END IF;
end;

call Lesson1("张三");



-- 3.给定一个值，计算阶乘  n! = n*(n-1)....*1


-- 4.查询每年出生的学生信息，时间范围限制在85到89年、91到95年（要求单个循环处理）

