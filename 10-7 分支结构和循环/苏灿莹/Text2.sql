-- 2.给定课程ID，如果优秀率大于优良率的，则打印学霸班；否则打印普通班
drop PROCEDURE if EXISTS `Lesson2`;
create PROCEDURE Lesson2(in par_classid int)
begin
	DECLARE num1 int DEFAULT 0;
	DECLARE num2 int DEFAULT 0;
	DECLARE youxiu decimal(5,2) DEFAULT 0; 
	DECLARE youliang decimal(5,2) DEFAULT 0;
	DECLARE entire int DEFAULT 0;
	IF par_classid=1 THEN
	
		set num1 = (select count(*) from sc where cid=01 and score>=85);
		set num2 = (select count(*) from sc where cid=01 and score>=75);
		set entire = (select count(*) from sc where cid=01 );
		set youxiu =num1/entire*100;
		set youliang=num2/entire*100;
		IF youxiu > youliang THEN
		
			select "学霸班";
		ELSE
			select "普通班";
		END IF;

	ELSEIF par_classid=2 THEN
	
		set num1 = (select count(*) from sc where cid=02 and score>=85);
		set num2 = (select count(*) from sc where cid=02 and score>=75);
		set entire = (select count(*) from sc where cid=02 );
		set youxiu =num1/entire*100;
		set youliang=num2/entire*100;
		IF youxiu > youliang THEN
			select "学霸班";
		ELSE
			select "普通班";
		END IF;
		
	ELSEIF par_classid=3 THEN
	
		set num1 = (select count(*) from sc where cid=03 and score>=85);
		set num2 = (select count(*) from sc where cid=03 and score>=75);
		set entire = (select count(*) from sc where cid=03 );
		set youxiu =num1/entire*100;
		set youliang=num2/entire*100;
		IF youxiu > youliang THEN
			select "学霸班";
		ELSE
			select "普通班";
		END IF;
		
	ELSE
		select "请输入正确的课程ID";
END IF;
end

CALL lesson2(1);
CALL lesson2(2);
CALL lesson2(3);

