#过程：1.输入教师名称，查询学过该教师课程的学生姓名
 # 如果人数超过3人，则打印出大班；否则小班；
 
 -- 创建一个有参过程
 drop PROCEDURE if EXISTS `myaa1`;
 CREATE PROCEDURE myaa1(teaname VARCHAR(30))
 BEGIN
		#设置临时变量的分区
	 DECLARE num int DEFAULT 0;
	 DECLARE num1 int DEFAULT 0;
	 DECLARE num2 int DEFAULT 0;
	 #将得到的值存储在临时变量中
	 SELECT tid into num from teacher where tname=teaname; #求教师的教师编号 
	 SELECT cid into num1 from course where course.tid=num; #求老师教学的课程号
	 SELECT COUNT(*) into num2 from sc where sc.cid=num1; #通过课程号来判学习该课程的学生数量
	 SELECT (SELECT sname from student where student.sid=sc.sid) as 学生姓名  from sc where sc.cid=num1; #通过子查询输出学生信息
	 #通过if语句来判断大班或者小班
	 IF num2 > 3 THEN
	SELECT '大班'; #满足大于CREATE PROCEDURE myaa1(teaname VARCHAR(30))
 BEGIN
		#设置临时变量的分区
	 DECLARE num int DEFAULT 0;
	 DECLARE num1 int DEFAULT 0;
	 DECLARE num2 int DEFAULT 0;
	 #将得到的值存储在临时变量中
	 SELECT tid into num from teacher where tname=teaname; #求教师的教师编号 
	 SELECT cid into num1 from course where course.tid=num; #求老师教学的课程号
	 SELECT COUNT(*) into num2 from sc where sc.cid=num1; #通过课程号来判学习该课程的学生数量
	 SELECT (SELECT sname from student where student.sid=sc.sid) as 学生姓名  from sc where sc.cid=num1; #通过子查询输出学生信息
	 #通过if语句来判断大班或者小班
	 IF num2 > 3 THEN
	SELECT '大班'; #满足大于三个人就是大班 
ELSE
	SELECT '小班';
END IF;
 END;三个人就是大班 
ELSE
	SELECT '小班';
END IF;
 END;
 #设置一个变量存储姓名 判断结果是否正确
 set @aa='王五';
 CALL myaa1(@aa); #call 调用的过程名称
 
 #2.给定课程ID，如果优秀率大于优良率的，则打印学霸班；否则打印普通班
 # 给定的是id 返回的是varchar类型
 drop PROCEDURE if EXISTS `myaa2`;
 CREATE PROCEDURE myaa2(couid VARCHAR(30))
 BEGIN
			#设置临时变量存储优秀率和优良率
			DECLARE num DECIMAL(3,2) DEFAULT 0.00;
			DECLARE num1 DECIMAL(3,2) DEFAULT 0.00;
			#将优秀率和优良率存储到变量内
			SELECT (COUNT(IF(score>80,1,NULL))/COUNT(*)) into num from sc where sc.cid=couid;
			SELECT (COUNT(IF(score<80 && score>=70,1,NULL))/COUNT(*)) into num1 from sc where sc.cid=couid;
			#判断两者的大小来判断条件要求
			IF num > num1 THEN
	SELECT '学霸班';
ELSE
	SELECT '普通班';
END IF;
 END;
 
 set @bb ='03';
 CALL myaa2(@bb);
 
 #3.给定一个值，计算阶乘  n! = n*(n-1)....*1
  drop PROCEDURE if EXISTS `myaa3`;
 CREATE PROCEDURE myaa3(type int)
 BEGIN
		#给一个总和 用于存放总和
		DECLARE sum int DEFAULT 1;
		#通过while计算阶乘
		WHILE type > 0 DO
	  set sum = type * sum;
		set type = type -1;
END WHILE;
	 #输出结果
	 SELECT sum;
 END;
 
 #验证第三题结果
 CALL myaa3(5);
 