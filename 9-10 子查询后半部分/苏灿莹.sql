
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

/*
创建数据库
*/
drop DATABASE if EXISTS bank;
create DATABASE bank DEFAULT  CHARACTER set utf8 COLLATE utf8_general_ci;
use bank;

-- ----------------------------
-- 创建账户表
-- ----------------------------
DROP TABLE IF EXISTS `accountinfo`;
CREATE TABLE `accountinfo`  (
  `AccountId` int(11) NOT NULL COMMENT '账户ID',
  `AccountCode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '账号',
  `AccountPhone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '手机号码',
  `RealName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '真实姓名',
  `OpenTime` datetime(0) NULL DEFAULT NULL COMMENT '开户时间',
  PRIMARY KEY (`AccountId`) USING BTREE,
  INDEX `accounts`(`AccountCode`, `AccountPhone`) USING BTREE,
  INDEX `IX_AccountInfo`(`OpenTime`) USING BTREE,
  INDEX `index_code`(`AccountCode`) USING BTREE,
  INDEX `index_name`(`AccountPhone`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- 添加账户表记录
-- ----------------------------
INSERT INTO `accountinfo` VALUES (2, '420107199602034138', '13859003393', '刘备', '2021-08-10 16:00:00');
INSERT INTO `accountinfo` VALUES (3, '420107199602034139', '13859003394', '关羽', '2021-08-10 16:02:00');
INSERT INTO `accountinfo` VALUES (4, '420107199602034140', '13859003395', '张飞', '2021-08-10 16:02:00');
INSERT INTO `accountinfo` VALUES (5, '420107199602034145', '18769001180', '赵云', '2021-08-10 17:24:00');
INSERT INTO `accountinfo` VALUES (6, '420107199602034190', '18769001190', '曹操', '2021-08-10 17:25:00');
INSERT INTO `accountinfo` VALUES (8, '420107199602034199', '13859003394', '吕布', '2021-08-10 21:00:00');

-- ----------------------------
-- 创建银行卡表
-- ----------------------------
DROP TABLE IF EXISTS `bankcard`;
CREATE TABLE `bankcard`  (
  `CardId` int(11) NOT NULL AUTO_INCREMENT COMMENT '卡号ID',
  `CardNo` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '卡号码',
  `AccountId` int(11) NOT NULL COMMENT '账户ID',
  `CardPwd` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '卡密码',
  `CardMoney` decimal(19, 4) NOT NULL COMMENT '余额',
  `CardState` tinyint(4) NOT NULL COMMENT '卡状态\r\n0 正常\r\n1.异常\r\n-1 注销',
  `CardTime` datetime(0) NULL DEFAULT NULL COMMENT '开卡时间',
  PRIMARY KEY (`CardId`) USING BTREE,
  INDEX `FK__BankCard__Accoun__267ABA7A`(`AccountId`) USING BTREE,
  CONSTRAINT `FK__BankCard__Accoun__267ABA7A` FOREIGN KEY (`AccountId`) REFERENCES `accountinfo` (`AccountId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- 添加银行卡表记录
-- ----------------------------
INSERT INTO `bankcard` VALUES (1, '6225098134285', 3, '123456', 0.1000, 0, '2021-08-10 21:09:00');
INSERT INTO `bankcard` VALUES (2, '6225098234146', 4, '444444', 90.8900, 0, '2021-08-13 10:35:00');
INSERT INTO `bankcard` VALUES (3, '6225098234196', 4, '444444', 90.8900, 0, '2021-08-13 10:35:00');
INSERT INTO `bankcard` VALUES (4, '6225098234234', 2, '123456', 101800.0000, 0, '2021-08-10 16:04:00');
INSERT INTO `bankcard` VALUES (5, '6225098234235', 3, '123456', 3000.0000, 0, '2021-08-10 16:05:00');
INSERT INTO `bankcard` VALUES (6, '6225098234236', 4, '123456', 400000.0000, 0, '2021-08-10 16:05:00');
INSERT INTO `bankcard` VALUES (7, '6225098234260', 5, '888888', 0.0000, 0, '2021-08-10 17:24:00');
INSERT INTO `bankcard` VALUES (8, '6225098234285', 3, '123456', 400000.0000, 0, '2021-08-10 20:58:00');
INSERT INTO `bankcard` VALUES (9, '6225098234290', 6, '888888', 0.0000, 0, '2021-08-10 17:25:00');
INSERT INTO `bankcard` VALUES (10, '6225098234296', 4, '444444', 90.8900, 0, '2021-08-13 10:33:00');
INSERT INTO `bankcard` VALUES (11, '6225098234299', 8, '123456', 0.1000, 0, '2021-08-10 21:00:00');
INSERT INTO `bankcard` VALUES (12, '6235098234146', 4, '444444', 90.8900, 0, '2021-08-13 10:36:00');

-- ----------------------------
-- 存取款表
-- ----------------------------
DROP TABLE IF EXISTS `cardexchange`;
CREATE TABLE `cardexchange`  (
  `ExchangeId` int(11) NOT NULL COMMENT 'ID',
  `CardId` int(11) NULL DEFAULT NULL COMMENT '卡号ID',
  `MoneyInBank` decimal(19, 4) NOT NULL COMMENT '存款金额',
  `MoneyOutBank` decimal(19, 4) NOT NULL COMMENT '取款金额',
  `ExchangeTime` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`ExchangeId`) USING BTREE,
  INDEX `CardId`(`CardId`) USING BTREE,
  CONSTRAINT `CardId` FOREIGN KEY (`CardId`) REFERENCES `bankcard` (`CardId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- 存取款表记录
-- ----------------------------
INSERT INTO `cardexchange` VALUES (2, 1, 2000.0000, 0.0000, '2021-08-10 16:13:00');
INSERT INTO `cardexchange` VALUES (3, 2, 8000.0000, 0.0000, '2021-08-10 16:15:00');
INSERT INTO `cardexchange` VALUES (4, 3, 500000.0000, 0.0000, '2021-08-10 16:15:00');
INSERT INTO `cardexchange` VALUES (5, 2, 0.0000, 5000.0000, '2021-08-10 21:41:00');
INSERT INTO `cardexchange` VALUES (8, 4, 0.0000, 100.0000, '2021-08-11 20:55:00');
INSERT INTO `cardexchange` VALUES (9, 1, 0.0000, 100.0000, '2021-08-11 20:57:00');

-- ----------------------------
-- 转账表
-- ----------------------------
DROP TABLE IF EXISTS `cardtransfer`;
CREATE TABLE `cardtransfer`  (
  `TransferId` int(11) NOT NULL,
  `CardIdOut` int(11) NULL DEFAULT NULL COMMENT '转出卡号ID',
  `CardIdIn` int(11) NULL DEFAULT NULL COMMENT '转入卡号ID',
  `TransferMoney` decimal(19, 4) NULL DEFAULT NULL COMMENT '转账金额',
  `TransferTime` datetime(0) NULL DEFAULT NULL COMMENT '转账时间',
  PRIMARY KEY (`TransferId`) USING BTREE,
  INDEX `CardIdOut`(`CardIdOut`) USING BTREE,
  INDEX `CardIdIn`(`CardIdIn`) USING BTREE,
  CONSTRAINT `CardIdIn` FOREIGN KEY (`CardIdIn`) REFERENCES `bankcard` (`CardId`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `CardIdOut` FOREIGN KEY (`CardIdOut`) REFERENCES `bankcard` (`CardId`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- 转账表记录
-- ----------------------------
INSERT INTO `cardtransfer` VALUES (1, 1, 3, 1000.0000, '2021-08-10 16:18:00');
INSERT INTO `cardtransfer` VALUES (2, 3, 4, 1000.0000, '2021-08-11 21:06:00');
INSERT INTO `cardtransfer` VALUES (3, 2, 5, 100000.0000, '2021-08-11 21:06:00');


SET FOREIGN_KEY_CHECKS = 1;



-- select 
-- 1.查询所有用户信息以及卡数量

select *,
(
	select count(accountid) from bankcard where accountinfo.AccountId=bankcard.AccountId
)数量
from accountinfo


-- from 
-- 2.查询所有用户信息以及卡数量和总余额
select *,
(
	select count(accountid) from bankcard where accountinfo.AccountId=bankcard.AccountId
)数量
,
(
	select sum(cardmoney) from bankcard where accountinfo.AccountId=bankcard.AccountId
)总金额
from accountinfo

-- 查询每个用户的卡片数量和总余额
select accountinfo.*,temp.总金额,temp.数量 from 
	(select accountid,sum(cardmoney) 总金额,count(*) 数量 from bankcard group by accountid) as temp 
	inner join accountinfo on accountinfo.AccountId=temp.AccountId 

-- join
select * from accountinfo
inner join (select accountid,count(*) 数量,sum(cardmoney) 总数量 from bankcard group by accountid) as temp
on accountinfo.AccountId =temp.accountid

-- where 
-- 3.查询余额最高的账户

select * from accountinfo where accountid =
(
	select accountid from 
	(
		select accountid, sum(cardmoney) as money from bankcard
		group by accountid
		order by money desc
		LIMIT 1
	) temp
);

-- 4.查询出其他储户余额比关羽某张银行卡多的银行卡号(6225098234235)，显示卡号，身份证，姓名，余额

select * from accountinfo inner join 
	(select accountid from bankcard where cardmoney>
	(
		select cardmoney from bankcard where cardno='6225098234235'
	)
) as temp on accountinfo.AccountId=temp.accountid 
where accountinfo.accountid !=3;

-- 5.从所有账户信息中查询出余额最高的交易明细（存钱取钱信息）

select * from cardexchange where cardid in
(
	select cardid from bankcard where accountid=
	(
		select accountid from 
		(
			select accountid,sum(cardmoney) as money from bankcard
			group by accountid
			order by money desc
			LIMIT 1
		) temp
	)
)

-- 6.查询有取款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额
-- 自己的
select CardNo,AccountCode 身份证,RealName,cardmoney from accountinfo 
inner join 
(
	select * from bankcard where  CardId in
	(
		select CardId from cardexchange where MoneyOutBank>0
	)
)temp on accountinfo.AccountId=temp.AccountId

-- 老师的
SELECT bankcard.CardNo,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney from accountinfo 
inner join bankcard on bankcard.AccountId=accountinfo.AccountId
where bankcard.CardId in 
(
	select CardId from cardexchange where MoneyOutBank>0
)
-- 7.查询没有存款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额
-- 自己的
select CardNo,AccountCode 身份证,RealName,cardmoney from accountinfo 
left join
(
	select * from bankcard where CardId not in
	(
		select CardId from cardexchange where MoneyInBank>0 
	)
)temp on accountinfo.AccountId=temp.AccountId


-- 老师的
SELECT bankcard.CardNo,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney from accountinfo 
left join bankcard on bankcard.AccountId=accountinfo.AccountId
where bankcard.CardId  not in 
(
	select CardId from cardexchange where MoneyInBank>0
) or bankcard.CardId is null
-- 8.查询出没有转账交易记录的银行卡账户信息，显示卡号，身份证，姓名，余额
select CardNo,AccountCode 身份证,RealName,cardmoney from accountinfo 
inner join
(
	select * from bankcard where CardId not in
	(
		select CardIdIn from cardtransfer where CardIdIn > 0
	)
	and CardId not in 
	(
		select CardIdOut from cardtransfer where CardIdOut> 0 
	)
) temp on accountinfo.AccountId=temp.AccountId

-- on
-- 9.查询所有余额超过100RMB的银行卡信息，包括账户名称
select temp.*,RealName from accountinfo 
inner join 
(
	select * from bankcard where CardMoney >100
) temp on accountinfo.AccountId= temp.accountid

-- HAVING

-- 10.查询交易次数（存取款）最多的银行卡账户信息，卡号，身份证，姓名，余额，交易次数

select CardNo 卡号,AccountCode 身份证,RealName 姓名,CardMoney 余额,temp.money 交易次数 from accountinfo
inner join bankcard on bankcard.AccountId=accountinfo.AccountId
inner join (
select CardId,count(*) as money from cardexchange
GROUP BY CardId
order by money DESC
LIMIT 1
) temp on temp.CardId=bankcard.CardId




