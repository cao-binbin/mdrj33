
-- select 
-- 1.查询所有用户信息以及卡数量

select * from accountinfo
SELECT AccountId,COUNT(*) FROM bankcard GROUP BY AccountId
select accountinfo.* ,tr.卡数量 from accountinfo inner join (SELECT AccountId,COUNT(*) 卡数量  FROM bankcard GROUP BY AccountId)tr
 on tr.AccountId=accountinfo.AccountId
 
SELECT *,
(
	SELECT count(*) from bankcard where bankcard.AccountId=5
)卡数量
from accountinfo


-- from 
-- 2.查询所有用户信息以及卡数量和总余额
SELECT *,
(
	SELECT count(*) from bankcard where bankcard.AccountId=accountinfo.AccountId
)卡数量,
(
	SELECT sum(CardMoney) from bankcard where bankcard.AccountId=accountinfo.AccountId
)总余额
from accountinfo;

-- 查询所有用户信息以及卡数量和总余额

select accountinfo.AccountId,AccountCode,count(*)卡数量,SUM(CardMoney)总余额 from accountinfo 
inner join bankcard on bankcard.AccountId = accountinfo.AccountId
GROUP BY accountinfo.AccountId,AccountCode


-- join


select accountinfo.*,t.卡数量,t.总余额 from  
accountinfo
inner join (select AccountId,count(*)卡数量,sum(CardMoney)总余额 from bankcard GROUP BY AccountId
)t on accountinfo.AccountId=t.AccountId



-- where 


-- 3. 查询卡余额最高的账户

select * from accountinfo where AccountId in 
(select AccountId from bankcard where CardMoney>=(SELECT max(CardMoney) from bankcard));

-- 4.查询出其他储户余额比关羽某张银行卡多的银行卡号(6225098234235)，显示卡号，身份证，姓名，余额
select * from accountinfo inner join
(select AccountId, CardMoney from bankcard where CardMoney>
(select CardMoney from bankcard where CardNo='6225098234235')
GROUP BY AccountId) abc on accountinfo.AccountId=abc.AccountId;

-- 5.从所有账户信息中查询出余额最高的交易明细（存钱取钱信息）

select * from cardexchange where CardId in 
(select CardId from bankcard where CardMoney>=(select max(CardMoney) from bankcard ))


-- 6.查询有取款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额

SELECT bankcard.CardNo,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney from accountinfo 
inner join bankcard on bankcard.AccountId=accountinfo.AccountId
where bankcard.CardId in (select CardId from cardexchange where MoneyOutBank>0)


-- 7.查询没有存款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额

SELECT bankcard.CardNo,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney from accountinfo 
left join bankcard on bankcard.AccountId=accountinfo.AccountId
where bankcard.CardId  not in (select CardId from cardexchange where MoneyInBank>0) 





-- 8.查询出没有转账交易记录的银行卡账户信息，显示卡号，身份证，姓名，余额
select CardIdIn from  cardtransfer;
select bankcard.CardNo 卡号,accountinfo.AccountCode 身份证,accountinfo.RealName 姓名,bankcard.CardMoney  余额 from       accountinfo inner join bankcard on accountinfo.AccountId=bankcard.AccountId 
where bankcard.CardId not in( select CardIdIn from  cardtransfer);
  

-- on
-- 9.查询所有余额超过100RMB的银行卡信息，包括账户名称
 select bankcard.CardNo 卡号,accountinfo.AccountCode 身份证,accountinfo.RealName 姓名,bankcard.CardMoney  余额 from  accountinfo inner join bankcard on accountinfo.AccountId=bankcard.AccountId where bankcard.CardMoney  >100

-- HAVING

-- 10.查询交易次数（存取款）最多的银行卡账户信息，卡号，身份证，姓名，余额，交易次数
    select CardId, count(*)交易次数 from cardexchange GROUP BY CardId ;
		select bankcard.CardNo 卡号,accountinfo.AccountCode 身份证,accountinfo.RealName 姓名,bankcard.CardMoney  余额 ,
		from accountinfo inner join bankcard on accountinfo.AccountId=bankcard.AccountId 
    where  bankcard.CardId IN(select CardId FROM (select CardId, count(*)交易次数 from cardexchange  GROUP BY CardId
		LIMIT 2 )A);
  
 
  select CardId FROM (select count(*)交易次数 from cardexchange  GROUP BY CardId LIMIT 2 )A;
  
