
-- select 
-- 1.查询所有用户信息以及卡数量
SELECT
	*,
	( SELECT count( * ) FROM bankcard WHERE accountinfo.AccountId = bankcard.AccountId ) 
FROM
	accountinfo;
-- from 
-- 2.查询所有用户信息以及卡数量和总余额
SELECT
	*,
	( SELECT count( * ) FROM bankcard WHERE accountinfo.AccountId = bankcard.AccountId ),
	( SELECT sum( CardMoney ) FROM bankcard WHERE accountinfo.AccountId = bankcard.AccountId ) 
FROM
	accountinfo;
-- where 
-- 3.查询余额最高的账户
-- (1)某张卡余额最高
SELECT
	* 
FROM
	accountinfo
	INNER JOIN bankcard 
WHERE
	accountinfo.AccountId = bankcard.AccountId 
	AND CardMoney = ( SELECT max( CardMoney ) FROM bankcard );
-- (2)总余额最高
SELECT
	* 
FROM
	accountinfo 
WHERE
	AccountId = ( SELECT AccountId FROM ( SELECT AccountId, sum( CardMoney ) 总余额 FROM bankcard GROUP BY AccountId ORDER BY 总余额 DESC LIMIT 1 ) a );
-- 4.查询出其他储户余额比关羽某张银行(6225098234235)卡多的银行卡号，显示卡号，身份证，姓名，余额
SELECT
	bankcard.CardNo 卡号,
	accountinfo.AccountCode 身份证,
	accountinfo.RealName 姓名,
	bankcard.CardMoney 
FROM
	accountinfo
	INNER JOIN bankcard 
WHERE
	CardMoney > ( SELECT CardMoney FROM bankcard WHERE CardNo = '6225098234235' ) 
	AND accountinfo.AccountId = bankcard.AccountId 
	AND accountinfo.RealName != '关羽';
-- 5.从所有账户信息中查询出余额最高的交易明细（存钱取钱信息）
SELECT
	* 
FROM
	cardexchange 
WHERE
	CardId = (
SELECT
	CardId 
FROM
	( SELECT CardId, sum( CardMoney ) 总余额 FROM bankcard GROUP BY AccountId ORDER BY 总余额 DESC ) a 
WHERE
	CardId 
	LIMIT 1 
	);
-- 6.查询有取款记录的银行卡及账户信息，显示卡号，身份证，姓名，余额
SELECT
	bankcard.CardNo,
	accountinfo.AccountCode,
	accountinfo.RealName,
	bankcard.CardMoney 
FROM
	bankcard
	INNER JOIN cardexchange ON bankcard.CardId = cardexchange.CardId
	INNER JOIN accountinfo ON accountinfo.AccountId = bankcard.AccountId 
WHERE
	MoneyOutBank != 0;

-- 7.查询没有存款记录的银行卡及账户信息，显示卡号，身份证，姓名，余额
SELECT
	bankcard.CardNo,
	accountinfo.AccountCode,
	accountinfo.RealName,
	bankcard.CardMoney 
FROM
	bankcard
	INNER JOIN cardexchange ON bankcard.CardId = cardexchange.CardId
	INNER JOIN accountinfo ON accountinfo.AccountId = bankcard.AccountId 
WHERE
	MoneyInBank = 0;

-- 8.查询出没有转账交易记录的银行卡账户信息，显示卡号，身份证，姓名，余额
SELECT
	bankcard.CardNo,
	accountinfo.AccountCode,
	accountinfo.RealName,
	bankcard.CardMoney 
FROM
	accountinfo
	inner JOIN bankcard ON accountinfo.AccountId = bankcard.AccountId
WHERE
	bankcard.CardId NOT IN ( SELECT CardIdOut FROM cardtransfer WHERE TransferMoney > 0 ) 
	AND bankcard.CardId NOT IN ( SELECT CardIdIn FROM cardtransfer WHERE TransferMoney > 0 );
-- on
-- 9.查询所有余额超过100RMB的银行卡信息，包括账户名称
SELECT
	accountinfo.*,
	a.CardNo,
	a.总金额 
FROM
	accountinfo
	INNER JOIN ( SELECT AccountId, CardNo, sum( CardMoney ) 总金额 FROM bankcard GROUP BY AccountId HAVING 总金额 > 100 ) a ON accountinfo.AccountId = a.AccountId;
-- HAVING

-- 10.查询交易次数（存取款）最多的银行卡账户信息，卡号，身份证，姓名，余额，交易次数
SELECT
	bankcard.CardNo 卡号,
	accountinfo.AccountCode 身份证,
	accountinfo.RealName 姓名,
	bankcard.CardMoney 余额,
	a.交易次数 
FROM
	accountinfo
	INNER JOIN bankcard ON accountinfo.AccountId = bankcard.AccountId
	INNER JOIN ( SELECT CardId, count( * ) 交易次数 FROM cardexchange GROUP BY CardId ) a ON bankcard.CardId = a.CardId
