
-- select 
-- 1.查询所有用户信息以及卡数量

SELECT *,
(
SELECT count(*) FROM bankcard WHERE bankcard.AccountId=accountinfo.AccountId
)卡数量
from  accountinfo



-- from 
-- 2.查询所有用户信息以及卡数量和总余额
SELECT *,
(
	SELECT count(*) from bankcard where bankcard.AccountId=accountinfo.AccountId
)卡数量,
(
	SELECT sum(CardMoney) from bankcard where bankcard.AccountId=accountinfo.AccountId
)总余额
from accountinfo;


select accountinfo.*,temp.卡数量,temp.总余额 from 
(SELECT AccountId,count(*) 卡数量,sum(CardMoney) 总余额 from bankcard GROUP BY AccountId) temp
inner join accountinfo on accountinfo.AccountId=temp.AccountId;


select accountinfo.*,temp.卡数量,temp.总余额 from accountinfo
inner join (SELECT AccountId,count(*) 卡数量,sum(CardMoney) 总余额 from bankcard GROUP BY AccountId) temp
on accountinfo.AccountId=temp.AccountId;
-- where 
-- 3.查询余额最高的账户
select CardNo FROM bankcard WHERE CardMoney=(select max(CardMoney) from bankcard);
SELECT * from bankcard




select * FROM accountinfo WHERE AccountId=
(
	select AccountId from 
	(
		select AccountId,sum(CardMoney) as money from bankcard 
		GROUP BY AccountId
		ORDER BY money desc
		LIMIT 1
	)temp
)
;


-- 4.查询出其他储户余额比关羽某张银行卡多的银行卡号，显示卡号，身份证，姓名，余额

SELECT accountinfo.*,temp.CardMoney from accountinfo
inner join 
(	SELECT AccountId,CardMoney from bankcard 
	where CardMoney>
	(SELECT CardMoney from bankcard where CardNo='6225098234235')
)temp 
on temp.AccountId=accountinfo.AccountId
where accountinfo.AccountId !=3;

-- 5.从所有账户信息中查询出余额最高的交易明细（存钱取钱信息）

select * from cardexchange where CardId in(
	select CardId from bankcard where AccountId = 
	(
		select AccountId from 
		(
			select AccountId,sum(CardMoney) as money from bankcard 
			GROUP BY AccountId
			ORDER BY money desc
			LIMIT 1
		)temp
	)
)

