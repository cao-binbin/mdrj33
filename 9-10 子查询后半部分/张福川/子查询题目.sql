
-- select 
-- 1.查询所有用户信息以及卡数量

select * from accountinfo

SELECT *,(
	SELECT count(*) from bankcard where bankcard.AccountId=accountinfo.AccountId
)卡数量
from accountinfo


-- from 
-- 2.查询所有用户信息以及卡数量和总余额
SELECT *,
(
	SELECT count(*) from bankcard where bankcard.AccountId=accountinfo.AccountId
)卡数量,
(
	SELECT sum(CardMoney) from bankcard where bankcard.AccountId=accountinfo.AccountId
)总余额
from accountinfo;

-- 查询每个用户的卡片数量和总余额
select accountinfo.*,temp.卡数量,temp.总余额 from 
(SELECT AccountId,count(*) 卡数量,sum(CardMoney) 总余额 from bankcard GROUP BY AccountId) temp
inner join accountinfo on accountinfo.AccountId=temp.AccountId;

-- join
select accountinfo.*,temp.卡数量,temp.总余额 from accountinfo
inner join (SELECT AccountId,count(*) 卡数量,sum(CardMoney) 总余额 from bankcard GROUP BY AccountId) temp
on accountinfo.AccountId=temp.AccountId;
-- where 
-- 3.查询余额最高的账户

select CardNo FROM bankcard WHERE CardMoney=(select max(CardMoney) from bankcard);
SELECT * from bankcard


select max(maxMoney.money)
from (select AccountId,sum(CardMoney) as money from bankcard 
GROUP BY AccountId) maxMoney


select * FROM accountinfo WHERE AccountId=
(
	select AccountId from 
	(
		select AccountId,sum(CardMoney) as money from bankcard 
		GROUP BY AccountId
		ORDER BY money desc
		LIMIT 1
	)temp
)
;


-- 4.查询出其他储户余额比关羽某张银行卡多的银行卡号(6225098234235)，显示卡号，身份证，姓名，余额

SELECT accountinfo.*,temp.CardMoney from accountinfo
inner join 
(	SELECT AccountId,CardMoney from bankcard 
	where CardMoney>
	(SELECT CardMoney from bankcard where CardNo='6225098234235')
)temp 
on temp.AccountId=accountinfo.AccountId
where accountinfo.AccountId !=3;


-- 5.从所有账户信息中查询出余额最高的交易明细（存钱取钱信息）

select * from cardexchange where CardId in(
	select CardId from bankcard where AccountId = 
	(
		select AccountId from 
		(
			select AccountId,sum(CardMoney) as money from bankcard 
			GROUP BY AccountId
			ORDER BY money desc
			LIMIT 1
		)temp
	)
)

-- 6.查询有取款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额
SELECT bankcard.CardNo,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney from accountinfo 
inner join bankcard on bankcard.AccountId=accountinfo.AccountId
where bankcard.CardId in 
(
	select CardId from cardexchange where MoneyOutBank>0
)

-- 7.查询没有存款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额
select bankcard.CardNo,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney from accountinfo 
left join bankcard on bankcard.AccountId=accountinfo.AccountId
where bankcard.CardId  not in 
(
	select CardId from cardexchange where MoneyInBank>0
) or bankcard.CardId is null


-- 8.查询出没有转账交易记录的银行卡账户信息，显示卡号，身份证，姓名，余额
select bankcard.CardNo,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney 
from accountinfo
inner join bankcard on accountinfo.AccountId=bankcard.AccountId
where bankcard.CardNo not in
(
select CardIdOut from cardtransfer where TransferMoney > 0
)
and bankcard.CardId NOT IN 
( 
select CardIdIn from cardtransfer where TransferMoney > 0 
)

-- on
-- 9.查询所有余额超过100RMB的银行卡信息，包括账户名称
SELECT accountinfo.*,a.CardNo,a.总金额 FROM	accountinfo
INNER JOIN 
(
SELECT AccountId, CardNo, sum( CardMoney ) 总金额 FROM bankcard GROUP BY AccountId HAVING 总金额 > 100 
)
a ON accountinfo.AccountId = a.AccountId

-- HAVING

-- 10.查询交易次数（存取款）最多的银行卡账户信息，卡号，身份证，姓名，余额，交易次数
SELECT bankcard.CardNo 卡号,accountinfo.AccountCode 身份证,accountinfo.RealName 姓名,bankcard.CardMoney 余额,a.交易次数 
FROM accountinfo
INNER JOIN bankcard ON accountinfo.AccountId = bankcard.AccountId
INNER JOIN 
(
SELECT CardId, count( * ) 交易次数 FROM cardexchange GROUP BY CardId 
) a ON bankcard.CardId = a.CardId


