
-- select 
-- 1.查询所有用户信息以及卡数量
SELECT accountinfo.*, 
(
SELECT COUNT(*)  from bankcard where bankcard.AccountId=accountinfo.AccountId

)卡数量
from accountinfo




-- from 
-- 2.查询所有用户信息以及卡数量和总余额




SELECT accountinfo.*, 
(
SELECT COUNT(*) FROM bankcard where bankcard.AccountId=accountinfo.AccountId
)卡数量,
(
SELECT SUM(CardMoney) FROM bankcard WHERE bankcard.AccountId=accountinfo.AccountId
)总余额
FROM accountinfo


-- where 
-- 3.查询余额最高的账户


SELECT CardNo FROM bankcard WHERE CardMoney=(SELECT MAX(CardMoney) FROM bankcard)

-- 4.查询出其他储户余额比关羽某张银行卡多的银行卡号，显示卡号，身份证，姓名，余额

SELECT accountinfo.* FROM accountinfo inner JOIN 
(SELECT AccountId,CardMoney FROM bankcard WHERE CardMoney>
(SELECT CardMoney FROM bankcard WHERE CardNo='6225098234235')
GROUP BY AccountId)temp on accountinfo.AccountId=temp.AccountId
where accountinfo.AccountId!=3




-- 5.从所有账户信息中查询出余额最高的交易明细（存钱取钱信息）
SELECT * FROM cardexchange WHERE CardId in(

SELECT CardId FROM bankcard WHERE CardMoney>=
(SELECT MAX(CardMoney) FROM bankcard )
)



-- 6.查询有取款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额

SELECT bankcard.CardNo,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney FROM accountinfo inner join 
bankcard on bankcard.AccountId=accountinfo.AccountId
where  bankcard.CardId in
(SELECT CardId FROM cardexchange WHERE MoneyOutBank>0)
 

-- 7.查询没有存款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额

SELECT bankcard.CardNo,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney FROM accountinfo inner join 
 bankcard on bankcard.AccountId=accountinfo.AccountId
WHERE bankcard.CardId not in 
(SELECT CardId FROM cardexchange WHERE MoneyInBank>0)
or CardId is null


-- 8.查询出没有转账交易记录的银行卡账户信息，显示卡号，身份证，姓名，余额
SELECT CardNo,AccountCode,RealName,CardMoney
FROM accountinfo
inner JOIN 
(
SELECT *FROM bankcard WHERE CardId not  in (
SELECT CardIdIn FROM cardtransfer WHERE CardIdIn>0
)
and  CardId not in (


SELECT CardIdOut FROM cardtransfer WHERE CardIdOut>0
)temp on accountinfo.AccountId=temp.AccountId





-- on
-- 9.查询所有余额超过100RMB的银行卡信息，包括账户名称

SELECT temp.*, accountinfo.RealName FROM accountinfo
inner join 
(
SELECT * FROM bankcard WHERE CardMoney>100
)temp on accountinfo.AccountId=temp.AccountId



-- HAVING

-- 10.查询交易次数（存取款）最多的银行卡账户信息，卡号，身份证，姓名，余额，交易次数
SELECT bankcard.CardNo 卡号,accountinfo.AccountCode 身份证号码,accountinfo.RealName 姓名,bankcard.CardMoney 余额,abc.交易次数 FROM accountinfo

inner join bankcard on bankcard.AccountId=accountinfo.AccountId
inner join 
(SELECT CardId,count(*) 交易次数 FROM cardexchange GROUP BY CardId)abc on bankcard.CardId=abc.CardId


