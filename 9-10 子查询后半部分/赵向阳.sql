-- select 
-- 1.查询所有用户信息以及卡数量

select * ,
(
select count(*) from bankcard where bankcard.AccountId=accountinfo.AccountId
)temp
from accountinfo

-- from 
-- 2.查询所有用户信息以及卡数量和总余额
select * ,
(select  sum(CardMoney) from bankcard where bankcard.AccountId=accountinfo.AccountId),
(select count(*) from bankcard where bankcard.AccountId=accountinfo.AccountId)
from accountinfo
-- where 
-- 3.查询余额最高的账户

SELECT bankcard.CardNo from bankcard where CardMoney>=
(
select MAX(CardMoney) from bankcard
)

select * from 
(
		select AccountId,sum(CardMoney)总余额 
		from bankcard GROUP BY AccountId
	)temp
	where temp.总余额>=
	(
	select max(总余额) from 
	(
		select AccountId,sum(CardMoney)总余额 
		from bankcard GROUP BY AccountId
	)temp
)
-- 4.查询出其他储户余额比关羽6225098234235银行卡多的银行卡号，显示卡号，身份证，姓名，余额
SELECT * from accountinfo inner join 
(SELECT * from bankcard where CardMoney>(SELECT CardMoney from bankcard where CardNo='6225098234235'))temp
on accountinfo.AccountId=temp.AccountId
where accountinfo.AccountId!=3

-- 5.从所有账户信息中查询出余额最高的交易明细（存钱取钱信息）
select * from cardexchange where CardId in 
(
	select CardId from bankcard where CardMoney>=
	(
	 select max(CardMoney) from bankcard 
	)
)

-- 6.查询有取款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额
SELECT * from accountinfo inner join 
(SELECT AccountId, bankcard.CardNo from bankcard where CardId in
(SELECT CardId from cardexchange where cardexchange.MoneyOutBank>0))a 
on accountinfo.AccountId=a.AccountId

-- 7.查询没有存款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额

SELECT * from accountinfo inner join 
(SELECT AccountId, bankcard.CardNo from bankcard where CardId not  in
(SELECT CardId from cardexchange where cardexchange.MoneyInBank>0 ))a 
on accountinfo.AccountId=a.AccountId 


-- 8.查询出没有转账交易记录的银行卡账户信息，显示卡号，身份证，姓名，余额
SELECT * from accountinfo inner join 
(SELECT AccountId, bankcard.CardNo from bankcard where CardId  not in
(SELECT CardId from cardexchange where cardexchange.MoneyOutBank>0 and cardexchange.MoneyInBank>0))a 
on accountinfo.AccountId=a.AccountId

- on
-- 9.查询所有余额超过100RMB的银行卡信息，包括账户名称
select bankcard.CardNo 银行卡号, accountinfo.RealName 姓名, bankcard.CardMoney 余额 from  accountinfo
inner join bankcard on bankcard.AccountId=accountinfo.AccountId
where bankcard.CardMoney>100
-- HAVING

-- 10.查询交易次数（存取款）最多的银行卡账户信息，卡号，身份证，姓名，余额，交易次数
select bankcard.CardNo 卡号,accountinfo.AccountCode 身份证,accountinfo.RealName 姓名,bankcard.CardMoney 余额,a.交易次数 from accountinfo
	inner join bankcard on accountinfo.AccountId = bankcard.AccountId
	inner join ( select CardId, count( * ) 交易次数 from cardexchange group by CardId ) a on bankcard.CardId = a.CardId
