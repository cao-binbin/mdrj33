
-- 6.查询有取款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额
SELECT bankcard.CardId,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney FROM accountinfo
INNER JOIN bankcard
on accountinfo.AccountId=bankcard.AccountId
WHERE CardId  in
(SELECT CardId FROM cardexchange WHERE MoneyOutBank!=0)

-- 7.查询没有存款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额
SELECT bankcard.CardId,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney FROM accountinfo
INNER JOIN bankcard
on accountinfo.AccountId=bankcard.AccountId 
WHERE CardId NOT in
(SELECT CardId FROM cardexchange WHERE MoneyInBank!=0);


-- 8.查询出没有转账交易记录的银行卡账户信息，显示卡号，身份证，姓名，余额
SELECT bankcard.CardId,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney FROM accountinfo
INNER JOIN bankcard
on accountinfo.AccountId=bankcard.AccountId 
WHERE CardId NOT in
( SELECT cardtransfer.CardIdIn FROM cardtransfer)
and CardId NOT in
(SELECT cardtransfer.CardIdOut FROM cardtransfer)


-- on
-- 9.查询所有余额超过100RMB的银行卡信息，包括账户名称
SELECT accountinfo.* ,bankcard.CardNo ,bankcard.CardPwd,bankcard.CardMoney FROM accountinfo
INNER JOIN bankcard on accountinfo.AccountId = bankcard.AccountId
where bankcard.CardId in(SELECT CardId from bankcard WHERE CardMoney>100)


-- HAVING

-- 10.查询交易次数（存取款）最多的银行卡账户信息，卡号，身份证，姓名，余额，交易次数

SELECT temp2.*,temp3.交易次数 from 
(
	SELECT accountinfo.AccountCode,bankcard.CardNo ,accountinfo.RealName ,bankcard.CardMoney, bankcard.CardId FROM accountinfo
	INNER JOIN bankcard on accountinfo.AccountId = bankcard.AccountId
	where bankcard.CardId in
	(
		SELECT CardId FROM
		(
			SELECT CardId,  COUNT(*)  from cardexchange 
			GROUP BY CardId
			HAVING COUNT(*) =
			(SELECT   COUNT(*)  from cardexchange GROUP BY CardId LIMIT 1)
		) temp
	) 
) temp2
INNER JOIN 
(
	
				SELECT CardId,  COUNT(*) 交易次数  from cardexchange 
				GROUP BY CardId
				HAVING COUNT(*) =
				(SELECT   COUNT(*)  from cardexchange GROUP BY CardId LIMIT 1)
)temp3

on temp2.cardid =temp3.cardid












