
-- select 
-- 1.查询所有用户信息以及卡数量
SELECT *,
(
SELECT count(*) from bankcard where bankcard.AccountId=accountinfo.AccountId
)卡数量
from accountinfo 


SELECT *,
(
SELECT count(AccountId) from bankcard where bankcard.AccountId=accountinfo.AccountId
)卡数量
from accountinfo 


-- from 
-- 2.查询所有用户信息以及卡数量和总余额
SELECT *,
(
SELECT count(AccountId) from bankcard where bankcard.AccountId=accountinfo.AccountId
)卡数量,
(
SELECT sum(CardMoney) FROM bankcard where bankcard.AccountId=accountinfo.AccountId
)总金额
from accountinfo 


-- where 
-- 3.查询余额最高的账户
SELECT * FROM accountinfo where AccountId=
(
SELECT AccountId from 
(
SELECT AccountId,max(CardMoney) money from bankcard 
GROUP BY AccountId
ORDER BY money desc
LIMIT 1
)temp
);


-- 4.查询出其他储户余额比关羽某张银行卡多的银行卡号(6225098234235)，显示卡号，身份证，姓名，余额
SELECT AccountId,CardMoney FROM bankcard where CardNo='6225098234235'

SELECT * from bankcard where CardMoney>
(
SELECT CardMoney FROM bankcard where CardNo='6225098234235'
)

SELECT AccountCode,RealName,CardMoney FROM accountinfo 
inner join 
(
SELECT AccountId,CardMoney from bankcard where CardMoney>
(SELECT CardMoney FROM bankcard where CardNo='6225098234235') 
)temp  on accountinfo.AccountId=temp.AccountId 


-- 5.从所有账户信息中查询出余额最高的交易明细（存钱取钱信息）
SELECT * FROM cardexchange where CardId in
(
SELECT CardId from bankcard WHERE CardMoney=
(SELECT MAX(CardMoney) FROM bankcard)
)


-- 6.查询有取款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额

SELECT CardNo,AccountCode,RealName,CardMoney from accountinfo 
inner join bankcard on accountinfo.AccountId=bankcard.AccountId
where bankcard.CardId in (SELECT CardId from cardexchange where MoneyOutBank>0)



-- 7.查询没有存款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额
SELECT CardNo,AccountCode,RealName,CardMoney from accountinfo 
inner join bankcard on accountinfo.AccountId=bankcard.AccountId
where bankcard.CardId not in (SELECT CardId from cardexchange where MoneyOutBank>0)
or bankcard.CardId is null

-- 8.查询出没有转账交易记录的银行卡账户信息，显示卡号，身份证，姓名，余额
SELECT * from cardtransfer;

SELECT CardNo,AccountCode,RealName,CardMoney from accountinfo 
inner join bankcard on accountinfo.AccountId=bankcard.AccountId
where CardId not in
(SELECT CardIdIn from cardtransfer)
and CardId not in (SELECT CardIdIn from cardtransfer)

-- on
-- 9.查询所有余额超过100RMB的银行卡信息，包括账户名称
SELECT * from accountinfo 
inner join (select AccountId,sum(CardMoney) from bankcard GROUP BY AccountId
HAVING sum(CardMoney)>100)temp on accountinfo.AccountId=temp.AccountId

-- HAVING

-- 10.查询交易次数（存取款）最多的银行卡账户信息，卡号，身份证，姓名，余额，交易次数

SELECT bankcard.CardNo,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney from accountinfo
inner join bankcard on accountinfo.AccountId=bankcard.AccountId
inner join (SELECT CardId,count(*) from cardexchange GROUP BY CardId)temp
on bankcard.CardId=temp.CardId
where bankcard.CardId=
(
SELECT CardId from (SELECT CardId,count(*) from cardexchange GROUP BY
CardId ORDER BY count(*) desc LIMIT 1)tmm
)

