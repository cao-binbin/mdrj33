-- select 
-- 1.查询所有用户信息以及卡数量


select *,
(
select  count(*) from bankcard where bankcard.AccountId=accountinfo.AccountId
)卡数量
from accountinfo


-- from 
-- 2.查询所有用户信息以及卡数量和总余额
select  *,
(
	select  count(*) from bankcard where bankcard.AccountId=accountinfo.AccountId
)卡数量,
(
	select  sum(CardMoney) from bankcard where bankcard.AccountId=accountinfo.AccountId
)总余额
from accountinfo;

-- where 
-- 3.查询余额最高的账户
select CardNo from  bankcard WHERE CardMoney=(select max(CardMoney) from bankcard);
select  * from bankcard


-- 4.查询出其他储户余额比关羽某张银行卡多的银行卡号，显示卡号，身份证，姓名，余额
select  accountinfo.*,temp.CardMoney from accountinfo
inner join 
(	select  AccountId,CardMoney from bankcard 
	where CardMoney>
	(select  CardMoney from bankcard where CardNo='6225098234235')
)temp 
on temp.AccountId=accountinfo.AccountId
where accountinfo.AccountId !=3;


-- 5.从所有账户信息中查询出余额最高的交易明细（存钱取钱信息）
select * from cardexchange where CardId in(
	select CardId from bankcard where AccountId = 
	(
		select AccountId from 
		(
			select AccountId,sum(CardMoney) as money from bankcard 
			GROUP BY AccountId
			ORDER BY money desc
			LIMIT 1
		)shuai
	)
)
-- 6.查询有取款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额
select bankcard.CardNo,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney from accountinfo
left join bankcard on bankcard.AccountId=accountinfo.AccountId
where bankcard.CardId in (select cardexchange.CardId from cardexchange where cardexchange.MoneyOutBank>0)
-- 7.查询没有存款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额

select * from accountinfo 
inner JOIN bankcard on accountinfo.AccountId=bankcard.Accountid
where CardId  not in 
(
select CardId from cardexchange where MoneyInBank >0
)
-- 8.查询出没有转账交易记录的银行卡账户信息，显示卡号，身份证，姓名，余额

select a.*,temp.CardMoney from accountinfo as a
inner JOIN
(
		select accountid,CardMoney from bankcard where Cardid in
		(
		select CardIdOut from cardtransfer
		)or Cardid in(
		select CardIdIn from cardtransfer
		)
	)temp on temp.accountid=a.accountid
	-- 9.查询所有余额超过100RMB的银行卡信息，包括账户名称
select * from accountinfo as a inner join
(
select * from bankcard where CardMoney >100
)temp on temp.accountid=a.accountid;
- 10.查询交易次数（存取款）最多的银行卡账户信息，卡号，身份证，姓名，余额，交易次数
select bankcard.CardNo 卡号,accountinfo.AccountCode 身份证,accountinfo.RealName 姓名,bankcard.CardMoney 余额 from bankcard 
left join (select cardexchange.CardId CardId,count(*) sum from cardexchange group by cardexchange.CardId )ss
on bankcard.CardId=ss.CardId
inner join accountinfo on accountinfo.AccountId=bankcard.AccountId
where sum=ss.sum