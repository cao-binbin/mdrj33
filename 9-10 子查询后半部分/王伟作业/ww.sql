
-- select 
-- 1.查询所有用户信息以及卡数量
select * from accountinfo;
select * from bankcard;
select accountinfo.*,
(select count(*) from bankcard where bankcard.AccountId=accountinfo.AccountId)tm
from accountinfo;

-- from 
-- 2.查询所有用户信息以及卡数量和总余额
select * from accountinfo INNER JOIN
(select AccountId,COUNT(*)卡数量, SUM(CardMoney)总余额 from bankcard GROUP BY AccountId) tm
on accountinfo.AccountId=tm.AccountId;

-- where 
-- 3.查询余额最高的账
select * from accountinfo where AccountId=(
select AccountId from 
(select AccountId,SUM(CardMoney) from bankcard GROUP BY AccountId
ORDER BY SUM(CardMoney) desc LIMIT 1)g);
-- 4.查询出其他储户余额比关羽某张银行卡多的银行卡号6225098234235，显示卡号，身份证，姓名，余额
SELECT accountinfo.*,temp.CardMoney from accountinfo
inner join 
(	SELECT AccountId,CardMoney from bankcard where CardMoney>
(SELECT CardMoney from bankcard where CardNo='6225098234235')
)temp on temp.AccountId=accountinfo.AccountId
where accountinfo.AccountId !=3;


-- 5.从所有账户信息中查询出余额最高的交易明细（存钱取钱信息）
select * from cardexchange where CardId in
(select CardId from bankcard where AccountId=(
select AccountId from 
(select AccountId,SUM(CardMoney) from bankcard GROUP BY AccountId
ORDER BY SUM(CardMoney) desc LIMIT 1)g));

-- 6.查询有取款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额
select * from bankcard;
select * from cardexchange;
select * from accountinfo;
select bankcard.CardNo,accountinfo.AccountCode,accountinfo.RealName,ba  kcard.CardMoney from accountinfo INNER JOIN
bankcard on accountinfo.AccountId=bankcard.AccountId 
where CardId in (
select CardId from cardexchange where MoneyOutBank>0);
-- 7.查询没有存款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额
select bankcard.CardNo,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney from accountinfo INNER JOIN
bankcard on accountinfo.AccountId=bankcard.AccountId 
where CardId not in (
select CardId from cardexchange where MoneyOutBank>0);


-- 8.查询出没有转账交易记录的银行卡账户信息，显示卡号，身份证，姓名，余额
select *  from cardtransfer;
select bankcard.CardNo,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney from accountinfo INNER JOIN
bankcard on accountinfo.AccountId=bankcard.AccountId 
where CardId not in (
select CardIdIn from cardtransfer) AND
CardId not in (select CardIdOut from cardtransfer);


-- on
-- 9.查询所有余额超过100RMB的银行卡信息，包括账户名称
select * FROM accountinfo INNER JOIN
(select AccountId,SUM(CardMoney) from bankcard GROUP BY AccountId
HAVING SUM(CardMoney)>100)tm on accountinfo.AccountId=tm.AccountId;

-- HAVING

-- 10.查询交易次数（存取款）最多的银行卡账户信息，卡号，身份证，姓名，余额，交易次数
select bankcard.CardNo,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney
from accountinfo 
INNER JOIN bankcard on accountinfo.AccountId=bankcard.AccountId 
inner join (select CardId , count(*) from cardexchange GROUP BY 
CardId)tm on bankcard.CardId=tm.CardId
where bankcard.CardId =
(select CardId from (select CardId , count(*) from cardexchange GROUP BY 
CardId order by count(*) desc limit 1)tmm)
