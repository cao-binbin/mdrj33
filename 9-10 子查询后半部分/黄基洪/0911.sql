-- 1.查询"01"课程比"02"课程成绩高的学生的信息及课程分数
 -- 1.查询"01"课程比"02"课程成绩高的学生的信息及课程分数
select student.*,class1 成绩1,class2 成绩2 from student
right JOIN 
(select t1.sid,class1,class2 from 
(select sid,score as class1 from sc where sc.cid='01') as t1,
(SELECT sid,score as class2 from sc where sc.cid='02') as t2
where t1.sid=t2.sid and t1.class1 > t2.class2
) t3 on t3.sid=student.sid
select student.*,t3.class1 课程1分数,t3.class2 课程2分数 from student
RIGHT JOIN
(
	select t1.sid,class1,class2 from 
	(select sid,score as class1 from sc where sc.cid='01') as t1,
	(SELECT sid,score as class2 from  sc where sc.cid='02') as t2
	where t1.sid=t2.sid and t1.class1 > t2.class2
)t3 on t3.sid=student.sid;


-- 1.1 查询存在" 01 "课程但可能不存在" 02 "课程的情况(不存在时显示为 null )
select * from 
(select * from sc where sc.cid='01') t1
left JOIN
(SELECT * from sc where sc.cid='02') t2
on t1.sid=t2.sid



-- 1.2 查询同时存在01和02课程的情况
select * from 
(select * from sc where sc.cid='01') t1
inner JOIN
(SELECT * from sc where sc.cid='02') t2
on t1.sid=t2.sid


-- 1.3 查询选择了02课程但没有01课程的情况
-- 没有 null 一般来说就是左查询或者是右查询 left join right join
select * from
(select * from sc where sc.cid='02') t1
left JOIN
(select * from sc where sc.cid='01') t2
on t1.sid=t2.sid

-- 跟上面一样 通过左连接来判断为Null的字段 left join

-- 2.查询平均成绩大于等于 60 分的同学的学生编号和学生姓名和平均成绩
-- 1.平均成绩大于等于60 2.学生编号和姓名 还有平均成绩 avg(列名) 

	-- avg(score)>=60 sid sc是存在三个 所有说肯定会有分组查询 GROUP BY sid
	select sc.sid 学生编号,sname 学生姓名,avg(score) 平均成绩 from student
	inner join sc on sc.sid=student.sid
	GROUP BY sc.sid
	HAVING AVG(score) >=60

	
	-- 通过两张表连接 在通过avg输出平均成绩 在通过having来输出条件判断是否正确 排列学号进行输出

-- 3.查询在 SC 表存在成绩的学生信息
	-- distinct 防止重复
	SELECT DISTINCT student.* from sc,student
	where sc.sid=student.sid


-- 4.查询所有同学的学生编号、学生姓名、选课总数、所有课程的成绩总和
	-- 通过两张表连接来算出 
		SELECT sc.sid 学生编号,sname 学生姓名,count(*) 选课总数,SUM(score) 成绩总和 from student
		inner join sc where sc.sid=student.sid
		GROUP BY sc.sid
	
-- 5.查询「李」姓老师的数量
	-- 老师 teacher 数量 COUNT(*) 李姓 % LIKE 模糊查询
	select count(*) 李姓老师的数量 from teacher where tname LIKE '李%'

-- 6.查询学过「张三」老师授课的同学的信息
-- 同学信息 student 张三老师授课 teacher 三表连接查询 
SELECT student.* from student
inner join sc on sc.sid=student.sid
inner join course on course.cid=sc.cid
inner join teacher on teacher.tid=course.tid
where teacher.tname='张三'


select * from course
inner join sc where 



-- 7.查询没有学全所有课程的同学的信息
-- 没有学全<3 student sc
	select student.*,count(cid) 选课数量 from sc
	RIGHT JOIN student on student.sid=sc.sid
	GROUP BY sc.sid
	HAVING COUNT(cid) < (SELECT count(*) from course )
	-- 通过having 条件 还有count函数 right join 来进行完成的 没有学全 通过course 来判断课程数量 在通过<所求出数量来判断是否完成题目要求



select student.*,COUNT(cid) 课程数量 from sc RIGHT JOIN student
on student.sid=sc.sid
group by sc.sid
HAVING count(cid)<( 
select count(cid) from course
)




-- 8.查询至少有一门课与学号为" 01 "的同学所学相同的同学的信息
	-- select * from sc where sc.sid='01' student sc 两张表 inner join
	select * from sc where sc.sid='01'
	
	SELECT DISTINCT student.* from student 
	inner join sc on sc.sid=student.sid
	where sc.cid in
	(
	select cid from sc where sc.sid='01'
	)and student.sid !='01'

	
	
	


SELECT s.*
FROM sc INNER JOIN student AS s
ON sc.sid = s.sid
WHERE sc.cid IN (SELECT cid
FROM sc
WHERE sid = '01') AND sc.sid != '01'
GROUP BY sc.sid;

select cid from sc where sid='01'