
-- 2.查询平均成绩大于等于 60 分的同学的学生编号和学生姓名和平均成绩
 

Select s1.sid 学生编号,s1.sname 学生名字,sc2.cj2 平均成绩 
from student s1 inner join (Select sid,ROUND(avg(score),2) as cj2 
from  sc  GROUP BY sid HAVING avg(score) >60 ) sc2 
on s1.sid=sc2.sid ;
-- 3.查询在 SC 表存在成绩的学生信息

Select * from student where sid in (Select DISTINCT sid from sc );

-- 4.查询所有同学的学生编号、学生姓名、选课总数、所有课程的成绩总和

Select sc1.sid 学生编号,sc1.sname 学生名字,sc2.选课总数,sc2.成绩总和 from student as sc1 

inner join (Select sid,count(cid) 选课总数,sum(score) 成绩总和 from sc  GROUP BY sid ) as sc2 

on sc1.sid=sc2.sid;




-- 5.查询「李」姓老师的数量

select COUNT(*) 李老师数量 from teacher where tname LIKE"李%";

-- 6.查询学过「张三」老师授课的同学的信息

Select * from student where sid in

(Select sid from  sc where cid in 

				(Select cid from course where tid in 

						(Select tid from teacher where tname='张三') 
				)
)

-- 7.查询没有学全所有课程的同学的信息

Select * from student as sc1 inner join (Select sid,COUNT(cid) as sl2 
from sc  GROUP BY sid  HAVING sl2<3) as sc2
on sc1.sid=sc2.sid

-- 8.查询至少有一门课与学号为" 01 "的同学所学相同的同学的信息

Select * from student 
where sid in (Select DISTINCT sid from sc  where cid  in (Select cid from sc where sid='01'))


-- 9.查询和" 01 "号的同学学习的课程完全相同的其他同学的信息

SELECT student.*,sc1.cid ,sc2.cid ,sc3.cid from student 
LEFT join sc sc1 on  student.sid=sc1.sid and sc1.cid='01' 
LEFT join sc sc2 on  student.sid=sc2.sid and sc2.cid='02' 
LEFT join sc sc3 on  student.sid=sc3.sid and sc3.cid='03' 
where student.sid !='02'
and IFNULL(sc1.cid,0) =  IFNULL((SELECT cid from sc where sid='02' and cid='01'),0)
and IFNULL(sc2.cid,0) =  IFNULL((SELECT cid from sc where sid='02' and cid='02'),0)
and IFNULL(sc3.cid,0) =  IFNULL((SELECT cid from sc where sid='02' and cid='03'),0)



-- 10.查询没学过"张三"老师讲授的任一门课程的学生姓名

Select * from student where sid in (Select sid from sc  where cid not in (Select cid from course where tid IN (Select tid from teacher where tname='张三')))

-- 11.查询两门及其以上不及格课程的同学的学号，姓名及其平均成绩


Select student.sid,student.sname,sc2.avg1 as 平均成绩 ,sc2.s1 不及格课程数量

from student

inner join

(Select sid,COUNT(cid) s1,ROUND(avg(score) ,2)as avg1 from  sc where score<60 GROUP BY sid HAVING COUNT(cid)>=2) as sc2

on student.sid=sc2.sid 


-- 2
	select sid,sname,
	(
		SELECT ROUND(AVG(score),1) from sc where sc.sid =student.sid
	)平均成绩
	from student where sid in
	(
		SELECT sid from sc where score<60
		GROUP BY sid
		HAVING count(*)>=2
	)




-- 12.检索" 01 "课程分数小于 60，按分数降序排列的学生信息

Select *,sc1.score from student inner join (Select sid,score from sc where cid='01' and score<60 )  as sc1
on student.sid=sc1.sid  ORDER BY score 

-- 13.按平均成绩从高到低显示所有学生的所有课程的成绩以及平均成绩
Select *, ROUND(avg(score),2) av1 from  sc GROUP BY sid ORDER BY av1 DESC

-- 14.查询各科成绩最高分、最低分和平均分,以如下形式显示：
-- 课程 ID，课程 name，最高分，最低分，平均分，及格率，中等率，优良率，优秀率
-- 及格为>=60，中等为：70-80，优良为：80-90，优秀为：>=90
-- 要求输出课程号和选修人数，查询结果按人数降序排列，若人数相同，按课程号升序

Select*
from course INNER JOIN

(Select cid,max(score) 最高分,min(score) 最低分,avg(score) 平均分,
(count(IF(score>=60,score,null))/count(*)) 及格率,
(count(IF(score>=70 and score<=80,score,NULL))/COUNT(*)) 中等率,
(COUNT(IF(score>=80 and score<-90,score,NULL))/COUNT(*)) 优良率,
(COUNT(IF(score>=90,score,NULL))/COUNT(*)) 优秀率

from sc  GROUP BY cid) as ts1  

on course.cid=ts1.cid




-- 15.按各科成绩进行排序，并显示排名， Score 重复时保留名次空缺
     

SELECT sc1.sid,sc1.cid,sc1.score,count(sc2.score)+1 名次 from sc sc1
left join sc sc2 on sc1.sid != sc2.sid and sc1.cid=sc2.cid and sc2.score>sc1.score
GROUP BY sc1.sid,sc1.cid,sc1.score
order by sc1.cid asc,名次 asc;

-- 15.1 按各科成绩进行行排序，并显示排名， Score 重复时合并名次
SELECT sc1.sid,sc1.cid,sc1.score,count(DISTINCT sc2.score)+1 名次 from sc sc1
left join sc sc2 on sc1.sid != sc2.sid and sc1.cid=sc2.cid and sc2.score>sc1.score
GROUP BY sc1.sid,sc1.cid,sc1.score
order by sc1.cid asc,名次 asc;

-- 17 统计各科成绩和各分数阶段人数，课程编号，课程名称[100-85] [85-70] [ 70-60] [60-0] 的所占百分比



-- 18.查询各科成绩前三名的记录

Select * from (Select *,(
Select COUNT(score)+1 from sc t1 where t1.cid=sc.sid 

and t1.score>sc.score

)  名次 from sc ORDER BY cid asc,名次 asc)as temp
where temp.名次<=3



-- 19.查询每门课程被选修的学生数

Select *,(
Select count(*) from sc where sc.cid=course.cid
) 数量
from course  


-- 20.查询出只选修两门课程的学生学号和姓名
Select * from student where sid in( Select sid from sc GROUP BY sid HAVING COUNT(*)=2);

-- 21. 查询男生、女生人数

Select COUNT(sid) as 男生人数,(Select COUNT(sid) from student where  ssex='女') as 女生人数 from student where  ssex='男'

-- 22. 查询名字中含有「风」字的学生信息

Select * from student where sid in(Select sid from student where sname LIKE'%风%');


-- 23查询同名同性学生名单，并统计同名人数
Select sname,COUNT(sname) 同名同姓两人 from student  GROUP BY sname HAVING COUNT(sname)>1;


-- 24.查询 1990 年出生的学生名单

Select * from student where sage like'1990%';

Select * From 	student where YEAR(sage)=1990;

-- 25.查询每门课程的平均成绩，结果按平均成绩降序排列，平均成绩相同时，按课程编号升序排列
Select cid,Avg(score) as 平均成绩 from  sc  GROUP BY  cid ORDER BY 平均成绩 desc,cid asc

-- 26.查询平均成绩大于等于 85 的所有学生的学号、姓名和平均成绩
Select sid,Avg(score) 平均成绩 from sc GROUP BY sid HAVING 平均成绩>85



-- 27.查询课程名称为「数学」，且分数低于 60 的学生姓名和分数


Select * from student sc1 inner join (Select sid,score from sc where score<60 and cid in(Select cid from course where cname='数学')) as sc2 on sc1.sid=sc2.sid 



