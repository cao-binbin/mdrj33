-- 6.查询有取款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额

Select bankcard.CardNo,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney from bankcard inner join cardexchange on bankcard.CardId=cardexchange.CardId inner JOIN accountinfo on accountinfo.AccountId=bankcard.AccountId
where cardexchange.MoneyOutBank>'0'


-- 7.查询没有存款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额


-- 1
Select temp1.CardNo,accountinfo.AccountCode,accountinfo.RealName,temp1.CardMoney from accountinfo 

left join (Select * from bankcard where CardId not in (Select CardId from cardexchange where MoneyInBank>0) )as temp1 o

n accountinfo.AccountId=temp1.AccountId 


-- 2 
Select * from accountinfo left join bankcard on accountinfo.AccountId=bankcard.AccountId
where bankcard.CardId not in 
(
Select CardId from  cardexchange where MoneyInBank>0 
) or bankcard.CardId is null


-- 8.查询出没有转账交易记录的银行卡账户信息，显示卡号，身份证，姓名，余额


Select temp12.CardNo,accountinfo.AccountCode,accountinfo.RealName,temp12.CardMoney
from accountinfo 
LEFT JOIN (Select * from bankcard  where CardId not in (Select CardIdOut from cardtransfer where CardIdOut>=1)

and CardId not in(Select CardIdIn from cardtransfer where CardIdIn>=1) or bankcard.CardId is NULL) as temp12

on accountinfo.AccountId=temp12.AccountId


-- on
-- 9.查询所有余额超过100RMB的银行卡信息，包括账户名称



Select * from bankcard inner join (Select *,

(Select sum(CardMoney) from bankcard where bankcard.AccountId=accountinfo.AccountId) as a2 

from accountinfo ) as temp13  on bankcard.AccountId=temp13.AccountId where temp13.a2>100




Select bankcard.AccountId,temp14.car1,temp14.car2 from bankcard inner join ( Select accountinfo.AccountId,accountinfo.RealName, 

(select count(*) from bankcard where bankcard.AccountId=accountinfo.AccountId ) as car1,

(select sum(CardMoney) from bankcard where bankcard.AccountId=accountinfo.AccountId) as car2  

from accountinfo ) as temp14 on bankcard.AccountId=temp14.AccountId


-- HAVING

-- 10.查询交易次数（存取款）最多的银行卡账户信息，卡号，身份证，姓名，余额，交易次数


Select bankcard.CardNo,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney,temp16.max1 as 交易次数 from bankcard inner join (select CardId,count(CardId )as max1  from cardexchange group by CardId ORDER BY max(max1) desc limit 1) as temp16 on bankcard.CardId=temp16.CardId inner join accountinfo on bankcard.AccountId=accountinfo.AccountId

