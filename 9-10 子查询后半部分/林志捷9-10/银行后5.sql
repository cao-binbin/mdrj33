-- 6.查询有取款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额
select a.*,temp.CardMoney from accountinfo as a
inner join (
				select accountid,CardMoney from bankcard where Cardid in
								(select Cardid from cardexchange where MoneyOutBank!=0)
	)temp on temp.accountid=a.accountid;

-- 7.查询没有存款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额

select * from cardexchange where MoneyInBank>0 -- 有存款记录

select temp.cardno,a.accountcode,a.realname,temp.CardMoney from accountinfo as a
inner join(
select accountid,CardMoney from bankcard where Cardid not in 
			(
			select Cardid from cardexchange where MoneyInBank>0
				)
)temp on temp.accountid=a.accountid;

-- 8.查询出没有转账交易记录的银行卡账户信息，显示卡号，身份证，姓名，余额
select a.*,temp.CardMoney from accountinfo as a
inner JOIN(
		select accountid,CardMoney from bankcard where Cardid in
		(
		select CardIdOut from cardtransfer
 		)or 
		Cardid in
		(
		select CardIdIn from cardtransfer
		)
	)temp on temp.accountid=a.accountid
-- on
-- 9.查询所有余额超过100RMB的银行卡信息，包括账户名称

select * from accountinfo as a inner join
(
select * from bankcard where CardMoney >100
)temp on temp.accountid=a.accountid;

-- HAVING

-- 10.查询交易次数（存取款）最多的银行卡账户信息，卡号，身份证，姓名，余额，交易次数
select a.*,num.cardmoney,num.交易次数 from accountinfo as a
inner join
(
		select b.*,temp.交易次数 from bankcard as b
		inner join(
		select cardid,count(cardid)交易次数 from cardexchange
		group by cardid
		having count(cardid)>=(
										select count(cardid) from cardexchange
										group by cardid
										limit 1)
		)temp on temp.cardid=b.cardid
)num on num.accountid=a.accountid
;