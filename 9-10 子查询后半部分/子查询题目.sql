use ds;
-- select 
-- 1.查询所有用户信息以及卡数量
select accountinfo.* ,
(select count(*) from bankcard where accountinfo.AccountId=bankcard.AccountId) 
from accountinfo ;


select accountinfo.*,ss.`卡数` 卡数量 from accountinfo
inner join (select bankcard.AccountId,count(*) 卡数 from bankcard group by bankcard.AccountId) ss
on ss.AccountId=accountinfo.AccountId;

-- from 
-- 2.查询所有用户信息以及卡数量和总余额
select accountinfo.*,ss.`卡数量`,zz.`总余额` from accountinfo
inner join (select bankcard.AccountId,count(*) 卡数量 from bankcard group by bankcard.AccountId) ss
on ss.AccountId=accountinfo.AccountId
inner join (select bankcard.AccountId,sum(bankcard.CardMoney) 总余额 from bankcard group by bankcard.AccountId) zz
on accountinfo.AccountId=zz.AccountId;

-- where 
-- 3.查询余额最高的账户
-- 一张卡的
select * from bankcard where bankcard.CardMoney=(select max(bankcard.CardMoney) from bankcard);

-- 总余额的
select ss.AccountId, max(ss.`总余额`) 
from (select bankcard.AccountId AccountId,sum(bankcard.CardMoney) 总余额 from bankcard group by bankcard.AccountId) ss;


-- 4.查询出其他储户余额比关羽余额第二多的银行卡多的银行卡号，显示卡号，身份证，姓名，余额
select bankcard.CardNo 卡号,accountinfo.AccountCode 身份证,accountinfo.AccountCode 姓名,bankcard.CardMoney 余额 from accountinfo
inner join bankcard on accountinfo.AccountId=bankcard.AccountId
where bankcard.CardMoney>
(
  select s.CardMoney from
 (
    select * from bankcard 
    where bankcard.AccountId in 
    (
      select accountinfo.AccountId from accountinfo where accountinfo.RealName like '关羽'
    )
    order by bankcard.CardMoney desc
    limit 2
  ) s
    order by s.CardMoney ASC
    limit 1
)
and bankcard.AccountId!=(select accountinfo.AccountId from accountinfo where accountinfo.RealName like '关羽');



-- 5.从所有账户信息中查询出余额最高的交易明细（存钱取钱信息）
select * from cardexchange;
select * from cardexchange where cardexchange.CardId in
(
  select bankcard.AccountId from bankcard where bankcard.CardMoney=(select max(bankcard.CardMoney) from bankcard)
);

-- 6.查询有取款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额
select bankcard.CardNo,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney from accountinfo
left join bankcard on bankcard.AccountId=accountinfo.AccountId
where bankcard.CardId in (select cardexchange.CardId from cardexchange where cardexchange.MoneyOutBank>0)


-- 7.查询没有存款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额
select bankcard.CardNo,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney from accountinfo
left join bankcard on accountinfo.AccountId=bankcard.AccountId
where bankcard.CardId 
not in(select cardexchange.CardId from cardexchange where cardexchange.MoneyInBank>0)
or bankcard.CardMoney is NULL;


-- 8.查询出没有转账交易记录的银行卡账户信息，显示卡号，身份证，姓名，余额
select * from accountinfo where accountinfo.AccountId not in
(select cardtransfer.CardIdIn from cardtransfer)and
accountinfo.AccountId not in (select cardtransfer.CardIdOut from cardtransfer );



-- on
-- 9.查询所有余额超过100RMB的银行卡信息，包括账户名称
select accountinfo.RealName,bankcard.* from bankcard 
right join accountinfo on bankcard.AccountId=accountinfo.AccountId
where bankcard.CardMoney>100;

-- HAVING

-- 10.查询交易次数（存取款）最多的银行卡账户信息，卡号，身份证，姓名，余额，交易次数


select bankcard.CardNo 卡号,accountinfo.AccountCode 身份证,accountinfo.RealName 姓名,bankcard.CardMoney 余额 from bankcard 
left join (select cardexchange.CardId CardId,count(*) sum from cardexchange group by cardexchange.CardId )ss
on bankcard.CardId=ss.CardId
inner join accountinfo on accountinfo.AccountId=bankcard.AccountId
where sum=ss.sum















