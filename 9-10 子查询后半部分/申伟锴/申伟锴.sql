
-- select 
-- 1.查询所有用户信息以及卡数量
select *,(
select count(*) from bankcard where accountinfo.AccountId=bankcard.AccountId
) from accountinfo


-- from 
-- 2.查询所有用户信息以及卡数量和总余额
select *,(
select count(*) from bankcard where accountinfo.AccountId=bankcard.AccountId
) 卡数量,(select sum(CardMoney) from bankcard where accountinfo.AccountId=bankcard.AccountId
) 总金额
from accountinfo

-- where 
-- 3.查询余额最高的账户
select CardNo from bankcard where CardMoney=(select max(CardMoney) from bankcard )
-- 4.查询出其他储户余额比关羽某张银行卡多的银行卡号，显示卡号，身份证，姓名，余额
select accountinfo.*,ren.CardMoney from accountinfo
inner join
(select AccountId,CardMoney from bankcard where CardMoney>
(select CardMoney from bankcard where CardNo='6225098234235'))ren
on ren.AccountId=accountinfo.AccountId where accountinfo.AccountId !=3;


-- 5.从所有账户信息中查询出余额最高的交易明细（存钱取钱信息）
select * from cardexchange where CardId in 
(
select CardId from bankcard where CardMoney>=
	(
	 select max(CardMoney) from bankcard 
	)
)

-- 6.查询有取款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额
select bankcard.CardNo,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney from accountinfo
inner join bankcard on bankcard.AccountId=accountinfo.AccountId
where bankcard.CardId in
(select CardId from cardexchange where MoneyInBank)

-- 7.查询没有存款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额
select  bankcard.CardNo,accountinfo.AccountCode,accountinfo.RealName,bankcard.CardMoney from accountinfo 
left join bankcard on bankcard.AccountId=accountinfo.AccountId
where bankcard.CardId  not in 
(
	select CardId from cardexchange where MoneyInBank>0
) or bankcard.CardId is null


-- 8.查询出没有转账交易记录的银行卡账户信息，显示卡号，身份证，姓名，余额
select 
	bankcard.CardNo,
	accountinfo.AccountCode,
	accountinfo.RealName,
	bankcard.CardMoney 
from 
	accountinfo
	inner join bankcard on accountinfo.AccountId = bankcard.AccountId
where 
	bankcard.CardId not in ( select  CardIdOut from  cardtransfer where  TransferMoney > 0 ) 
	AND bankcard.CardId not in ( select  CardIdIn from cardtransfer where TransferMoney > 0 );


-- on
-- 9.查询所有余额超过100RMB的银行卡信息，包括账户名称
select 
	accountinfo.*,
	a.CardNo,
	a.总金额 
from
	accountinfo
	inner join (select  AccountId, CardNo, sum( CardMoney ) 总金额 from bankcard group by AccountId having 总金额 > 100 ) a on accountinfo.AccountId = a.AccountId;

-- HAVING

-- 10.查询交易次数（存取款）最多的银行卡账户信息，卡号，身份证，姓名，余额，交易次数
select 
	bankcard.CardNo 卡号,
	accountinfo.AccountCode 身份证,
	accountinfo.RealName 姓名,
	bankcard.CardMoney 余额,
	a.交易次数 
from
	accountinfo
	inner join bankcard on accountinfo.AccountId = bankcard.AccountId
	inner join (select CardId, count( * ) 交易次数 from cardexchange group by CardId ) a on bankcard.CardId = a.CardId

