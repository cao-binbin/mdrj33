
-- select 
-- 1.查询所有用户信息以及卡数量

SELECT COUNT(*) 卡数量,accountinfo.* from bankcard inner JOIN accountinfo 
on accountinfo.AccountId=bankcard.AccountId
GROUP BY AccountId


-- from 
-- 2.查询所有用户信息以及卡数量和总余额

SELECT accountinfo.*,SUM(CardMoney) 总余额,COUNT(*) 卡数量 from bankcard inner JOIN accountinfo 
on accountinfo.AccountId=bankcard.AccountId
GROUP BY AccountId

-- where 
-- 3.查询余额最高的账户
select CardId,CardNo from bankcard
where CardMoney in
(
	select MAX(CardMoney) from bankcard
)

-- 4.查询出其他储户余额比关羽某张银行卡多的银行卡号(6225098234235)，显示卡号，身份证，姓名，余额
select AccountCode,AccountPhone,RealName,CardMoney from accountinfo inner join bankcard 
on accountinfo.AccountId=bankcard.AccountId
where bankcard.CardNo in (
SELECT CardNo from bankcard 
where CardMoney>(select CardMoney from bankcard where CardNo='6225098234235'))


-- 5.从所有账户信息中查询出余额最高的交易明细（存钱取钱信息）
SELECT * from cardexchange WHERE CardId in(
SELECT CardId from bankcard WHERE CardMoney>=(
SELECT max(CardMoney) from bankcard))



-- 6.查询有取款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额
SELECT accountinfo.*,tep.CardMoney from accountinfo inner join (
SELECT * from bankcard WHERE CardId in(
SELECT CardId FROM cardexchange WHERE MoneyOutBank>0)) tep on accountinfo.AccountId=tep.AccountId
 

-- 7.查询没有存款记录的银行卡及账户信息，显示卡号，身份证，姓名，姓名余额
SELECT accountinfo.*,tep.CardMoney from accountinfo inner join (
SELECT * from bankcard WHERE CardId in(
SELECT CardId FROM cardexchange WHERE MoneyInBank=0) or CardId is NULL) tep on accountinfo.AccountId=tep.AccountId

-- 8.查询出没有转账交易记录的银行卡账户信息，显示卡号，身份证，姓名，余额

SELECT accountinfo.*,tep.CardMoney from accountinfo inner join (
SELECT * from bankcard WHERE CardId not in(
SELECT CardIdIn from cardtransfer WHERE TransferMoney>0) or CardId is NULL) tep
on accountinfo.AccountId=tep.AccountId

-- on
-- 9.查询所有余额超过100RMB的银行卡信息，包括账户名称
SELECT * from accountinfo inner join (
SELECT * from bankcard where CardMoney>100) tep on accountinfo.AccountId=tep.AccountId
-- HAVING

-- 10.查询交易次数（存取款）最多的银行卡账户信息，卡号，身份证，姓名，余额，交易次数

SELECT tep2.CardNo,accountinfo.AccountCode,accountinfo.RealName
from (SELECT * from bankcard WHERE CardId in 
(
			SELECT CardId from 
(SELECT CardId,COUNT(*) 存款数 from cardexchange GROUP BY CardId)tep
WHERE tep.存款数>=(SELECT MAX(存款数) 最大值	from
				(SELECT CardId,COUNT(*) 存款数 from cardexchange GROUP BY CardId) tep )

) tep2 
INNER JOIN accountinfo on accountinfo.AccountId=tep2.AccountId








SELECT * from accountinfo INNER join 
(
						SELECT MAX(存款数) 最大值	from
						(SELECT CardId,COUNT(*) 存款数 from cardexchange GROUP BY CardId) tep 
)tep1


SELECT MAX(存款数) 最大值	from
						(SELECT CardId,COUNT(*) 存款数 from cardexchange GROUP BY CardId) tep 

	


















