CREATE TABLE `abnormal` (
`orderID` int(10) NOT NULL COMMENT '订单id',
`preferential` decimal(10,2) NOT NULL COMMENT '优惠价格',
`userID` int(10) NOT NULL COMMENT '用户id',
`paymentAmount` decimal(10,2) NOT NULL COMMENT '支付金额'
)
ENGINE = InnoDB
AUTO_INCREMENT = 0
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Dynamic;

CREATE TABLE `aftermarket` (
`userID` int(10) NOT NULL COMMENT '用户id',
`dishesID` int(10) NOT NULL COMMENT '菜品id',
`orderID` int(10) NOT NULL COMMENT '订单id',
`riderID` int(10) NOT NULL COMMENT '骑手id',
`cause` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '原因',
`schedule` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '解决进度'
)
ENGINE = InnoDB
AUTO_INCREMENT = 0
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Dynamic;

CREATE TABLE `classify` (
`id` int(10) NOT NULL COMMENT '商家分类id',
`name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商家分类名称',
PRIMARY KEY (`id`) 
)
ENGINE = InnoDB
AUTO_INCREMENT = 0
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Dynamic;

CREATE TABLE `concessions` (
`userID` int(10) NOT NULL COMMENT '用户id',
`order` int(100) NOT NULL COMMENT '订单数',
`discount` decimal(10,2) NOT NULL COMMENT '优惠券价格',
`distribution` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '发放情况'
)
ENGINE = InnoDB
AUTO_INCREMENT = 0
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Dynamic;

CREATE TABLE `dishes` (
`id` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜品id',
`name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜品名',
`upload` datetime NULL DEFAULT NULL COMMENT '上架时间',
`price` decimal(10,2) NULL DEFAULT NULL COMMENT '单价',
`monthlySales` int(100) NULL DEFAULT NULL COMMENT '月销量',
`totalQty` int(10) NULL DEFAULT NULL COMMENT '总数量',
`praise` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '好评',
`poorRating` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '差评',
PRIMARY KEY (`id`) 
)
ENGINE = InnoDB
AUTO_INCREMENT = 1
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Dynamic;

CREATE TABLE `merchant` (
`id` int(10) NOT NULL COMMENT '商家id',
`classifyID` int(10) NOT NULL COMMENT '商家分类id',
`name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商家名称',
`address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商家地址',
`phone` int(11) NOT NULL COMMENT '商家电话',
PRIMARY KEY (`id`) 
)
ENGINE = InnoDB
AUTO_INCREMENT = 0
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Dynamic;

CREATE TABLE `order` (
`id` int(10) NOT NULL COMMENT '订单id',
`userID` int(10) NOT NULL COMMENT '用户id',
`riderID` int(10) NOT NULL COMMENT '骑手id',
`buyAfew` int(100) NOT NULL COMMENT '购买数',
`totalPrice` decimal(10,2) NOT NULL COMMENT '总价格',
`site` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '送餐地址',
`OrderID` int(10) NOT NULL COMMENT '订单号',
`time` datetime NULL COMMENT '订单创建时间',
`orderStatus` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '订单状态',
`remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '备注',
`merchantID` int(10) NOT NULL COMMENT '商家id',
PRIMARY KEY (`id`) 
)
ENGINE = InnoDB
AUTO_INCREMENT = 0
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Dynamic;

CREATE TABLE `rider` (
`id` int(10) NOT NULL COMMENT '骑手id',
`name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '骑手姓名',
`age` int(2) NOT NULL COMMENT '骑手年龄',
`phone` int(11) NOT NULL COMMENT '骑手电话',
`site` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '骑手位置',
`state` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '骑手状态',
PRIMARY KEY (`id`) 
)
ENGINE = InnoDB
AUTO_INCREMENT = 0
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Dynamic;

CREATE TABLE `user` (
`id` int(10) NOT NULL COMMENT '用户id',
`name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
`sex` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '性别',
`site` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '送餐地址',
`mobile` int(11) NOT NULL COMMENT '手机号',
`order` int(100) NOT NULL COMMENT '订单数',
`createdate` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
PRIMARY KEY (`id`) 
)
ENGINE = InnoDB
AUTO_INCREMENT = 0
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Dynamic;


ALTER TABLE `order` ADD CONSTRAINT `fk_order_user_1` FOREIGN KEY (`userID`) REFERENCES `user` (`id`);
ALTER TABLE `order` ADD CONSTRAINT `fk_order_rider_1` FOREIGN KEY (`riderID`) REFERENCES `rider` (`id`);
ALTER TABLE `merchant` ADD CONSTRAINT `fk_merchant_classify_1` FOREIGN KEY (`classifyID`) REFERENCES `classify` (`id`);
ALTER TABLE `abnormal` ADD CONSTRAINT `fk_abnormal_order_1` FOREIGN KEY (`orderID`) REFERENCES `order` (`id`);
ALTER TABLE `abnormal` ADD CONSTRAINT `fk_abnormal_user_1` FOREIGN KEY (`userID`) REFERENCES `user` (`id`);
ALTER TABLE `concessions` ADD CONSTRAINT `fk_concessions_user_1` FOREIGN KEY (`userID`) REFERENCES `user` (`id`);
ALTER TABLE `concessions` ADD CONSTRAINT `fk_concessions_user_2` FOREIGN KEY (`order`) REFERENCES `user` (`order`);
ALTER TABLE `aftermarket` ADD CONSTRAINT `fk_aftermarket_user_1` FOREIGN KEY (`userID`) REFERENCES `user` (`id`);
ALTER TABLE `aftermarket` ADD CONSTRAINT `fk_aftermarket_dishes_1` FOREIGN KEY (`dishesID`) REFERENCES `dishes` (`id`);
ALTER TABLE `aftermarket` ADD CONSTRAINT `fk_aftermarket_order_1` FOREIGN KEY (`orderID`) REFERENCES `order` (`id`);
ALTER TABLE `aftermarket` ADD CONSTRAINT `fk_aftermarket_rider_1` FOREIGN KEY (`riderID`) REFERENCES `rider` (`id`);
ALTER TABLE `order` ADD CONSTRAINT `fk_order_merchant_1` FOREIGN KEY (`merchantID`) REFERENCES `merchant` (`id`);

