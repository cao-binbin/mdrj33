create database shopData;

use shopData;

-- 地址表
create table address(
addressId int primary key auto_increment comment "地址id",
province varchar(10) not null COMMENT "省份",
city varchar(10) not null COMMENT "市区"
);

-- 仓库表
create table dataTable(
dataId int primary key anto_increment COMMENT "仓库id",
dataName varchar9(10) unique not null comment "仓库名称",
dateSum int not null comment "仓库存储最大数量",
addressId int not null COMMENT "地址id",
foreign key(addressId) references address(addressId)
);


-- 商品类表
create table commClass(
commClassId int primary key auto_increment COMMENT "商品类id",
commClassName varchar(10) not null COMMENT "商品名称",
commClassInfo varchar(100) not null COMMENT "商品类详情"
);

-- 商品表
create table commTable(
commId int primary key auto_increment COMMENT "商品id",
commClassId int not null COMMENT "商品类表",
commName varchar(20) not null COMMENT "商品名称表",
commMoney decimal(10,2) not null COMMENT "商品金额",
commInfo varchar(100) not null COMMENT "商品详细信息",
foreign key(commClassId) references commClass(commClassId)
);



-- 商家记录表
create table recor(
recorId int primary key auto_increment COMMENT "记录id",
recorName varchar(10) not null COMMENT "记录"
);

-- 代金卷表

create table toKen(
toKenId int primary key auto_increment COMMENT "代金卷Id",
toKenMoney decimal(10,2) not null COMMENT "代金卷金额",
minMoney DECIMAL(10,2) not null COMMENT "最小使用金额"
);

-- 商家信息表
create table commHome(
commHomeId int primary key auto_increment COMMENT "商家id",
commId int not null COMMENT "商品id",
commClass int not null COMMENT "商品类id",
commHomeName varchar(10) not null COMMENT "商家名称",
addressId int not null COMMENT "地址id",
commHomeInfo varchar(100) not null COMMENT "商家详情",
tokenId int not  null COMMENT "代金卷Id",
recorId int not null COMMENT "记录Id",
foreign key(commId) references commTable(commId),
foreign key(tokenId) references toKen(toKenId),
foreign key(recorId) references recor(recorId),
foreign key(addressId) references address(addressId),
foreign key(commClassId) references commClass(commClassId)
);

-- 仓库进货出货表
create table dataOrTable(
dataOrId int primary key anto_increment COMMENT  "仓库进出单号",
dataId int not null COMMENT "仓库id",
commId int not null COMMENT "商品id",
commClassId int not null COMMENT "商品类id",
commHomeId int not null COMMENT "商家id",
overDataTime datetime not null COMMENT "进库时间",
outDataTime datetime not null COMMENT "出库时间",
overNum int not null COMMENT "进库数量",
outNum int not null COMMENT "出库数量",
foreign key(commClassId) references commClass(commClassId),
foreign key(commId) references commTable(commId),
foreign key(commHomeId) references commHome(commHomeId),
foreign key(dataId) references dataTable(dataId)
);





-- 用户表
create table userTable(
userId int primary key auto_increment COMMENT "用户id",
userName varchar(10) not null comment"用户名称",
userPaw varchar(10) not null COMMENT"用户密码",
userInfo varchar(100) not null COMMENT "用户详情"
);

-- 购物车表
create table shopping(
shopId int primary key auto_increment COMMENT "购物车id",
userId int not null COMMENT "用户id",
commId int not null COMMENT "商品id",
commHomeId int not null COMMENT "商家id",
commNum int not null COMMENT "商品数量",
foreign key(commClassId) references commClass(commClassId),
foreign key(commHomeId) references commHome(commHomeId),
foreign key(userId) references userTable(userId)
);

-- 用户界面商品表
create table userCommTable(
usercommId int primary key auto_increment COMMENT "条目id",
commId int not null COMMENT "商品id",
commClassId int not null COMMENT "商品类表",
usercommName varchar(20) not null COMMENT "商品名称表",
commHomeId int not null comment "商家id", 
usercommMoney decimal(10,2) not null COMMENT "商品金额",
usercommInfo varchar(100) not null COMMENT "商品详细信息",
foreign key(commClassId) references commClass(commClassId),
foreign key(commHomeId) references commHome(commHomeId),
foreign key(commId) references commTable(commId)
);


-- 物流表

create table stream(
streamId int primary key auto_increment COMMENT "物流Id",
steamName varchar(20) not null COMMENT "物流公司名称",
toutsTime datetime not null COMMENT "揽件时间",
outDataTime datetime not null COMMENT "出库时间",
estiTime datetime  not null COMMENT "预计到达时间",
addressId int not null COMMENT "地址Id",
foreign key(addressId) references address(addressId)
);

-- 订单号码表 
create table orderID(
orderId int primary key auto_increment COMMENT "订单号"
);


-- 订单表

create table order(
Num int primary key auto_increment COMMENT "数码",
userId int not null COMMENT "用户id",
commId int not null COMMENT "商品Id",
orderId int not null COMMENT "订单号",
usercommId int not null COMMENT "用户商品界面商品Id",
commHomeId int not null COMMENT "商家Id",
steamId int not null COMMENT "物流Id",
startTime datetime not null COMMENT "下单时间",
commNum int not null COMMENT "商品数量",
commSumMoney decimal(10,2) not null COMMENT "商品总金额",
foreign key(userId) references userTable(userId),
foreign key(commClassId) references commClass(commClassId),
foreign key(commHomeId) references commHome(commHomeId),
foreign key(orderId) references orderID(orderId),
foreign key(steamId) references stream(streamId)
);


-- 部门表

create table department(
departemntId int primary key auto_increment COMMENT "部门id",
departemntName varchar(10) not null COMMENT "部门名称",
departemntManage varchar(10) not null COMMENT "部门管理"
);


-- 公司人员表

create table perSon(
perSonId int primary key auto_increment COMMENT "人员id",
deparId int not null COMMENT "部门Id",
perSonName varchar(10) not null COMMENT "员工姓名",
personInfo varchar(100) not null COMMENT "员工信息",
foreign key(departemntId) references department(departementId)
);




-- 商品上下线状况表

create table overOutInfo(
ooiID int primary key auto_increment COMMENT "上下线数码Id",
commId int not null COMMENT "商品Id",
commHomeId int not null COMMENT "商家Id",
foreign key(commHomeId) references commHome(commHomeId),
foreign key(commId) references commTable(commId)
);


-- 商家收入表

create table revenue(
revenueId int primary key auto_increment COMMENT "收入码数",
commHomeId int not null COMMENT "商家Id",
orderId int not null COMMENT "单号",
commId int not null COMMENT "商品Id",
revenMoney decimal(10,2) not null COMMENT "收入金额",
commMONey decimal(10,2) not null 
COMMENT "商品原价",
revenTime datetime not null COMMENT "收入时间",
foreign key(commHomeId) references commHome(commHomeId),
foreign key(commId) references commTable(commId)
);


-- 售后表

create table afterTable(
afterId int primary key auto_increment COMMENT "售后数码",
noYesOut bit not null COMMENT "是否退还",
userId int not null COMMENT "用户Id",
commHomeId int not null COMMENT "商家Id",
userInfo varchar(100) COMMENT "用户评价",
foreign key(commHomeId) references commHome(commHomeId),
foreign key(userId) references userTable(userId)
);


-- 用户浏览记录

create table history(
historyId int PRIMARY key auto_increment COMMENT "历史数码",
userId int not null COMMENT "用户Id",
commHomeId int not null COMMENT "商家Id"
commId int not null comment "商品Id"
foreign key(commHomeId) references commHome(commHomeId),
foreign key(userId) references userTable(userId),
foreign key(commId) references commTable(commId)
);

