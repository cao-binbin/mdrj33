CREATE TABLE `carstatus` (
`CarStatus_Id` int NOT NULL AUTO_INCREMENT COMMENT '车辆状态编号',
`CarStatus_Name` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '车辆状态名称',
PRIMARY KEY (`CarStatus_Id`) ,
UNIQUE INDEX `CarStatus_Name` (`CarStatus_Name` ASC) USING BTREE
)
ENGINE = InnoDB
AUTO_INCREMENT = 0
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Dynamic;
CREATE TABLE `naturetype` (
`Naturee_Id` int NOT NULL AUTO_INCREMENT COMMENT '性质分类编号',
`Nature_Name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '性质分类名称',
PRIMARY KEY (`Naturee_Id`) ,
UNIQUE INDEX `Nature_Name` (`Nature_Name` ASC) USING BTREE
)
ENGINE = InnoDB
AUTO_INCREMENT = 0
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Dynamic;
CREATE TABLE `orderstatus` (
`OrderStatus_Id` int NOT NULL AUTO_INCREMENT COMMENT '订单状态编号',
`OrderStatus_Name` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '订单状态名称',
PRIMARY KEY (`OrderStatus_Id`) ,
UNIQUE INDEX `OrderStatus_Name` (`OrderStatus_Name` ASC) USING BTREE
)
ENGINE = InnoDB
AUTO_INCREMENT = 0
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Dynamic;
CREATE TABLE `partner` (
`partner_Id` int NOT NULL AUTO_INCREMENT COMMENT '合作商编号',
`partner_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '合作商名称',
`partner_Type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '合作商分类',
`partner_StTime` datetime NOT NULL COMMENT '合作开始时间',
`partner_OverTime` datetime NOT NULL COMMENT '合作结束时间',
PRIMARY KEY (`partner_Id`) ,
UNIQUE INDEX `partner_name` (`partner_name` ASC) USING BTREE,
UNIQUE INDEX `partner_Type` (`partner_Type` ASC) USING BTREE
)
ENGINE = InnoDB
AUTO_INCREMENT = 0
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Dynamic;
CREATE TABLE `post` (
`Post_Id` int NOT NULL AUTO_INCREMENT COMMENT '职位编号',
`Post_Name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '职位名称',
PRIMARY KEY (`Post_Id`) ,
UNIQUE INDEX `Post_Name` (`Post_Name` ASC) USING BTREE
)
ENGINE = InnoDB
AUTO_INCREMENT = 0
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Dynamic;
CREATE TABLE `producttype` (
`ProductType_Id` int NOT NULL AUTO_INCREMENT COMMENT '产品分类编号',
`ProductType_Name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '产品分类名称',
PRIMARY KEY (`ProductType_Id`) ,
UNIQUE INDEX `ProductType_Name` (`ProductType_Name` ASC) USING BTREE
)
ENGINE = InnoDB
AUTO_INCREMENT = 0
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Dynamic;
CREATE TABLE `state` (
`State_Id` int NOT NULL AUTO_INCREMENT COMMENT '状态编号',
`State_Name` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态名称',
PRIMARY KEY (`State_Id`) ,
UNIQUE INDEX `State_Name` (`State_Name` ASC) USING BTREE
)
ENGINE = InnoDB
AUTO_INCREMENT = 0
AVG_ROW_LENGTH = 0
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
KEY_BLOCK_SIZE = 0
MAX_ROWS = 0
MIN_ROWS = 0
ROW_FORMAT = Dynamic;
CREATE TABLE `table_1` (
);
CREATE TABLE `UserInfo` (
`User_Id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户编号',
`User_Name` varchar(10) NOT NULL COMMENT '用户名字',
`User_Type` varchar(10) NOT NULL COMMENT '性质编号',
`User_Age` varchar(3) NOT NULL COMMENT '用户年龄',
`User_Sex` varchar(1) NOT NULL DEFAULT 男 COMMENT '用户性别',
`User_TLP` varchar(11) NOT NULL COMMENT '用户电话号码',
`User_identity ` varchar(18) NOT NULL COMMENT '身份证号码',
`User_Address` varchar(100) NOT NULL COMMENT '详细地址',
`User_CrTime` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
`User_Updatetime` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
PRIMARY KEY (`User_Id`) ,
UNIQUE INDEX `unique_key_TLP` (`User_TLP` ASC) COMMENT '电话号码唯一' ,
UNIQUE INDEX `unique_key_Identity` () COMMENT '身份证号码唯一' 
);
CREATE TABLE `Staff` (
`Staff_ID` int NOT NULL AUTO_INCREMENT COMMENT '用户编号',
`Staff_Name` varchar(10) NOT NULL COMMENT '名字',
`Staff_Age` varchar(3) NOT NULL COMMENT '年龄',
`Staff_Sex` varchar(1) NOT NULL DEFAULT 男 COMMENT '性别',
`Staff_TLP` varchar(11) NOT NULL COMMENT '电话号码',
`Staff_Address` varchar(100) NOT NULL COMMENT '详细地址',
`Staff_Identity` varchar(18) NOT NULL COMMENT '身份证号码',
`Staff_Post` int NOT NULL COMMENT '职位',
`Staff_State` int NOT NULL COMMENT '状态',
`Staff_CrTime` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
`Staff_UpdateTime` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
PRIMARY KEY (`Staff_ID`) ,
UNIQUE INDEX `Staff_TLP_key` (`Staff_TLP` ASC) COMMENT '手机号唯一' ,
UNIQUE INDEX `Staff_identity_key` (`Staff_Identity` ASC) COMMENT '身份证唯一' 
);
CREATE TABLE `Carmessage` (
`Car_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '车编号',
`Car_Number` int(10) NOT NULL COMMENT '车牌号',
`Car_Weight` double(100,0) NOT NULL COMMENT '车重量',
`Car_State` int NOT NULL COMMENT '车状态',
`Car_Driver ` int NOT NULL COMMENT '驾驶人',
`Car_at_Address` varchar(100) NOT NULL COMMENT '车辆目前所在地址',
`Car_CrTime` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
`Car_UpdateTime` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
PRIMARY KEY (`Car_ID`) ,
UNIQUE INDEX `Car_Number_UQ` (`Car_Number` ASC) COMMENT '车牌号唯一' ,
UNIQUE INDEX `Car_Driver ` () COMMENT '驾驶人唯一' 
);
CREATE TABLE `table_2` (
);
CREATE TABLE `Mail` (
`Mail_Id` int NOT NULL COMMENT '寄件编号',
`Mail_MailNumber` int NOT NULL COMMENT '发货单号',
`Mail_Name` varchar(10) NOT NULL COMMENT '寄件名称',
`Mail_Number` int NOT NULL COMMENT '寄件数量',
`Mail_registrant` int NOT NULL COMMENT '登记人员',
`Mail_consigner ` int(255) NOT NULL COMMENT '发货人',
`Mail_Address` varchar(255) NOT NULL COMMENT '发货地址',
`Mail_TLP` varchar(11) NOT NULL COMMENT '发货人电话',
`consignee_Name` varchar(10) NOT NULL COMMENT '收件人名字',
`consignee_TLP` int(11) NOT NULL COMMENT '收件人电话',
`consignee_Address` varchar(255) NOT NULL COMMENT '收件人地址',
`Mail_manney` int NOT NULL COMMENT '运费',
`Mail_pay` int NOT NULL COMMENT '付款方式',
`Mail_Car` int NOT NULL COMMENT '运货车辆',
`Mail_Cardriver` int NOT NULL COMMENT '运货司机',
`Mail_CreTime` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
`Mail_UpdateTime` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
PRIMARY KEY (`Mail_Id`) 
);
CREATE TABLE `addressee` (
`Addressee_Id` int(11) NOT NULL AUTO_INCREMENT,
`Addressee_operating` varchar(255) NULL COMMENT '操作人员编号',
`Addressee_MailNumber` int NULL COMMENT '发货单号',
`Addressee_departure` varchar(255) NULL COMMENT '出发地',
`Addressee_destination` varchar(255) NULL COMMENT '目的地',
`Addressee_Car` varchar(255) NULL COMMENT '运输司机车',
`Addressee_CarDriver` int NULL COMMENT '司机名字',
`Addressee_MailName` int NULL COMMENT '货品名字',
`Addressee_Number` int NULL COMMENT '到货数量',
`Addressee_Time` datetime NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '到货时间',
`Addressee_refueling` decimal(10,2) NULL COMMENT '加邮费',
`Addressee_Toll` decimal(10,2) NULL COMMENT '过路费',
`Addressee_ S_mileage` float(10,1) NULL COMMENT '起步里程',
`Addewssee_O_mileage` float(10,1) NULL COMMENT '回里程',
PRIMARY KEY (`Addressee_Id`) 
);

ALTER TABLE `partner` ADD CONSTRAINT `partner_Type_key` FOREIGN KEY (`partner_Type`) REFERENCES `naturetype` (`Naturee_Id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `UserInfo` ADD CONSTRAINT `UserInfo_Type_FK` FOREIGN KEY (`User_Type`) REFERENCES `naturetype` (`Naturee_Id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `Staff` ADD CONSTRAINT `Staff_Post_FK` FOREIGN KEY (`Staff_Post`) REFERENCES `post` (`Post_Id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `Staff` ADD CONSTRAINT `Staff_State_FK` FOREIGN KEY (`Staff_State`) REFERENCES `state` (`State_Id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `Carmessage` ADD CONSTRAINT `Car_status_FK` FOREIGN KEY (`Car_State`) REFERENCES `carstatus` (`CarStatus_Id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

