/*==============================================================*/
/* DBMS name:      Sybase SQL Anywhere 12                       */
/* Created on:     2021/9/7 22:35:03                            */
/*==============================================================*/


if exists(select 1 from sys.sysforeignkey where role='FK_MEDIC_REFERENCE_DEPARTME') then
    alter table Medic
       delete foreign key FK_MEDIC_REFERENCE_DEPARTME
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_USER_REFERENCE_HOSPITAL') then
    alter table "User"
       delete foreign key FK_USER_REFERENCE_HOSPITAL
end if;

drop table if exists Ambulance;

drop table if exists Blood;

drop table if exists Department;

drop table if exists Hospitalized;

drop table if exists Instrument;

drop index if exists Medic.Index_1;

drop table if exists Medic;

drop table if exists Outpatient;

drop table if exists "User";

drop table if exists Warehouse;

/*==============================================================*/
/* Table: Ambulance                                             */
/*==============================================================*/
create table Ambulance 
(
   ambulance_id         int                            null,
   id                   int                            null
);

/*==============================================================*/
/* Table: Blood                                                 */
/*==============================================================*/
create table Blood 
(
   blood_id             int                            not null,
   blood_name           varchar(10)                    null,
   blood_xhao           varchar(10)                    null,
   blood_number         int                            null,
   blood_sate           varchar(20)                    null,
   constraint PK_BLOOD primary key clustered (blood_id)
);

/*==============================================================*/
/* Table: Department                                            */
/*==============================================================*/
create table Department 
(
   id                   int                            not null,
   department_name      varchar                        not null,
   constraint PK_DEPARTMENT primary key clustered (id)
);

/*==============================================================*/
/* Table: Hospitalized                                          */
/*==============================================================*/
create table Hospitalized 
(
   user_id              int                            null,
   hospitalized_because varchar(30)                    null,
   hospitalized_id      int                            not null,
   hospitalized_number  int                            null,
   constraint PK_HOSPITALIZED primary key clustered (hospitalized_id)
);

/*==============================================================*/
/* Table: Instrument                                            */
/*==============================================================*/
create table Instrument 
(
   instrument_id        int                            not null,
   instrument_name      varchar(10)                    null,
   instrument_classify  varchar(10)                    null,
   "instrument-number"  int                            null,
   instrument_sate      varchar(30)                    null,
   constraint PK_INSTRUMENT primary key clustered (instrument_id)
);

/*==============================================================*/
/* Table: Medic                                                 */
/*==============================================================*/
create table Medic 
(
   medic_id             int                            not null,
   medic_name           varchar(10)                    not null,
   medic_age            varchar(10)                    not null,
   medic_sex            varchar(10)                    not null,
   medic_work           varchar(10)                    not null,
   department_id        int                            not null,
   medic_sate           varchar(10)                    null,
   constraint PK_MEDIC primary key clustered (medic_id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on Medic (
medic_name ASC
);

/*==============================================================*/
/* Table: Outpatient                                            */
/*==============================================================*/
create table Outpatient 
(
   user_id              int                            null,
   depatemp_id          int                            null,
   outpatient_protict   varchar(10)                    null,
   outpatient_id        int                            not null,
   outpatient_number    int                            null,
   outpatient_cost      decimal(20,2)                  null,
   constraint PK_OUTPATIENT primary key clustered (outpatient_id)
);

/*==============================================================*/
/* Table: "User"                                                */
/*==============================================================*/
create table "User" 
(
   users_id             int                            not null,
   users_name           varchar(10)                    null,
   users_sex            varchar(10)                    not null,
   users_age            int                            not null,
   users_adress         varchar(30)                    null,
   users_identity       integer                        null,
   users_phone          integer                        null,
   history              varchar(100)                   null,
   constraint PK_USER primary key clustered (users_id)
);

/*==============================================================*/
/* Table: Warehouse                                             */
/*==============================================================*/
create table Warehouse 
(
   warehouse_id         int                            not null,
   warehouse_name       varchar(10)                    not null,
   warehouse_number     int                            null,
   warehouse_price      decimal(10,2)                  null,
   warehouse_time       datetime                       null,
   warehouse_dd         datetime                       null,
   constraint PK_WAREHOUSE primary key clustered (warehouse_id)
);

alter table Medic
   add constraint FK_MEDIC_REFERENCE_DEPARTME foreign key (department_id)
      references Department (id)
      on update restrict
      on delete restrict;

alter table "User"
   add constraint FK_USER_REFERENCE_HOSPITAL foreign key (users_id)
      references Hospitalized (hospitalized_id)
      on update restrict
      on delete restrict;

