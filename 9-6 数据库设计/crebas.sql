/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/7 18:42:55                            */
/*==============================================================*/


drop index Index_1 on User;

drop table if exists User;

drop table if exists abnormal;

drop index Index_1 on class;

drop table if exists class;

drop table if exists cuisine;

drop index Index_1 on merchant;

drop table if exists merchant;

drop table if exists "on sale";

drop table if exists onsale;

drop index Index_2 on "order";

drop index Index_1 on "order";

drop table if exists "order";

drop index Index_1 on rider;

drop table if exists rider;

/*==============================================================*/
/* Table: User                                                  */
/*==============================================================*/
create table User
(
   Uid                  int not null auto_increment,
   Uname                varchar(30),
   sex                  char(1),
   site                 varchar(30),
   mobile               int,
   "order"              int,
   primary key (Uid)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on User
(
   Uid
);

/*==============================================================*/
/* Table: abnormal                                              */
/*==============================================================*/
create table abnormal
(
   Oid                  int,
   favourable           decimal(10,2),
   Uid                  int,
   payment              decimal(10,2)
);

/*==============================================================*/
/* Table: class                                                 */
/*==============================================================*/
create table class
(
   Cid                  int not null auto_increment,
   Cname                varchar(20),
   primary key (Cid)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on class
(
   Cid
);

/*==============================================================*/
/* Table: cuisine                                               */
/*==============================================================*/
create table cuisine
(
   Cid                  int,
   CAIid                int not null auto_increment,
   CAIname              varchar(30),
   issueddate           datetime default CURRENT_TIMESTAMP,
   unit                 decimal(10,2),
   sales                int,
   "order"              int,
   evaluate             char(1),
   primary key (CAIid)
);

/*==============================================================*/
/* Table: merchant                                              */
/*==============================================================*/
create table merchant
(
   Mid                  int not null auto_increment,
   Cid                  int,
   CAIid                int,
   Mname                varchar(30),
   Msite                varchar(30),
   "order"              int,
   primary key (Mid)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on merchant
(
   Mid
);

/*==============================================================*/
/* Table: "on sale"                                             */
/*==============================================================*/
create table "on sale"
(
   Uid                  int,
   number               int,
   preferential         decimal(10,2),
   distribution         char(1)
);

/*==============================================================*/
/* Table: onsale                                                */
/*==============================================================*/
create table onsale
(
   Uid                  int,
   CAIid                int,
   Oid                  int,
   Rid                  int,
   cause                varchar(50),
   schedule             varchar(20)
);

/*==============================================================*/
/* Table: "order"                                               */
/*==============================================================*/
create table "order"
(
   Oid                  int not null auto_increment,
   Uid                  int,
   Mid                  int,
   CAIid                int,
   Rid                  int,
   buy                  int,
   tatol                decimal(10,2),
   site                 varchar(30),
   creationtime         datetime,
   Ostate               varchar(10),
   remark               varchar(50),
   primary key (Oid)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on "order"
(
   Oid
);

/*==============================================================*/
/* Index: Index_2                                               */
/*==============================================================*/
create unique index Index_2 on "order"
(
   Oid
);

/*==============================================================*/
/* Table: rider                                                 */
/*==============================================================*/
create table rider
(
   Rid                  int not null auto_increment,
   Rname                varchar(30),
   age                  int,
   mobile               int(11),
   place                varchar(30),
   state                varchar(10),
   primary key (Rid)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on rider
(
   Rid
);

alter table abnormal add constraint FK_Reference_14 foreign key (Oid)
      references "order" (Oid) on delete restrict on update restrict;

alter table abnormal add constraint FK_Reference_4 foreign key (Uid)
      references User (Uid) on delete restrict on update restrict;

alter table cuisine add constraint FK_Reference_3 foreign key (Cid)
      references class (Cid) on delete restrict on update restrict;

alter table merchant add constraint FK_Reference_1 foreign key (Cid)
      references class (Cid) on delete restrict on update restrict;

alter table merchant add constraint FK_Reference_2 foreign key (CAIid)
      references cuisine (CAIid) on delete restrict on update restrict;

alter table "on sale" add constraint FK_Reference_6 foreign key (Uid)
      references User (Uid) on delete restrict on update restrict;

alter table onsale add constraint FK_Reference_13 foreign key (Oid)
      references "order" (Oid) on delete restrict on update restrict;

alter table onsale add constraint FK_Reference_5 foreign key (Uid)
      references User (Uid) on delete restrict on update restrict;

alter table onsale add constraint FK_Reference_8 foreign key (CAIid)
      references cuisine (CAIid) on delete restrict on update restrict;

alter table "order" add constraint FK_Reference_10 foreign key (Mid)
      references merchant (Mid) on delete restrict on update restrict;

alter table "order" add constraint FK_Reference_11 foreign key (Uid)
      references User (Uid) on delete restrict on update restrict;

alter table "order" add constraint FK_Reference_12 foreign key (CAIid)
      references cuisine (CAIid) on delete restrict on update restrict;

alter table "order" add constraint FK_Reference_9 foreign key (Rid)
      references rider (Rid) on delete restrict on update restrict;

