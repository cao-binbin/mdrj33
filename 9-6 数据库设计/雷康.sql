/*==============================================================*/
/* DBMS name:      Sybase SQL Anywhere 12                       */
/* Created on:     2021/9/7 11:15:01                            */
/*==============================================================*/


if exists(select 1 from sys.sysforeignkey where role='FK_办卡表_REFERENCE_银行卡型表') then
    alter table 办卡表
       delete foreign key FK_办卡表_REFERENCE_银行卡型表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_办卡表_REFERENCE_用户表') then
    alter table 办卡表
       delete foreign key FK_办卡表_REFERENCE_用户表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_办卡表_REFERENCE_员工表') then
    alter table 办卡表
       delete foreign key FK_办卡表_REFERENCE_员工表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_存款表_REFERENCE_用户表') then
    alter table 存款表
       delete foreign key FK_存款表_REFERENCE_用户表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_抵押表_REFERENCE_用户表') then
    alter table 抵押表
       delete foreign key FK_抵押表_REFERENCE_用户表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_用户表_REFERENCE_用户类型表') then
    alter table 用户表
       delete foreign key FK_用户表_REFERENCE_用户类型表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_贷款表_REFERENCE_用户表') then
    alter table 贷款表
       delete foreign key FK_贷款表_REFERENCE_用户表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_银行信息表_REFERENCE_银行卡型表') then
    alter table 银行信息表
       delete foreign key FK_银行信息表_REFERENCE_银行卡型表
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_银行信息表_REFERENCE_用户表') then
    alter table 银行信息表
       delete foreign key FK_银行信息表_REFERENCE_用户表
end if;

drop index if exists 分行表.Index_1;

drop table if exists 分行表;

drop table if exists 办卡表;

drop index if exists 员工表.Index_1;

drop table if exists 员工表;

drop table if exists 存款表;

drop table if exists 抵押表;

drop table if exists 用户类型表;

drop index if exists 用户表.Index_1;

drop table if exists 用户表;

drop table if exists 贷款表;

drop table if exists 银行信息表;

drop table if exists 银行卡型表;

/*==============================================================*/
/* Table: 分行表                                                   */
/*==============================================================*/
create table 分行表 
(
   id                   int                            not null,
   name                 varchar(30)                    null,
   "add"                varchar(30)                    null,
   分行createtime         datetime                       null,
   income               decimal(10,2)                  null,
   pay                  decimal(10,2)                  null,
   "income and pay"     decimal(10,2)                  null,
   constraint PK_分行表 primary key clustered (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on 分行表 (
name ASC
);

/*==============================================================*/
/* Table: 办卡表                                                   */
/*==============================================================*/
create table 办卡表 
(
   id                   int                            not null,
   number               int                            null,
   type                 int                            null,
   "user"               int                            null,
   worker               int                            null,
   "time"               datetime                       null,
   constraint PK_办卡表 primary key clustered (id)
);

/*==============================================================*/
/* Table: 员工表                                                   */
/*==============================================================*/
create table 员工表 
(
   id                   int                            not null,
   name                 varvhar(30)                    null,
   sex                  bit                            null default 男,
   age                  int                            null,
   position             varchar(30)                    null,
   number               int                            null,
   "add"                varchar(30)                    null,
   constraint PK_员工表 primary key clustered (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on 员工表 (
number ASC
);

/*==============================================================*/
/* Table: 存款表                                                   */
/*==============================================================*/
create table 存款表 
(
   id                   int                            not null,
   number               int                            null,
   money                decimal(10,2)                  null,
   "bank save"          deciaml(10,2)                  null,
   "time"               datetime                       null,
   constraint PK_存款表 primary key clustered (id)
);

/*==============================================================*/
/* Table: 抵押表                                                   */
/*==============================================================*/
create table 抵押表 
(
   id                   int                            not null,
   "user"               int                            null,
   type                 int                            null,
   money                deciaml(10,2)                  null,
   constraint PK_抵押表 primary key clustered (id)
);

/*==============================================================*/
/* Table: 用户类型表                                                 */
/*==============================================================*/
create table 用户类型表 
(
   id                   int                            not null,
   type                 varchar(30)                    null,
   constraint PK_用户类型表 primary key clustered (id)
);

/*==============================================================*/
/* Table: 用户表                                                   */
/*==============================================================*/
create table 用户表 
(
   id                   int                            not null,
   name                 varchar(30)                    null,
   sex                  bit                            null default 男,
   age                  int                            null,
   number               int                            null,
   "add"                varchar(30)                    null,
   type                 int                            null,
   constraint PK_用户表 primary key clustered (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create unique index Index_1 on 用户表 (
number ASC
);

/*==============================================================*/
/* Table: 贷款表                                                   */
/*==============================================================*/
create table 贷款表 
(
   id                   int                            not null,
   money                decimal(10,2)                  null,
   "time"               datetime                       null,
   "pay money"          decimal(10,2)                  null,
   "pay moeny"          decimal(10,2)                  null,
   "pay time"           datetime                       null,
   "user"               int                            null,
   constraint PK_贷款表 primary key clustered (id)
);

/*==============================================================*/
/* Table: 银行信息表                                                 */
/*==============================================================*/
create table 银行信息表 
(
   id                   int                            not null,
   type                 int                            null,
   "user"               int                            null,
   constraint PK_银行信息表 primary key clustered (id)
);

/*==============================================================*/
/* Table: 银行卡型表                                                 */
/*==============================================================*/
create table 银行卡型表 
(
   id                   int                            not null,
   type                 varchar(30)                    null,
   constraint PK_银行卡型表 primary key clustered (id)
);

alter table 办卡表
   add constraint FK_办卡表_REFERENCE_银行卡型表 foreign key (type)
      references 银行卡型表 (id)
      on update restrict
      on delete restrict;

alter table 办卡表
   add constraint FK_办卡表_REFERENCE_用户表 foreign key ("user")
      references 用户表 (id)
      on update restrict
      on delete restrict;

alter table 办卡表
   add constraint FK_办卡表_REFERENCE_员工表 foreign key (worker)
      references 员工表 (id)
      on update restrict
      on delete restrict;

alter table 存款表
   add constraint FK_存款表_REFERENCE_用户表 foreign key (id)
      references 用户表 (id)
      on update restrict
      on delete restrict;

alter table 抵押表
   add constraint FK_抵押表_REFERENCE_用户表 foreign key ("user")
      references 用户表 (id)
      on update restrict
      on delete restrict;

alter table 用户表
   add constraint FK_用户表_REFERENCE_用户类型表 foreign key (type)
      references 用户类型表 (id)
      on update restrict
      on delete restrict;

alter table 贷款表
   add constraint FK_贷款表_REFERENCE_用户表 foreign key ("user")
      references 用户表 (id)
      on update restrict
      on delete restrict;

alter table 银行信息表
   add constraint FK_银行信息表_REFERENCE_银行卡型表 foreign key (type)
      references 银行卡型表 (id)
      on update restrict
      on delete restrict;

alter table 银行信息表
   add constraint FK_银行信息表_REFERENCE_用户表 foreign key ("user")
      references 用户表 (id)
      on update restrict
      on delete restrict;

