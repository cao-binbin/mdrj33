CREATE TABLE `table_1` (
`ClassId` int NOT NULL AUTO_INCREMENT,
`ClassNum` varchar(10) NOT NULL,
`ClassNam` varchar(10) NULL,
`StartTime` decimal NULL,
PRIMARY KEY (`ClassId`) 
);

CREATE TABLE `Student` (
`id` int NOT NULL AUTO_INCREMENT,
`Snum` int NULL,
`Sname` varchar(10) NULL,
`Ssex` varchar(1) NULL,
`Stime` date NULL,
`StartTime` time NULL,
`Cid` int(11) NULL,
PRIMARY KEY (`id`) ,
UNIQUE INDEX `Snum` (`Snum` ASC)
);


ALTER TABLE `Student` ADD CONSTRAINT `111` FOREIGN KEY (`Cid`) REFERENCES `table_1` (`ClassNum`) ON DELETE RESTRICT ON UPDATE RESTRICT;

