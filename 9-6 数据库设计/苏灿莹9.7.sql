/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/6 22:36:32                            */
/*==============================================================*/


drop table if exists icu表;

drop table if exists j救护车表;

drop table if exists 仪器表;

drop table if exists 住院表;

drop table if exists 医护表;

drop table if exists 器官表;

drop table if exists 库房;

drop table if exists 挂号表;

drop table if exists 用户表;

drop table if exists 病房表;

drop table if exists 血库表;

drop table if exists 部门表;

drop table if exists 门诊表;

/*==============================================================*/
/* Table: icu表                                                  */
/*==============================================================*/
create table icu表
(
   icu编号                int not null,
   icu部门名称              varchar(20) not null,
   现用床位病人id             int not null,
   icu共有床位              int,
   primary key (icu编号)
);

/*==============================================================*/
/* Table: j救护车表                                                 */
/*==============================================================*/
create table j救护车表
(
   救护车id                int not null,
   医护人员id               int not null,
   使用情况                 varchar(10) not null,
   primary key (救护车id)
);

/*==============================================================*/
/* Table: 仪器表                                                   */
/*==============================================================*/
create table 仪器表
(
   仪器id                 int not null,
   仪器名称                 varchar(20) not null,
   仪器分类                 varchar(20) not null,
   数量                   int not null,
   状态                   varchar(20) not null,
   primary key (仪器id)
);

/*==============================================================*/
/* Table: 住院表                                                   */
/*==============================================================*/
create table 住院表
(
   住院id                 char(10) not null,
   用户id                 int not null,
   病房id                 int,
   名字                   varchar(10) not null,
   年龄                   int not null,
   性别                   varchar(1) not null,
   病房号                  int,
   床位号                  int not null,
   primary key (住院id)
);

/*==============================================================*/
/* Table: 医护表                                                   */
/*==============================================================*/
create table 医护表
(
   医生id                 int not null auto_increment,
   部门id                 int,
   医生姓名                 varchar(10) not null,
   医生年龄                 int not null,
   医生性别                 varchar(1) not null,
   职位                   varchar(20) not null,
   职属id                 varchar(20) not null,
   是否在值                 varbinary(4) not null,
   primary key (医生id)
);

/*==============================================================*/
/* Table: 器官表                                                   */
/*==============================================================*/
create table 器官表
(
   器官id                 int not null,
   名称                   varchar(20) not null,
   数量                   int not null,
   分类                   varchar(10) not null,
   状态                   varchar(10) not null,
   入库时间                 datetime not null,
   出库时间                 datetime not null,
   primary key (器官id)
);

/*==============================================================*/
/* Table: 库房                                                    */
/*==============================================================*/
create table 库房
(
   药品id                 char(10) not null,
   药名                   varchar(20) not null,
   药数                   int not null,
   单价                   decimal(10,2) not null,
   出口时间                 datetime,
   入库时间                 datetime not null,
   primary key (药品id)
);

/*==============================================================*/
/* Table: 挂号表                                                   */
/*==============================================================*/
create table 挂号表
(
   id                   int not null,
   用户id                 int not null,
   姓名                   varchar(20) not null,
   预约部门id               int not null,
   预约医生id               int not null,
   primary key (id)
);

/*==============================================================*/
/* Table: 用户表                                                   */
/*==============================================================*/
create table 用户表
(
   用户id                 int not null,
   用户姓名                 varchar(10) not null,
   用户年龄                 int not null,
   用户性别                 varchar(1) not null,
   地址                   varchar(50) not null,
   电话                   varchar(11) not null,
   身份证                  varchar(18) not null,
   病史                   varchar(100) not null,
   primary key (用户id)
);

/*==============================================================*/
/* Table: 病房表                                                   */
/*==============================================================*/
create table 病房表
(
   病房id                 int not null,
   床位号id                int not null,
   病人id                 int not null,
   primary key (病房id)
);

/*==============================================================*/
/* Table: 血库表                                                   */
/*==============================================================*/
create table 血库表
(
   血液id                 int not null,
   名称                   varchar(20) not null,
   型号                   varchar(20) not null,
   数量                   int not null,
   状态                   varchar(20) not null,
   primary key (血液id)
);

/*==============================================================*/
/* Table: 部门表                                                   */
/*==============================================================*/
create table 部门表
(
   部门id                 int not null auto_increment,
   部门名称                 varchar(20) not null,
   primary key (部门id)
);

/*==============================================================*/
/* Table: 门诊表                                                   */
/*==============================================================*/
create table 门诊表
(
   流水号                  int not null,
   药品id                 char(10),
   门诊项目                 varchar(20) not null,
   药物id                 char(10),
   价格                   decimal(10,2) not null,
   数量                   int not null,
   总金额                  decimal(10,2) not null,
   primary key (流水号)
);

alter table icu表 add constraint FK_Reference_10 foreign key (现用床位病人id)
      references 用户表 (用户id) on delete restrict on update restrict;

alter table j救护车表 add constraint FK_Reference_9 foreign key (医护人员id)
      references 医护表 (医生id) on delete restrict on update restrict;

alter table 住院表 add constraint FK_Reference_2 foreign key (用户id)
      references 用户表 (用户id) on delete restrict on update restrict;

alter table 住院表 add constraint FK_Reference_3 foreign key (病房id)
      references 病房表 (病房id) on delete restrict on update restrict;

alter table 医护表 add constraint FK_Reference_1 foreign key (部门id)
      references 部门表 (部门id) on delete restrict on update restrict;

alter table 挂号表 add constraint FK_Reference_6 foreign key (用户id)
      references 用户表 (用户id) on delete restrict on update restrict;

alter table 挂号表 add constraint FK_Reference_7 foreign key (预约部门id)
      references 部门表 (部门id) on delete restrict on update restrict;

alter table 挂号表 add constraint FK_Reference_8 foreign key (预约医生id)
      references 医护表 (医生id) on delete restrict on update restrict;

alter table 病房表 add constraint FK_Reference_4 foreign key (病人id)
      references 用户表 (用户id) on delete restrict on update restrict;

alter table 门诊表 add constraint FK_Reference_5 foreign key (药品id)
      references 库房 (药品id) on delete restrict on update restrict;

