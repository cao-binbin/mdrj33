/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/9/7 17:53:03                            */
/*==============================================================*/


drop table if exists bacaID;

drop table if exists bank;

drop table if exists card;

drop table if exists deposit;

drop table if exists loans;

drop table if exists pledge;

drop index Index_1 on staff;

drop table if exists staff;

drop table if exists user;

/*==============================================================*/
/* Table: bacaID                                                */
/*==============================================================*/
create table bacaID
(
   bankCID              int not null auto_increment,
   type                 varchar(10),
   userID               int,
   primary key (bankCID)
);

/*==============================================================*/
/* Table: bank                                                  */
/*==============================================================*/
create table bank
(
   bankID               int not null auto_increment,
   bankName             varchar(30),
   address              varchar(30) binary,
   createtime           datetime,
   income               decimal(10,2),
   expend               decimal(10,2),
   expenses             decimal(10,2),
   primary key (bankID)
);

/*==============================================================*/
/* Table: card                                                  */
/*==============================================================*/
create table card
(
   cardID               int not null auto_increment,
   quantity             int,
   type                 varchar(10),
   userID               int,
   staffID              int,
   createtime           datetime,
   primary key (cardID)
);

/*==============================================================*/
/* Table: deposit                                               */
/*==============================================================*/
create table deposit
(
   depositID            int not null auto_increment,
   userID               int,
   cardID               int,
   balance              decimal(10,2),
   money                decimal(10,2),
   time                 datetime,
   rate                 varchar(10),
   primary key (depositID)
);

/*==============================================================*/
/* Table: loans                                                 */
/*==============================================================*/
create table loans
(
   loansID              int not null auto_increment,
   userID               int,
   money                decimal(10,2),
   loanstime            datetime,
   interest             varchar(10),
   refund               decimal(10,2),
   refundtime           datetime,
   primary key (loansID)
);

/*==============================================================*/
/* Table: pledge                                                */
/*==============================================================*/
create table pledge
(
   pledgeID             int not null auto_increment,
   userID               int,
   type                 varchar(20),
   plemoney             decimal(10,2),
   primary key (pledgeID)
);

/*==============================================================*/
/* Table: staff                                                 */
/*==============================================================*/
create table staff
(
   staffID              int not null auto_increment,
   bankID               int,
   staffName            varchar(10) binary,
   sex                  bit,
   age                  int,
   post                 varchar(10) binary,
   phone                int,
   adress               varchar(30) binary,
   primary key (staffID)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create index Index_1 on staff
(
   staffID
);

/*==============================================================*/
/* Table: user                                                  */
/*==============================================================*/
create table user
(
   userID               int not null auto_increment,
   username             varchar(10) binary,
   sex                  bit,
   age                  int,
   phone                int,
   adress               varchar(30) binary,
   type                 varchar(10) binary,
   primary key (userID)
);

alter table bacaID add constraint FK_Reference_4 foreign key (userID)
      references user (userID) on delete restrict on update restrict;

alter table card add constraint FK_Reference_2 foreign key (staffID)
      references staff (staffID) on delete restrict on update restrict;

alter table card add constraint FK_Reference_3 foreign key (userID)
      references user (userID) on delete restrict on update restrict;

alter table deposit add constraint FK_Reference_5 foreign key (userID)
      references user (userID) on delete restrict on update restrict;

alter table deposit add constraint FK_Reference_6 foreign key (cardID)
      references card (cardID) on delete restrict on update restrict;

alter table loans add constraint FK_Reference_8 foreign key (userID)
      references user (userID) on delete restrict on update restrict;

alter table pledge add constraint FK_Reference_7 foreign key (userID)
      references user (userID) on delete restrict on update restrict;

alter table staff add constraint FK_Reference_1 foreign key (bankID)
      references bank (bankID) on delete restrict on update restrict;

