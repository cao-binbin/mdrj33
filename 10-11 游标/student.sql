SELECT * from student;
SELECT * from course;
SELECT * from sc;
-- 以下试题全部使用存储过程和游标：
-- 1.输入一个课程号，打印该课程的平均分，最高分，最低分
DROP PROCEDURE if EXISTS `testCursor`;
delimiter $$
CREATE PROCEDURE testCursor(class_id int)
BEGIN
DECLARE text_avg  DOUBLE DEFAULT 0;
DECLARE text_MAX  DOUBLE DEFAULT 0;
DECLARE text_MIN  DOUBLE DEFAULT 0;
DECLARE myCur CURSOR for 
(
	SELECT avg(score),MAX(score),MIN(score) 
	from sc where cid=class_id
);
OPEN myCur;
	FETCH myCur into text_avg,text_MAX,text_MIN;
CLOSE myCur;
select text_avg,text_MAX,text_MIN;
END
$$
delimiter;
call testCursor(01);


-- 2.输入教师ID，返回所有学生信息，比如
-- 张三(01),李四（08），....,王五（09）
DROP PROCEDURE if EXISTS `testCursor1`;
delimiter $$
CREATE PROCEDURE testCursor1(par_tid varchar(10))
BEGIN 
	DECLARE temp_sid varchar(10) DEFAULT '';
	DECLARE temp_sname varchar(10) DEFAULT '';
  DECLARE flag int DEFAULT TRUE;
  DECLARE msg varchar(250) DEFAULT '';
  DECLARE mycur CURSOR FOR
	(
    SELECT sid,sname from student where sid in
     (
       SELECT sid from sc where cid=
        (
           SELECT cid from course where tid=par_tid
        )
     )
	);
DECLARE CONTINUE HANDLER for not found set flag=FALSE;
OPEN mycur;
aa:WHILE TRUE DO 
	FETCH mycur INTO temp_sid,temp_sname;
	IF flag=FALSE THEN
		 LEAVE aa;
	END if;

if msg='' THEN
  SET msg=CONCAT(temp_sname,'(',temp_sid,')');
ELSE
  SET msg=CONCAT(msg,',',temp_sname,'(',temp_sid,')');
END if;
END WHILE;
CLOSE mycur;
SELECT msg;
END
$$
delimiter;
CALL testCursor1('02');



-- 3.输入学号，返回成绩组合值，比如打印该整串字符：
-- 张三：语文（80），数学（90），英语（60）
DROP PROCEDURE if EXISTS `testCursor2`;
delimiter $$
CREATE PROCEDURE testCursor2(par_sid int)
BEGIN
DECLARE temp_score varchar(20) DEFAULT '';
DECLARE temp_cname varchar(20) DEFAULT '';
DECLARE temp_sname varchar(20) DEFAULT '';
DECLARE flag int DEFAULT TRUE;
DECLARE msg varchar(250) DEFAULT '';
DECLARE mycur CURSOR FOR
(
  SELECT sname ,cname, score from sc 
		INNER JOIN course on course.cid=sc.cid
		INNER JOIN student on student.sid=sc.sid
		where student.sid=par_sid
); 
DECLARE CONTINUE HANDLER for not found set flag=FALSE;
 OPEN mycur;
 aa:WHILE TRUE do
    FETCH mycur into temp_score,temp_sname,temp_cname;
		if flag=FALSE then
		  LEAVE aa;
		end if;
		
		if msg='' then 
		  set msg=concat(temp_sname,':',temp_cname,'(',temp_sccore,')');
		ELSE
		  set msg=concat(msg,',',temp_sname,':',temp_cname,'(',temp_sccore,')'); 		
    end if;
		end WHILE;
		CLOSE mycur;
		SELECT msg; 
END
$$
delimiter;
call testCursor2(01);




-- 4.打印姓名中有相同字的同学的学号和姓名，比如（张可可，林林等）
drop procedure if exists `par_text4`;
delimiter $$
create procedure par_text4()
begin
	declare type varchar(10) default '';
	declare num int default 0;
	declare flag int default true;
	declare mycur cursor for(
	select sname from student);
	
	declare CONTINUE handler for not found set flag=false;
	open mycur;
	WHILE flag DO
		fetch mycur into type;
		IF flag=true THEN
			set num = (select char_length(type));
			if mid(type,num,1)=mid(type,num-1,1) or mid(type,num-1,1)=mid(type,num-2,1) then
				select type;
			end if;
		END IF;
	END WHILE;
end
$$
delimiter ;

call par_text4();



