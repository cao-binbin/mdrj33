/*
1.输入一个课程号，打印该课程的平均分，最高分，最低分
*/
drop PROCEDURE if EXISTS `demo1`;
delimiter $$
CREATE PROCEDURE demo1(par_cid int)
BEGIN
		 DECLARE temp_avg varchar(20) DEFAULT '';
		 DECLARE temp_max varchar(20) DEFAULT '';
		 DECLARE temp_min varchar(20) DEFAULT '';
		 
		 
		 DECLARE mycur CURSOR for
		(		
					select avg(score),max(score),min(score) from sc where cid=par_cid
		);

		

		 OPEN mycur;
	 
		 FETCH mycur into temp_avg,temp_max,temp_min;
		 
		 CLOSE mycur;
		 select temp_avg,temp_max,temp_min;
END
$$
delimiter ;

call demo1(1);

/*
2.输入教师ID，返回所有学生信息，比如
张三(01),李四（08），....,王五（09）
*/
drop PROCEDURE if EXISTS `demo2`;
delimiter $$
create PROCEDURE demo2(par_tid int)
BEGIN
		
		 DECLARE temp_sid varchar(30) DEFAULT '';
		 DECLARE temp_sname VARCHAR(30) DEFAULT '';
		 DECLARE flag int DEFAULT TRUE;
		 DECLARE msg  VARCHAR(255) DEFAULT '';
		
		 DECLARE mycur CURSOR for
		 (
			select  sid ,sname from student where sid in(
			select sid from sc where cid =(
			select cid from course where tid=par_tid
			)
			)
		  );
			DECLARE CONTINUE HANDLER for not found set flag =FALSE;
				

		OPEN mycur;
		
		aa:WHILE TRUE DO
			FETCH mycur INTO temp_sid,temp_sname;
			if flag =FALSE THEN
					LEAVE aa;
			END IF;
			IF msg='' THEN
				SET msg = CONCAT(',',temp_sname,'(',temp_sid,')');
			ELSE
				SET msg = CONCAT(temp_sname,'(',temp_sid,')');
			END IF;		
			END WHILE;
		CLOSE mycur;
		select msg;
END
$$
delimiter ;

call demo2(01);



/*
3.输入学号，返回成绩组合值，比如打印该整串字符：
张三：语文（80），数学（90），英语（60）
*/
drop PROCEDURE if EXISTS `demo3`;
create PROCEDURE demo3(par_sid int)
BEGIN
		
		DECLARE temp_sname varchar(20) DEFAULT '';
		DECLARE temp_cname varchar(20) DEFAULT '';
		DECLARE temp_score varchar(20) DEFAULT '';
 		DECLARE flag int DEFAULT TRUE;
		DECLARE msg varchar(250) DEFAULT '';
		
		DECLARE mycur CURSOR for
		(
		select student.sname,course.cname,sc.score from student
		inner join sc on student.sid=sc.sid
		inner join  course on course.cid=sc.cid
		where  sc.sid=par_sid
		);
		DECLARE CONTINUE HANDLER for not found set flag = FALSE;
	
		OPEN mycur;
		aa:WHILE TRUE DO
				FETCH mycur into temp_sname,temp_cname,temp_score;
				if flag=FALSE then
				leave aa;
				end if;
				if msg='' THEN
					set msg = CONCAT(temp_sname,':',temp_cname,'(',temp_score,')');
					else
					set	msg = CONCAT(msg,temp_cname,'(',temp_score,')');
				end if;
		END WHILE;
		CLOSE mycur;
		select msg;
END;


call demo3(1);


/*
4.打印姓名中有相同字的同学的学号和姓名，比如（张可可，林林等）
*/

DROP PROCEDURE if exists `demo4`;
CREATE PROCEDURE demo4()
BEGIN
						declare type varchar(10) default '';
						declare num int default 0;
						declare flag int default true;
						
						declare mycur cursor for
						(
							select sid,sname from student
						);

						declare CONTINUE HANDLER for not found set flag=false;
						
						open mycur;
						
						WHILE TRUE DO
							fetch mycur into type;
							IF flag=true THEN
								set num = (select char_length(type));
						WHILE num>1 DO
							IF mid(type,num,1)=mid(type,num-1,1) then
										select type;
							END IF;
							
							set num=num-1;
							END WHILE;
							END IF;
						  END WHILE;
END;
	
	
					call work5();













