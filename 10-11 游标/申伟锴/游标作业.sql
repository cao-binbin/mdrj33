-- 以下试题全部使用存储过程和游标：
-- 1.输入一个课程号，打印该课程的平均分，最高分，最低分
drop procedure if exists hh;
create procedure hh(k_id varchar(10))
begin
declare a varchar(10) default '';
declare b varchar(10) default '';
declare c varchar(10) default '';

declare mycur cursor for select avg(score), max(score),min(score) from sc where cid=k_id group by cid;
open mycur;
fetch mycur into a,b,c;
select a 平均分,b 最高分,c 最低分;
close mycur;
end;
call hh('02');
-- 2.输入教师ID，返回所有学生信息，比如
-- 张三(01),李四（08），....,王五（09）
drop procedure if exists job;
delimiter &&
create procedure job(t_id varchar(2))
begin
	declare s_sname varchar(30) default '';
	declare s_sid varchar(30) default '';
	declare s_msg varchar(255) default '';
	declare num int default 0;
	declare flag int default true;
	declare mycur cursor for select sname,sid from student where sid in(select  sid from sc where cid in (select cid from course where tid='01'));
	DECLARE continue HANDLER for not found set flag=FALSE;
	open mycur;
	aa:WHILE flag DO
		FETCH mycur into s_sname,s_sid;
		set num=num+1;
		if num=1 then
			set s_msg=concat(s_sname,'(',s_sid,')');
			ITERATE aa;
		end if;
		IF flag=FALSE THEN
 			LEAVE aa;
    END IF;
		set s_msg=concat(s_msg,',',s_sname,'(',s_sid,')');
  END WHILE;
	select s_msg;
	close mycur;
end;
&&
delimiter ;
call job('01')

-- 3.输入学号，返回成绩组合值，比如打印该整串字符：
-- 张三：语文（80），数学（90），英语（60）
drop procedure if exists job2;
delimiter $$
create procedure job2(t_id varchar(2))
begin
	declare temp_sname varchar(30) default '';
	declare temp_cname varchar(30) default '';
	declare temp_score int default 0;
	declare s_msg varchar(250) default '';
	declare flag int default true;
	declare mycur cursor for 	
	(
		select sname,cname,score from sc 
		inner join student on student.sid = sc.sid
		inner join course on course.cid=sc.cid
		where sc.sid=t_id
	);
	
	DECLARE continue HANDLER for not found set flag=FALSE;
	open mycur;
	aa:WHILE flag DO
		FETCH mycur into temp_sname,temp_cname,temp_score;
-- 		set num=num+1;
-- 		if num=1 then
-- 			set s_msg=concat(s_msg,':',cname,'(',score,')');
-- 			ITERATE aa;
-- 		end if;
		IF flag=FALSE THEN
 			LEAVE aa;
    END IF;
			if s_msg= '' THEN
				set s_msg=concat(temp_cname,'(',temp_score,')');
			else 
				set s_msg=concat(s_msg,',',temp_cname,'(',temp_score,')');
			end if;
			
  END WHILE;
	set s_msg=concat(temp_sname,':',s_msg);
	select s_msg;
	close mycur;
end;
$$
delimiter ;


call job2('06');


-- 4.打印姓名中有相同字的同学的学号和姓名，比如（张可可，林林等）
DROP PROCEDURE if EXISTS `pro_aa`;
delimiter $$ 
CREATE PROCEDURE pro_aa()
BEGIN
	DECLARE pro_sid VARCHAR(10) DEFAULT '';
	DECLARE pro_sname VARCHAR(10) DEFAULT '';
  DECLARE pro_char VARCHAR(10) DEFAULT ''; 
	DECLARE pro_her VARCHAR(250) DEFAULT ''; 
	DECLARE pro_num int DEFAULT 0;
	





