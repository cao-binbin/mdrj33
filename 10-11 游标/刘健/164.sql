drop procedure if exists `demo_1`
delimiter $$
#1.输入一个课程号，打印该课程的平均分，最高分，最低分
create procedure demo_1(cid int)
begin
	declare cid_1 int default 0;
	declare Ascourse float default 0;
	declare Xscourse float default 0;
	declare Nscourse float default 0;
	
	create look cursor for 
	(
		select cid, avg(score),max(score),min(score) from sc
	);
	
	open look;
	
	fetch look into cid_1,Ascourse,Xscourse,Nscourse;
	
	close look;
	
	select cid_1,Ascourse,Xscourse,Nscourse;


end;
$$
delimiter ;
call demo_1();

#2.输入教师ID，返回所有学生信息，比如
#张三(01),李四（08），....,王五（09）


drop procedure if exists demo_2;
delimiter $$
create procedure demo_2(tid int)
begin
	DECLARE t_name VARCHAR(30) DEFAULT '';
	DECLARE t_cid int DEFAULT 0;
	DECLARE t_char_sum VARCHAR(200) DEFAULT '';
	DECLARE t_flag BLOB DEFAULT TRUE; -- 创建循环控制变量
	-- 创建游标
	DECLARE t_fun2 CURSOR for(
	SELECT sname,sid from student where sid in (Select sid from sc where cid in(SELECT cid from course  where tid=t_id))
	);
	DECLARE CONTINUE HANDLER for not found set t_flag =FALSE;
open t_fun2;
aa:WHILE t_flag=TRUE DO
		FETCH t_fun2 into t_name,t_cid;
		if t_flag=FALSE then
			LEAVE aa;
		end if;
	if t_char_sum='' then 
	set t_char_sum=CONCAT(t_name,"(",t_id,")");
	ELSE
	set t_char_sum=concat(t_char_sum,",",t_name,"(",t_id,")");
	end if;
END WHILE;
	CLOSE t_fun2;
	SELECT t_char_sum;		

end;
$$
delimiter ; 
#

#
#3.输入学号，返回成绩组合值，比如打印该整串字符：
#张三：语文（80），数学（90），英语（60）
#
drop PROCEDURE if EXISTS demo_3;
delimiter $$
CREATE PROCEDURE demo_3(sid_1 int)
BEGIN
	DECLARE s_name VARCHAR(30) DEFAULT '';
	DECLARE s_language FLOAT DEFAULT 0;
	DECLARE s_math FLOAT DEFAULT 0;
	DECLARE s_english FLOAT DEFAULT 0;
	
	DECLARE s_sum VARCHAR(200) DEFAULT '';
	-- 创建游标
	DECLARE t_fun3 CURSOR FOR(
		Select sname,
		(Select score from sc where sc.sid=student.sid AND cid='01') 语文,
		(Select score from sc where sc.sid=student.sid AND cid='02') 数学,
		(Select score from sc where sc.sid=student.sid AND cid='03') 英语
		
		from student where sid=sid_1 
	);
	OPEN t_fun3;
	FETCH t_fun3 into s_name,s_language,s_math,s_english;
	set s_sum =CONCAT(s_name,":","语文(",s_language,")",",","数学(",s_math,")",",","英语(",s_english,")");
	CLOSE t_fun3;
	SELECT s_sum 学生成绩表;
END;
$$
delimiter;

drop PROCEDURE if EXISTS demo_4;
delimiter $$
CREATE PROCEDURE demo_4()
BEGIN
	  DECLARE name_1 VARCHAR(10) DEFAULT '';  -- 名字1
		DECLARE name_2 VARCHAR(10) DEFAULT '';	-- 名字2
		DECLARE name_3 VARCHAR(10) DEFAULT '';	-- 名字3
		DECLARE sumname VARCHAR(100) DEFAULT ''; -- 组装名字
		DECLARE if_name BLOB DEFAULT true;     -- 启动循环
		DECLARE name_int int DEFAULT 0;
		-- 创建游标
		DECLARE t_fun4 CURSOR for(
		Select sname from student
		);
		declare CONTINUE handler for not found set if_name=false;
		-- 打开游标
		OPEN t_fun4;
		aa:WHILE if_name DO
			FETCH t_fun4 into sumname;
			set name_int = (select char_length(sumname));
			if name_int=2 THEN
						Set name_1=substring(sumname,1,1);
						Set name_2=substring(sumname,2,1);
						if name_1=name_2 THEN
						set sumname=CONCAT(name_1,name_2);
						Select * from student where sname=sumname;
						end if;
			ELSEIF  name_int =3 THEN
						set name_1=substring(sumname,1,1);
						Set name_2=substring(sumname,2,1);
						Set name_3=substring(sumname,3,1);
						
						if name_2=name_3 THEN
						set sumname=CONCAT(name_1,name_2,name_3);
						Select * from student where sname=sumname;
						end if;	
			end if;	
		END WHILE;
		CLOSE t_fun4;
END;
$$ 
delimiter;


