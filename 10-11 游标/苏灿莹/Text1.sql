-- 以下试题全部使用存储过程和游标：
-- 1.输入一个课程号，打印该课程的平均分，最高分，最低分

drop PROCEDURE if EXISTS `Lesson1`;
delimiter $$
CREATE PROCEDURE Lesson1 (in classid int)
BEGIN
	DECLARE stu_avg decimal(10,2) DEFAULT 0.0;
	DECLARE stu_max decimal(10,2) DEFAULT 0.0;
	DECLARE stu_min decimal(10,2) DEFAULT 0.0;
	
DECLARE myCur CURSOR for
(
		SELECT avg(score),max(score),min(score) from sc where cid = classid group by cid

);
	OPEN mycur;
	FETCH myCur into stu_avg,stu_max,stu_min ; 
	CLOSE myCur;
	select stu_avg,stu_max,stu_min;
END
$$
delimiter ;

call Lesson1(1);