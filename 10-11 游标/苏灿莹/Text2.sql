-- 2.输入教师ID，返回所有学生信息，比如
-- 张三(01),李四（08），....,王五（09）

drop PROCEDURE if exists `Lesson2`;
delimiter $$
create PROCEDURE Lesson2(in teacherid varchar(20))
BEGIN
	DECLARE temp_id varchar(20) DEFAULT  '';
	DECLARE temp_name varchar(20) DEFAULT  '';
	DECLARE flog int DEFAULT TRUE;
		declare msg varchar(250) DEFAULT '';
	DECLARE myCur CURSOR for
(	
		select sid,sname from student where sid in
		(
			SELECT sid from sc where cid=
			(
				select cid from course where tid=teacherid
		  )
    )
);
	DECLARE CONTINUE HANDLER for not found set flog =FALSE;	
open myCur;
	aa:WHILE true DO
		FETCH mycur into temp_id,temp_name;
		IF flog=FALSE THEN
			LEAVE aa;
		END IF;
		IF msg = '' THEN
			set msg = concat(temp_name,'(',temp_id,')');
		ELSE
			set msg = concat(msg,',',temp_name,'(',temp_id,')');
		END IF;
	END WHILE;
close myCur;

select msg;
END
$$
delimiter ;

CALL Lesson2('02');

