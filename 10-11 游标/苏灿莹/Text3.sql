-- 3.输入学号，返回成绩组合值，比如打印该整串字符：
-- 张三：语文（80），数学（90），英语（60）
drop PROCEDURE if EXISTS `Lesson3`;
CREATE PROCEDURE Lesson3(in studentid int)
BEGIN
	DECLARE stu_name varchar(20) DEFAULT '';
	DECLARE stu_yuwen int DEFAULT 0;
	DECLARE stu_shuxue int DEFAULT 0;
	DECLARE stu_yingyu int DEFAULT 0;
	DECLARE flag int DEFAULT true;
	DECLARE msg varchar(300) DEFAULT '';

DECLARE mycur CURSOR for
(
	select DISTINCT sname,
	IFNULL((
		select score from student inner join sc on student.sid=sc.sid where cid =01 and student.sid =studentid
	),0)
	,
	IFNULL((
		select score from student inner join sc on student.sid=sc.sid where cid =02 and student.sid =studentid
	),0)
	,
	IFNULL((
		select score from student inner join sc on student.sid=sc.sid where cid =03 and student.sid =studentid
	),0)
	from student inner join sc on student.sid=sc.sid where student.sid =studentid
);
	DECLARE CONTINUE HANDLER for not found set flag =FALSE;	
	open mycur;
		aa:WHILE true DO
		FETCH mycur into stu_name,stu_yuwen,stu_shuxue,stu_yingyu;
				IF flag=FALSE THEN
					LEAVE aa;
				END IF;
					if msg='' then
						set msg = concat(stu_name,': 语文(',stu_yuwen,'),数学(',stu_shuxue,'),英语(',		stu_yingyu,')');
					else
						set msg = concat(msg,',',temp_sname,'(',temp_sid,')');
					end if;		
				END WHILE;
	close mycur;
	select msg;
END;

call Lesson3(03);
-- 08找不到，没有学号，其他都可以