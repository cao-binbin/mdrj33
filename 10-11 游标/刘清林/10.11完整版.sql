-- 以下试题全部使用存储过程和游标：
-- 1.输入一个课程号，打印该课程的平均分，最高分，最低分
drop PROCEDURE IF EXISTS `work1`;
delimiter $$
CREATE PROCEDURE work1(par_cid INT)
BEGIN
-- 	DECLARE temp_cid VARCHAR(10) DEFAULT '';
	DECLARE temp_avg VARCHAR(10) DEFAULT '';
	DECLARE temp_max VARCHAR(10) DEFAULT '';
	DECLARE temp_min VARCHAR(10) DEFAULT '';
	DECLARE flag int DEFAULT TRUE;
	declare msg varchar(250) DEFAULT '';
	DECLARE mycur CURSOR FOR
	(
--  	select cid,
-- 		(select avg(score) from sc where sc.cid=course.cid)平均分,
-- 		(select max(score) from sc where sc.cid=course.cid)最高分,
-- 		(select min(score) from sc where sc.cid=course.cid)最低分
-- 	FROM course WHERE cid=par_cid
	SELECT AVG(score),MAX(score),MIN(score)FROM sc WHERE cid=par_cid
	);
	DECLARE CONTINUE HANDLER FOR NOT found set flag = FALSE;
	OPEN mycur;
	aa:WHILE TRUE DO
			FETCH mycur INTO temp_avg,temp_max,temp_min;
			IF flag=FALSE THEN
					LEAVE aa;
			END IF;
		IF msg='' THEN
				SET msg = CONCAT('平均分：',temp_avg,',','最高分：',temp_max,',','最低分：',temp_min);
			ELSE
				SET msg = CONCAT(msg,',','平均分：',temp_avg,',','最高分：',temp_max,',','最低分：',temp_min);
		END IF;
	END WHILE;
	CLOSE mycur;
	SELECT msg;
END;
$$
delimiter ;
CALL work1('02');
-- 
-- 2.输入教师ID，返回所有学生信息，比如
-- 张三(01),李四（08），....,王五（09）
-- 
drop PROCEDURE if EXISTS work2;
delimiter $$
CREATE PROCEDURE work2 (temp_id int)
BEGIN
	DECLARE temp_name VARCHAR(30) DEFAULT '';
	DECLARE temp_cid int DEFAULT 0;
	DECLARE temp_char VARCHAR(200) DEFAULT '';
	DECLARE flag BLOB DEFAULT TRUE; 
	DECLARE mycur CURSOR for
	(SELECT sname,sid from student where sid in 
		(Select sid from sc where cid in
			(SELECT cid from course  where tid=temp_id)
		)
	);
	DECLARE CONTINUE HANDLER for not found set flag =FALSE;
open mycur;
aa:WHILE flag=TRUE DO
		FETCH mycur into temp_name,temp_cid;
		if flag=FALSE then
			LEAVE aa;
		end if;
	if temp_char=''then 
	set temp_char=CONCAT(temp_name,"(",temp_id,")");
	ELSE
	set temp_char=concat(temp_char,",",temp_name,"(",temp_id,")");
	end if;
END WHILE;
	CLOSE mycur;
	SELECT temp_char;		
END;
$$
delimiter ;

call work2 ('02');
-- 3.输入学号，返回成绩组合值，比如打印该整串字符：
-- 张三：语文（80），数学（90），英语（60）
-- 
drop PROCEDURE if EXISTS work3;
delimiter $$
CREATE PROCEDURE work3(stu_id int)
BEGIN
	DECLARE stu_name VARCHAR(30) DEFAULT '';
	DECLARE stu_china FLOAT DEFAULT 0;
	DECLARE stu_math FLOAT DEFAULT 0;
	DECLARE stu_english FLOAT DEFAULT 0;
	
	DECLARE flag VARCHAR(200) DEFAULT '';
	DECLARE mycur CURSOR for
	(Select sname,(Select score from sc where sc.sid=student.sid AND cid='01'),
		(Select score from sc where sc.sid=student.sid AND cid='02'),
		(Select score from sc where sc.sid=student.sid AND cid='03')from student where sid=stu_id 
	);
	DECLARE CONTINUE HANDLER for not found set flag =FALSE;
OPEN mycur;
		FETCH mycur into stu_name,stu_china,stu_math,stu_english;
	set flag=CONCAT(stu_name,":","语文(",stu_china,")",",","数学(",stu_math,")",",","英语(",stu_english,")");
	CLOSE mycur;
	SELECT flag;	
END;
$$
delimiter ;

call work3 ('02');
-- 4.打印姓名中有相同字的同学的学号和姓名，比如（张可可，林林等）
-- 
-- 
DROP PROCEDURE IF EXISTS `work4`;
delimiter $$
CREATE PROCEDURE work4(par_sname VARCHAR(20))
BEGIN
#存储
	DECLARE st_sid int DEFAULT 0;
	DECLARE st_sname VARCHAR(20) DEFAULT '';
#定义方法
	declare sum varchar(20) default '';
	declare num varchar(255) default '';
	
	#范围索引号
	declare i int default 0;
	declare k int default 0;
#定义游标
	declare flag int default true;
	DECLARE mycur CURSOR FOR
(
#需要查询内容
	SELECT sid,sname FROM student
);
#使用方法
	DECLARE CONTINUE HANDLER FOR NOT found SET flag = FALSE;
#打开游标
OPEN mycur;
	aa:WHILE TRUE DO
#使用游标:将数据存入存储中
FETCH mycur INTO st_sid,st_sname;
	IF flag = FALSE THEN
		LEAVE aa;
	END IF;
#定义 i为st_sname字符长度
	set i=char_length(st_sname);
#如果i>0时
		bb:while i>0 do
# set sum= REPLACE('张可1可','可','');  '张1'
#将的‘可’替换为空值
		set sum= REPLACE(st_sname,substr(st_sname,i,1),'');
#减去长度
		set k=CHAR_LENGTH(st_sname)-char_length(sum);
			if k>=2 THEN
				set num=concat(num,',(',st_sid,')',st_sname);
				leave bb;
			end if;
			set i=i-1;
		end while;
	end while;
	close mycur;
	select num;
END;
$$
delimiter ;
CALL work4 ('02');



