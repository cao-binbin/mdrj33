#以下试题全部使用存储过程和游标：
#1.输入一个课程号，打印该课程的平均分，最高分，最低分
	
DROP PROCEDURE if EXISTS `type`;
delimiter $$
CREATE PROCEDURE type(type1 int)
BEGIN
		#定义变量
		DECLARE num1 int DEFAULT 0;
		DECLARE num2 int DEFAULT 0;
		DECLARE num3 int DEFAULT 0;

		#创建游标
	DECLARE mycur CURSOR for
	(select 
		(
			select avg(score) from sc where sc.cid=course.cid
		)平均分,
		(
			select max(score) from sc where sc.cid=course.cid
		)最高分,
		(
				select min(score) from sc where sc.cid=course.cid
		)最低分 from course where cid=type1
	);
		#打开游标
		OPEN mycur;
		
		#使用游标
	 FETCH mycur into num1,num2,num3;
		
		#关闭游标
		CLOSE mycur;
		
		select num1,num2,num3;
END
$$
delimiter ;

call type(02);


#2.输入教师ID，返回所有学生信息，比如
#张三(01),李四（08），....,王五（09）
select sid,sname from student where sid in
	(select sid from sc where cid=
		(select tid from teacher where tid=01
		)
	)


DROP PROCEDURE if EXISTS `type2`;
delimiter $$
CREATE PROCEDURE type2(type2 int)
BEGIN
	DECLARE temp_sid int DEFAULT 0;
	DECLARE temp_sname varchar(10) DEFAULT '';
	DECLARE flag int DEFAULT TRUE;
	DECLARE asd VARCHAR(250) DEFAULT '';
	
	DECLARE mycur CURSOR for
		(
		select sid,sname from student where sid in
			(select sid from sc where cid=
				(select tid from teacher where tid=type2
				)
			)
		);
			DECLARE CONTINUE HANDLER for not found set flag =FALSE;	
		#打开游标
		OPEN mycur;
		
		#使用游标
	 aa:WHILE TRUE DO
			FETCH mycur into temp_sid,temp_sname;
				IF flag=FALSE THEN
		LEAVE aa;
		
			end if;
				IF asd='' THEN
					set asd = concat(temp_sname,'(',temp_sid,')');
				ELSE
					set asd = concat(asd,',',temp_sname,'(',temp_sid,')');
			END IF;
	END WHILE;

		#关闭游标
		CLOSE mycur;
		select asd;
end;
$$
delimiter ;

call type2(01);


#3.输入学号，返回成绩组合值，比如打印该整串字符：
#张三：语文（80），数学（90），英语（60）
   
drop procedure if exists `pro_cname`;
create procedure pro_cname(pa_sid int)
begin
	declare ts_sname varchar(20) default '';
	declare ts_cname varchar(20) default '';
	declare ts_score varchar(20) default '';
	declare ts_msg varchar(255) default '';
	declare flag int default true;
	declare num cursor for(
	select student.sname,course.cname,sc.score from sc
	inner join student on student.sid=sc.sid
	inner join course on course.cid=sc.cid
	where sc.sid=pa_sid);
	declare continue handler for not found set flag=false;
	open num;
			aa:while true do
	fetch num into ts_sname,ts_cname,ts_score;
			if flag=false then
				leave aa;
			end if;
			if ts_msg='' then
				set ts_msg=concat(ts_sname,'：',ts_cname,'(',ts_score,')');
			else
				set ts_msg=concat(ts_msg,',',ts_cname,'(',ts_score,')');
			end if;
			end while;
	close num;
	select ts_msg;
end;
call pro_cname(2);
#4.打印姓名中有相同字的同学的学号和姓名，比如（张可可，林林等）

