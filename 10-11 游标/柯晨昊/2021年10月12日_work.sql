-- 以下试题全部使用存储过程和游标：
-- 1.输入一个课程号，打印该课程的平均分，最高分，最低分
select AVG(score),max(score),min(score) from sc where cid='01' group by cid 

drop procedure if exists `work1`;
delimiter $$
create procedure `work1`(type varchar(10))
BEGIN
	DECLARE avg_score float default 0;
	DECLARE max_score float default 0;
	DECLARE min_score float default 0;
-- 	DECLARE msg varchar(255) default '';
-- 	DECLARE flag int default true;
	DECLARE mycur cursor for (
		select AVG(score),max(score),min(score) from sc where cid=type group by cid
	);
-- 	DECLARE CONTINUE HANDLER for not found set flag=false;
	
	open mycur;
	
	FETCH mycur into avg_score,max_score,min_score;
	close mycur;
	select avg_score,max_score,min_score;

END
$$
delimiter ;

call work1('01');




-- 
-- 2.输入教师ID，返回所有学生信息，比如
-- 张三(01),李四（08），....,王五（09）

select * from student where sid in (
	select sid from sc where cid=
		(
			select cid from course where tid='02'
		)
)

drop procedure if exists `work2`;
delimiter $$
create procedure `work2`(type varchar(10))
begin
	DECLARE typeid varchar(10) default '';
	DECLARE typename varchar(10) default '';
	declare msg varchar(250) default '';
	declare flag int default true;
	DECLARE mycur cursor for(
		select sid,sname from student where sid in (
		select sid from sc where cid=
			(
				select cid from course where tid=type
			)
			)
	);
	DECLARE CONTINUE handler for not found set flag=false;
	open mycur;
	WHILE flag DO
		FETCH mycur into typeid,typename;
		IF flag=true then 
			IF msg = '' THEN
		-- 	 张三(01),李四（08），....,王五（09）
				set msg=CONCAT(typename,'(',typeid,')');
			ELSE
				set msg=CONCAT(msg,',',typename,'(',typeid,')');
			END IF;
		end if;
	END WHILE;
	close mycur;
	SELECT msg;
END
$$
delimiter ;

call work2('01');


-- 
-- 3.输入学号，返回成绩组合值，比如打印该整串字符：
-- 张三：语文（80），数学（90），英语（60）

select * from sc where sid='01';

drop procedure if exists `work3`;
delimiter $$
create procedure `work3`(type varchar(10))
begin
	declare typename varchar(10) default '';
	declare par_chinese int default 0;
	declare par_english int default 0;
	declare par_math int default 0;
	declare msg varchar(255) default '';
	declare mycur cursor for(
		select * from sc where sid=type
	);
	select sname into typename from student where sid=type;
	open mycur;
	FETCH mycur into par_chinese,par_math,par_english;
	close mycur;
	set msg=CONCAT(typename,':','语文(',par_chinese,')',',数学(',par_math,')',',英语(',par_english,')');
	select msg;
	
end
$$
delimiter ;
call work3('01');

-- 
-- 4.打印姓名中有相同字的同学的学号和姓名，比如（张可可，林林等）




drop procedure if exists `work4`;
delimiter $$
create procedure `work4`()
begin
	declare type varchar(10) default '';
	declare num int default 0;
	declare flag int default true;
	declare mycur cursor for(
	select sname from student);
	
	declare CONTINUE handler for not found set flag=false;
	open mycur;
	WHILE flag DO
		fetch mycur into type;
		IF flag=true THEN
			set num = (select char_length(type));
			if mid(type,num,1)=mid(type,num-1,1) or mid(type,num-1,1)=mid(type,num-2,1) then
				select type;
			end if;
		END IF;
	END WHILE;
end
$$
delimiter ;

call work4();
-- 

/*
select *,mid(sname,1,len(sname)) from student where 

SELECT CHAR_LENGTH("你好") AS LengthOfString;
SELECT CHARACTER_LENGTH("你好") AS LengthOfString;
SELECT FIND_IN_SET("c", "a,b,d,e");
set @type='李元元';
select @type;
select mid(@type,0,1)
select CHAR_LENGTH(@type)
select substring_index(@type,'元',1)
select STRCMP(@type,@type)
set @temp1 = (select mid(@type,1,1));
set @temp2 = (select mid(@type,2,1));
set @temp3 = (select mid(@type,3,1));
select @temp1,@temp2,@temp3
select @temp2=@temp3
set @temp=(select @temp2=@temp3)
SELECT @temp
CASE @temp2
	WHEN 1 THEN
		select '1';
	ELSE
		select '2';
END CASE;
*/
