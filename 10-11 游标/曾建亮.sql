-- 以下试题全部使用存储过程和游标：
-- 1.输入一个课程号，打印该课程的平均分，最高分，最低分

	drop PROCEDURE if exists `tes`;

	CREATE procedure tes(in par_challid int)
	BEGIN
		DECLARE temp_avg decimal(10,2) DEFAULT 0;
		DECLARE temp_max int DEFAULT 0;
		DECLARE temp_min int DEFAULT 0;
		
		DECLARE mycur cursor for(
			select AVG(score),MAX(score),min(score) from sc where score in
			(select score from sc where cid=par_challid)
		);
		open mycur;
		
			FETCH mycur into temp_avg ,temp_max,temp_min;
			
		close mycur;
		select temp_avg ,temp_max,temp_min;
	END;

	call tes(01);
		
-- 2.输入教师ID，返回所有学生信息，比如
-- 张三(01),李四（08），....,王五（09）
		drop PROCEDURE if exists `test_action2`;
delimiter $$
create PROCEDURE test_action2(par_tid varchar(10))
BEGIN
	DECLARE temp_sid varchar(10) DEFAULT '';
	DECLARE temp_sname varchar(10) DEFAULT '';
	DECLARE flag int DEFAULT TRUE;
	declare msg varchar(250) DEFAULT '';
	DECLARE mycur CURSOR for 
	(
		select sid,sname from student where sid in
		(
			SELECT sid from sc where cid=
			(
				select cid from course where tid=par_tid
		  )
    )
	);
	DECLARE CONTINUE HANDLER for not found set flag =FALSE;	
	open mycur;	
	aa:WHILE TRUE DO
		FETCH mycur into temp_sid,temp_sname;
		if flag=FALSE then
			LEAVE aa;
		end if;
		if msg='' then
			set msg = concat(temp_sname,'(',temp_sid,')');
		else
			set msg = concat(msg,',',temp_sname,'(',temp_sid,')');
		end if;		
	END WHILE;
	close mycur;
	select msg;
END;
$$
delimiter ;
		call text1('');


-- 3.输入学号，返回成绩组合值，比如打印该整串字符：
-- 张三：语文（80），数学（90），英语（60）
		drop PROCEDURE if EXISTS `text2`;
		create PROCEDURE text2(par_id varchar(10))
		BEGIN
			declare typename varchar(10) default '';
			declare par_chinese int default 0;
			declare par_english int default 0;
			declare par_math int default 0;
			declare msg varchar(255) default '';
			declare mycur cursor for(
				select * from sc where sid=par_id
			);
			select sname into typename from student where sid=par_id;
			open mycur;
			FETCH mycur into par_chinese,par_math,par_english;
			close mycur;
			set msg=CONCAT(typename,':','语文(',par_chinese,')',',数学(',par_math,')',',英语(',par_english,')');
			select msg;
		END;


-- 4.打印姓名中有相同字的同学的学号和姓名，比如（张可可，林林等）


