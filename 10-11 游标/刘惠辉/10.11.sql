#以下试题全部使用存储过程和游标：
# 1.输入一个课程号，打印该课程的平均分，最高分，最低分
  drop procedure  if exists  `course_id `;
	create procedure course_id( c_id varchar(10))
	begin 
	declare  min_cid  DECIMAL  default 0;
	declare avg_cid DECIMAL  default 0;
	declare max_cid DECIMAL  default 0;
	 DECLARE myCid CURSOR for
	(
	  select   avg(score),max(score),min(score) from  sc where cid =c_id
	);
	open myCid;
	fetch myCid into avg_cid,max_cid,min_cid;
	select  avg_cid,max_cid,min_cid;
	close myCid;
	end ;
  
	call course_id('01');

#2.输入教师ID，返回所有学生信息，比如
#张三(01),李四（08），....,王五（09）
drop procedure if exists `teacher_id`;
delimiter $$
create procedure teacher_id (t_cid varchar(10）)
begin 
 declare t_sid varchar(10) DEFAULT '';
 declare t_name varchar(10)  DEFAULT'';
 declare flag int default true ;
 declare msg varchar(250) DEFAULT '';
 declare myTeacher cursor for
 (
 select sid,sname from student where sid in
 (
  select sid from sc where cid in
(
	select cid from teacher  where tid= t_cid
 )
 )
 );
open myTeacher ;
#declare CONTINUE HANDLER for not found set flag =false;
DECLARE CONTINUE HANDLER for not found set flag =FALSE;	
 rt:WHILE true DO
	fetch myTeacher into t_sid,t_name;
	if flag=FALSE then
			LEAVE rt;
		end if;
if msg='' then
			set msg = concat(t_name,'(',t_sid,')');
		else
			set msg = concat(msg,',',t_name,'(',t_sid,')');
		end if;	
 end if;
select msg;
close myTeacher;
end;

$$
delimiter ;

drop PROCEDURE if exists `test_action2`;
delimiter $$
create PROCEDURE test_action2(par_tid varchar(10))
BEGIN
	DECLARE temp_sid varchar(10) DEFAULT '';
	DECLARE temp_sname varchar(10) DEFAULT '';
	DECLARE flag int DEFAULT TRUE;
	declare msg varchar(250) DEFAULT '';
	DECLARE mycur CURSOR for 
	(
		select sid,sname from student where sid in
		(
			SELECT sid from sc where cid=
			(
				select cid from course where tid=par_tid
		  )
    )
	);
	DECLARE CONTINUE HANDLER for not found set flag =FALSE;	
	open mycur;	
	aa:WHILE TRUE DO
		FETCH mycur into temp_sid,temp_sname;
		if flag=FALSE then
			LEAVE aa;
		end if;
		-- 张三(01),李四（08），....,王五（09）
		if msg='' then
			set msg = concat(temp_sname,'(',temp_sid,')');
		else
			set msg = concat(msg,',',temp_sname,'(',temp_sid,')');
		end if;		
	END WHILE;
	close mycur;
	select msg;
END;
$$
delimiter ;
call test_action2('02');

#3.输入学号，返回成绩组合值，比如打印该整串字符：
#张三：语文（80），数学（90），英语（60）
drop procedure if exists ` student_id`;
create procedure student_id (s_sid varchar(10))
begin 
declare s_sname varchar(10) default '';
declare chinese DECIMAL default 0;
declare math DECIMAL default 0;
declare english DECIMAL default 0;
declare msg varchar (150) default '';
declare myStudent CURSOR for 
(
 select sname ,a.score,b.score,c.score from student ,sc a ,sc b ,sc c ,course where student .sid= a.sid and a.sid=b.sid and b.sid =c.sid and 
c.cid =course.cid and a.cid=01 and b.cid =02 and c.cid=03  and student.sid =s_sid
);
open myStudent;
fetch myStudent into s_sname ,chinese,math,english;
#张三：语文（80），数学（90），英语（60）
set msg =CONCAT(mas,s_name,':','语文(',chinese,'),','数学(',math,'),','英语(',english,')');
 select msg;
close myStudent;
end ;
call student_id('01');
#4.打印姓名中有相同字的同学的学号和姓名，比如（张可可，林林等）
drop procedure if exists `name_test`;
 create procedure name_test()
 begin 
  declare s_same varchar (10) default '';
	declare s_sid varchar (10) default '';
	declare same CURSOR for
	select sid ,sname from student ;
	 open same ;
	 fetch same into s_sid,s_sname;
	 IF  THEN
	statement_list
ELSE
	statement_list
END IF;

	end ;
	 declare bun int default 0;
	set @bun='12311';
	select 
	REPEAT(@bun,3)