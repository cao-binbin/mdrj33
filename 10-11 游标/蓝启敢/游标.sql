-- 2.输入教师ID，返回所有学生信息，比如
-- 张三(01),李四（08），....,王五（09）
drop PROCEDURE if exists `test_action2`;
delimiter $$
create PROCEDURE test_action2(par_tid varchar(10))
BEGIN
	DECLARE temp_sid varchar(10) DEFAULT '';
	DECLARE temp_sname varchar(10) DEFAULT '';
	DECLARE flag int DEFAULT TRUE;
	declare msg varchar(250) DEFAULT '';
	DECLARE mycur CURSOR for 
	(
		select sid,sname from student where sid in
		(
			SELECT sid from sc where cid=
			(
				select cid from course where tid=par_tid
		  )
    )
	);
	DECLARE CONTINUE HANDLER for not found set flag =FALSE;	
	open mycur;	
	aa:WHILE TRUE DO
		FETCH mycur into temp_sid,temp_sname;
		if flag=FALSE then
			LEAVE aa;
		end if;
	
		if msg='' then
			set msg = concat(temp_sname,'(',temp_sid,')');
		else
			set msg = concat(msg,',',temp_sname,'(',temp_sid,')');
		end if;		
	END WHILE;
	close mycur;
	select msg;
END;
$$
delimiter ;
call test_action2('02');


以下试题全部使用存储过程和游标：
1.输入一个课程号，打印该课程的平均分，最高分，最低分
DROP PROCEDURE IF EXISTS `texb`;
delimiter $$
CREATE PROCEDURE texb(num int)
BEGIN
DECLARE avg DOUBLE DEFAULT 0;
DECLARE max DOUBLE DEFAULT 0;
DECLARE min DOUBLE DEFAULT 0;
	DECLARE mycur CURSOR for
		(SELECT AVG(score) , MAX(score),MIN(score) from sc where cid=num);
	OPEN mycur;
		FETCH mycur into avg, max,min;
	CLOSE mycur;
	SELECT avg ,max,min;
END
$$
delimiter;
call texb(1);



3.输入学号，返回成绩组合值，比如打印该整串字符：
张三：语文（80），数学（90），英语（60）
drop PROCEDURE if EXISTS `texb1`
delimiter $$
create PROCEDURE texb1(sc_score VARCHAR(20))
BEGIN
	DECLARE temp1_sid varchar(10) DEFAULT '';
	DECLARE temp1_sname varchar(10) DEFAULT '';
	DECLARE flag1 int DEFAULT TRUE;
	declare msg varchar(250) DEFAULT '';
DECLARE mycur CURSOR for
(
SELECT sid,sname from student where sid in 
(
	SELECT sid from sc where cid=
	(
	select cid from sc where tid=texb1
)
)
);
	DECLARE CONTINUE HANDLER for not found set flag1 =FALSE;	
OPEN mycur;

aa:WHILE TRUE DO
		FETCH mycur into temp_sid,temp_sname;
		if flag=FALSE then
			LEAVE aa;
		end if;
	
		end if;		
	END WHILE;
	close mycur;
	select msg;

END;
&&
delimiter;
call texb1('张三');
4.打印姓名中有相同字的同学的学号和姓名，比如（张可可，林林等）



