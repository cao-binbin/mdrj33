-- 以下试题全部使用存储过程和游标：
-- 1.输入一个课程号，打印该课程的平均分，最高分，最低分

DROP PROCEDURE IF EXISTS `textNum`;
delimiter //
CREATE PROCEDURE textNum(classId varchar(10))
BEGIN
	declare textAvg varchar(10);
	declare textMax varchar(10);
	declare textMin varchar(10);
	declare flag int default true;

	declare a cursor for
	(
		select avg(score),max(score),min(score) from sc where cid=classId
	) ;
	declare continue handler for not found set flag=false ;
	open a ;
		while flag do
			fetch a into textAvg,textMax,textMin ;
				select textAvg,textMax,textMin ;
		END while ;
	close a ;
END
//
delimiter ;
call textNum('01');



-- ---------------------------------------------------------
-- 2.输入教师ID，返回所有学生信息，比如
-- 张三(01),李四（08），....,王五（09）

drop procedure if exists `textName`;
delimiter //
create procedure textName(par_teacherId varchar(10))
BEGIN
	declare temp_stuName varchar(10) default '';
	declare temp_stuId varchar(10) default '';
	declare msg varchar(255) default '';
	declare flag int default true;
	
	declare stuMsg cursor for
	(
		select sid,sname from student where sid in 
		(
			select sid from sc where cid=
			(
				select cid from course where tid=par_teacherId
			)
		)
	);
	declare continue handler for not found set flag=false ;
	
	open stuMsg ;
		a:while true do
			fetch stuMsg into temp_stuId,temp_stuName;
			IF flag=false THEN
				leave a ;
			END IF ;

			IF msg = '' then
				SET msg = concat(temp_stuName,'(',temp_stuId,')');
			ELSE
				SET msg = concat(msg,',',temp_stuName,'(',temp_stuId,')');
			END IF ;
		END WHILE ;
	close stuMsg ;
	select msg ;
END;
//
delimiter ;
CALL textName('02');



-- --------------------------------------------------------
-- 3.输入学号，返回成绩组合值，比如打印该整串字符：
-- 张三：语文（80），数学（90），英语（60）
drop procedure if exists `stuScore` ;
create procedure stuScore(par_stuID varchar(10))
begin
	declare temp_name varchar(10) default '' ;
	declare temp_score varchar(10) default '' ;
	declare msg varchar(255) default '' ;
	declare flag int default true ;
	
	declare stuSc cursor for
	(
		select cname,score from sc join course on sc.cid=course.cid where sc.sid=par_stuID
	) ;
	declare continue handler for not found set flag=false;
	open stuSc;
		a:WHILE true DO
			fetch stuSc into temp_name,temp_score ;
			if flag = false then
				leave a ;
			end if ;

			if msg = '' then
			set msg = concat(temp_name,'(',temp_score,')') ;
			else
			set msg = concat(msg,',',temp_name,'(',temp_score,')');
			end if ;
		END WHILE;
	close stuSc ;
	select msg ;
end ;
call stuScore('01');


-- --------------------------------------------------------------
-- 4.打印姓名中有相同字的同学的学号和姓名，比如（张可可，林林等）

drop procedure if exists `sameName`;
delimiter //
create procedure sameName()
begin
	declare stuName varchar(10) default '';
	declare flag int default true;
	declare num int default 1;
	declare same cursor for
	(
		select sname from student
	);
	declare continue handler for not found set flag=false;
	
	open same;
		a:WHILE flag DO
				fetch same into stuName;	
				if flag = false then
					leave a ;
				end if;
				set num =(select char_length(stuName));
				while num >0 do
					if SUBSTRING(stuName , num , 1) = SUBSTRING(stuName , num-1 , 1) then
						select stuName;
					end if;
					set num = num - 1 ;
				end while;
		end while;
	close same;
end;
//
delimiter ;
call sameName();

set @type ='李元元';
select substring(@type,1,1)

