-- 1.输入一个课程号，打印该课程的平均分，最高分，最低分
drop procedure if exists cour;
delimiter//
create PROCEDURE `cour`(pro_cid varchar(10))
begin
DECLARE temp_avg DECIMAL(10,2) default 0;
DECLARE temp_max DECIMAL(10,2) default 0;
DECLARE temp_min DECIMAL(10,2) default 0;
DECLARE myyou cursor for
(
select avg(score),max(score),min(score) from sc where cid=pro_cid
);
open myyou;
fetch myyou into temp_avg,temp_max,temp_min;
close myyou;
select temp_avg,temp_max,temp_min;
end
//
delimiter ;


call cour('01');


-- 2.输入教师ID，返回所有学生信息，比如
-- 张三(01),李四（08），....,王五（09）
drop PROCEDURE if EXISTS tec23;
delimiter $$
create PROCEDURE tec23 (pro_tid varchar(10))
begin
DECLARE temp_sum varchar(255) default '';
DECLARE temp_sid varchar(10) default '';
declare temp_sname varchar(10) default '';
declare flag BLOB default true;

DECLARE myyou cursor for
(

select sid,sname from student where sid in (
select sid from sc where cid=(
select cid from course where tid=pro_tid))

);
DECLARE continue HANDLER for not found set flag=false;
 
OPEN myyou;
aa:while true do
fetch myyou into temp_sid,temp_sname;
if flag=false then
leave aa;
end if;
if temp_sum='' then
set temp_sum=concat(temp_name,'(',temp_sid,')');
else
set temp_sum=concat(temp_sum,temp_name,'(',temp_sid,')');
end if;
end while;

close myyou;

select temp_sum;




end;
$$
delimiter;
call tec23(01);




-- 3.输入学号，返回成绩组合值，比如打印该整串字符：
-- 张三：语文（80），数学（90），英语（60）
delimiter //
create PROCEDURE `stu`(pro_sid)
begin
declare typename varchar(10) default '';
	declare par_chinese int default 0;
	declare par_english int default 0;
	declare par_math int default 0;
	declare msg varchar(255) default '';
	declare mycur cursor for(
		select * from sc where sid=type
	);
	select sname into typename from student where sid=type;
	open mycur;
	FETCH mycur into par_chinese,par_math,par_english;
	close mycur;
	set msg=CONCAT(typename,':','语文(',par_chinese,')',',数学(',par_math,')',',英语(',par_english,')');
	select msg;
	
end
$$
delimiter ;
call stu(01);
