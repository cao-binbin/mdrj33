-- 以下试题全部使用存储过程和游标：
-- 1.输入一个课程号，打印该课程的平均分，最高分，最低分
drop procedure if exists hao;
delimiter $$
create procedure hao(in shu1 varchar(10))
begin 
declare temp_cid varchar(10) default'';
declare temp_avg varchar(10) default'';
declare temp_max varchar(10) default'';
declare temp_min varchar(10) default'';
declare judge int default true;
declare result varchar(255) default'';
#创建游标
declare biao cursor for 
(
select cid,
(select avg(score) from sc where sc.cid=course.cid)平均数,
(select max(score) from sc where sc.cid=course.cid)最高分,
(select min(score) from sc where sc.cid=course.cid)最低分,
from course where tid=shu1
);
#当找不到游标的值的时候把judge的值改成false
declare continue handler for found set judge=false;
#打开游标
open biao;
#使用游标
aa:while judge do
IF judge=true THEN
	fetch biao into temp_cid,temp_avg,temp_max,temp_min;
	set result=concat('课程编号：',temp_cid,'平均分：',temp_avg,'最高分：',temp_max,'最低分：',temp_min);
ELSE
	leave aa;
END IF;
end while;
#关闭游标
close biao;
select result;
end;
$$
delimiter;
call hao(01);




-- 2.输入教师ID，返回所有学生信息，比如
-- 张三(01),李四（08），....,王五（09）
drop procedure if exists procedure2;
delimiter //
create procedure procedure2(in p2 varchar(10))
BEGIN
	declare judge int default true;
	declare temp_sname varchar(20) default '';
	declare temp_sid varchar(10) default '';
	declare result varchar(255) default '';
	declare zz int default 0;
	#创建游标
	declare cursor2 cursor FOR
	(
		select sname,sid from student where sid in
		(
			select sid from sc where cid in
			(
				select cid from course where tid=p2
			)
		)
	);
  declare continue handler for not found set judge=false;
	#打开游标
	open cursor2;
	#使用游标
  aa:WHILE judge do 
			sif judge=true then
				fetch cursor2 into temp_sname,temp_sid;
         set zz=zz+1;
          if zz=1 then 
            set result=concat(temp_sname,'(',temp_sid,')');
          else
	      -- 张三(01),李四（08），....,王五（09）
				    set result= concat(result,'，',temp_sname,'(',temp_sid,')');
          end if;
			 else 
				leave aa;
			end if;
  end while;
	#关闭游标
	close cursor2;
  select result;
end;
//
delimiter ;

call procedure2('01');



-- 3.输入学号，返回成绩组合值，比如打印该整串字符：
-- 张三：语文（80），数学（90），英语（60）
drop procedure if exists procedure3;
delimiter //
create procedure procedure3(in p3 varchar(10))
begin

  declare jodge int default true;
	declare temp_sname varchar(10) default '';
	declare temp_Language varchar(10) default '';
	declare temp_Mathematics varchar(10) default '';
	declare temp_English varchar(10) default '';
	declare temp_result varchar(255) default '';
  #创建游标
	declare cursor3 cursor for
	(
	
		select sname 姓名,		(select score from sc where student.sid=sc.sid and sc.cid='01') 语文,
		(select score from sc where student.sid=sc.sid and sc.cid='02') 数学,
		(select score from sc where student.sid=sc.sid and sc.cid='03') 英语
		from student where sid=p3
	);
	declare continue handler for not found set jodge=false;
	#打开游标
	open cursor3;
	#使用游标
		aa:while jodge do
			if jodge=true then 
			  fetch cursor3 into temp_sname,temp_Language,temp_Mathematics,temp_English;
				#张三：语文（80），数学（90），英语（60）
				set temp_result =concat(temp_sname,':语文(',temp_Language,'),数学(',temp_Mathematics,'),英语(',temp_English,')');
			else 
			leave aa;
			end if;
		end while;
		
	#关闭游标
  close cursor3;
	select temp_result;
end;
//
delimiter ;

call procedure3('01');



-- 4.打印姓名中有相同字的同学的学号和姓名，比如（张可可，林林等）
drop procedure if exists procedure4;
delimiter //
create procedure procedure4()
begin
	declare jodge_1 int default true ;
	declare temp_sid varchar(10) default '';
	declare temp_sname varchar(10) default '';
	declare sname2 varchar(10) default '';
	declare aa varchar(10) default '';
	declare bb varchar(10) default '';
	declare cc varchar(10) default '';
	declare dd varchar(10) default '';
	declare result varchar(255) default '';
  #创建游标cursor4
	declare cursor4 cursor for
	(
	  select sid,sname from student
	);
	declare continue handler for not found set jodge_1=false ;
	#打开游标
	open cursor4;
	#使用游标cursor4
	zz:while jodge_1 do
		if jodge_1=true then
			fetch cursor4 into temp_sid,temp_sname;
			set sname2=temp_sname;
			select substring(sname2,1,1) into aa;
			set aa=ifnull(aa,1000001);
			select substring(sname2,2,1) into bb;
			set bb=ifnull(bb,1000002);
			select substring(sname2,3,1) into cc;
			set cc=ifnull(cc,1000003);
			select substring(sname2,4,1) into dd;
			set dd=ifnull(dd,100004);
			if aa=bb || aa=cc || aa=dd || bb=cc || bb=dd || cc=dd then
			  set result=concat(result,temp_sid,temp_sname,'   ');
			end if ;
		end if;
	end while;
	#关闭游标
	close cursor4;
	select result,aa,bb,cc,dd;
end;
//
delimiter ;

call procedure4();





