
-- 以下试题全部使用存储过程和游标：
-- 1.输入一个课程号，打印该课程的平均分，最高分，最低
drop procedure if exists `bbbbs`;
delimiter $$
create procedure bbbbs(nam int)
begin
declare t1 double default 0;
declare t2 double default 0;
declare t3 double default 0;
declare mas int default true ;

declare myCur cursor for(
    select avg(score),max(score),min(score) from sc where cid=nam
);
open myCur;
fetch myCur into t1,t2,t3;
close myCur;
select t1,t2,t3;
end;
$$
delimiter;
call bbbbs(01);

-- 2.输入教师ID，返回所有学生信息，比如
-- 张三(01),李四（08），....,王五（09）
drop procedure if exists `wwww`;
delimiter $$
create procedure wwww(num int)
begin
declare a1 varchar(20) default 0;
declare a2 varchar(30) default 0;
declare suu int default true;
declare sum  varchar(200) default;
declare maysss cursor for(
    select sid,sname from studnet where sid in
		(
		   select sid from sc where cid=
			 (
			    select cid from course tid=wwww
			 )
		)
);
DECLARE CONTINUE HANDLER for not found set suu = FALSE;
open maysss;
aa: WHILE suu do
FETCH maysss into a1,a2;
if suu=FALSE then
			LEAVE aa;
		end if;
if sum='' then
			set sum = concat(a2,'(',a1,')');
		else
			set sum = concat(sum,',',a2,'(',a1,')');
		end if;	
end WHILE;
CLOSE maysss;
SELECT sum;
end;
$$
delimiter; 

call wwww(01);

-- 3.输入学号，返回成绩组合值，比如打印该整串字符：
-- 张三：语文（80），数学（90），英语（60）
drop procedure if exists procedure3;
delimiter $$
create procedure procedure3(in p3 varchar(10))
begin

  declare jodge int default true;
	declare temp_sname varchar(10) default '';
	declare temp_Language varchar(10) default '';
	declare temp_Mathematics varchar(10) default '';
	declare temp_English varchar(10) default '';
	declare temp_result varchar(255) default '';
	declare cursor3 cursor for
	(
		select sname 姓名,		(select score from sc where student.sid=sc.sid and sc.cid='01') 语文,
		(select score from sc where student.sid=sc.sid and sc.cid='02') 数学,
		(select score from sc where student.sid=sc.sid and sc.cid='03') 英语
		from student where sid=p3
	);
	declare continue handler for not found set jodge=false;
	open cursor3;
		aa:while jodge do
			if jodge=true then 
			  fetch cursor3 into temp_sname,temp_Language,temp_Mathematics,temp_English;
				#张三：语文（80），数学（90），英语（60）
				set temp_result =concat(temp_sname,':语文(',temp_Language,'),数学(',temp_Mathematics,'),英语(',temp_English,')');
			else 
			leave aa;
			end if;
		end while;	
  close cursor3;
	select temp_result;
end;
$$
delimiter ;
call procedure3('02');

-- 4.打印姓名中有相同字的同学的学号和姓名，比如（张可可，林林等）
drop procedure if exists `rrrr`;
delimiter $$
create procedure rrrr()
begin
	declare jodge_1 int default true ;
	declare temp_sid varchar(10) default '';
	declare temp_sname varchar(10) default '';
	declare sname2 varchar(10) default '';
	declare a1 varchar(10) default '';
	declare a2 varchar(10) default '';
	declare a3 varchar(10) default '';
	declare a4 varchar(10) default '';
	declare result varchar(255) default '';
	declare cosses cursor for
	(
	  select sid,sname from student
	);
	declare continue handler for not found set jodge_1=false ;
	open cosses;
	zz:while jodge_1 do
		if jodge_1=true then
			fetch cosses into temp_sid,temp_sname;
			set sname2=temp_sname;
			select substring(sname2,1,1) into a1;
			set a1=ifnull(a1,1000001);
			select substring(sname2,2,1) into a2;
			set a2=ifnull(a2,1000002);
			select substring(sname2,3,1) into a3;
			set a3=ifnull(a3,1000003);
			select substring(sname2,4,1) into a4;
			set a4=ifnull(a4,100004);
			if a1=a2 || a1=a3 || a1=a4 || a2=a3 || a2=a4 || a3=a4 then
			  set result=concat(result,temp_sid,temp_sname,'   ');
			end if;
		end if;
	end while;
	close cosses;
	select result,a1,a2,a3,a4;
end;
$$
delimiter ;
call rrrr();
