-- 以下试题全部使用存储过程和游标：
-- 1.输入一个课程号，打印该课程的平均分，最高分，最低分
drop procedure if exists `test1`;
delimiter $$
create procedure test1(in par_cid int)
begin
     declare test_avg  float default 0;
		 declare test_max float default 0;
		 declare test_min float default 0;
		 
		 declare mycur cursor for
		 (
		     select avg(score),max(score),min(score) from sc where cid=par_cid group by cid
		 );
		 open mycur;
		 fetch mycur into test_avg,test_max,test_min ;
		 close mycur;
		 select test_avg,test_max,test_min;
end
$$
delimiter ;

call test1(2);



-- 2.输入教师ID，返回所有学生信息，比如
-- 张三(01),李四（08），....,王五（09）

drop PROCEDURE if exists `test2`;
delimiter $$
create PROCEDURE test2(in teacherid varchar(20))
BEGIN
	DECLARE temp_id varchar(20) DEFAULT  '';
	DECLARE temp_name varchar(20) DEFAULT  '';
	DECLARE flog int DEFAULT TRUE;
		declare msg varchar(250) DEFAULT '';
	DECLARE myCur CURSOR for
(	
		select sid,sname from student where sid in
		(
			SELECT sid from sc where cid=
			(
				select cid from course where tid=teacherid
		  )
    )
);
	DECLARE CONTINUE HANDLER for not found set flog =FALSE;	
open myCur;
	aa:WHILE true DO
		FETCH mycur into temp_id,temp_name;
		IF flog=FALSE THEN
			LEAVE aa;
		END IF;
		IF msg = '' THEN
			set msg = concat(temp_name,'(',temp_id,')');
		ELSE
			set msg = concat(msg,',',temp_name,'(',temp_id,')');
		END IF;
	END WHILE;
close myCur;

select msg;
END
$$
delimiter ;

CALL test2('02');



-- 3.输入学号，返回成绩组合值，比如打印该整串字符：
-- 张三：语文（80），数学（90），英语（60）
DROP PROCEDURE IF EXISTS `test3`;
CREATE PROCEDURE test3(par_sid INT)
BEGIN 
DECLARE temp_sname varchar(30) DEFAULT'';
DECLARE temp_language INT DEFAULT NULL;
DECLARE temp_math INT DEFAULT NULL;
DECLARE temp_English INT DEFAULT NULL;
DECLARE msg varchar(200) DEFAULT'';
DECLARE flag INT DEFAULT TRUE;
  DECLARE myCur CURSOR FOR
  (
			SELECT sname,
						(
						SELECT score FROM sc WHERE student.sid=sc.sid and sc.cid='01'
						)语文,
							(
							SELECT score FROM sc WHERE student.sid=sc.sid and sc.cid='02'
							)数学,
						(
						SELECT score FROM sc WHERE student.sid=sc.sid and sc.cid='03'
						)英语
	FROM student  WHERE sid=par_sid
	);
	DECLARE CONTINUE HANDLER FOR NOT found SET flag=FALSE;
	OPEN myCur;

	aa:WHILE TRUE DO
	  FETCH myCur INTO temp_sname,temp_language,temp_math,temp_English;
		IF flag=FALSE THEN
   	     LEAVE aa;
END IF;
			SET msg=CONCAT(temp_sname,':语文(',temp_language,'),数学(',temp_math,'),英语,(',temp_English,')');
END WHILE;

CLOSE myCur;
SELECT msg;
END;

CALL test3(04);





















