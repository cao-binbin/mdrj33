-- 以下试题全部使用存储过程和游标：
-- 1.输入一个课程号，打印该课程的平均分，最高分，最低分
DROP PROCEDURE IF EXISTS `aa`;
delimiter $$
CREATE PROCEDURE aa(num int)
BEGIN
DECLARE avg DOUBLE DEFAULT 0;
DECLARE max DOUBLE DEFAULT 0;
DECLARE min DOUBLE DEFAULT 0;
	DECLARE cur CURSOR for
		(SELECT AVG(score) , MAX(score),MIN(score) from sc where cid=num);
	OPEN cur;
		FETCH cur into avg, max,min;
	CLOSE cur;
	SELECT avg ,max,min;
END
$$
delimiter;
call aa(1);


-- 
-- 2.输入教师ID，返回所有学生信息，比如
-- 张三(01),李四（08），....,王五（09）
DROP PROCEDURE IF EXISTS `bb`;
delimiter $$
CREATE PROCEDURE bb( num int )
BEGIN
DECLARE s_name varchar(10) DEFAULT '';
DECLARE s_id varchar(10) DEFAULT '';
DECLARE flat int DEFAULT true;
DECLARE msg varchar(250) DEFAULT '';

#创建游标
	DECLARE cur CURSOR FOR
		(SELECT sid ,sname FROM student where sid in
			(SELECT sid from sc where cid=
				(SELECT cid from course WHERE tid=num)
			)
		);
	DECLARE CONTINUE HANDLER for not found set flat=FALSE;
#打开游标
	OPEN cur;
#使用游标
	aa:WHILE TRUE DO
		FETCH cur into s_id ,s_name;
		if flat=FALSE then
			LEAVE aa;
		end if;
	IF msg='' then
			set msg =CONCAT(msg,s_name,'(',s_id,')');
		ELSE
			set msg =CONCAT(msg,',',s_name,'(',s_id,')');
		end if;

	end while;
#关闭游标
	CLOSE cur;

	SELECT msg;
END
$$
delimiter;

call bb(1);


-- 
-- 3.输入学号，返回成绩组合值，比如打印该整串字符：
-- 张三：语文（80），数学（90），英语（60）
DROP PROCEDURE if EXISTS `cc`;
delimiter $$
CREATE PROCEDURE cc(num int )
BEGIN
DECLARE tem_cnamt varchar(10) DEFAULT '';
DECLARE tem_sscore varchar(10) DEFAULT '';
DECLARE tem_sname varchar(10) DEFAULT '';
DECLARE flat int DEFAULT TRUE;
DECLARE msg VARCHAR(250) DEFAULT ''; 

#创建游标
	
	DECLARE cur CURSOR FOR
	(	
		SELECT sname ,cname, score from sc 
		INNER JOIN course on course.cid=sc.cid
		INNER JOIN student on student.sid=sc.sid
		where student.sid=num
	);

DECLARE CONTINUE HANDLER for not found set flat=FALSE;

#打开游标
	OPEN cur;
#使用游标
	aa:WHILE TRUE DO
	FETCH cur into tem_sname, tem_cnamt, tem_sscore;
		IF flat=FALSE THEN
			LEAVE aa;
		end if;
		
		if msg='' then
			set msg=CONCAT(tem_sname,': ',tem_cnamt,'(',tem_sscore,')');
		else 
			set msg=CONCAT(msg,', ',tem_cnamt,'(',tem_sscore,')');
		end if;

	end while;

#关闭游标
	CLOSE cur;
	SELECT msg;
	
END
$$
delimiter;

call cc(1);


-- 
-- 4.打印姓名中有相同字的同学的学号和姓名，比如（张可可，林林等）












