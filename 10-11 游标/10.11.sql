#1.输入一个课程号，打印该课程的平均分，最高分，最低分
		
drop procedure if exists course_grade;
delimiter $$
create procedure course_grade(in par_cid varchar(10))
BEGIN
	declare sc_cid varchar(10) default '';
	declare sc_avg varchar(10) default '';
	declare sc_max varchar(10) default '';
	declare sc_min varchar(10) default '';
  declare flag1 int default true;
  declare msg varchar(255) default '';
	declare mycur cursor for
	(
		select cid,
		(select avg(score) from sc where sc.cid=course.cid) 平均分,
		(select max(score) from sc where sc.cid=course.cid) 最高分,
		(select min(score) from sc where sc.cid=course.cid) 最低分
		 from course where cid=par_cid
	);
  declare continue handler for not found set flag1=false;
  open mycur ;
  aa:WHILE flag1 do 
		if flag1=true then
			 fetch mycur into sc_cid,sc_avg,sc_max,sc_min;
       set msg =concat('课程编号:',sc_cid,'   平均分:',sc_avg,'   最高分:',sc_max,'   最低分:',sc_min);
    ELSE
       leave aa;
		end if;
  end while;
  close mycur ;
  select msg;

end;
$$
delimiter ;

call course_grade('02');
#2.输入教师ID，返回所有学生信息，比如
#张三(01),李四（08），....,王五（09）

DROP PROCEDURE if EXISTS `teac_id`;
delimiter $$
CREATE PROCEDURE teac_id(par_tid VARCHAR(10))
BEGIN
	DECLARE teac_sid VARCHAR(10) DEFAULT '';
	DECLARE teac_sname VARCHAR(10) DEFAULT '';
	DECLARE flag int DEFAULT true;
	DECLARE mgs varchar(255) DEFAULT '';
	declare mgs2 int default 0;
	DECLARE mycur2 CURSOR for
	(
		select sid,sname from student where sid in
		(
			select sid from sc where cid in
			(
				select cid from course where tid=par_tid
			)
		) 
	);
	DECLARE CONTINUE HANDLER for not found set flag=FALSE;
	open mycur2;
	 bb:WHILE TRUE DO
			FETCH mycur2 into teac_sid,teac_sname;
			set mgs2=mgs2+1;
			IF flag=FALSE THEN
							LEAVE bb;
					END IF;
					IF mgs2=1 THEN
					   set mgs=CONCAT(teac_sname,'(',teac_sid,')');
				  ELSE
						set mgs=CONCAT(mgs,',',teac_sname,'(',teac_sid,')');
				  END IF;
    END WHILE;
CLOSE mycur2;
SELECT mgs;
END;
$$
delimiter ;

call teac_id('01')

#3.输入学号，返回成绩组合值，比如打印该整串字符：
#张三：语文（80），数学（90），英语（60）
drop PROCEDURE if EXISTS `stu_id`;
delimiter $$
CREATE PROCEDURE stu_id(par_sid varchar(10))
BEGIN
	DECLARE cou_cname VARCHAR(10) DEFAULT '';
	DECLARE stu_sname VARCHAR(10) DEFAULT '';
	DECLARE sc_score varchar(10) DEFAULT '';
	DECLARE flag3 int DEFAULT TRUE;
	DECLARE draw varchar(255) DEFAULT '';
	DECLARE mycur3 CURSOR for
	(
			select 
			(
				select score from sc where cid=01 and student.sid=sc.sid
			)语文,
			(
				select score from sc where cid=02 and student.sid=sc.sid
			)数学,
			(
				select score from sc where cid=03 and student.sid=sc.sid
			)英语
			from student where sid=par_sid
	);
	DECLARE CONTINUE HANDLER for not found set flag3=FALSE;
	open mycur3;
	cc:WHILE flag3 DO
	IF flag3=TRUE THEN
	fetch mycur3 into cou_cname,stu_sname,sc_score;
	set draw = concat(stu_sname,',',cou_cname,'(',sc_score,')');
ELSE
	LEAVE cc;
END IF;
END WHILE;

  close mycur3 ;
  select draw;

end;
$$
delimiter ;


call stu_id('01')


#4.打印姓名中有相同字的同学的学号和姓名，比如（张可可，林林等）
DROP procedure if exists stu_name; 
create procedure stu_name()
begin
	declare stu_sid varchar(10) default '';
	declare stu_sname varchar(10) default '';
	declare flag4 varchar(250) default '';
	declare mycur4 cursor for
	(
	);
	open mycur4;
	fetch mycur4 into stu_sid,stu_sname;
	if flag4='' then
	set flag4 = concat(stu_sid,':',stu_sname);
	else
	set flag4 = concat(flag4,',',stu_sid,':',stu_sname,)
	
	close mycur4;
	select flag4;

end;

call stu_name();



















