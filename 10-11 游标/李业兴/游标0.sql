-- 1.输入一个课程号，打印该课程的平均分，最高分，最低分
drop PROCEDURE if exists `Score`;
delimiter $$
create PROCEDURE Score(course varchar(10))
BEGIN
	DECLARE Avg1 int DEFAULT 0;
	declare Max1 int default 0;
	declare Min1 int default 0;
-- 创建游标
	DECLARE num cursor for
	(
		select avg(score),max(score),min(score) from sc where cid=course
	);

-- 	打开游标
	open num;
-- 	使用游标
	fetch num into Avg1,Max1,Min1;
-- 关闭游标
	close num;
		select Avg1,Max1,Min1;
end;
$$
delimiter ;

call Score('01');
call Score('02');
call Score('03');

-- 2.输入教师ID，返回所有学生信息，比如
-- 张三(01),李四（08），....,王五（09）
drop PROCEDURE if EXISTS `class`;
create procedure class(par_tid varchar(10))
begin
	DECLARE NName varchar(10) DEFAULT '';
	declare IId varchar(10) default '';
	declare flag int default true;
	declare sum varchar(200) default '';
	
-- 创建游标
-- 根据输入的教师ID,对库进行查询,得出结果
	DECLARE num2 cursor for
	(
		select sid,sname from student where sid in
		(
			select sid from sc where cid=
			(
				select cid from course where tid=01
			)
		)
	);
-- 	当没有找到记录时，设置flag为FALSE
	declare continue handler for not found set flag=false;
	
-- 	打开游标
	open num2;
	
-- 使用游标
-- while循环
	aa:WHILE true DO
		fetch num2 into IID,NName;
-- 		if判断输入的值没有找到,离开循环。
		IF flag=false THEN
			leave aa;
		END IF;
		
		IF sum='' THEN
			set sum = CONCAT(NName,'(',IId,')');
		ELSE
			set sum = CONCAT(sum,',',NName,'(',IId,')');
		END IF;
	END WHILE;
-- 	关闭游标
close num2;
select sum;
end;

call class('01');
call class('02');
call class('03');
-- 3.输入学号，返回成绩组合值，比如打印该整串字符：
-- 张三：语文（80），数学（90），英语（60）

drop PROCEDURE if EXISTS `S_ID`;
delimiter $$
create procedure S_ID(par_ID varchar(10))
begin
	DECLARE typeName varchar(10) DEFAULT '';
	declare Chinese int default 0;
	declare Math int default 0;
	declare English varchar(10) default 0;
	declare msg VARCHAR(200) default '';
	
	
	declare num1 cursor for(
		select * from sc where sid=par_ID
	);
	select sname into typeName from student where sid=par_ID;
	open num1;
	fetch num1 into Chinese,Math,English;
		set msg = CONCAT(typeName,':','语文(',Chinese,'),数学(',Math,'),英语(',English,')');
	close num1;
	select msg;
end;
$$
delimiter ;
call S_ID('01');
