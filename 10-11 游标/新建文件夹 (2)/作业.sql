	-- 以下试题全部使用存储过程和游标：
	-- 1.输入一个课程号，打印该课程的平均分，最高分，最低分
DROP PROCEDURE IF EXISTS `work_1`;
delimiter $$
CREATE PROCEDURE work_1(par_cid int)
BEGIN 
   DECLARE temp_avg VARCHAR(10) DEFAULT '';
   DECLARE temp_max VARCHAR(10) DEFAULT '';
   DECLARE temp_min VARCHAR(10) DEFAULT '';
   DECLARE flag int DEFAULT TRUE;
   DECLARE per CURSOR FOR
   (
     SELECT avg(score),MAX(score),MIN(score) FROM sc where cid=par_cid
   );
OPEN per;

FETCH per into temp_avg,temp_max,temp_min;
CLOSE per;
SELECT temp_avg,temp_max,temp_min;
END
$$
delimiter ;
call work_1(02);

########################################################
	-- 2.输入教师ID，返回所有学生信息，比如
	-- 张三(01),李四（08），....,王五（09）
drop PROCEDURE if exists `work_2`;
delimiter $$
create PROCEDURE work_2(par_tid varchar(10))
BEGIN
	DECLARE temp_sid varchar(10) DEFAULT '';
	DECLARE temp_sname varchar(10) DEFAULT '';
	DECLARE flag int DEFAULT TRUE;
	declare msg varchar(250) DEFAULT '';
	DECLARE mycur CURSOR for 
	(
		select sid,sname from student where sid in
		(
			SELECT sid from sc where cid=
			(
				select cid from course where tid=par_tid
		  )
    )
	);
	DECLARE CONTINUE HANDLER for not found set flag =FALSE;	
	open mycur;	
	aa:WHILE TRUE DO
		FETCH mycur into temp_sid,temp_sname;
		if flag=FALSE then
			LEAVE aa;
		end if;
		-- 张三(01),李四（08），....,王五（09）
		if msg='' then
			set msg = concat(temp_sname,'(',temp_sid,')');
		else
			set msg = concat(msg,',',temp_sname,'(',temp_sid,')');
		end if;		
	END WHILE;
	close mycur;
	select msg;
END;
$$
delimiter ;
call work_2('02');
############################################################
	-- 3.输入学号，返回成绩组合值，比如打印该整串字符：
	-- 张三：语文（80），数学（90），英语（60）
drop procedure if exists `work3`;
delimiter $$
create procedure `work3`(type varchar(10))
begin
	declare typename varchar(10) default '';
	declare par_chinese int default 0;
	declare par_english int default 0;
	declare par_math int default 0;
	declare msg varchar(255) default '';
	declare mycur cursor for(
		select * from sc where sid=type
	);
	select sname into typename from student where sid=type;
	open mycur;
	FETCH mycur into par_chinese,par_math,par_english;
	close mycur;
	set msg=CONCAT(typename,':','语文(',par_chinese,')',',数学(',par_math,')',',英语(',par_english,')');
	select msg;
	
end
$$
delimiter ;
call work3('02');















###################################################
	-- 4.打印姓名中有相同字的同学的学号和姓名，比如（张可可，林林等）