-- 1.输入一个课程号，打印该课程的平均分，最高分，最低分
-- 删除存储
DROP PROCEDURE IF EXISTS `test1`;
-- 创建存储
CREATE PROCEDURE test1(par_id INT)
BEGIN 
	-- 定义临时变量
	DECLARE temp_max DECIMAL(5,2) DEFAULT NULL;
	DECLARE temp_min DECIMAL(5,2) DEFAULT NULL;
	DECLARE temp_avg DECIMAL(5,2) DEFAULT NULL;
	
	-- 创建游标
	DECLARE myCur CURSOR FOR
	(     
	
	     
	     SELECT MAX(score),MIN(score),AVG(score) FROM sc WHERE score IN   -- 求取这门课程的最高分，最低分，平均分
			    (
					   
					   SELECT score FROM sc WHERE cid='02'     -- 根据传入的课程号的id进行查出这门课程的所有成绩
    			)
	);
	
	-- 打开游标
	OPEN myCur;
	
	-- 使用游标
	   
	FETCH myCur INTO temp_max,temp_min,temp_avg;  -- 将求出的最高分，最低分，平均分保存到临时变量中。
	
	-- 关闭游标
	CLOSE mycur;
	-- 打印输出语句
  SELECT temp_max,temp_min,temp_avg;
END 

CALL test1(02);       -- 调用存储过程





-- 2.输入教师ID，返回所有学生信息，比如
-- 张三(01),李四（08），....,王五（09）
-- 删除存储
DROP PROCEDURE IF EXISTS `test2`;

-- 创建存储
CREATE PROCEDURE test2(par_id INT)
BEGIN 
-- 定义临时变量
DECLARE temp_sid varchar(10) DEFAULT'';
DECLARE temp_sname varchar(10) DEFAULT'';
DECLARE flag int DEFAULT TRUE;
DECLARE msg varchar(260) DEFAULT'';
-- 创建游标
 DECLARE myCur CURSOR FOR
 (
      SELECT sid,sname FROM student WHERE sid IN
	(
	   	(
	  SELECT sid FROM sc WHERE cid=
	       (
		       -- 根据教师的id从课程信息表中查询出老师所教的课程是哪一门
							SELECT cid FROM course WHERE tid='01'
				 )
			)
	)
 );
 
 -- 当flag循环没有找到数据时，设置flag为false
    DECLARE CONTINUE HANDLER FOR NOT found SET flag=FALSE; 
 
-- 打开游标
  OPEN myCur;
	
-- 使用游标
  -- 将查询出的sql语句通过游标放到临时变量中
	
	
	-- 使用游标进行一行一行读取数据(循环语句)
	 aa: WHILE TRUE DO
	    FETCH myCur INTO temp_sid,temp_sname;
			IF flag=FALSE THEN
	      LEAVE aa;
      END IF;
    -- 张三(01),李四(08),...王五(09)
 		IF msg='' THEN
	     SET msg=CONCAT(temp_sname,'(',temp_sid,')');
 ELSE
 	     SET msg=CONCAT( msg,',',temp_sname,'(',temp_sid,')');
 END IF;
-- 
END WHILE;
	
-- 关闭游标
CLOSE myCur;
-- 打印输出
SELECT msg;
END;

-- 调用存储过程
CALL test2(01);
-- 
-- 3.输入学号，返回成绩组合值，比如打印该整串字符：
-- 张三：语文（80），数学（90），英语（60）
DROP PROCEDURE IF EXISTS `test3`;
CREATE PROCEDURE test3(par_sid INT)
BEGIN 
--  定义四个临时变量
DECLARE temp_sname varchar(30) DEFAULT'';
DECLARE temp_language INT DEFAULT NULL;
DECLARE temp_math INT DEFAULT NULL;
DECLARE temp_English INT DEFAULT NULL;
DECLARE msg varchar(200) DEFAULT'';
DECLARE flag INT DEFAULT TRUE;
-- 创建游标
  DECLARE myCur CURSOR FOR
  (
			SELECT sname,
						(
						SELECT score FROM sc WHERE student.sid=sc.sid and sc.cid='01'
						)语文,
							(
							SELECT score FROM sc WHERE student.sid=sc.sid and sc.cid='02'
							)数学,
						(
						SELECT score FROM sc WHERE student.sid=sc.sid and sc.cid='03'
						)英语
	FROM student  WHERE sid=par_sid
	);
	DECLARE CONTINUE HANDLER FOR NOT found SET flag=FALSE;
	-- 打开游标
	OPEN myCur;
	
	-- 使用游标
	aa:WHILE TRUE DO
	  FETCH myCur INTO temp_sname,temp_language,temp_math,temp_English;
		IF flag=FALSE THEN
   	     LEAVE aa;
END IF;
      -- 张三：语文（80），数学（90），英语（60）
			SET msg=CONCAT(temp_sname,':语文(',temp_language,'),数学(',temp_math,'),英语,(',temp_English,')');
END WHILE;

-- 关闭游标
CLOSE myCur;
SELECT msg;
END;

CALL test3(04);




-- 4.打印姓名中有相同字的同学的学号和姓名，比如（张可可，林林等）
-- 删除存储
DROP PROCEDURE IF EXISTS `test4`;
-- 创建存储
CREATE PROCEDURE test4()
BEGIN
-- 定义临时变量
  DECLARE temp_sid varchar(30) DEFAULT '';
	DECLARE temp_sname varchar(30) DEFAULT '';
	-- 创建游标
	DECLARE myCur CURSOR FOR
	(
	-- 查询出学生信息表
	 SELECT sid,sname FROM student;
	)
	
	-- 打开游标
  OPEN myCur;
	
	-- 使用游标
   WHILE TRUE DO
	    FETCH sid,sname INTO temp_sid,temp_sname;
END WHILE;

-- 关闭游标
CLOSE myCur;  
	 
END

CALL test4();