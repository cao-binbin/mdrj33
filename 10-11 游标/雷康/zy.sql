-- 以下试题全部使用存储过程和游标：
-- 1.输入一个课程号，打印该课程的平均分，最高分，最低分
-- 

drop procedure if exists `test_cursor1`;
delimiter $$
create procedure test_cursor1(num int)
BEGIN
	declare avg1 varchar(9) default '';
	declare max1 varchar(9) default '';
	declare min1 varchar(9) default '';

	DECLARE cursor1 cursor FOR
	( 
	select avg(score),max(score),min(score) from sc where cid =num
	);

	open cursor1;

	fetch cursor1 into avg1,max1,min1;	
		

	close cursor1;
select avg1,max1,min1;
END
$$
delimiter;

call test_cursor1(02);







-- 2.输入教师ID，返回所有学生信息，比如
-- 张三(01),李四（08），....,王五（09）

drop procedure if exists `test_cursor2` ;
delimiter $$
create procedure test_cursor2(num int)
BEGIN
	declare sid1 varchar(10) default '';
	declare sname1 varchar(10) default '';
	declare flag int default true;
	declare total varchar(250) default '';
	declare cursor2 cursor FOR
	(
		select sid,sname from student where sid in 
			(
					select sid from sc where cid =
						(
								select cid from course where tid=num
						)
			)
	);
	declare continue handler for not found set flag=false;
	
	open cursor2;
	aa:WHILE  TRUE   DO
	fetch cursor2  into sid1,sname1;
	 if flag = false THEN
	  leave aa;
	 end if;

	if total = '' THEN
		set total = concat(sname1,'(',sid1,')');
	ELSE
	  set total = concat(total,',',sname1,'(',sid1,')');
	end if;
	end while;
	close cursor2;
	select total;
end
$$
delimiter;


call test_cursor2(03);


select sc11.*,sc3.* FROM
	(	
		select sc1.sid as sid100,sc1.sname,sc1.cid cid1,sc1.score score1,sc2.cid cid2,sc2.score score2 from
		(
		select sc.*,student.sname from student 
		inner join sc on student.sid=sc.sid
		where cid =1
		) sc1 
		inner join 
		(
		select sid, cid , score from sc 
		where cid =2
		) sc2 on sc1.sid=sc2.sid
	) sc11
inner join 
(
  select sid ,cid  cid3, score score3 from sc
  where cid =3
) sc3 on sc11.sid100=sc3.sid
 
where sid100=1



 
-- 3.输入学号，返回成绩组合值，比如打印该整串字符：
-- 张三：语文（80），数学（90），英语（60）

drop procedure if exists test_cursor3;
create procedure test_cursor3(num int)
begin 
	declare name1 varchar(10) default '';
	declare china varchar(10) default '';
	declare ss varchar(10) default '';

	declare english varchar(10) default '';
	declare cursor3 cursor FOR
	(
select sc11.sname,sc11.score1,sc11.score2,sc3.score3 FROM
	(	
		select sc1.sid as sid100,sc1.sname,sc1.cid cid1,sc1.score score1,sc2.cid cid2,sc2.score score2 from
		(
		select sc.*,student.sname from student 
		inner join sc on student.sid=sc.sid
		where cid =1
		) sc1 
		inner join 
		(
		select sid, cid , score from sc 
		where cid =2
		) sc2 on sc1.sid=sc2.sid
	) sc11
inner join 
(
  select sid ,cid  cid3, score score3 from sc
  where cid =3
) sc3 on sc11.sid100=sc3.sid
where sid100=num
	);
	
	open cursor3;

	FETCH cursor3 into name1,ss,china,english;
	set @total = concat(name1,':','语文','(',china,')',',','数学','(',ss,')','英语','(',english,')');
	close cursor3;
	select @total;

END;


call test_cursor3(2);









-- 4.打印姓名中有相同字的同学的学号和姓名，比如（张可可，林林等）

drop procedure if exists test_cuosor4;
create procedure test_cuosor4()
BEGIN
	declare sid1 varchar(10) default '';
	declare sname1 varchar(10) default '';
	declare total varchar(250) default '';
	declare flag int default true;
	declare mycur cursor for 
	(
	 select sid,sname from student 
	);
		declare continue handler for not found set flag=false;
	open mycur;
	aa:while flag do
	fetch mycur into sid1,sname1;
	if flag = false then
		leave aa;
	end if;
	if SUBSTRING(sname1,1,1)=SUBSTRING(sname1,2,1) or SUBSTRING(sname1,1,1)=SUBSTRING(sname1,3,1) or SUBSTRING(sname1,3,1)=SUBSTRING(sname1,2,1) then
	if total='' then
	set total = concat(sid1,':',sname1);
	else 
	set total = concat(total,',',sid1,':',sname1);
	end if;
	end if;
	end while;
	close mycur;
	select total;
END;
call test_cuosor4();