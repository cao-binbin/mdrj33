-- 1.输入一个课程号，打印该课程的平均分，最高分，最低分
DROP PROCEDURE if EXISTS `text1`;

CREATE PROCEDURE text1(text_tid INT)
BEGIN
 DECLARE text_avg varchar(10) DEFAULT '';
 DECLARE text_max varchar(10) DEFAULT '';
 DECLARE text_min varchar(10) DEFAULT '';
DECLARE myCur CURSOR FOR
(
SELECT avg(score)平均分,max(score)最高分,min(score)最低分 from sc where cid in 
(
SELECT cid from course where tid=text_tid
)
);
 open myCur;

 FETCH myCur into  text_avg,text_max,text_min ;
 SELECT text_avg,text_max,text_min;
 CLOSE myCur;
END;

CALL text1(03);


-- 2.输入教师ID，返回所有学生信息，比如
-- 张三(01),李四（08），....,王五（09）
DROP PROCEDURE if EXISTS `text2`;
delimiter $$
CREATE PROCEDURE text2(text_tid varchar(10))
BEGIN
DECLARE text_sanme varchar(20) DEFAULT '';
DECLARE text_sid varchar(10) DEFAULT '';
DECLARE msg varchar(250) DEFAULT '';
DECLARE flag int DEFAULT true;

declare myCur CURSOR FOR
(
SELECT sname,sid from student where sid in
(
SELECT sid from sc where cid=
			(
				select cid from course where tid=text_tid
		  )
)
);
	DECLARE CONTINUE HANDLER for not found set flag =FALSE;	
OPEN myCur;
	aa:WHILE TRUE DO
		FETCH mycur into text_sid,text_sanme;
		if flag=FALSE then
			LEAVE aa;
		end if;
	
		if msg='' then
			set msg = concat(text_sanme,'(',text_sid,')');
		else
			set msg = concat(msg,',',text_sanme,'(',text_sid,')');
		end if;		
	END WHILE;
	close mycur;
	select msg;

end;
$$
delimiter ;

call text2('01');
-- 3.输入学号，返回成绩组合值，比如打印该整串字符：
-- 张三：语文（80），数学（90），英语（60）

 drop procedure if exists `work3`;
delimiter $$
create procedure `work3`(type varchar(10))
begin
	declare typename varchar(10) default '';
	declare par_chinese int default 0;
	declare par_english int default 0;
	declare par_math int default 0;
	declare msg varchar(255) default '';
	declare mycur cursor for(
		select * from sc where sid=type
	);
	select sname into typename from student where sid=type;
	open mycur;
	FETCH mycur into par_chinese,par_math,par_english;
	close mycur;
	set msg=CONCAT(typename,':','语文(',par_chinese,')',',数学(',par_math,')',',英语(',par_english,')');
	select msg;
	
end
$$
delimiter ;
call work3('01');
 
 
 
 -- 4.打印姓名中有相同字的同学的学号和姓名，比如（张可可，林林等）



drop procedure if exists `work4`;
delimiter $$
create procedure `work4`()
begin
	declare type varchar(10) default '';
	declare num int default 0;
	declare flag int default true;
	declare mycur cursor for(
	select sname from student);
	
	declare CONTINUE handler for not found set flag=false;
	open mycur;
	WHILE flag DO
		fetch mycur into type;
		IF flag=true THEN
			set num = (select char_length(type));
			if mid(type,num,1)=mid(type,num-1,1) or mid(type,num-1,1)=mid(type,num-2,1) then
				select type;
			end if;
		END IF;
	END WHILE;
end
$$
delimiter ;

call work4();
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


