drop PROCEDURE if exists`test_work1`;
create PROCEDURE test_work1(num int )
begin
 declare a VARCHAR(10) default'';
 declare b VARCHAR(10) default'';
 declare c varchar(10) default'';
 declare work1 cursor for 
 (
 select avg(score),max(score),min(score) from sc where cid = num
 
 );
 
 open work1;
 fetch work1  into a,b,c;
 
 close work1 ;
 select a,b,c;
 end;
 call test_work1(02);
 
 create procedure test_work2(num int);
 
 
 
 
 drop PROCEDURE if exists `test_work2`;
create PROCEDURE test_work2(par_tid varchar(10))
BEGIN
	DECLARE temp_sid varchar(10) DEFAULT '';
	DECLARE temp_sname varchar(10) DEFAULT '';
	DECLARE flag int DEFAULT TRUE;
	declare msg varchar(250) DEFAULT '';
	DECLARE mycur CURSOR for 
	(
		select sid,sname from student where sid in
		(
			SELECT sid from sc where cid=
			(
				select cid from course where tid=par_tid
		  )
    )
	);
	DECLARE CONTINUE HANDLER for not found set flag =FALSE;	
	open mycur;	
	aa:WHILE TRUE DO
		FETCH mycur into temp_sid,temp_sname;
		if flag=FALSE then
			LEAVE aa;
		end if;
		if msg='' then
			set msg = concat(temp_sname,'(',temp_sid,')');
		else
			set msg = concat(msg,',',temp_sname,'(',temp_sid,')');
		end if;		
	END WHILE;
	close mycur;
	select msg;
END;
call test_work2('03');


drop procedure if exists test_work3;
create procedure test_work3(num int)
begin 
	declare name1 varchar(10) default '';
	declare china varchar(10) default '';
	declare ss varchar(10) default '';

	declare english varchar(10) default '';
	declare cursor3 cursor FOR
	(
select sc11.sname,sc11.score1,sc11.score2,sc3.score3 FROM
	(	
		select sc1.sid as sid100,sc1.sname,sc1.cid cid1,sc1.score score1,sc2.cid cid2,sc2.score score2 from
		(
		select sc.*,student.sname from student 
		inner join sc on student.sid=sc.sid
		where cid =1
		) sc1 
		inner join 
		(
		select sid, cid , score from sc 
		where cid =2
		) sc2 on sc1.sid=sc2.sid
	) sc11
inner join 
(
  select sid ,cid  cid3, score score3 from sc
  where cid =3
) sc3 on sc11.sid100=sc3.sid
where sid100=num
	);
	
	open cursor3;

	FETCH cursor3 into name1,ss,china,english;
	set @total = concat(name1,':','语文','(',china,')',',','数学','(',ss,')','英语','(',english,')');
	close cursor3;
	select @total;

END;


call test_work3(4);
