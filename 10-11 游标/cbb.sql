-- 以下试题全部使用存储过程和游标：
-- 1.输入一个课程号，打印该课程的平均分，最高分，最低分
-- 
drop PROCEDURE if EXISTS `test_action1`;
delimiter $$
CREATE PROCEDURE test_action1(in classid int)
BEGIN
DECLARE temp_avg float DEFAULT 0;
DECLARE temp_max int DEFAULT 0;
DECLARE temp_min int DEFAULT 0;
DECLARE mycur cursor for
(
 select avg(score),max(score),min(score) from sc where cid=classid GROUP BY cid
);
OPEN mycur;
 
 FETCH mycur into temp_avg,temp_max,temp_min;

END WHILE;
CLOSE mycur;
select temp_avg,temp_max,temp_min;

END;
$$
delimiter;

call test_action1();






-- 2.输入教师ID，返回所有学生信息，比如
-- 张三(01),李四（08），....,王五（09）

drop PROCEDURE if exists `test_action2`;
delimiter $$
CREATE PROCEDURE test_action2(par_tid varchar(10))
BEGIN
DECLARE temp_sid VARCHAR(10) DEFAULT '';
DECIMAL temp_sname varchar(10) DEFAULT '';
DECIMAL flag int DEFAULT TRUE;
DECLARE msg varchar(100) DEFAULT '';
DECLARE mycur CURSOR for
(
  select sid,sname from student where sid in
   (
     select sid from sc where cid=
		 (
		  select cid from course where tid=par_tid
		 )
   )
);
DECLARE CONTINUE handler for not found set flag=FALSE;

OPEN mycur;

aa:WHILE true DO
	FETCH mycur into temp_sid,temp_sname;
	IF flag=FALSE THEN
	LEAVE aa;
end if;
if msg='' then
set msg = concat(temp_sname,'(',temp_sid,')');
else
 set msg=concat(msg,',',temp_sname,'(',temp_sid,')');
 end if;
 END WHILE;
   CLOSE mycur;
   select msg;
END;
$$
delimiter;

call test_action2();






-- 
-- 3.输入学号，返回成绩组合值，比如打印该整串字符：
-- 张三：语文（80），数学（90），英语（60）
-- 
drop PROCEDURE if EXISTS `Lesson3`;
DELIMITER $$
CREATE PROCEDURE Lesson3(in studentid int)
BEGIN
	DECLARE stu_name varchar(20) DEFAULT '';
	DECLARE stu_yuwen int DEFAULT 0;
	DECLARE stu_shuxue int DEFAULT 0;
	DECLARE stu_yingyu int DEFAULT 0;
	DECLARE flag int DEFAULT true;
	DECLARE msg varchar(300) DEFAULT '';

DECLARE mycur CURSOR for
(
	select DISTINCT sname,
	IFNULL((
		select score from student inner join sc on student.sid=sc.sid where cid =01 and student.sid =studentid
	),0)
	,
	IFNULL((
		select score from student inner join sc on student.sid=sc.sid where cid =02 and student.sid =studentid
	),0)
	,
	IFNULL((
		select score from student inner join sc on student.sid=sc.sid where cid =03 and student.sid =studentid
	),0)
	from student inner join sc on student.sid=sc.sid where student.sid =studentid
);
	DECLARE CONTINUE HANDLER for not found set flag =FALSE;	
	open mycur;
		aa:WHILE true DO
		FETCH mycur into stu_name,stu_yuwen,stu_shuxue,stu_yingyu;
				IF flag=FALSE THEN
					LEAVE aa;
				END IF;
					if msg='' then
						set msg = concat(stu_name,': 语文(',stu_yuwen,'),数学(',stu_shuxue,'),英语(',		stu_yingyu,')');
					else
						set msg = concat(msg,',',temp_sname,'(',temp_sid,')');
					end if;		
				END WHILE;
	close mycur;
	select msg;
END;
$$
DELIMITER;
call Lesson3(03);






-- 4.打印姓名中有相同字的同学的学号和姓名，比如（张可可，林林等）

