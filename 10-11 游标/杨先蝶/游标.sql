-- 以下试题全部使用存储过程和游标：
-- 1.输入一个课程号，打印该课程的平均分，最高分，最低分
drop PROCEDURE if EXISTS course;
delimiter$$
create PROCEDURE course(cour_cid int)
BEGIN
	DECLARE c_cid int   DEFAULT 0;
	DECLARE c_avg int DEFAULT 0;
	DECLARE c_max int DEFAULT 0;
	DECLARE c_min int DEFAULT 0;
	-- 创建游标
	DECLARE ybiao CURSOR for (
		select cid,AVG(score),MAX(score),AVG(score) from sc where cid=cour_cid 
	);
	-- 打开游标
	open ybiao;
	-- 使用游标
	fetch ybiao into cour_cid,c_avg,c_max,c_min;
	-- 结束游标
	close ybiao;
	
	select cour_cid,c_avg,c_max,c_min;;
	
END;
$$
delimiter;

call course(01);




-- 
-- 2.输入教师ID，返回所有学生信息，比如
-- 张三(01),李四（08），....,王五（09）

drop PROCEDURE if exists `work2`;
delimiter $$
create PROCEDURE work2(par_tid varchar(10))
BEGIN
	DECLARE temp_sid varchar(10) DEFAULT '';
	DECLARE temp_sname varchar(10) DEFAULT '';
	DECLARE flag int DEFAULT TRUE;
	declare msg varchar(200) DEFAULT '';
	DECLARE ybiao CURSOR for 
	(
		select sid,sname from student where sid in
		(
			SELECT sid from sc where cid=
			(
				select cid from course where tid=par_tid
		  )
    )
	);
	open ybiao;	
	aa:WHILE TRUE DO
		FETCH ybiao into temp_sid,temp_sname;
		if flag=FALSE then
			LEAVE aa;
		end if;
		-- 张三(01),李四（08），....,王五（09）
		if msg='' then
			set msg = concat(temp_sname,'(',temp_sid,')');
		else
			set msg = concat(msg,',',temp_sname,'(',temp_sid,')');
		end if;		
	END WHILE;
	close ybiao;
	select ybiao;
END;
$$
delimiter ;
call work2('01');

-- 
-- 3.输入学号，返回成绩组合值，比如打印该整串字符：
-- 张三：语文（80），数学（90），英语（60）
drop PROCEDURE if EXISTS work3;
delimiter $$
CREATE PROCEDURE work3(s_id int)
BEGIN
	DECLARE w_name VARCHAR(30) DEFAULT '';
	DECLARE w_language int DEFAULT 0;
	DECLARE w_math int DEFAULT 0;
	DECLARE w_english int DEFAULT 0;
	DECLARE w_sum VARCHAR(30) DEFAULT '';

	DECLARE ybiao CURSOR FOR(
		Select sname,
		(Select score from sc where sc.sid=student.sid AND cid='01') 语文,
		(Select score from sc where sc.sid=student.sid AND cid='02') 数学,
		(Select score from sc where sc.sid=student.sid AND cid='03') 英语
		
		from student where sid=s_id 
	);
	OPEN ybiao;
	FETCH ybiao into w_name,w_language,w_math,w_english;
	set w_sum =CONCAT(w_name,":","语文(",w_language,")",",","数学(",w_math,")",",","英语(",w_english,")");
	CLOSE ybiao;
	SELECT w_sum 学生成绩表;
END;
$$
delimiter;
call work3(01);

-- 
-- 4.打印姓名中有相同字的同学的学号和姓名，比如（张可可，林林等）

drop procedure if exists `work4`;
delimiter $$
create procedure `work4`()
begin
	declare type varchar(10) default '';
	declare num int default 0;
	declare flag int default true;
	declare mycur cursor for(
	select sname from student);
	
	declare CONTINUE handler for not found set flag=false;
	open mycur;
	WHILE flag DO
		fetch mycur into type;
		IF flag=true THEN
			set num = (select char_length(type));
			if mid(type,num,1)=mid(type,num-1,1) or mid(type,num-1,1)=mid(type,num-2,1) then
				select type;
			end if;
		END IF;
	END WHILE;
end
$$
delimiter ;

call work4();