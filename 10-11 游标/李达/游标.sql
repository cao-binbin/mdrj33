-- 以下试题全部使用存储过程和游标：
-- 1.输入一个课程号，打印该课程的平均分，最高分，最低分
drop PROCEDURE if EXISTS L;
delimiter$$
create PROCEDURE L(Lcid int)
BEGIN
	DECLARE a_cid int   DEFAULT 0;
	DECLARE a_avg int DEFAULT 0;
	DECLARE a_max int DEFAULT 0;
	DECLARE a_min int DEFAULT 0;

	DECLARE you CURSOR for (
		select cid,AVG(score),MAX(score),AVG(score) from sc where cid=cour_cid 
	);

	open you;
	fetch you into cour_cid,a_avg,a_max,a_min;
	close you;
	
	select cour_cid,a_avg,a_max,a_min;;
	
END;
$$
delimiter;

call L(01);




-- 
-- 2.输入教师ID，返回所有学生信息，比如
-- 张三(01),李四（08），....,王五（09）

drop PROCEDURE if exists `A`;
delimiter $$
create PROCEDURE A(p_tid varchar(10))
BEGIN
	DECLARE temp_sid varchar(10) DEFAULT '';
	DECLARE temp_sname varchar(10) DEFAULT '';
	DECLARE f int DEFAULT TRUE;
	declare m varchar(200) DEFAULT '';
	DECLARE you CURSOR for 
	(
		select sid,sname from student where sid in
		(
			SELECT sid from sc where cid=
			(
				select cid from course where tid=p_tid
		  )
    )
	);
	open you;	
	bb:WHILE TRUE DO
		FETCH you into temp_sid,temp_sname;
		if f=FALSE then
			LEAVE bb;
		end if;
		-- 张三(01),李四（08），....,王五（09）
		if m='' then
			set m = concat(temp_sname,'(',temp_sid,')');
		else
			set m = concat(m,',',temp_sname,'(',temp_sid,')');
		end if;		
	END WHILE;
	close you;
	select you;
END;
$$
delimiter ;
call A('01');

-- 
-- 3.输入学号，返回成绩组合值，比如打印该整串字符：
-- 张三：语文（80），数学（90），英语（60）
drop PROCEDURE if EXISTS I;
delimiter $$
CREATE PROCEDURE I(s_id int)
BEGIN
	DECLARE k_name VARCHAR(30) DEFAULT '';
	DECLARE k_language int DEFAULT 0;
	DECLARE k_math int DEFAULT 0;
	DECLARE k_english int DEFAULT 0;
	DECLARE k_sum VARCHAR(30) DEFAULT '';

	DECLARE biao CURSOR FOR(
		Select sname,
		(Select score from sc where sc.sid=student.sid AND cid='01') 语文,
		(Select score from sc where sc.sid=student.sid AND cid='02') 数学,
		(Select score from sc where sc.sid=student.sid AND cid='03') 英语
		
		from student where sid=s_id 
	);
	OPEN biao;
	FETCH biao into k_name,k_language,k_math,k_english;
	set k_sum =CONCAT(k_name,":","语文(",k_language,")",",","数学(",k_math,")",",","英语(",k_english,")");
	CLOSE biao;
	SELECT k_sum 学生成绩表;
END;
$$
delimiter;
call L(01);

-- 
-- 4.打印姓名中有相同字的同学的学号和姓名，比如（张可可，林林等）

