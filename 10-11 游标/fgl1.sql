-- 以下试题全部使用存储过程和游标：
-- 1.输入一个课程号，打印该课程的平均分，最高分，最低分
   drop procedure if exists `test`;

	 create procedure test(par_cid int)
	 begin
	    declare demp_max varchar(10) default'';
			declare demp_min varchar(10) default'';
			declare demp_avg varchar(10) default'';
			DECLARE flag int DEFAULT TRUE;
			declare msg varchar(250) default'';
	     declare mycur Cursor for 
			 (
			  select max(score), min(score),avg(score)from sc     
				where cid=par_cid
			 );
			 declare continue handler for not found set flag =false;
			 open mycur;
			
			   fetch mycur into demp_max,demp_min,demp_avg;
					set msg =concat(msg,'最小值',demp_min,',','最大值',demp_max,',','平均值', demp_avg);
					
			close mycur; 
			select msg;
	 end;

call test('02');
-- 2.输入教师ID，返回所有学生信息，比如
-- 张三(01),李四（08），....,王五（09）
  drop procedure if exists `test1`
	create procedure test1(par_tid int )
begin  
  declare demp_sid varchar(10) default'';
	declare demp_sname varchar(10) default'';
	declare flag int default true;
	declare msg varchar(250) default '';
	declare mycur cursor for
	(  
	 	select sid,sname from student where sid in
		(
			SELECT sid from sc where cid=
			(
				select cid from course where tid=par_tid
		  )
			
    )
	);
	declare continue handler for not found set flag =false;
	
	open mycur ;
	aa:while true do 
	 fetch mycur into demp_sid,demp_sname;
	 if flag = false then 
	      leave aa;
				end if;
					if msg=''then
			set msg = concat(demp_sname,'(',demp_sid,')');
		else
			set msg = concat(msg,',',demp_sname,'(',demp_sid,')');
		end if;		
	END WHILE;
	close mycur;
		select msg;
end;

-- 3.输入学号，返回成绩组合值，比如打印该整串字符：
-- 张三：语文（80），数学（90），英语（60）
  drop procedure if exists `test2`
	create procedure test2(par_sid int )
	begin
			declare demp_sname varchar(10) default '';
			declare demp_score varchar(10) default '';
			declare demp_cname varchar(10) default '';
	
			declare msg varchar(250) default '';
			declare mycur cursor for 
			(
		    select *from sc where sid=par_sid
			);
			
			open mycur;
			 
			 
			close mycur;
	end;
-- 4.打印姓名中有相同字的同学的学号和姓名，比如（张可可，林林等）

