#1011 游标学习
#myslq的游标 只能前进 不能后退和跳跃
#创建游标--打开游标--使用游标--关闭游标
#1.创建无参过程函数
drop PROCEDURE if EXISTS `mycur`;
delimiter $$
CREATE PROCEDURE mycur()
BEGIN
#创建临时变量存储学号和学生姓名
DECLARE temp_sid VARCHAR(10) DEFAULT '';
DECLARE temp_sname VARCHAR(10) DEFAULT '';

#创建flag判断是否超出范围
DECLARE flag int DEFAULT true;

#创建一个msg组合语句concat
DECLARE msg VARCHAR(200) DEFAULT '';
#创建游标
DECLARE cur1 CURSOR for
(SELECT sid,sname from student ORDER BY sid asc);

#如果没有找到数据 not found set 修改为false
DECLARE CONTINUE HANDLER for not found set flag=false;

#打开游标
open cur1;

#使用游标
aa:WHILE flag DO
		#通过FETCH 游标名称 into 变量1，变量2; 来使用游标
		#通过msg来接受拼接的语句
FETCH cur1 into temp_sid,temp_sname;
		#如果超出范围则退出while循环 leave 循环的别名
if flag =false then 
	LEAVE aa;
end if;		
if msg='' then
			set msg =CONCAT(temp_sname);
	end if;
set msg =CONCAT(msg,',',temp_sname);
END WHILE;

		#退出游标 close 游标名称
		close cur1;		
		#输出msg的内容
		SELECT msg;
END
$$
delimiter ;

CALL mycur();


-- #以下试题全部使用存储过程和游标：
-- 1.输入一个课程号，打印该课程的平均分，最高分，最低分
	 drop PROCEDURE if EXISTS `test_1`;	 
 CREATE PROCEDURE test_1(temp_cid VARCHAR(10))
 BEGIN
			#创建临时变量
			DECLARE temp_avg DECIMAL(3,1)  DEFAULT 0.0;
			DECLARE temp_max DECIMAL(3,1)  DEFAULT 0.0; 
			DECLARE temp_min DECIMAL(3,1)  DEFAULT 0.0;
			#创建游标
			DECLARE mycur CURSOR for
			(SELECT avg(score),max(score),min(score)	from sc where cid=temp_cid);
		  #打开游标
			open mycur;
			#使用游标
			FETCH mycur into temp_avg,temp_max,temp_min;
			SELECT temp_avg,temp_max,temp_min;
		  #关闭游标
			CLOSE mycur;
 END;
		
call test_1('03');
		
	
-- 2.输入教师ID，返回所有学生信息，比如
-- 张三(01),李四（08），....,王五（09）
	 #创建一个有参返回过程
	 drop PROCEDURE if EXISTS `test_2`;	 
	 deLIMITer $$
   CREATE PROCEDURE test_2(temp_tid VARCHAR(10))
	 BEGIN
			#设置临时变量存储学生名称和学号
			DECLARE temp_sid VARCHAR(10) DEFAULT '';
			DECLARE temp_sname  VARCHAR(10) DEFAULT '';
			#设置变量flag 和msg
			DECLARE flag int DEFAULT true;
			DECLARE msg VARCHAR(150) DEFAULT '';
			#创建游标
			DECLARE cur_text2 CURSOR for
			(SELECT sid,sname from student);
			#书写防止超出范围的语句
			DECLARE CONTINUE HANDLER for not found set flag=false;
			
			#打开游标
			open cur_text2;
			#使用游标
		aa:WHILE flag DO
		FETCH cur_text2 into 	temp_sid,temp_sname;
		#如果超出则退出循环
		if flag = false then
			 LEAVE aa;
		end if;
		#如果为第一个数值的话 则不为其添加逗号
		if msg = '' then 
		-- 张三(01),李四（08），....,王五（09）
				set msg=CONCAT(temp_sname,'(',temp_sid,')');
				else
					set msg=CONCAT(msg,',',temp_sname,'(',temp_sid,')');
		end if;			
END WHILE;			
			#关闭游标
			CLOSE cur_text2;	 
			#输出数据
			SELECT msg;
	 END
	 $$
	 
	 delimiter ;
	 call test_2('01');
	 





-- 3.输入学号，返回成绩组合值，比如打印该整串字符：
-- 张三：语文（80），数学（90），英语（60）
#创建过程
drop PROCEDURE if EXISTS `text_34`;
CREATE PROCEDURE text_34(temp_sid VARCHAR(10))
BEGIN
	#设置临时变量
	DECLARE temp_sname VARCHAR(10) DEFAULT '';
	DECLARE temp_chinese int DEFAULT 0;
  DECLARE temp_math int DEFAULT 0;
	DECLARE temp_eng int DEFAULT 0;
	DECLARE msg VARCHAR(200) DEFAULT '';
	DECLARE cur_score CURSOR for
	(SELECT * from sc where sid='01');
	#将名称赋值到学生姓名内into
	SELECT sname into temp_sname from student where sid=temp_sid;
	#打开游标
	open cur_score;
	#使用游标
	FETCH cur_score into temp_chinese,temp_math,temp_eng;
	#关闭游标
	close cur_score;
	#连接字符
	-- 张三：语文（80），数学（90），英语（60）
	set msg=CONCAT(temp_sname,':','语文(',temp_chinese,')','数学(',temp_math,')','英语(',temp_eng,')');
	SELECT msg;
END;

call text_34('01');
-- 4.打印姓名中有相同字的同学的学号和姓名，比如（张可可，林林等）